pipeline {
    agent {
        label 'docker-ce'
    }
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8'
                    reuseNode true
                }
            }
            steps {
                script {
                    env.POM_VERSION = readMavenPom().version
                    currentBuild.displayName = env.POM_VERSION
                }
                sh 'mvn --batch-mode -Dmaven.test.failure.ignore clean install'
            }
        }
        stage('SonarQube analysis') {
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8'
                    reuseNode true
                }
            }
            steps {
                withCredentials([string(credentialsId: 'sonarqube', variable: 'TOKEN')]) {
                    sh 'mvn --batch-mode -Dsonar.login=$TOKEN -Dsonar.branch=${BRANCH_NAME} sonar:sonar'
                }
            }
        }
        stage('Publish') {
            when {
                branch 'master'
            }
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8'
                    reuseNode true
                }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'artifactory', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'mvn --batch-mode -Dartifactory.username=$USERNAME -Dartifactory.password=$PASSWORD deploy'
                }
            }
        }
        stage('Build Docker image') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker build -t registry.esss.lu.se/ics-software/naming-convention-tool:latest -t registry.esss.lu.se/ics-software/naming-convention-tool:${POM_VERSION} NamingConventionTool'
            }
        }
        stage('Push Docker image') {
            when {
                branch 'master'
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'gitlab', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'docker login registry.esss.lu.se --username ${USERNAME} --password ${PASSWORD}'
                }
                sh 'docker push registry.esss.lu.se/ics-software/naming-convention-tool:latest'
                sh 'docker push registry.esss.lu.se/ics-software/naming-convention-tool:${POM_VERSION}'
                sh 'docker logout registry.esss.lu.se'
            }
        }
        stage('Deploy to test environment') {
            when {
                branch 'master'
            }
            agent {
                docker {
                    image 'registry.esss.lu.se/ics-docker/tower-cli:3.2'
                    reuseNode true
                }
            }
            environment {
                ANSIBLE_AWX_HOST = 'https://icsv-awx01.esss.lu.se/api/v2'
                TEST_ENVIRONMENT_HOST = 'icsv-wildfly02.esss.lu.se'
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'ansible-awx', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'tower-cli job launch -h icsv-awx01.esss.lu.se -u ${USERNAME} -p ${PASSWORD} -J deploy-naming-convention-tool -e "naming_docker_image_tag=${POM_VERSION}" --monitor --limit icsv-wildfly02.esss.lu.se'
                }
            }
        }
        stage('Deploy to staging environment') {
            when {
                branch 'master'
                expression { ! env.POM_VERSION.endsWith("-SNAPSHOT") }
            }
            agent {
                docker {
                    image 'registry.esss.lu.se/ics-docker/tower-cli:3.2'
                    reuseNode true
                }
            }
            input {
                message "Deploy to staging environment?"
                submitter "ICS Control System Infrastructure group"
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'ansible-awx', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'tower-cli job launch -h icsv-awx01.esss.lu.se -u ${USERNAME} -p ${PASSWORD} -J deploy-naming-convention-tool -e "naming_docker_image_tag=${POM_VERSION}" --monitor --limit icsvs-app01.esss.lu.se'
                }
            }
        }
    }
}
