variables:
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
  CONTAINER_BRANCH_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

stages:
  - compile
  - test
  - package
  - analyse
  - publish
  - image
  - release
  - deploy

compile:
  stage: compile
  image: registry.esss.lu.se/ics-docker/maven:openjdk-8
  tags:
    - docker
  script:
    - mvn --batch-mode compile
  artifacts:
    paths:
      - target/
      - NamingConventionTool/target/
      - NamingConventionTool-Client/target
      - NamingConventionTool-JAXB/target
    expire_in: 1 day

test:
  stage: test
  image: registry.esss.lu.se/ics-docker/maven:openjdk-8
  tags:
    - docker
  script:
    - mvn --batch-mode test
  artifacts:
    reports:
      junit:
      - NamingConventionTool/target/surefire-reports/TEST-*.xml

package:
  stage: package
  image: registry.esss.lu.se/ics-docker/maven:openjdk-8
  tags:
    - docker
  script:
    - mvn --batch-mode install
  artifacts:
    paths:
      - NamingConventionTool/target/names-*.war

sonar:
  stage: analyse
  image: registry.esss.lu.se/ics-docker/maven:openjdk-11
  tags:
    - docker
  script:
    - mvn --batch-mode sonar:sonar -Dsonar.login=$SONARQUBE_TOKEN -Dsonar.branch.name=$CI_COMMIT_REF_SLUG
  only:
    - branches@ics-software/naming-convention-tool

publish:
  stage: publish
  image: registry.esss.lu.se/ics-docker/maven:openjdk-8
  tags:
    - docker
  script:
    - mvn --batch-mode deploy -Dartifactory.username=${ARTIFACTORY_USERNAME} -Dartifactory.password=${ARTIFACTORY_PASSWORD}
  only:
    - master@ics-software/naming-convention-tool

docker-build:
  stage: image
  image: docker:latest
  tags:
    - docker
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build -t $CONTAINER_BRANCH_IMAGE NamingConventionTool
    - docker push $CONTAINER_BRANCH_IMAGE
  only:
    - branches

docker-release:
  stage: release
  image: docker:latest
  tags:
    - docker
  before_script:
    - apk add xmlstarlet
    - POM_VERSION=$(xmlstarlet sel -N pom=http://maven.apache.org/POM/4.0.0 -t -v "/pom:project/pom:version" pom.xml)
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker pull $CONTAINER_BRANCH_IMAGE
    - docker tag $CONTAINER_BRANCH_IMAGE $CI_REGISTRY_IMAGE:$POM_VERSION
    - docker tag $CONTAINER_BRANCH_IMAGE $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:$POM_VERSION
    - docker push $CI_REGISTRY_IMAGE:latest
  only:
    - master

.deploy:
  stage: deploy
  image: registry.esss.lu.se/ics-docker/awxkit
  tags:
    - docker
  script:
    - >
      awx job_templates launch ${AWX_JOB_TEMPLATE}
      --extra_vars "naming_container_image_tag: $CI_COMMIT_REF_SLUG"
      --monitor
  dependencies: []
  only:
    - branches@ics-software/naming-convention-tool
  when: manual

deploy-test:
  extends: .deploy
  variables:
    AWX_JOB_TEMPLATE: deploy-naming-test
  environment:
    name: test
    url: https://naming-test-01.cslab.esss.lu.se/

pages:
  dependencies: []
  image: registry.esss.lu.se/ics-docker/maven:openjdk-8
  tags:
    - docker
  script:
    - mvn --batch-mode javadoc:aggregate
    - mv target/site/apidocs public
  artifacts:
    paths:
      - public
  only:
    - master@ics-software/naming-convention-tool
