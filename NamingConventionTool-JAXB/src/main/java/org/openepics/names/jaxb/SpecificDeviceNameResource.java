/*
 * Copyright (c) 2014 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.jaxb;

import io.swagger.annotations.ApiOperation;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This resource provides specific device name data.
 *
 * @author Andraz Pozar
 * @author Sunil Sah
 * @author Banafsheh Hajinasab
 * @author Lars Johansson
 *
 * @see DeviceNamesResource
 */
public interface SpecificDeviceNameResource {

    /**
     * Finds device name by uuid or name (exact match, case sensitive), not by name equivalence.
     *
     * @param string uuid or device name
     * @return a device name (most recent)</tt>
     */
    @GET
    @ApiOperation(
            value = "Finds device name by uuid or name",
            notes = "Note uuid or name (exact match, case sensitive). Returns a device name (most recent)",
            response = DeviceNameElement.class)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public DeviceNameElement getDeviceName(@PathParam("uuid") String string);

    /**
     * Finds device name by uuid or name (exact match, case sensitive)
     * or name equivalence (exact match, case insensitive).
     *
     * @param string uuid or device name
     * @return a device name (most recent)</tt>
     */
    public DeviceNameElement getDeviceNameElement(String string);

}
