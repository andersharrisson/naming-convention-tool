/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.jaxb;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This resource provides healthcheck of Naming application.
 *
 * @author Lars Johansson
 */
@Path("healthcheck")
@Api(value = "/healthcheck")
public interface HealthcheckResource {

    /**
     * Performs healthcheck of Naming application as string (HTTP response code).
     *
     * @return server timestamp
     */
    @GET
    @ApiOperation(
            value = "Performs healthcheck of Naming application",
            notes = "Note HTTP response code. Returns server timestamp",
            response = String.class)
    @Produces({MediaType.TEXT_PLAIN, MediaType.TEXT_PLAIN})
    public String getHealthcheck();

}
