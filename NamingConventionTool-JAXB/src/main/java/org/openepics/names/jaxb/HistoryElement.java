/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.jaxb;

import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Data transfer object representing history of name elements for JSON and XML serialization.
 *
 * @author Lars Johansson
 */
@XmlRootElement
@ApiModel
public class HistoryElement {

    private UUID uuid;
    private String fullName;
    private String mnemonic;
    private String description;
    private String status;
    private String date;
    private String user;
    private String message;

    /**
     * Constructor.
     */
    public HistoryElement() {
    }

    /**
     * Constructor.
     *
     * @param uuid           uuid
     * @param fullName       naming convention name
     * @param mnemonic       mnemonic
     * @param description    description
     * @param status         status
     * @param date           date
     * @param user           user
     * @param message        message
     */
    public HistoryElement(UUID uuid, String fullName, String mnemonic, String description, String status,
            String date, String user, String message) {
        super();
        this.uuid = uuid;
        this.fullName = fullName;
        this.mnemonic = mnemonic;
        this.description = description;
        this.status = status;
        this.date = date;
        this.user = user;
        this.message = message;
    }

    /**
     * @return the uuid
     */
    @ApiModelProperty(required = true, value = "UUID")
    public UUID getUuid() {
        return uuid;
    }
    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
    /**
     * @return the fullName
     */
    @ApiModelProperty(required = true)
    public String getFullName() {
        return fullName;
    }
    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    /**
     * @return the mnemonic
     */
    @ApiModelProperty(required = true)
    public String getMnemonic() {
        return mnemonic;
    }
    /**
     * @param mnemonic the mnemonic to set
     */
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }
    /**
     * @return the description
     */
    @ApiModelProperty(required = true)
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the status
     */
    @ApiModelProperty(
            value = "The status can have different values: Archived, Pending, Approved, Rejected, Cancelled, "
                    + "an empty string",
            allowableValues = "Archived, Pending, Approved, Rejected, Cancelled, an empty string",
            required = true)
    public String getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    /**
     * @return the date
     */
    @ApiModelProperty(required = true)
    public String getDate() {
        return date;
    }
    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }
    /**
     * @return the user
     */
    @ApiModelProperty(required = true)
    public String getUser() {
        return user;
    }
    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }
    /**
     * @return the message
     */
    @ApiModelProperty(required = true)
    public String getMessage() {
        return message;
    }
    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
