/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.jaxb;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Contact;
import io.swagger.annotations.ExternalDocs;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

/**
 * This resource provides history for names and name parts data.
 *
 * Intended usage is for ESS Name Registry, System Structure and Device Structure.
 *
 * @author Lars Johansson
 */
@Path("history")
@Api(value = "/history")
@Produces({"application/json"})
@SwaggerDefinition(
        info = @Info(
                description = "\n"
                        + "    This is a documentation for all REST interfaces of Naming service. \n"
                        + "    You can find out more about Naming service in Naming application,  \n"
                        + "        https://naming.esss.lu.se",
                version = "1.0.2",
                title = "Naming service API documentation",
                termsOfService = "http://swagger.io/terms/",
                contact = @Contact(
                        name = "Support",
                        email = "Icsscontrolsystemsupport@esss.se")
                ),
        externalDocs = @ExternalDocs(
                value = "ESS Naming Convention",
                url = "https://chess.esss.lu.se/enovia/link/ESS-0000757/21308.51166.45568.45993/valid"))
public interface HistoryResource {

    /**
     * Finds history for name part by uuid.
     * Note uuid (exact match).
     *
     * @param uuid uuid to look for
     * @return a list of entries for history of name part
     */
    @GET
    @Path("parts/uuid/{uuid}")
    @ApiOperation(
            value = "Finds history for name part by uuid",
            notes = "Note uuid (exact match). Returns a list of entries for history of name part",
            response = HistoryElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<HistoryElement> getPartHistoryForUuid(@PathParam("uuid") String uuid);

    /**
     * Finds history for ESS name by uuid.
     * Note uuid (exact match).
     *
     * @param uuid uuid to look for
     * @return a list of entries for history of name part
     */
    @GET
    @Path("deviceNames/uuid/{uuid}")
    @ApiOperation(
            value = "Finds history for ESS name by uuid",
            notes = "Note uuid (exact match). Returns a list of entries for history of ESS name",
            response = HistoryElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<HistoryElement> getNameHistoryForUuid(@PathParam("uuid") String uuid);

}
