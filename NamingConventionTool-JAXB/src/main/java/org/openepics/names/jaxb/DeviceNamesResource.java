package org.openepics.names.jaxb;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This resource provides bulk device name data, and has a subresource for
 * retrieving data of specific device names.
 *
 * @author Andraz Pozar
 * @author Sunil Sah
 */
@Path("deviceNames")
public interface DeviceNamesResource {

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNames();

    @Path("{uuid}")
    public SpecificDeviceNameResource getSpecificDeviceNameSubresource();

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("subsection/{subsection}")
    public List<DeviceNameElement> getAllDeviceNamesBySubSection(@PathParam("subsection") String subSection);

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("section/{section}")
    public List<DeviceNameElement> getAllDeviceNamesBySection(@PathParam("section") String section);

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("discipline/{discipline}")
    public List<DeviceNameElement> getAllDeviceNamesByDiscipline(@PathParam("discipline") String subSection);

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("devicetype/{devicetype}")
    public List<DeviceNameElement> getAllDeviceNamesByDeviceType(@PathParam("devicetype") String deviceType);

}
