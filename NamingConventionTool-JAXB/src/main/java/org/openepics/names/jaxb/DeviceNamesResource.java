/*
 * Copyright (c) 2014 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.jaxb;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.ExternalDocs;
import io.swagger.annotations.SwaggerDefinition;

/**
 * This resource provides bulk device name data, and has a subresource for
 * retrieving data of specific device name.
 *
 * @author Andraz Pozar
 * @author Sunil Sah
 * @author Banafsheh Hajinasab
 * @author Lars Johansson
 *
 * @see SpecificDeviceNameResource
 */
@Path("deviceNames")
@Api(value = "/deviceNames")
@Produces({"application/json"})
@SwaggerDefinition(
        info = @Info(
                description = "\n"
                        + "    This is a documentation for all REST interfaces of Naming service. \n"
                        + "    You can find out more about Naming service in Naming application,  \n"
                        + "        https://naming.esss.lu.se",
                version = "1.0.2",
                title = "Naming service API documentation",
                termsOfService = "http://swagger.io/terms/",
                contact = @Contact(
                        name = "Support",
                        email = "Icsscontrolsystemsupport@esss.se")
                ),
        externalDocs = @ExternalDocs(
                value = "ESS Naming Convention",
                url = "https://chess.esss.lu.se/enovia/link/ESS-0000757/21308.51166.45568.45993/valid"))
public interface DeviceNamesResource {

    // ----------------------------------------------------------------------------------------------------
    // table of content
    // ----------------------------------------------------------------------------------------------------
    // overall (list, specific, search)
    // name structure (specific, search)
    //     system
    //         system
    //         subsystem
    //     device
    //         discipline
    //         device type
    // ----------------------------------------------------------------------------------------------------

    // ----------------------------------------------------------------------------------------------------
    // overall (list, specific, search)
    // ----------------------------------------------------------------------------------------------------

    /**
     * Lists all device names.
     *
     * @return a list of (all) device names
     */
    @GET
    @ApiOperation(
            value = "Lists all device names",
            notes = "Returns a list of (all) device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNames();

    /**
     * Returns resource that can provide specific device name data.
     *
     * @return resource that can provide specific device name data
     */
    @Path("{uuid}")
    public SpecificDeviceNameResource getSpecificDeviceNameSubresource();

    /**
     * Finds all device names by device name search.<br/>
     * Note
     * <ul>
     * <li>device name (search, case sensitive, regex)
     * <li>search done for all parts of device name
     * </ul>
     *
     * @param deviceName device name to search for
     * @return a list of device names
     */
    @GET
    @Path("search/{deviceName}")
    @ApiOperation(
            value = "Finds all device names by device name search",
            notes = "Note device name (search, case sensitive, regex). Returns a list of device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesSearch(@PathParam("deviceName") String deviceName);

    // ----------------------------------------------------------------------------------------------------
    // name structure (specific, search)
    //     system
    //         system
    //         subsystem
    //     device
    //         discipline
    //         device type
    // ----------------------------------------------------------------------------------------------------

    /**
     * Finds all device names by system.
     * Note system (exact match, case sensitive).
     *
     * @param system system to look for
     * @return a list of device names
     */
    @GET
    @Path("system/{system}")
    @ApiOperation(
            value = "Finds all devices by system",
            notes = "Note system (exact match, case sensitive). Returns a list of device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesBySystem(@PathParam("system") String system);

    /**
     * Finds all device names by system search.
     * Note system (search, case sensitive, regex).
     *
     * @param system system to search for
     * @return a list of device names
     */
    @GET
    @Path("system/search/{system}")
    @ApiOperation(
            value = "Finds all devices by system search",
            notes = "Note system (search, case sensitive, regex). Returns a list of device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesBySystemSearch(@PathParam("system") String system);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Finds all device names by subsystem.
     * Note subsystem (exact match, case sensitive).
     *
     * @param subsystem subsystem to look for
     * @return a list of device names
     */
    @GET
    @Path("subsystem/{subsystem}")
    @ApiOperation(
            value = "Finds all devices by subsystem",
            notes = "Note subsystem (exact match, case sensitive). Returns a list of device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesBySubsystem(@PathParam("subsystem") String subsystem);

    /**
     * Finds all device names by subsystem search.
     * Note subsystem (search, case sensitive, regex).
     *
     * @param subsystem subsystem to search for
     * @return a list of device names
     */
    @GET
    @Path("subsystem/search/{subsystem}")
    @ApiOperation(
            value = "Finds all device names by subsystem search",
            notes = "Note subsystem (search, case sensitive, regex). Returns a list of device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesBySubsystemSearch(@PathParam("subsystem") String subsystem);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Finds all device names by discipline.
     * Note discipline (exact match, case sensitive).
     *
     * @param discipline discipline to look for
     * @return a list of device names
     */
    @GET
    @Path("discipline/{discipline}")
    @ApiOperation(
            value = "Finds all device names by discipline",
            notes = "Note discipline (exact match, case sensitive). Returns a list of device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesByDiscipline(@PathParam("discipline") String discipline);

    /**
     * Finds all device names by discipline search.
     * Note discipline (search, case sensitive, regex).
     *
     * @param discipline discipline to search for
     * @return a list of device names
     */
    @GET
    @Path("discipline/search/{discipline}")
    @ApiOperation(
            value = "Finds all device names by discipline search",
            notes = "Note discipline (search, case sensitive, regex). Returns a list of device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesByDisciplineSearch(@PathParam("discipline") String discipline);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Finds all device names by device type.
     * Note device type (exact match, case sensitive).
     *
     * @param deviceType device type to look for
     * @return a list of device names
     */
    @GET
    @Path("devicetype/{devicetype}")
    @ApiOperation(
            value = "Finds all device names by device type",
            notes = "Note device type (exact match, case sensitive). Returns a list of device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesByDeviceType(@PathParam("devicetype") String deviceType);

    /**
     * Finds all device names by device type search.
     * Note device type (search, case sensitive, regex).
     *
     * @param deviceType device type to search for
     * @return a list of device names
     */
    @GET
    @Path("devicetype/search/{devicetype}")
    @ApiOperation(
            value = "Finds all device names by device type search",
            notes = "Note device type (search, case sensitive, regex). Returns a list of device names",
            response = DeviceNameElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DeviceNameElement> getAllDeviceNamesByDeviceTypeSearch(@PathParam("devicetype") String deviceType);

}
