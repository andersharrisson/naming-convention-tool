/*
 * Copyright (c) 2014 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.jaxb;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.UUID;

/**
 * Data transfer object representing Devices for JSON and XML serialization.
 *
 * @author Andraz Pozar
 * @author Banafsheh Hajinasab

 */
@XmlRootElement
@ApiModel
public class DeviceNameElement {

    private UUID uuid;
    private String systemGroup;
    private String system;
    private String subsystem;
    private String discipline;
    private String deviceType;
    private String instanceIndex;
    private String name;
    private String description;
    private String status;

    /**
     * Constructor
     *
     * @param uuid      the unique uuid of the name that this element is accociated with.
     * @param name      the naming convention name
     * @param status    of the name Element. DELETED if the name associated with this uuid has been deleted.
     *                  ACTIVE if the name element is not delted and is the  most recent name element for this uuid.
     *                  OBSOLETE if the name element is neither deleted nor active
     *                  (i.e. there exists a newer version for the same uuid..)
     */
    public DeviceNameElement(UUID uuid, String name, String status) {
        this.uuid = uuid;
        this.name = name;
        this.status = status;
    }

    public DeviceNameElement() {

    }

    /**
     * @return the uuid
     */
    @ApiModelProperty(required = true, value = "UUID")
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the systemGroup
     */
    @ApiModelProperty(required = true)
    public String getSystemGroup() {
        return systemGroup;
    }

    /**
     * @param systemGroup the systemGroup to set
     */
    public void setSystemGroup(String systemGroup) {
        this.systemGroup = systemGroup;
    }

    /**
     * @return the system
     */
    @ApiModelProperty(required = true)
    public String getSystem() {
        return system;
    }

    /**
     * @param system the system to set
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     * @return the subsystem
     */
    @ApiModelProperty(required = true)
    public String getSubsystem() {
        return subsystem;
    }

    /**
     * @param subsystem the subsystem to set
     */
    public void setSubsystem(String subsystem) {
        this.subsystem = subsystem;
    }

    /**
     * @return the discipline
     */
    @ApiModelProperty(required = true)
    public String getDiscipline() {
        return discipline;
    }

    /**
     * @param discipline the discipline to set
     */
    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    /**
     * @return the deviceType
     */
    @ApiModelProperty(required = true)
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType the deviceType to set
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return the instanceIndex
     */
    @ApiModelProperty(required = true)
    public String getInstanceIndex() {
        return instanceIndex;
    }

    /**
     * @param instanceIndex the instanceIndex to set
     */
    public void setInstanceIndex(String instanceIndex) {
        this.instanceIndex = instanceIndex;
    }

    /**
     * @return the name
     */
    @ApiModelProperty(required = true)
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    @ApiModelProperty(required = true)
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the status
     */
    @ApiModelProperty(
            value = "The status can have three different values: DELETED, ACTIVE, OBSOLETE",
            allowableValues = "DELETED, ACTIVE, OBSOLETE",
            required = true)
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

}
