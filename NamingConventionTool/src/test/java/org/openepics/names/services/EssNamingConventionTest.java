/*
* Copyright (c) 2014 European Spallation Source
* Copyright (c) 2014 Cosylab d.d.
*
* This file is part of Naming Service.
* Naming Service is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or any newer version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
*/
package org.openepics.names.services;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.business.NameType;
import org.openepics.names.util.As;

import com.google.common.collect.ImmutableList;
import static org.junit.Assert.*;

/**
 * Unit tests for EssNamingConvention class.
 *
 * @author Karin Rathsman
 * @author Lars Johansson
 *
 * @see NamingConvention
 * @see EssNamingConvention
 */
public class EssNamingConventionTest {

    // ----------------------------------------------------------------------------------------------------
    // table of content
    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     validateMnemonic
    //         system structure
    //         device structure
    //     validateMnemonic (2)
    //         system structure
    //         device structure
    //     isInstanceIndexValid
    //         device
    //     isInstanceIndexValid (2)
    //         device
    //     equivalenceClassRepresentative
    //     conventionName
    // name in relation to other
    //     canMnemonicsCoexist
    //         system structure
    //         device structure
    //         system structure & device structure
    //     canMnemonicsCoexist (2)
    //         system structure
    //         device structure
    //         system structure & device structure
    // ----------------------------------------------------------------------------------------------------

    private static final String EMPTY = "";
    private static final String NULL  = null;
    private static final String SPACE = " ";

    private static final String A = "A";
    private static final String B = "B";
    private static final String C = "C";

    private static final String SUP = "Sup";
    private static final String SYS = "Sys";
    private static final String SUB = "Sub";
    private static final String DIS = "Dis";
    private static final String CAT = "Cat";
    private static final String GRP = "Grp";
    private static final String DEV = "Dev";
    private static final String IDX = "Idx";

    private static final String SUP_DIS_DEV         = "Sup:Dis-Dev";
    private static final String SUP_DIS_DEV_IDX     = "Sup:Dis-Dev-Idx";
    private static final String SYS_SUB             = "Sys-Sub";
    private static final String SYS_DIS_DEV         = "Sys:Dis-Dev";
    private static final String SYS_DIS_DEV_IDX     = "Sys:Dis-Dev-Idx";
    private static final String SYS_SUB_DIS_DEV     = "Sys-Sub:Dis-Dev";
    private static final String SYS_SUB_DIS_DEV_IDX = "Sys-Sub:Dis-Dev-Idx";
    private static final String DIS_DEV             = "Dis-Dev";

    private static final String SYS_COLON           = "Sys:";
    private static final String SYS_COLON_S3        = "Sys:   ";

    private static final String CHOP_G = "ChopG";
    private static final String ECAT10 = "ECAT10";
    private static final String IOC    = "IOC";
    private static final String OTHER  = "Other";
    private static final String TDS    = "TDS";
    private static final String TD180  = "TD180";

    private static final String NUM_0         = "0";
    private static final String NUM_1         = "1";
    private static final String NUM_12        = "12";
    private static final String NUM_123       = "123";
    private static final String NUM_1234      = "1234";
    private static final String NUM_12345     = "12345";
    private static final String NUM_123456    = "123456";
    private static final String NUM_1234567   = "1234567";
    private static final String NUM_12345678  = "12345678";
    private static final String NUM_123456789 = "123456789";

    private static final String ALPHABETIC_NOT_OK       = "alphabetic value not allowed";
    private static final String ALPHABETIC_OK           = "alphabetic value allowed";
    private static final String ALPHANUMERIC_NOT_OK     = "alphanumeric value not allowed";
    private static final String ALPHANUMERIC_OK         = "alphanumeric value allowed";
    private static final String BLANKS_NOT_OK           = "value with blanks not allowed";
    private static final String EMPTY_NOT_OK            = "empty value not allowed";
    private static final String EMPTY_OK                = "empty value allowed";
    private static final String LENGTH_NOT_OK           = "length of value not allowed";
    private static final String LENGTH_OK               = "length of value allowed";
    private static final String NON_ALPHANUMERIC_NOT_OK = "non-alphanumeric value not allowed";
    private static final String NUMERIC_OK              = "numeric value allowed";
    private static final String NULL_NOT_OK             = "null value not allowed";
    private static final String ONLY_ZEROS_NOT_OK       = "value with only zeros not allowed";
    private static final String ONLY_ZEROS_OK           = "value with only zeros allowed";

    private static EssNamingConvention namingConvention;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        namingConvention = new EssNamingConvention();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        namingConvention = null;
    }

    // utility methods
    private MnemonicValidation testSystemGroup(String mnemonic){
        return namingConvention.validateMnemonic(ImmutableList.of(mnemonic),                NameType.SYSTEM_STRUCTURE);
    }
    private MnemonicValidation testSystem(String mnemonic){
        return namingConvention.validateMnemonic(ImmutableList.of(EMPTY, mnemonic),         NameType.SYSTEM_STRUCTURE);
    }
    private MnemonicValidation testSubsystem(String mnemonic){
        return namingConvention.validateMnemonic(ImmutableList.of(EMPTY, SYS, mnemonic),    NameType.SYSTEM_STRUCTURE);
    }
    private MnemonicValidation testDiscipline(String mnemonic){
        return namingConvention.validateMnemonic(ImmutableList.of(mnemonic),                NameType.DEVICE_STRUCTURE);
    }
    private MnemonicValidation testDeviceGroup(String mnemonic){
        return namingConvention.validateMnemonic(ImmutableList.of(DIS, mnemonic),           NameType.DEVICE_STRUCTURE);
    }
    private MnemonicValidation testDeviceType(String mnemonic){
        return namingConvention.validateMnemonic(ImmutableList.of(DIS, EMPTY, mnemonic),    NameType.DEVICE_STRUCTURE);
    }

    private MnemonicValidation testSystem(String systemGroup, String mnemonic){
        return namingConvention.validateMnemonic(ImmutableList.of(systemGroup, mnemonic),   NameType.SYSTEM_STRUCTURE);
    }
    private MnemonicValidation testSubsystem(String systemGroup, String system, String mnemonic){
        return namingConvention.validateMnemonic(
                ImmutableList.of(systemGroup, system, mnemonic), NameType.SYSTEM_STRUCTURE);
    }

    /**
     * Test if mnemonic is required for mnemonic path and name type.
     */
    @Test
    public void isMnemonicRequired(){
        final List<String> systemPath1       = ImmutableList.of(SUP);
        final List<String> systemPath12      = ImmutableList.of(SUP,   SYS);
        final List<String> systemPath123     = ImmutableList.of(SUP,   SYS,   SUB);
        final List<String> systemPath2       = ImmutableList.of(EMPTY, SYS);
        final List<String> systemPath23      = ImmutableList.of(EMPTY, SYS,   SUB);

        final List<String> devicePath1       = ImmutableList.of(DIS);
        final List<String> devicePath12      = ImmutableList.of(DIS,   CAT);
        final List<String> devicePath123     = ImmutableList.of(DIS,   CAT,   DEV);

        assertFalse(namingConvention.isMnemonicRequired(systemPath1,   NameType.SYSTEM_STRUCTURE));
        assertTrue (namingConvention.isMnemonicRequired(systemPath12,  NameType.SYSTEM_STRUCTURE));
        assertTrue (namingConvention.isMnemonicRequired(systemPath123, NameType.SYSTEM_STRUCTURE));
        assertTrue (namingConvention.isMnemonicRequired(systemPath2,   NameType.SYSTEM_STRUCTURE));
        assertTrue (namingConvention.isMnemonicRequired(systemPath23,  NameType.SYSTEM_STRUCTURE));

        assertTrue (namingConvention.isMnemonicRequired(devicePath1,   NameType.DEVICE_STRUCTURE));
        assertFalse(namingConvention.isMnemonicRequired(devicePath12,  NameType.DEVICE_STRUCTURE));
        assertTrue (namingConvention.isMnemonicRequired(devicePath123, NameType.DEVICE_STRUCTURE));

        assertFalse(
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY),               NameType.SYSTEM_STRUCTURE));
        assertFalse(
                namingConvention.isMnemonicRequired(ImmutableList.of(SUP),                 NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, EMPTY),        NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, SYS),          NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(SUP,   EMPTY),        NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(SUP,   SYS),          NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, EMPTY, EMPTY), NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, EMPTY, SUB),   NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, SYS,   EMPTY), NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, SYS,   SUB),   NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(SUP,   EMPTY, EMPTY), NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(SUP,   EMPTY, SUB),   NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(SUP,   SYS,   EMPTY), NameType.SYSTEM_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(SUP  , SYS,   SUB),   NameType.SYSTEM_STRUCTURE));

        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY),               NameType.DEVICE_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(DIS),                 NameType.DEVICE_STRUCTURE));
        assertFalse(
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, EMPTY),        NameType.DEVICE_STRUCTURE));
        assertFalse(
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, CAT),          NameType.DEVICE_STRUCTURE));
        assertFalse(
                namingConvention.isMnemonicRequired(ImmutableList.of(DIS,   EMPTY),        NameType.DEVICE_STRUCTURE));
        assertFalse(
                namingConvention.isMnemonicRequired(ImmutableList.of(DIS,   CAT),          NameType.DEVICE_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, EMPTY, EMPTY), NameType.DEVICE_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, EMPTY, DEV),   NameType.DEVICE_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, CAT,   EMPTY), NameType.DEVICE_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(EMPTY, CAT,   DEV),   NameType.DEVICE_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(DIS,   EMPTY, EMPTY), NameType.DEVICE_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(DIS,   EMPTY, DEV),   NameType.DEVICE_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(DIS,   CAT,   EMPTY), NameType.DEVICE_STRUCTURE));
        assertTrue (
                namingConvention.isMnemonicRequired(ImmutableList.of(DIS,   CAT,   DEV),   NameType.DEVICE_STRUCTURE));
    }


    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     validateMnemonic
    //         system structure
    //         device structure
    // ----------------------------------------------------------------------------------------------------

    // empty
    // blank
    // alphabetic
    // numeric
    // alphanumeric
    // non-alphanumeric
    // length
    // mix

    /**
     * Test validity of mnemonic for system group.
     */
    @Test
    public void isSystemGroupValid(){
        assertEquals(EMPTY_OK,                MnemonicValidation.VALID,                testSystemGroup(EMPTY));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystemGroup(SPACE));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystemGroup("Sys "));
        assertEquals(ALPHABETIC_OK,           MnemonicValidation.VALID,                testSystemGroup(SYS));
        assertEquals(NUMERIC_OK,              MnemonicValidation.VALID,                testSystemGroup(NUM_123));
        assertEquals(ONLY_ZEROS_OK,           MnemonicValidation.VALID,                testSystemGroup("000"));
        assertEquals(ALPHANUMERIC_OK,         MnemonicValidation.VALID,                testSystemGroup("Sys0"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystemGroup(":"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystemGroup(SYS_COLON));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.TOO_LONG,             testSystemGroup(SYS_COLON_S3));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystemGroup(NUM_1));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystemGroup(NUM_12));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystemGroup(NUM_123));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystemGroup(NUM_1234));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystemGroup(NUM_12345));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystemGroup(NUM_123456));
        assertEquals(LENGTH_NOT_OK,           MnemonicValidation.TOO_LONG,             testSystemGroup(NUM_1234567));

        // other
        assertEquals(ALPHANUMERIC_OK,         MnemonicValidation.VALID,                testSystemGroup("Ac1"));
        assertEquals(ALPHABETIC_OK,           MnemonicValidation.VALID,                testSystemGroup("Acc"));
    }

    /**
     * Test validity of mnemonic for system.
     */
    @Test
    public void isSystemValid() {
        assertEquals(EMPTY_NOT_OK,            MnemonicValidation.EMPTY,                testSystem(EMPTY));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SPACE));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem("Sys "));
        assertEquals(ALPHABETIC_OK,           MnemonicValidation.VALID,                testSystem(SYS));
        assertEquals(NUMERIC_OK,              MnemonicValidation.VALID,                testSystem(NUM_123));
        assertEquals(ONLY_ZEROS_OK,           MnemonicValidation.VALID,                testSystem("000"));
        assertEquals(ALPHANUMERIC_OK,         MnemonicValidation.VALID,                testSystem("Sys0"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(":"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SYS_COLON));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SYS_COLON_S3));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(NUM_1));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(NUM_12));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(NUM_123));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(NUM_1234));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(NUM_12345));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(NUM_123456));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(NUM_1234567));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(NUM_12345678));
        assertEquals(LENGTH_NOT_OK,           MnemonicValidation.TOO_LONG,             testSystem(NUM_123456789));

        // system group - SUP
        assertEquals(EMPTY_NOT_OK,            MnemonicValidation.EMPTY,                testSystem(SUP, EMPTY));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SUP, SPACE));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SUP, "Sys "));
        assertEquals(ALPHABETIC_OK,           MnemonicValidation.VALID,                testSystem(SUP, SYS));
        assertEquals(NUMERIC_OK,              MnemonicValidation.VALID,                testSystem(SUP, NUM_123));
        assertEquals(ONLY_ZEROS_OK,           MnemonicValidation.VALID,                testSystem(SUP, "000"));
        assertEquals(ALPHANUMERIC_OK,         MnemonicValidation.VALID,                testSystem(SUP, "Sys0"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SUP, ":"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SUP, SYS_COLON));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SUP, SYS_COLON_S3));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(SUP, NUM_1));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(SUP, NUM_12));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(SUP, NUM_123));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(SUP, NUM_1234));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(SUP, NUM_12345));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(SUP, NUM_123456));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(SUP, NUM_1234567));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSystem(SUP, NUM_12345678));
        assertEquals(LENGTH_NOT_OK,           MnemonicValidation.TOO_LONG,             testSystem(SUP, NUM_123456789));

        // other
        assertEquals(ALPHABETIC_OK,           MnemonicValidation.VALID,                testSystem("Acc", SYS));
    }

    /**
     * Test validity of mnemonic for subsystem.
     */
    @Test
    public void isSubsystemValid() {
        assertEquals(EMPTY_NOT_OK,            MnemonicValidation.EMPTY,                testSubsystem(EMPTY));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSubsystem(SPACE));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testSubsystem("Sub "));
        assertEquals(ALPHABETIC_OK,           MnemonicValidation.VALID,                testSubsystem(SUB));
        assertEquals(NUMERIC_OK,              MnemonicValidation.VALID,                testSubsystem(NUM_123));
        assertEquals(ONLY_ZEROS_OK,           MnemonicValidation.VALID,                testSubsystem("000"));
        assertEquals(ALPHANUMERIC_OK,         MnemonicValidation.VALID,                testSubsystem("Sub0"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testSubsystem(":"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testSubsystem("Sub:"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.TOO_LONG,             testSubsystem("Sub:     "));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSubsystem(NUM_1));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSubsystem(NUM_12));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSubsystem(NUM_123));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSubsystem(NUM_1234));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSubsystem(NUM_12345));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSubsystem(NUM_123456));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSubsystem(NUM_1234567));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testSubsystem(NUM_12345678));
        assertEquals(LENGTH_NOT_OK,           MnemonicValidation.TOO_LONG,             testSubsystem(NUM_123456789));

        // other
        assertEquals(ALPHABETIC_OK,
                MnemonicValidation.VALID,                testSubsystem("Lin", SYS, "cryo"));
        assertEquals(ALPHABETIC_OK,
                MnemonicValidation.VALID,                testSubsystem("Lin", SYS, "Cryo"));
    }

    /**
     * Test validity of mnemonic for discipline.
     */
    @Test
    public void isDisciplineValid() {
        assertEquals(EMPTY_NOT_OK,            MnemonicValidation.EMPTY,                testDiscipline(EMPTY));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testDiscipline(SPACE));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testDiscipline("Dis "));
        assertEquals(ALPHABETIC_OK,           MnemonicValidation.VALID,                testDiscipline(DIS));
        assertEquals(NUMERIC_OK,              MnemonicValidation.VALID,                testDiscipline(NUM_123));
        assertEquals(ONLY_ZEROS_OK,           MnemonicValidation.VALID,                testDiscipline("000"));
        assertEquals(ALPHANUMERIC_OK,         MnemonicValidation.VALID,                testDiscipline("Dis0"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testDiscipline(":"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testDiscipline("Dis:"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.TOO_LONG,             testDiscipline("Dis:   "));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDiscipline(NUM_1));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDiscipline(NUM_12));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDiscipline(NUM_123));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDiscipline(NUM_1234));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDiscipline(NUM_12345));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDiscipline(NUM_123456));
        assertEquals(LENGTH_NOT_OK,           MnemonicValidation.TOO_LONG,             testDiscipline(NUM_1234567));
    }

    /**
     * Test validity of mnemonic for device group.
     */
    @Test
    public void isDeviceGroupValid() {
        assertEquals(EMPTY_OK,                MnemonicValidation.VALID,                testDeviceGroup(EMPTY));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(SPACE));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup("Grp "));
        assertEquals(ALPHABETIC_OK,           MnemonicValidation.VALID,                testDeviceGroup(GRP));
        assertEquals(NUMERIC_OK,              MnemonicValidation.VALID,                testDeviceGroup(NUM_123));
        assertEquals(ONLY_ZEROS_OK,           MnemonicValidation.VALID,                testDeviceGroup("000"));
        assertEquals(ALPHANUMERIC_OK,         MnemonicValidation.VALID,                testDeviceGroup("Grp0"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(":"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup("Grp:"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup("Grp:   "));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceGroup(NUM_1));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceGroup(NUM_12));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceGroup(NUM_123));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceGroup(NUM_1234));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceGroup(NUM_12345));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceGroup(NUM_123456));
        assertEquals(LENGTH_OK,
                MnemonicValidation.VALID,                testDeviceGroup("123456789012345"));
    }

    /**
     * Test validity of mnemonic for device type.
     */
    @Test
    public void isDeviceTypeValid() {
        assertEquals(EMPTY_NOT_OK,            MnemonicValidation.EMPTY,                testDeviceType(EMPTY));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceType(SPACE));
        assertEquals(BLANKS_NOT_OK,           MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceType("Dev "));
        assertEquals(ALPHABETIC_OK,           MnemonicValidation.VALID,                testDeviceType(DEV));
        assertEquals(NUMERIC_OK,              MnemonicValidation.VALID,                testDeviceType(NUM_123));
        assertEquals(ONLY_ZEROS_OK,           MnemonicValidation.VALID,                testDeviceType("000"));
        assertEquals(ALPHANUMERIC_OK,         MnemonicValidation.VALID,                testDeviceType("Dev0"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceType(":"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceType("Dev:"));
        assertEquals(NON_ALPHANUMERIC_NOT_OK, MnemonicValidation.TOO_LONG,             testDeviceType("Dev:   "));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceType(NUM_1));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceType(NUM_12));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceType(NUM_123));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceType(NUM_1234));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceType(NUM_12345));
        assertEquals(LENGTH_OK,               MnemonicValidation.VALID,                testDeviceType(NUM_123456));
        assertEquals(LENGTH_NOT_OK,           MnemonicValidation.TOO_LONG,             testDeviceType(NUM_1234567));
    }

    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     validateMnemonic (2)
    //         system structure
    //         device structure
    // ----------------------------------------------------------------------------------------------------

    /**
     * Test validity of mnemonic for system structure.
     */
    @Test
    public void isSystemStructureValid() {
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY),              NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  A),          NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  B),          NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  C),          NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  A, A),       NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  A, B),       NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  A, C),       NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  B, A),       NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  B, B),       NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  B, C),       NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  C, A),       NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  C, B),       NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(EMPTY,  C, C),       NameType.SYSTEM_STRUCTURE));

        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A),                  NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, A),               NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, B),               NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, C),               NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, A, A),            NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, A, B),            NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, A, C),            NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, B, A),            NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, B, B),            NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, B, C),            NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, C, A),            NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, C, B),            NameType.SYSTEM_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, C, C),            NameType.SYSTEM_STRUCTURE));
    }

    /**
     * Test validity of mnemonic for device structure.
     */
    @Test
    public void isDeviceStructureValid() {
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A),                  NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, A),               NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, B),               NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, C),               NameType.DEVICE_STRUCTURE));

        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, A, A),            NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, A, B),            NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, A, C),            NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, B, A),            NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, B, B),            NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, B, C),            NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, C, A),            NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, C, B),            NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, C, C),            NameType.DEVICE_STRUCTURE));

        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, EMPTY,  A),       NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, EMPTY,  B),       NameType.DEVICE_STRUCTURE));
        assertEquals(MnemonicValidation.VALID,
                namingConvention.validateMnemonic(ImmutableList.of(A, EMPTY,  C),       NameType.DEVICE_STRUCTURE));
    }

    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     isInstanceIndexValid
    //         device
    //         device override
    //         device PID
    //         device PID override
    // ----------------------------------------------------------------------------------------------------

    /**
     * Test validity of instance index for device.
     */
    @Test
    public void isInstanceIndexValid() {
        final List<String> system     = ImmutableList.of(SUP, SYS, SUB);
        final List<String> deviceType = ImmutableList.of(DIS, CAT, DEV);

        assertFalse(NULL_NOT_OK,             namingConvention.isInstanceIndexValid(system, deviceType, NULL));
        assertFalse(EMPTY_NOT_OK,            namingConvention.isInstanceIndexValid(system, deviceType, EMPTY));
        assertFalse(BLANKS_NOT_OK,           namingConvention.isInstanceIndexValid(system, deviceType, SPACE));
        assertFalse(ALPHABETIC_NOT_OK,       namingConvention.isInstanceIndexValid(system, deviceType, IDX));
        assertFalse(ALPHABETIC_NOT_OK,       namingConvention.isInstanceIndexValid(system, deviceType, "abcdef"));
        assertTrue (NUMERIC_OK,              namingConvention.isInstanceIndexValid(system, deviceType, NUM_123));
        assertFalse(ONLY_ZEROS_NOT_OK,       namingConvention.isInstanceIndexValid(system, deviceType, "0"));
        assertFalse(ONLY_ZEROS_NOT_OK,       namingConvention.isInstanceIndexValid(system, deviceType, "00"));
        assertFalse(ONLY_ZEROS_NOT_OK,       namingConvention.isInstanceIndexValid(system, deviceType, "000"));
        assertFalse(ONLY_ZEROS_NOT_OK,       namingConvention.isInstanceIndexValid(system, deviceType, "0000"));
        assertFalse(ONLY_ZEROS_NOT_OK,       namingConvention.isInstanceIndexValid(system, deviceType, "00000"));
        assertFalse(ONLY_ZEROS_NOT_OK,       namingConvention.isInstanceIndexValid(system, deviceType, "000000"));
        assertFalse(LENGTH_NOT_OK,           namingConvention.isInstanceIndexValid(system, deviceType, "0000000"));
        assertFalse(ALPHANUMERIC_NOT_OK,     namingConvention.isInstanceIndexValid(system, deviceType, "abc123"));
        assertFalse(NON_ALPHANUMERIC_NOT_OK, namingConvention.isInstanceIndexValid(system, deviceType, "a!"));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, "01"));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, "001"));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, "0001"));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, "0110"));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, "10"));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, "100"));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, "1000"));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, NUM_1));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, NUM_12));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, NUM_123));
        assertTrue (LENGTH_OK,               namingConvention.isInstanceIndexValid(system, deviceType, NUM_1234));
        assertFalse(LENGTH_NOT_OK,           namingConvention.isInstanceIndexValid(system, deviceType, NUM_12345));
        assertFalse(LENGTH_NOT_OK,           namingConvention.isInstanceIndexValid(system, deviceType, NUM_123456));
        assertFalse(LENGTH_NOT_OK,           namingConvention.isInstanceIndexValid(system, deviceType, NUM_1234567));
    }

    /**
     * Test validity of instance index for device.
     */
    @Test
    public void isInstanceIndexValidOverride() {
        // override - length, characters

        final List<String> system     = ImmutableList.of(SUP, SYS, SUB);
        final List<String> deviceType = ImmutableList.of(DIS, CAT, DEV);

        assertFalse(namingConvention.isInstanceIndexValid(system, deviceType, NULL, true));
        assertFalse(namingConvention.isInstanceIndexValid(system, deviceType, EMPTY, true));
        assertFalse(namingConvention.isInstanceIndexValid(system, deviceType, SPACE, true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, IDX, true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "abcdef", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, NUM_123, true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "0", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "00", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "0000", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "00000", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "000000", true));
        assertFalse(namingConvention.isInstanceIndexValid(system, deviceType, "0000000", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "abc123", true));
        assertFalse(namingConvention.isInstanceIndexValid(system, deviceType, "a!", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "0001", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "0110", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "10", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "100", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, "1000", true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, NUM_1, true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, NUM_12, true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, NUM_123, true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, NUM_1234, true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, NUM_12345, true));
        assertTrue (namingConvention.isInstanceIndexValid(system, deviceType, NUM_123456, true));
        assertFalse(namingConvention.isInstanceIndexValid(system, deviceType, NUM_1234567, true));
    }

    /**
     * Test validity of instance index, for device.
     */
    @Test
    public void isInstanceIndexValidPIDNumeric() {
        // depend on discipline, device type

        final List<String> systemPath2      = ImmutableList.of(EMPTY,  TDS,   TD180);
        final List<String> deviceTypePath2  = ImmutableList.of("Mech", EMPTY, IOC);
        final List<String> deviceTypePath21 = ImmutableList.of("Cryo", EMPTY, IOC);
        final List<String> deviceTypePath22 = ImmutableList.of("EMR",  EMPTY, IOC);
        final List<String> deviceTypePath23 = ImmutableList.of("HVAC", EMPTY, IOC);
        final List<String> deviceTypePath24 = ImmutableList.of("Proc", EMPTY, IOC);
        final List<String> deviceTypePath25 = ImmutableList.of("SC",   EMPTY, IOC);
        final List<String> deviceTypePath26 = ImmutableList.of("Vac",  EMPTY, IOC);
        final List<String> deviceTypePath27 = ImmutableList.of("WtrC", EMPTY, IOC);

        final String _001a    = "001a";
        final String _001ab   = "001ab";
        final String _001abc  = "001abc";
        final String _001abcd = "001abcd";
        final String _001A    = "001A";
        final String _001AB   = "001AB";
        final String _001ABC  = "001ABC";
        final String _001ABCD = "001ABCD";

        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "000a"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "000a"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "000A"));

        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "1"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "01"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "001"));

        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001a));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001a));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001ab));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001ab));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001abc));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001abc));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2 , _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001abcd));

        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001ABCD));
    }

    /**
     * Test validity of instance index, for device.
     */
    @Test
    public void isInstanceIndexValidPID() {
        // depend on discipline

        final List<String> systemPath1      = ImmutableList.of(A, A, A);
        final List<String> deviceTypePath1  = ImmutableList.of(A, A, A);

        final List<String> systemPath2      = ImmutableList.of(EMPTY,  TDS,   TD180);
        final List<String> deviceTypePath2  = ImmutableList.of("Mech", EMPTY, TDS);
        final List<String> deviceTypePath21 = ImmutableList.of("Cryo", EMPTY, TDS);
        final List<String> deviceTypePath22 = ImmutableList.of("EMR",  EMPTY, TDS);
        final List<String> deviceTypePath23 = ImmutableList.of("HVAC", EMPTY, TDS);
        final List<String> deviceTypePath24 = ImmutableList.of("Proc", EMPTY, TDS);
        final List<String> deviceTypePath25 = ImmutableList.of("SC",   EMPTY, TDS);
        final List<String> deviceTypePath26 = ImmutableList.of("Vac",  EMPTY, TDS);
        final List<String> deviceTypePath27 = ImmutableList.of("WtrC", EMPTY, TDS);

        final String _001a    = "001a";
        final String _001ab   = "001ab";
        final String _001abc  = "001abc";
        final String _001abcd = "001abcd";
        final String _001A    = "001A";
        final String _001AB   = "001AB";
        final String _001ABC  = "001ABC";
        final String _001ABCD = "001ABCD";

        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  ""));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "0"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "00"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "0000"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "1"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "01"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "0001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "1001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "10001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "abc"));

        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, NULL));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "000a"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "000A"));

        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "1"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "01"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "001"));

        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001a));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001ab));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001abc));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2 , _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001abcd));

        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001ABCD));
    }

    /**
     * Test validity of instance index, for device.
     */
    @Test
    public void isInstanceIndexValidPIDOverride() {
        // depend on discipline
        // override - length, characters

        final List<String> systemPath1      = ImmutableList.of(A, A, A);
        final List<String> deviceTypePath1  = ImmutableList.of(A, A, A);

        final List<String> systemPath2      = ImmutableList.of(EMPTY,  TDS,   TD180);
        final List<String> deviceTypePath2  = ImmutableList.of("Mech", EMPTY, TDS);
        final List<String> deviceTypePath21 = ImmutableList.of("Cryo", EMPTY, TDS);
        final List<String> deviceTypePath22 = ImmutableList.of("EMR",  EMPTY, TDS);
        final List<String> deviceTypePath23 = ImmutableList.of("HVAC", EMPTY, TDS);
        final List<String> deviceTypePath24 = ImmutableList.of("Proc", EMPTY, TDS);
        final List<String> deviceTypePath25 = ImmutableList.of("SC",   EMPTY, TDS);
        final List<String> deviceTypePath26 = ImmutableList.of("Vac",  EMPTY, TDS);
        final List<String> deviceTypePath27 = ImmutableList.of("WtrC", EMPTY, TDS);

        final String _001a    = "001a";
        final String _001ab   = "001ab";
        final String _001abc  = "001abc";
        final String _001abcd = "001abcd";
        final String _001A    = "001A";
        final String _001AB   = "001AB";
        final String _001ABC  = "001ABC";
        final String _001ABCD = "001ABCD";

        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  NULL, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "0", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "00", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "0000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "1", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "0001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "1001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "10001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath1, deviceTypePath1,  "abc", true));

        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  NULL, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, NULL, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, NULL, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, NULL, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, NULL, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, NULL, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, NULL, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, NULL, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "000", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "000a", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "000a", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "000a", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "000a", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "000a", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "000a", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "000a", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "000a", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "000A", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "000A", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "000A", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "000A", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "000A", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "000A", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "000A", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "000A", true));

        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "1", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "1", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "1", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "1", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "1", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "1", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "1", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "1", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "01", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, "001", true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, "001", true));

        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001a, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001a, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001a, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001a, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001a, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001a, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001a, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001a, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001ab, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001ab, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001ab, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001ab, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001ab, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001ab, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001ab, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001ab, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001abc, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001abc, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001abc, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001abc, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001abc, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001abc, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001abc, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001abc, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2 , _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001abcd, true));

        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001A, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001A, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001A, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001A, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001A, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001A, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001A, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001A, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001AB, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001AB, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001AB, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001AB, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001AB, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001AB, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001AB, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001AB, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001ABC, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001ABC, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001ABC, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001ABC, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001ABC, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001ABC, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001ABC, true));
        assertTrue (namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001ABC, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath2,  _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath21, _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath22, _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath23, _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath24, _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath25, _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath26, _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemPath2, deviceTypePath27, _001ABCD, true));
    }

    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     equivalenceClassRepresentative
    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of equivalence class representative method.
     */
    @Test
    public void equivalenceClassRepresentative() {
        assertEquals("RFQ-10:EMR-TT-1",  namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-001"));
        assertEquals("RFQ-10:EMR-TT-10", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-010"));
        assertEquals("RFQ-10:EMR-TT-11", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-011"));
        assertEquals("RFQ-10:EMR-TT-12", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-012"));
        assertEquals("RFQ-10:EMR-TT-13", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-013"));
        assertEquals("RFQ-10:EMR-TT-14", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-014"));
        assertEquals("RFQ-10:EMR-TT-15", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-015"));
        assertEquals("RFQ-10:EMR-TT-16", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-016"));
        assertEquals("RFQ-10:EMR-TT-17", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-017"));
        assertEquals("RFQ-10:EMR-TT-18", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-018"));
        assertEquals("RFQ-10:EMR-TT-19", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-019"));

        assertEquals(
                "PB1-FPM1:CTR1-ECAT-100",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECAT-100"));
        assertEquals(
                "PB1-FPM1:CTR1-ECATC-101",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATC-101"));
        assertEquals(
                "PB1-FPM1:CTR1-ECATE-101",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATE-101"));
        assertEquals(
                "PB1-FPM1:CTR1-ECAT10-101",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATIO-101"));
        assertEquals(
                "PB1-FPM1:CTR1-ECAT10-102",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATIO-102"));
        assertEquals(
                "PB1-FPM1:CTR1-ECAT10-103",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATIO-103"));
        assertEquals(
                "PB1-FPM1:CTR1-ECAT10-104",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATIO-104"));
        assertEquals(
                "PB1-FPM1:CTR1-EVR-101",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-EVR-101"));
        assertEquals(
                "PB1-FPM1:CTR1-EVR-201",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-EVR-201"));
        assertEquals(
                "PB1-FPM1:CTR1-1PC-100",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-IPC-100"));
        assertEquals(
                "PB1-FPM1:CTR1-1PC-200",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-IPC-200"));
    }

    /**
     * Test of equivalence class representative method for similarity to <tt>1</tt>.
     */
    @Test
    public void equivalenceClassRepresentativeSimilarTo1() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative(NUM_1));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative("I"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative("l"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative("L"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative("i"));
        assertFalse(
                namingConvention.equivalenceClassRepresentative(NUM_1).equals(
                        namingConvention.equivalenceClassRepresentative("b")));
    }

    /**
     * Test of equivalence class representative method for similarity to <tt>0</tt>.
     */
    @Test
    public void equivalenceClassRepresentativeSimilarTo0() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_0),
                namingConvention.equivalenceClassRepresentative("o"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_0),
                namingConvention.equivalenceClassRepresentative("O"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_0),
                namingConvention.equivalenceClassRepresentative(NUM_0));
        assertFalse(
                namingConvention.equivalenceClassRepresentative(NUM_0).equals(
                        namingConvention.equivalenceClassRepresentative("b")));
    }

    /**
     * Test of equivalence class representative method for similarity to <tt>V</tt>.
     */
    @Test
    public void equivalenceClassRepresentativeSimilarToV() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative("V"),
                namingConvention.equivalenceClassRepresentative("v"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative("V"),
                namingConvention.equivalenceClassRepresentative("V"));
        assertFalse(
                namingConvention.equivalenceClassRepresentative("V1").equals(
                        namingConvention.equivalenceClassRepresentative("w1")));
        assertFalse(
                namingConvention.equivalenceClassRepresentative("V1").equals(
                        namingConvention.equivalenceClassRepresentative("W1")));
        assertFalse(
                namingConvention.equivalenceClassRepresentative("V").equals(
                        namingConvention.equivalenceClassRepresentative("w")));
        assertFalse(
                namingConvention.equivalenceClassRepresentative("V").equals(
                        namingConvention.equivalenceClassRepresentative("W")));
        assertFalse(
                namingConvention.equivalenceClassRepresentative("V").equals(
                        namingConvention.equivalenceClassRepresentative("b")));
    }

    /**
     * Test of equivalence class representative method for lower and upper case characters.
     */
    @Test
    public void equivalenceClassRepresentativeLowerAndUpperCaseCharacters() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative("tEsTS"),
                namingConvention.equivalenceClassRepresentative("TeSts"));
    }

    /**
     * Test of equivalence class representative method for zero prefixed number.
     */
    @Test
    public void equivalenceClassRepresentativeZeroPrefixedNumber() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative("01"),
                namingConvention.equivalenceClassRepresentative("001"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative("zero01"),
                namingConvention.equivalenceClassRepresentative("zero1"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative("ze01ro"),
                namingConvention.equivalenceClassRepresentative("ze1ro"));
    }

    /**
     * Test of equivalence class representative method for zero after alpha character.
     */
    @Test
    public void equivalenceClassRepresentativeZeroAfterAlphaCharacter() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative("Sub0001"),
                namingConvention.equivalenceClassRepresentative("Sub1"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative("01Sub001"),
                namingConvention.equivalenceClassRepresentative("01Sub1"));
    }

    /**
     * Test of equivalence class representative method for chained calls to get equivalence class.
     */
    @Test
    public void equivalenceClassRepresentativeTwiceEquivalence() {
        // note that of 2 chained calls to equivalenceClassRepresentative, 2nd call may modify
        // <==> 2nd output not same as 1st output
        assertEquals(
                "CRS-T1CP:CRY0-XZ-XXXXX1",
                namingConvention.equivalenceClassRepresentative("CrS-TICP:Cryo-XZ-XXXXX1"));
        assertEquals(
                "CRS-T1CP:CRY-XZ-XXXXX1",
                namingConvention.equivalenceClassRepresentative("CRS-T1CP:CRY0-XZ-XXXXX1"));
        assertEquals("CRY0",   namingConvention.equivalenceClassRepresentative("Cryo"));
        assertEquals("CRY",    namingConvention.equivalenceClassRepresentative("CRY0"));
    }

    /**
     * Test of equivalence class representative method for device type.
     */
    @Test
    public void equivalenceClassRepresentativeDeviceType() {
        // note handling of trailing zeroes
        assertEquals(ECAT10,   namingConvention.equivalenceClassRepresentative("ECATIO"));
        assertEquals("ECAT1",  namingConvention.equivalenceClassRepresentative("ECATI0"));
        assertEquals(ECAT10,   namingConvention.equivalenceClassRepresentative(ECAT10));
    }

    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     conventionName
    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of convention name.
     */
    @Test
    public void conventionNameStructures() {
        final List<String> systemPath1       = ImmutableList.of(SUP);
        final List<String> systemPath12      = ImmutableList.of(SUP,   SYS);
        final List<String> systemPath123     = ImmutableList.of(SUP,   SYS,   SUB);
        final List<String> systemPath2       = ImmutableList.of(EMPTY, SYS);
        final List<String> systemPath23      = ImmutableList.of(EMPTY, SYS,   SUB);

        final List<String> deviceTypePath1   = ImmutableList.of(DIS);
        final List<String> deviceTypePath12  = ImmutableList.of(DIS,   CAT);
        final List<String> deviceTypePath123 = ImmutableList.of(DIS,   CAT, DEV);

        assertEquals(SUP_DIS_DEV_IDX,       namingConvention.conventionName(systemPath1,   deviceTypePath123, IDX));
        assertEquals(SYS_DIS_DEV_IDX,       namingConvention.conventionName(systemPath12,  deviceTypePath123, IDX));
        assertEquals(SYS_SUB_DIS_DEV_IDX,   namingConvention.conventionName(systemPath123, deviceTypePath123, IDX));
        assertEquals(SYS_DIS_DEV_IDX,       namingConvention.conventionName(systemPath2,   deviceTypePath123, IDX));
        assertEquals(SYS_SUB_DIS_DEV_IDX,   namingConvention.conventionName(systemPath23,  deviceTypePath123, IDX));

        assertEquals(SUP_DIS_DEV,           namingConvention.conventionName(systemPath1,   deviceTypePath123, NULL));
        assertEquals(SYS_DIS_DEV,           namingConvention.conventionName(systemPath12,  deviceTypePath123, NULL));
        assertEquals(SYS_SUB_DIS_DEV,       namingConvention.conventionName(systemPath123, deviceTypePath123, NULL));
        assertEquals(SYS_DIS_DEV,           namingConvention.conventionName(systemPath2,   deviceTypePath123, NULL));
        assertEquals(SYS_SUB_DIS_DEV,       namingConvention.conventionName(systemPath23,  deviceTypePath123, NULL));

        assertEquals(SUP_DIS_DEV,           namingConvention.conventionName(systemPath1,   deviceTypePath123, EMPTY));
        assertEquals(SYS_DIS_DEV,           namingConvention.conventionName(systemPath12,  deviceTypePath123, EMPTY));
        assertEquals(SYS_SUB_DIS_DEV,       namingConvention.conventionName(systemPath123, deviceTypePath123, EMPTY));
        assertEquals(SYS_DIS_DEV,           namingConvention.conventionName(systemPath2,   deviceTypePath123, EMPTY));
        assertEquals(SYS_SUB_DIS_DEV,       namingConvention.conventionName(systemPath23,  deviceTypePath123, EMPTY));

        assertEquals(SUP,                   namingConvention.conventionName(systemPath1,   deviceTypePath1,   IDX));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath12,  deviceTypePath1,   IDX));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath123, deviceTypePath1,   IDX));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath2,   deviceTypePath1,   IDX));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath23,  deviceTypePath1,   IDX));

        assertEquals(SUP,                   namingConvention.conventionName(systemPath1,   deviceTypePath12,  IDX));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath12,  deviceTypePath12,  IDX));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath123, deviceTypePath12,  IDX));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath2,   deviceTypePath12,  IDX));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath23,  deviceTypePath12,  IDX));

        final List<String> pathNull = null;

        assertEquals(NULL,                  namingConvention.conventionName(pathNull,      deviceTypePath1,   NULL));
        assertEquals(NULL,                  namingConvention.conventionName(pathNull,      deviceTypePath12,  NULL));
        assertEquals(NULL,                  namingConvention.conventionName(pathNull,      deviceTypePath123, NULL));

        assertEquals(NULL,                  namingConvention.conventionName(pathNull,      deviceTypePath1,   EMPTY));
        assertEquals(NULL,                  namingConvention.conventionName(pathNull,      deviceTypePath12,  EMPTY));
        assertEquals(NULL,                  namingConvention.conventionName(pathNull,      deviceTypePath123, EMPTY));

        assertEquals(NULL,                  namingConvention.conventionName(pathNull,      deviceTypePath1,   IDX));
        assertEquals(NULL,                  namingConvention.conventionName(pathNull,      deviceTypePath12,  IDX));
        assertEquals(NULL,                  namingConvention.conventionName(pathNull,      deviceTypePath123, IDX));

        assertEquals(SUP,                   namingConvention.conventionName(systemPath1,   pathNull,          NULL));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath12,  pathNull,          NULL));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath123, pathNull,          NULL));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath2,   pathNull,          NULL));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath23,  pathNull,          NULL));

        assertEquals(SUP,                   namingConvention.conventionName(systemPath1,   pathNull,          EMPTY));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath12,  pathNull,          EMPTY));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath123, pathNull,          EMPTY));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath2,   pathNull,          EMPTY));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath23,  pathNull,          EMPTY));

        assertEquals(SUP,                   namingConvention.conventionName(systemPath1,   pathNull,          IDX));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath12,  pathNull,          IDX));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath123, pathNull,          IDX));
        assertEquals(SYS,                   namingConvention.conventionName(systemPath2,   pathNull,          IDX));
        assertEquals(SYS_SUB,               namingConvention.conventionName(systemPath23,  pathNull,          IDX));
    }

    /**
     * Test of convention name.
     */
    @Test
    public void conventionNameStructure() {
        // proper alternatives

        final List<String> systemPath1       = ImmutableList.of(SUP);
        final List<String> systemPath12      = ImmutableList.of(SUP,   SYS);
        final List<String> systemPath123     = ImmutableList.of(SUP,   SYS,   SUB);
        final List<String> systemPath2       = ImmutableList.of(EMPTY, SYS);
        final List<String> systemPath23      = ImmutableList.of(EMPTY, SYS,   SUB);

        assertEquals(SUP,       namingConvention.conventionName(systemPath1,   NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS,       namingConvention.conventionName(systemPath12,  NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS_SUB,   namingConvention.conventionName(systemPath123, NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS,       namingConvention.conventionName(systemPath2,   NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS_SUB,   namingConvention.conventionName(systemPath23,  NameType.SYSTEM_STRUCTURE));

        // import will resemble but may be different
        // values from Excel cell may be null, empty, value
        // system paths

        final List<String> s11 = ImmutableList.of(As.emptyIfNull(SUP),   As.emptyIfNull(NULL),  As.emptyIfNull(NULL));
        final List<String> s12 = ImmutableList.of(As.emptyIfNull(SUP),   As.emptyIfNull(NULL),  As.emptyIfNull(EMPTY));
        final List<String> s13 = ImmutableList.of(As.emptyIfNull(SUP),   As.emptyIfNull(NULL),  As.emptyIfNull(SUB));
        final List<String> s14 = ImmutableList.of(As.emptyIfNull(SUP),   As.emptyIfNull(EMPTY), As.emptyIfNull(NULL));
        final List<String> s15 = ImmutableList.of(As.emptyIfNull(SUP),   As.emptyIfNull(EMPTY), As.emptyIfNull(EMPTY));
        final List<String> s16 = ImmutableList.of(As.emptyIfNull(SUP),   As.emptyIfNull(EMPTY), As.emptyIfNull(SUB));
        final List<String> s17 = ImmutableList.of(As.emptyIfNull(SUP),   As.emptyIfNull(SYS),   As.emptyIfNull(NULL));
        final List<String> s18 = ImmutableList.of(As.emptyIfNull(SUP),   As.emptyIfNull(SYS),   As.emptyIfNull(EMPTY));
        final List<String> s19 = ImmutableList.of(As.emptyIfNull(SUP),   As.emptyIfNull(SYS),   As.emptyIfNull(SUB));

        final List<String> s21 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(SYS),   As.emptyIfNull(NULL));
        final List<String> s22 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(SYS),   As.emptyIfNull(EMPTY));
        final List<String> s23 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(SYS),   As.emptyIfNull(SUB));
        final List<String> s24 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(SYS),   As.emptyIfNull(NULL));
        final List<String> s25 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(SYS),   As.emptyIfNull(EMPTY));
        final List<String> s26 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(SYS),   As.emptyIfNull(SUB));

        final List<String> s31 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(NULL),  As.emptyIfNull(SUB));
        final List<String> s32 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(EMPTY), As.emptyIfNull(SUB));
        final List<String> s34 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(NULL),  As.emptyIfNull(SUB));
        final List<String> s35 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(EMPTY), As.emptyIfNull(SUB));

        assertEquals(SUP,       namingConvention.conventionName(s11, NameType.SYSTEM_STRUCTURE));
        assertEquals(SUP,       namingConvention.conventionName(s12, NameType.SYSTEM_STRUCTURE));
        assertEquals(SUP,       namingConvention.conventionName(s13, NameType.SYSTEM_STRUCTURE));
        assertEquals(SUP,       namingConvention.conventionName(s14, NameType.SYSTEM_STRUCTURE));
        assertEquals(SUP,       namingConvention.conventionName(s15, NameType.SYSTEM_STRUCTURE));
        assertEquals(SUP,       namingConvention.conventionName(s16, NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS,       namingConvention.conventionName(s17, NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS,       namingConvention.conventionName(s18, NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS_SUB,   namingConvention.conventionName(s19, NameType.SYSTEM_STRUCTURE));

        assertEquals(SYS,       namingConvention.conventionName(s21, NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS,       namingConvention.conventionName(s22, NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS_SUB,   namingConvention.conventionName(s23, NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS,       namingConvention.conventionName(s24, NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS,       namingConvention.conventionName(s25, NameType.SYSTEM_STRUCTURE));
        assertEquals(SYS_SUB,   namingConvention.conventionName(s26, NameType.SYSTEM_STRUCTURE));

        assertEquals(NULL,      namingConvention.conventionName(s31, NameType.SYSTEM_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(s32, NameType.SYSTEM_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(s34, NameType.SYSTEM_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(s35, NameType.SYSTEM_STRUCTURE));

        // proper alternatives

        final List<String> devicePath1   = ImmutableList.of(DIS);
        final List<String> devicePath12  = ImmutableList.of(DIS,   CAT);
        final List<String> devicePath123 = ImmutableList.of(DIS,   CAT,   DEV);

        assertEquals(NULL,      namingConvention.conventionName(devicePath1,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(devicePath12,  NameType.DEVICE_STRUCTURE));
        assertEquals(DIS_DEV,   namingConvention.conventionName(devicePath123, NameType.DEVICE_STRUCTURE));

        // device paths

        final List<String> d11 = ImmutableList.of(As.emptyIfNull(DIS),   As.emptyIfNull(NULL),  As.emptyIfNull(NULL));
        final List<String> d12 = ImmutableList.of(As.emptyIfNull(DIS),   As.emptyIfNull(NULL),  As.emptyIfNull(EMPTY));
        final List<String> d13 = ImmutableList.of(As.emptyIfNull(DIS),   As.emptyIfNull(NULL),  As.emptyIfNull(DEV));
        final List<String> d14 = ImmutableList.of(As.emptyIfNull(DIS),   As.emptyIfNull(EMPTY), As.emptyIfNull(NULL));
        final List<String> d15 = ImmutableList.of(As.emptyIfNull(DIS),   As.emptyIfNull(EMPTY), As.emptyIfNull(EMPTY));
        final List<String> d16 = ImmutableList.of(As.emptyIfNull(DIS),   As.emptyIfNull(EMPTY), As.emptyIfNull(DEV));
        final List<String> d17 = ImmutableList.of(As.emptyIfNull(DIS),   As.emptyIfNull(CAT),   As.emptyIfNull(NULL));
        final List<String> d18 = ImmutableList.of(As.emptyIfNull(DIS),   As.emptyIfNull(CAT),   As.emptyIfNull(EMPTY));
        final List<String> d19 = ImmutableList.of(As.emptyIfNull(DIS),   As.emptyIfNull(CAT),   As.emptyIfNull(DEV));

        final List<String> d21 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(CAT),   As.emptyIfNull(NULL));
        final List<String> d22 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(CAT),   As.emptyIfNull(EMPTY));
        final List<String> d23 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(CAT),   As.emptyIfNull(DEV));
        final List<String> d24 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(CAT),   As.emptyIfNull(NULL));
        final List<String> d25 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(CAT),   As.emptyIfNull(EMPTY));
        final List<String> d26 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(CAT),   As.emptyIfNull(DEV));

        final List<String> d31 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(NULL),  As.emptyIfNull(DEV));
        final List<String> d32 = ImmutableList.of(As.emptyIfNull(NULL),  As.emptyIfNull(EMPTY), As.emptyIfNull(DEV));
        final List<String> d34 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(NULL),  As.emptyIfNull(DEV));
        final List<String> d35 = ImmutableList.of(As.emptyIfNull(EMPTY), As.emptyIfNull(EMPTY), As.emptyIfNull(DEV));

        assertEquals(NULL,      namingConvention.conventionName(d11,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d12,   NameType.DEVICE_STRUCTURE));
        assertEquals(DIS_DEV,   namingConvention.conventionName(d13,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d14,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d15,   NameType.DEVICE_STRUCTURE));
        assertEquals(DIS_DEV,   namingConvention.conventionName(d16,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d17,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d18,   NameType.DEVICE_STRUCTURE));
        assertEquals(DIS_DEV,   namingConvention.conventionName(d19,   NameType.DEVICE_STRUCTURE));

        assertEquals(NULL,      namingConvention.conventionName(d21,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d22,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d23,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d24,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d25,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d26,   NameType.DEVICE_STRUCTURE));

        assertEquals(NULL,      namingConvention.conventionName(d31,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d32,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d34,   NameType.DEVICE_STRUCTURE));
        assertEquals(NULL,      namingConvention.conventionName(d35,   NameType.DEVICE_STRUCTURE));
    }

    // ----------------------------------------------------------------------------------------------------
    // name in relation to other
    //     canMnemonicsCoexist
    //         system structure
    //         device structure
    //         system structure & device structure
    // ----------------------------------------------------------------------------------------------------

    /**
     * Test if system group can coexist if equal.
     */
    @Test
    public void canSystemGroupCoexistIfEqual(){
        assertFalse("system group and system group cannot coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(SUP),                          NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(SUP),                          NameType.SYSTEM_STRUCTURE));
        assertTrue("system group and system group can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(EMPTY),                        NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(EMPTY),                        NameType.SYSTEM_STRUCTURE));
        assertTrue("system group and system can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(SUP),                          NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(EMPTY, SUP),                   NameType.SYSTEM_STRUCTURE));
        assertTrue("system group and system can coexist in a parent child relation",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(SUP),                          NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(SUP,   EMPTY),                 NameType.SYSTEM_STRUCTURE));
        assertFalse("system group and subsystem cannot coexist in a parent child relation",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(SUP),                          NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(SUP,   EMPTY, SUP),            NameType.SYSTEM_STRUCTURE));

        assertTrue("system and system can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(SUP,   SYS),                   NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(SUP,   EMPTY, SYS),            NameType.SYSTEM_STRUCTURE));
        assertTrue("system and system can coexist 1",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("Su1", SYS),                   NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of("Su2", SYS),                   NameType.SYSTEM_STRUCTURE));
        assertTrue("system and system can coexist 2",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(EMPTY, SYS),                   NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(SUP,   SYS),                   NameType.SYSTEM_STRUCTURE));

        assertTrue("subsystem and subsystem can coexist 1",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(EMPTY, SYS,   SUB),            NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(SUP,   SYS,   SUB),            NameType.SYSTEM_STRUCTURE));
        assertTrue("subsystem and subsystem can coexist 2",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("Su1", SYS,   SUB),            NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of("Su2", SYS,   SUB),            NameType.SYSTEM_STRUCTURE));
    }

    /**
     * Test if discipline can coexist if equal.
     */
    @Test
    public void canDisciplineCoexistIfEqual(){
        assertFalse("discipline and discipline cannot coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(DIS),                          NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(DIS),                          NameType.DEVICE_STRUCTURE));
        assertTrue("discipline and device group can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(DIS),                          NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(DIS,     DIS),                 NameType.DEVICE_STRUCTURE));
        assertFalse("discipline and device type cannot coexist in a parent child relation",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(DIS),                          NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(DIS,     EMPTY, DIS),          NameType.DEVICE_STRUCTURE));
        assertTrue("discipline and device type can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(DIS),                          NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(OTHER,   EMPTY, DIS),          NameType.DEVICE_STRUCTURE));
        assertTrue("discipline and system group can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(DIS),                          NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(DIS),                          NameType.SYSTEM_STRUCTURE));
        assertTrue("discipline and system can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(DIS),                          NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(EMPTY,   DIS),                 NameType.SYSTEM_STRUCTURE));
        assertTrue("discipline and subsystem can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(DIS),                          NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(EMPTY,   SYS,   DIS),          NameType.SYSTEM_STRUCTURE));
    }

    /**
     * Test if discipline can coexist if similar.
     */
    @Test
    public void canDisciplineCoexistIfSimilar(){
        assertFalse("discipline and discipline cannot coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("D1s"),                        NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(DIS),                          NameType.DEVICE_STRUCTURE));
        assertTrue("discipline and device group can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("D1s"),                        NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(DIS,     DIS),                 NameType.DEVICE_STRUCTURE));
        assertFalse("discipline and device type cannot coexist in a parent child relation",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("D1s"),                        NameType.DEVICE_STRUCTURE,
                        ImmutableList.of("D1s",   EMPTY, DIS),          NameType.DEVICE_STRUCTURE));
        assertTrue("discipline and device type can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("D1s"),                        NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(OTHER,   EMPTY, DIS),          NameType.DEVICE_STRUCTURE));
        assertTrue("discipline and system group can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("D1s"),                        NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(DIS),                          NameType.SYSTEM_STRUCTURE));
        assertTrue("discipline and system can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("D1s"),                        NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(EMPTY,   DIS),                 NameType.SYSTEM_STRUCTURE));
        assertTrue("discipline and subsystem can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("D1s"),                        NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(EMPTY,   SYS,   DIS),          NameType.SYSTEM_STRUCTURE));
    }

    /**
     * Test if first level can coexist if equal.
     */
    @Test
    public void canFirstLevelCoexistIfEqual() {
        assertTrue("system group and system can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(SYS),                          NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(SUP, SYS),                     NameType.SYSTEM_STRUCTURE));
    }

    /**
     * Test if systems can coexist.
     */
    @Test
    public void canSystemsCoexist() {
        final List<String> system1Path = ImmutableList.of(EMPTY, "SysW");
        final List<String> system2Path = ImmutableList.of(EMPTY, "SysW");

        assertFalse("system and system cannot coexist",
                namingConvention.canMnemonicsCoexist(
                        system1Path,                                    NameType.SYSTEM_STRUCTURE,
                        system2Path,                                    NameType.SYSTEM_STRUCTURE));
        assertFalse("system and subsystem cannot coexist in a parent child relation",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(EMPTY, SYS),                   NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(EMPTY, SYS,     SYS),          NameType.SYSTEM_STRUCTURE));
        assertTrue("system and subsystem can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(EMPTY, SYS),                   NameType.SYSTEM_STRUCTURE,
                        ImmutableList.of(EMPTY, OTHER, SYS),            NameType.SYSTEM_STRUCTURE));
        assertTrue("device group and system can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of(DIS,   SYS),                   NameType.DEVICE_STRUCTURE,
                        ImmutableList.of(SUP,   SYS),                   NameType.SYSTEM_STRUCTURE));
    }

    /**
     * Test if device types can coexist.
     */
    @Test
    public void canDeviceTypesCoexist() {
        assertTrue("device type and subsystem can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("BMD", "Chop",  CHOP_G),      NameType.DEVICE_STRUCTURE,
                        ImmutableList.of("Acc", "HBL",   CHOP_G),      NameType.SYSTEM_STRUCTURE));
        assertTrue("two device types can coexist under different disicplines",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("BMD", EMPTY,   CHOP_G),      NameType.DEVICE_STRUCTURE,
                        ImmutableList.of("BML", EMPTY,   CHOP_G),      NameType.DEVICE_STRUCTURE));
        assertTrue("device type and device group can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("BMD", "Chop",  CHOP_G),      NameType.DEVICE_STRUCTURE,
                        ImmutableList.of("BMD", CHOP_G),               NameType.DEVICE_STRUCTURE));
        assertTrue("device groups can coexist",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("BMD", "Chop"),                NameType.DEVICE_STRUCTURE,
                        ImmutableList.of("BMD", "Chop"),                NameType.DEVICE_STRUCTURE));
        assertFalse("two device types cannot coexist under the same discipline",
                namingConvention.canMnemonicsCoexist(
                        ImmutableList.of("BMD", "Chop",  CHOP_G),      NameType.DEVICE_STRUCTURE,
                        ImmutableList.of("BMD", EMPTY,   CHOP_G),      NameType.DEVICE_STRUCTURE));
    }

    // ----------------------------------------------------------------------------------------------------
    // name in relation to other
    //     canMnemonicsCoexist (2)
    //         system structure
    //         device structure
    //         system structure & device structure
    // ----------------------------------------------------------------------------------------------------

    /**
     * Test if system structures can coexist.
     */
    @Test
    public void canSystemStructuresCoexist() {
        // unique within parent child relation in structure (system / device)

        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(EMPTY,  A),                            NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(EMPTY,  B),                            NameType.SYSTEM_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, A),                                 NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(A, B),                                 NameType.SYSTEM_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(EMPTY, A, A),                          NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(EMPTY, A, B),                          NameType.SYSTEM_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(EMPTY, A, A),                          NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(EMPTY, B, A),                          NameType.SYSTEM_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, A, A),                              NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(A, A, B),                              NameType.SYSTEM_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, A, A),                              NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(A, B, A),                              NameType.SYSTEM_STRUCTURE));
        // equivalence class
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, A, "D1s"),                          NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(A, A, DIS),                            NameType.SYSTEM_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, B, "D1s"),                          NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(A, B, DIS),                            NameType.SYSTEM_STRUCTURE));
    }

    /**
     * Test if device structures can coexist.
     */
    @Test
    public void canDeviceStructuresCoexist() {
        // unique within parent child relation in structure (system / device)

        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, EMPTY),                             NameType.DEVICE_STRUCTURE,
                ImmutableList.of(B, EMPTY),                             NameType.DEVICE_STRUCTURE));
        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, A),                                 NameType.DEVICE_STRUCTURE,
                ImmutableList.of(A, B),                                 NameType.DEVICE_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, EMPTY,  A),                         NameType.DEVICE_STRUCTURE,
                ImmutableList.of(A, EMPTY,  B),                         NameType.DEVICE_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, EMPTY,  A),                         NameType.DEVICE_STRUCTURE,
                ImmutableList.of(B, EMPTY,  A),                         NameType.DEVICE_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, A, A),                              NameType.DEVICE_STRUCTURE,
                ImmutableList.of(A, A, B),                              NameType.DEVICE_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, A, A),                              NameType.DEVICE_STRUCTURE,
                ImmutableList.of(A, B, A),                              NameType.DEVICE_STRUCTURE));
        assertFalse(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A, A, "D1s"),                          NameType.DEVICE_STRUCTURE,
                ImmutableList.of(A, A, DIS),                            NameType.DEVICE_STRUCTURE));
    }

    /**
     * Test if system structures and device structures can coexist.
     */
    @Test
    public void canSystemStructuresDeviceStructuresCoexist() {
        // not consider system structure vs device structure
        // unique within parent child relation in structure (system / device)

        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(EMPTY,  A,         A),                 NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(A,      A,         A),                 NameType.DEVICE_STRUCTURE));
        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A,      A,         A),                 NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(A,      A,         A),                 NameType.DEVICE_STRUCTURE));
        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(EMPTY,  A,         A),                 NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(A,      EMPTY,     A),                 NameType.DEVICE_STRUCTURE));
        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(A,      A,         A),                 NameType.SYSTEM_STRUCTURE,
                ImmutableList.of(A,      EMPTY,     A),                 NameType.DEVICE_STRUCTURE));

        // --------------------------------------------------------------------------------
        // example
        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(EMPTY,  "Timing"),                     NameType.SYSTEM_STRUCTURE,
                ImmutableList.of("Cryo", EMPTY,     TDS),               NameType.DEVICE_STRUCTURE));
        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(EMPTY,  "Timing",  TD180),             NameType.SYSTEM_STRUCTURE,
                ImmutableList.of("Cryo", EMPTY,     TDS),               NameType.DEVICE_STRUCTURE));

        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(EMPTY,  TDS),                          NameType.SYSTEM_STRUCTURE,
                ImmutableList.of("Cryo", EMPTY,     TDS),               NameType.DEVICE_STRUCTURE));
        assertTrue(namingConvention.canMnemonicsCoexist(
                ImmutableList.of(EMPTY,  TDS,       TD180),             NameType.SYSTEM_STRUCTURE,
                ImmutableList.of("Cryo", EMPTY,     TDS),               NameType.DEVICE_STRUCTURE));
    }

}
