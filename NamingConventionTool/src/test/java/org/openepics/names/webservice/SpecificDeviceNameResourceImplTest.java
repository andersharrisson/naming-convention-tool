/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.webservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Map;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.UtilityBusinessTestFixture2;
import org.openepics.names.jaxb.DeviceNameElement;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.NameViews;
import org.openepics.names.nameviews.NameViewProvider.NameRevisions;
import org.openepics.names.services.EssNamingConvention;
import org.powermock.reflect.Whitebox;

/**
 * Unit tests for SpecificDeviceNameResourceImpl.
 *
 * <br><br>
 * Note
 * <ul>
 * <li> JUnit integration test
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see SpecificDeviceNameResourceImpl
 * @see NameViewProvider
 * @see NameViews
 * @see NameRevisions
 * @see UtilityBusinessTestFixture2
 */
public class SpecificDeviceNameResourceImplTest {

    /*
       Purpose to test SpecificDeviceNameResource part of REST API.

       In order to be able to test, NameViewProvider and associated parts of NameViews and NameRevisions to be set up,
       after which REST API test may proceed.

       Note
           test fixture is handled by utility class
           different values available for status for DeviceNameElement
           name vs. equivalence class representative for name
    */

    private static final String RFQ_010_EMR_TT_017 = "RFQ-010:EMR-TT-017";

    private static UtilityBusinessTestFixture2 testFixture;

    private static NameViewProvider nameViewProvider;
    private static Map<UUID, NameView> nameViewMap;

    private static SpecificDeviceNameResourceImpl specificDeviceNameResourceImpl;

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        testFixture = UtilityBusinessTestFixture2.getInstance();
        testFixture.setUp();

        setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        tearDown();

        testFixture.tearDown();
        testFixture = null;
    }

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @SuppressWarnings("unchecked")
    private static void setUp() throws Exception {
        nameViewProvider = new NameViewProvider();

        Whitebox.setInternalState(nameViewProvider, "namingConvention", new EssNamingConvention());
        Whitebox.setInternalState(nameViewProvider, "nameViews", new NameViews());
        Whitebox.setInternalState(nameViewProvider, "nameRevisions", nameViewProvider.new NameRevisions());

        nameViewMap = (Map<UUID, NameView>) Whitebox.getField(nameViewProvider.getNameViews().getClass(), "nameViewMap")
                .get(nameViewProvider.getNameViews());

        specificDeviceNameResourceImpl = new SpecificDeviceNameResourceImpl();
        Whitebox.setInternalState(specificDeviceNameResourceImpl, "nameViewProvider", nameViewProvider);

        // update name view provider with test fixture content

        for (NameRevision revision : testFixture.getNameRevisions()) {
            nameViewProvider.getNameViews().update(revision);

            // update nameRevisionMap with nameRevision in NameRevisions in NameViewProvider
            Whitebox.invokeMethod(nameViewProvider.getNameRevisions(), "update", revision);
        }
    }

    /**
     * One-time cleanup code.
     */
    private static void tearDown() {
        specificDeviceNameResourceImpl = null;

        if (nameViewMap != null) {
            nameViewMap.clear();
            nameViewMap = null;
        }

        nameViewProvider = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test content after setUp.
     */
    @Test
    public void setUpOk() {
        // check content after setUp
        assertNotNull(nameViewProvider);

        // check content of NameViews - system root
        assertEquals(14, nameViewProvider.getNameViews().getSystemRoot().getAllChildren().size());

        // check content of NameViews - device root
        assertEquals(16, nameViewProvider.getNameViews().getDeviceRoot().getAllChildren().size());

        // check content of NameViews - nameViewMap
        assertEquals(19, nameViewMap.size());

        // check content of NameRevisions - nameRevisionMap
        //     conventionName
        //     conventionNameEqClass
        //     (uuid)
        assertEquals(22, nameViewProvider.getNameRevisions().keySet().size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameNull() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName(null);

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameEmpty() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName("");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameSpace() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName(" ");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameUnknown() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName("device");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameConventionName() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName(RFQ_010_EMR_TT_017);

        assertNotNull(deviceNameElement);
        assertEquals(RFQ_010_EMR_TT_017, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture2.UUID_DEVICE_12706_INSTANCEINDEX,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameConventionNameLowerCase() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName("rfq-010:emr-tt-017");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameConventionNameEquivalenceClassRepresentative() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName("RFQ-10:EMR-TT-17");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameConventionNameEquivalenceClassRepresentativeLowerCase() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName("rfq-10:emr-tt-17");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameUuid() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceName(
                        UtilityBusinessTestFixture2.UUID_DEVICE_12706_INSTANCEINDEX);

        assertNotNull(deviceNameElement);
        assertEquals(RFQ_010_EMR_TT_017, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture2.UUID_DEVICE_12706_INSTANCEINDEX,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElement() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceNameElement(null);

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementEmpty() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceNameElement("");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementSpace() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceNameElement(" ");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementUnknown() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceNameElement("device");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementConventionName() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceNameElement(RFQ_010_EMR_TT_017);

        assertNotNull(deviceNameElement);
        assertEquals(RFQ_010_EMR_TT_017, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture2.UUID_DEVICE_12706_INSTANCEINDEX,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementConventionNameLowerCase() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceNameElement("rfq-010:emr-tt-017");

        assertNotNull(deviceNameElement);
        assertEquals(RFQ_010_EMR_TT_017, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture2.UUID_DEVICE_12706_INSTANCEINDEX,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementConventionNameEquivalenceClassRepresentative() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceNameElement("RFQ-10:EMR-TT-17");

        assertNotNull(deviceNameElement);
        assertEquals(RFQ_010_EMR_TT_017, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture2.UUID_DEVICE_12706_INSTANCEINDEX,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementConventionNameEquivalenceClassRepresentativeLowerCase() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceNameElement("rfq-10:emr-tt-17");

        assertNotNull(deviceNameElement);
        assertEquals(RFQ_010_EMR_TT_017, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture2.UUID_DEVICE_12706_INSTANCEINDEX,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementUuid() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceNameElement(
                        UtilityBusinessTestFixture2.UUID_DEVICE_12706_INSTANCEINDEX);

        assertNotNull(deviceNameElement);
        assertEquals(RFQ_010_EMR_TT_017, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture2.UUID_DEVICE_12706_INSTANCEINDEX,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

}
