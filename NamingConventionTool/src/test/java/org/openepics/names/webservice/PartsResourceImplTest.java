/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.webservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.UtilityBusinessTestFixture2;
import org.openepics.names.jaxb.PartElement;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.NameViews;
import org.openepics.names.nameviews.NameViewProvider.NameRevisions;
import org.openepics.names.services.EssNamingConvention;
import org.powermock.reflect.Whitebox;

/**
 * Unit tests for PartsResourceImpl.
 *
 * <br><br>
 * Note
 * <ul>
 * <li> JUnit integration test
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see PartsResourceImpl
 * @see NameViewProvider
 * @see NameViews
 * @see NameRevisions
 * @see UtilityBusinessTestFixture2
 */
public class PartsResourceImplTest {

    /*
       Purpose to test PartsResource part of REST API.

       In order to be able to test, NameViewProvider and associated parts of NameViews and NameRevisions to be set up,
       after which REST API test may proceed.

       Note
           test fixture is handled by utility class
           different values available for status for PartElement
           name vs. equivalence class representative for name
    */

    private static final String APPROVED = "Approved";

    private static final String DEVICE_STRUCTURE = "Device Structure";

    private static final String EMR = "EMR";
    private static final String RFQ = "RFQ";

    private static UtilityBusinessTestFixture2 testFixture;

    private static NameViewProvider nameViewProvider;
    private static Map<UUID, NameView> nameViewMap;

    private static PartsResourceImpl partsResourceImpl;

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        testFixture = UtilityBusinessTestFixture2.getInstance();
        testFixture.setUp();

        setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        tearDown();

        testFixture.tearDown();
        testFixture = null;
    }

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @SuppressWarnings("unchecked")
    private static void setUp() throws Exception {
        nameViewProvider = new NameViewProvider();

        Whitebox.setInternalState(nameViewProvider, "namingConvention", new EssNamingConvention());
        Whitebox.setInternalState(nameViewProvider, "nameViews", new NameViews());
        Whitebox.setInternalState(nameViewProvider, "nameRevisions", nameViewProvider.new NameRevisions());

        nameViewMap = (Map<UUID, NameView>) Whitebox.getField(nameViewProvider.getNameViews().getClass(), "nameViewMap")
                .get(nameViewProvider.getNameViews());

        partsResourceImpl = new PartsResourceImpl();
        Whitebox.setInternalState(partsResourceImpl, "nameViewProvider", nameViewProvider);

        // update name view provider with test fixture content

        for (NameRevision revision : testFixture.getNameRevisions()) {
            nameViewProvider.getNameViews().update(revision);

            // update nameRevisionMap with nameRevision in NameRevisions in NameViewProvider
            Whitebox.invokeMethod(nameViewProvider.getNameRevisions(), "update", revision);
        }
    }

    /**
     * One-time cleanup code.
     */
    private static void tearDown() {
        partsResourceImpl = null;

        if (nameViewMap != null) {
            nameViewMap.clear();
            nameViewMap = null;
        }


        nameViewProvider = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test content after setUp.
     */
    @Test
    public void setUpOk() {
        // check content after setUp

        // check content of NameViews - system root
        assertEquals(14, nameViewProvider.getNameViews().getSystemRoot().getAllChildren().size());

        // check content of NameViews - device root
        assertEquals(16, nameViewProvider.getNameViews().getDeviceRoot().getAllChildren().size());

        // check content of NameViews - nameViewMap
        assertEquals(19, nameViewMap.size());

        // check content of NameRevisions - nameRevisionMap
        //     conventionName
        //     conventionNameEqClass
        //     (uuid)
        assertEquals(22, nameViewProvider.getNameRevisions().keySet().size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicNull() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic(null);

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicEmpty() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("");

        // 8 name parts (uuid) in test fixture

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicUnknown() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("getAll");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSystemRFQ() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic(RFQ);

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementRFQ(partElement);
    }

    /**
     * Test case sensitivity for getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSystemRFQLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic(RFQ.toLowerCase());

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSubsystem010() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("010");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElement010(partElement);
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDisciplineEMR() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic(EMR);

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementEMR(partElement);
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDeviceTypeTT() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("TT");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementTT(partElement);
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDeviceTypeTT2() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("TT2");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementTT2(partElement);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchPatternSyntaxException() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("\\");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllPartsByMnemonicSearchNull() {
        partsResourceImpl.getAllPartsByMnemonicSearch(null);
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchEmpty() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("");

        // 8 name parts (uuid) in test fixture

        assertNotNull(partElements);
        assertEquals(8, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchUnknown() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("search");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchR() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("R");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());

        for (PartElement partElement : partElements) {
            switch (partElement.getUuid().toString()) {
                case UtilityBusinessTestFixture2.UUID_NAMEPART_7_SYSTEM_RFQ:
                    assertPartElementRFQ(partElement);
                    break;
                case UtilityBusinessTestFixture2.UUID_NAMEPART_239_DISCIPLINE_EMR:
                    assertPartElementEMR(partElement);
                    break;
                default:
                    fail();
            }
        }
    }

    /**
     * Test case sensitivity for getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchRLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("R".toLowerCase());

        assertNotNull(partElements);
        assertEquals(0, partElements.size());

    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchT() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("T");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());

        for (PartElement partElement : partElements) {
            switch (partElement.getUuid().toString()) {
                case UtilityBusinessTestFixture2.UUID_NAMEPART_1640_DEVICETYPE_TT2:
                    assertPartElementTT2(partElement);
                    break;
                case UtilityBusinessTestFixture2.UUID_NAMEPART_2016_DEVICETYPE_TT:
                    assertPartElementTT(partElement);
                    break;
                default:
                    fail();
            }
        }
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch0() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("0");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElement010(partElement);
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch1() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("1");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElement010(partElement);
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch01() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("01");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElement010(partElement);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPatternSyntaxException() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("\\");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllPartsByMnemonicPathSearchNull() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch(null);

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchEmpty() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("");

        assertNotNull(partElements);
        assertEquals(8, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchUnknown() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("RFQ-010:EMR-TT-001");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    // --------------------

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchAcc() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Acc");

        assertNotNull(partElements);
        assertEquals(3, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchAccRFQ() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Acc-RFQ");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchAccRFQ010() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Acc-RFQ-010");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchRFQ() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("RFQ");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchRFQ010() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("RFQ-010");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchRFQLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("rfq");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchRFQStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("^RFQ");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchRFQEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("RFQ$");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearch010() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("010");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    // --------------------

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchEMR() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("EMR");

        assertNotNull(partElements);
        assertEquals(5, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchEMRTT() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("EMR-TT");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchEMRTT2() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("EMR-TT2");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchEMRStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("^EMR");

        assertNotNull(partElements);
        assertEquals(5, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchEMREndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("EMR$");

        assertNotNull(partElements);
        assertEquals(3, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchTT() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("TT");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchTT2() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("TT2");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    // --------------------

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchColonMark() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch(":");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchMinus() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("-");

        assertNotNull(partElements);
        assertEquals(4, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchMinusStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("^-");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchMinusEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("-$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to check PartElement for particular content, RFQ.
     */
    private void assertPartElementRFQ(PartElement partElement) {
        assertNotNull(partElement);
        assertEquals("2", partElement.getLevel());
        assertEquals(RFQ, partElement.getMnemonic());
        assertEquals("Acc-RFQ", partElement.getMnemonicPath());
        assertEquals("Radio Frequency Quadrupole", partElement.getName());
        assertEquals("Accelerator-Radio Frequency Quadrupole", partElement.getNamePath());
        assertEquals(APPROVED, partElement.getStatus());
        assertEquals("System Structure", partElement.getType());
        assertEquals(UtilityBusinessTestFixture2.UUID_NAMEPART_7_SYSTEM_RFQ, partElement.getUuid().toString());
    }

    /**
     * Utility method to check PartElement for particular content, 010.
     */
    private void assertPartElement010(PartElement partElement) {
        assertNotNull(partElement);
        assertEquals("3", partElement.getLevel());
        assertEquals("010", partElement.getMnemonic());
        assertEquals("Acc-RFQ-010", partElement.getMnemonicPath());
        assertEquals("RFQ-010", partElement.getName());
        assertEquals("Accelerator-Radio Frequency Quadrupole-RFQ-010", partElement.getNamePath());
        assertEquals(APPROVED, partElement.getStatus());
        assertEquals("System Structure", partElement.getType());
        assertEquals(UtilityBusinessTestFixture2.UUID_NAMEPART_178_SUBSYSSTEM_010, partElement.getUuid().toString());
    }

    /**
     * Utility method to check PartElement for particular content, EMR.
     */
    private void assertPartElementEMR(PartElement partElement) {
        assertNotNull(partElement);
        assertEquals("1", partElement.getLevel());
        assertEquals(EMR, partElement.getMnemonic());
        assertEquals(EMR, partElement.getMnemonicPath());
        assertEquals(UtilityBusinessTestFixture2.ELECTROMAGNETIC_RESONATORS, partElement.getName());
        assertEquals(UtilityBusinessTestFixture2.ELECTROMAGNETIC_RESONATORS, partElement.getNamePath());
        assertEquals(APPROVED, partElement.getStatus());
        assertEquals(DEVICE_STRUCTURE, partElement.getType());
        assertEquals(UtilityBusinessTestFixture2.UUID_NAMEPART_239_DISCIPLINE_EMR, partElement.getUuid().toString());
    }

    /**
     * Utility method to check PartElement for particular content, TT.
     */
    private void assertPartElementTT(PartElement partElement) {
        assertNotNull(partElement);
        assertEquals("3", partElement.getLevel());
        assertEquals("TT", partElement.getMnemonic());
        assertEquals("EMR-TT", partElement.getMnemonicPath());
        assertEquals("Temperature Transmitter", partElement.getName());
        assertEquals("Electromagnetic Resonators-Control-Temperature Transmitter", partElement.getNamePath());
        assertEquals(APPROVED, partElement.getStatus());
        assertEquals(DEVICE_STRUCTURE, partElement.getType());
        assertEquals(UtilityBusinessTestFixture2.UUID_NAMEPART_2016_DEVICETYPE_TT, partElement.getUuid().toString());
    }

    /**
     * Utility method to check PartElement for particular content, TT2.
     */
    private void assertPartElementTT2(PartElement partElement) {
        assertNotNull(partElement);
        assertEquals("3", partElement.getLevel());
        assertEquals("TT2", partElement.getMnemonic());
        assertEquals("EMR-TT2", partElement.getMnemonicPath());
        assertEquals("Electromagnetic Resonators", partElement.getName());
        assertEquals("Electromagnetic Resonators-Misc-Electromagnetic Resonators", partElement.getNamePath());
        assertEquals(APPROVED, partElement.getStatus());
        assertEquals(DEVICE_STRUCTURE, partElement.getType());
        assertEquals(UtilityBusinessTestFixture2.UUID_NAMEPART_1640_DEVICETYPE_TT2, partElement.getUuid().toString());
    }

}
