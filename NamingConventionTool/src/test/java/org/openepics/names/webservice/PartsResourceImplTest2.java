/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.webservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.UtilityBusinessTestFixture3;
import org.openepics.names.jaxb.PartElement;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.NameViews;
import org.openepics.names.nameviews.NameViewProvider.NameRevisions;
import org.openepics.names.services.EssNamingConvention;
import org.powermock.reflect.Whitebox;

/**
 * Unit tests for PartsResourceImpl.
 *
 * <br><br>
 * Note
 * <ul>
 * <li> JUnit integration test
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see PartsResourceImpl
 * @see NameViewProvider
 * @see NameViews
 * @see NameRevisions
 * @see UtilityBusinessTestFixture3
 */
public class PartsResourceImplTest2 {

    /*
       Purpose to test PartsResource part of REST API.

       In order to be able to test, NameViewProvider and associated parts of NameViews and NameRevisions to be set up,
       after which REST API test may proceed.

       Note
           test fixture is handled by utility class
           different values available for status for PartElement
           name vs. equivalence class representative for name
    */

    private static UtilityBusinessTestFixture3 testFixture;

    private static NameViewProvider nameViewProvider;
    private static Map<UUID, NameView> nameViewMap;

    private static PartsResourceImpl partsResourceImpl;

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        testFixture = UtilityBusinessTestFixture3.getInstance();
        testFixture.setUp();

        setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        tearDown();

        testFixture.tearDown();
        testFixture = null;
    }

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @SuppressWarnings("unchecked")
    private static void setUp() throws Exception {
        nameViewProvider = new NameViewProvider();

        Whitebox.setInternalState(nameViewProvider, "namingConvention", new EssNamingConvention());
        Whitebox.setInternalState(nameViewProvider, "nameViews", new NameViews());
        Whitebox.setInternalState(nameViewProvider, "nameRevisions", nameViewProvider.new NameRevisions());

        nameViewMap = (Map<UUID, NameView>) Whitebox.getField(nameViewProvider.getNameViews().getClass(), "nameViewMap")
                .get(nameViewProvider.getNameViews());

        partsResourceImpl = new PartsResourceImpl();
        Whitebox.setInternalState(partsResourceImpl, "nameViewProvider", nameViewProvider);

        // update name view provider with test fixture content

        for (NameRevision revision : testFixture.getNameRevisions()) {
            nameViewProvider.getNameViews().update(revision);

            // update nameRevisionMap with nameRevision in NameRevisions in NameViewProvider
            Whitebox.invokeMethod(nameViewProvider.getNameRevisions(), "update", revision);
        }
    }

    /**
     * One-time cleanup code.
     */
    private static void tearDown() {
        partsResourceImpl = null;

        if (nameViewMap != null) {
            nameViewMap.clear();
            nameViewMap = null;
        }


        nameViewProvider = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test content after setUp.
     */
    @Test
    public void setUpOk() {
        // check content after setUp

        // check content of NameViews - system root
        assertEquals(17, nameViewProvider.getNameViews().getSystemRoot().getAllChildren().size());

        // check content of NameViews - device root
        assertEquals(33, nameViewProvider.getNameViews().getDeviceRoot().getAllChildren().size());

        // check content of NameViews - nameViewMap
        assertEquals(39, nameViewMap.size());

        // check content of NameRevisions - nameRevisionMap
        //     conventionName
        //     conventionNameEqClass
        //     (uuid)
        assertEquals(68, nameViewProvider.getNameRevisions().keySet().size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicNull() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic(null);

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicEmpty() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("");

        // 8 name parts (uuid) in test fixture

        assertNotNull(partElements);
        assertEquals(8, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSpace() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic(" ");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicUnknown() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("getAll");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSystemPBI() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("PBI");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSystemPBILowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("pbi");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSystemPB1() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("PB1");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSystemPB1LowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("pb1");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSubsystemFPM01() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("FPM01");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementFPM01(partElement);
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSubsystemFPM01LowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("fpm01");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSubsystemFPM1() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("FPM1");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSubsystemFPM1LowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("fpm1");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDisciplineCtrl() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("Ctrl");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementCtrl(partElement);
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDisciplineCtrlUpperCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("CTRL");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDisciplineCtrlLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("ctrl");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDisciplineCTR1() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("CTR1");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDisciplineCTR1LowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("ctr1");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDeviceTypeECATIO() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("ECATIO");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDeviceTypeECATIOLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("ecatio");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDeviceTypeECAT10() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("ECAT10");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonic method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicDeviceTypeECAT10LowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonic("ecat10");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchPatternSyntaxException() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("\\");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllPartsByMnemonicSearchNull() {
        partsResourceImpl.getAllPartsByMnemonicSearch(null);
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchEmpty() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("");

        // 28 name parts (uuid) in test fixture

        assertNotNull(partElements);
        assertEquals(28, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchSpace() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch(" ");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchUnknown() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("search");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchC() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("C");

        assertNotNull(partElements);
        assertEquals(10, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchCStartsWith() {
        // note regex

        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^C");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchCEndsWith() {
        // note regex

        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("C$");

        assertNotNull(partElements);
        assertEquals(4, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchCLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("c");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchCLowerCaseStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^c");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchCLowerCaseEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("c$");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchE() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("E");

        assertNotNull(partElements);
        assertEquals(11, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchEStartsWith() {
        // note regex

        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^E");

        assertNotNull(partElements);
        assertEquals(10, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchEEndsWith() {
        // note regex

        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("E$");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchELowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("e");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchELowerCaseStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^e");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchELowerCaseEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("e$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchI() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("I");

        assertNotNull(partElements);
        assertEquals(6, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchIStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^I");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchIEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("I$");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchILowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("i");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchILowerCaseStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^i");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchILowerCaseEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("i$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchM() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("M");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementFPM01(partElement);
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchMStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^M");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchMEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("M$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchMLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("m");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchMLowerCaseStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^m");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchMLowerCaseEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("m$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchP() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("P");

        assertNotNull(partElements);
        assertEquals(5, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchPStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^P");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchPEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("P$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchPLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("p");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchPLowerCaseStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^p");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchPLowerCaseEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("p$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchTR() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("TR");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchTRStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^TR");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchTREndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("TR$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchTRLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("tr");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementCtrl(partElement);
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchTRLowerCaseStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^tr");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearchTRLowerCaseEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("tr$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch0() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("0");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch1() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("1");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementFPM01(partElement);
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch01() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("01");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementFPM01(partElement);
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch01StartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^01");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch01EndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("01$");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());

        PartElement partElement = partElements.get(0);

        assertPartElementFPM01(partElement);
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch10() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("10");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch10StartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("^10");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicSearch10EndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicSearch("10$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPatternSyntaxException() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("\\");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllPartsByMnemonicPathSearchNull() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch(null);

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchEmpty() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("");

        assertNotNull(partElements);
        assertEquals(28, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchUnknown() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("PartsResourceImplTest");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    // --------------------

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchAcc() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Acc");

        assertNotNull(partElements);
        assertEquals(3, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchAccFEB() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Acc-FEB");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchAccFEB030Row() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Acc-FEB-030Row");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchFEB() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("FEB");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchFEB030Row() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("FEB-030Row");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearch030Row() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("030Row");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBI() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("PBI");

        assertNotNull(partElements);
        assertEquals(9, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBIFPM01() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("PBI-FPM01");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBILowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("pbi");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBIStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("^PBI");

        assertNotNull(partElements);
        assertEquals(9, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBIEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("PBI$");

        assertNotNull(partElements);
        assertEquals(3, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchFPM01() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("FPM01");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    // --------------------

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchCtrl() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Ctrl");

        assertNotNull(partElements);
        assertEquals(12, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchCtrlECAT() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Ctrl-ECAT");

        assertNotNull(partElements);
        assertEquals(4, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchCtrlECATC() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Ctrl-ECATC");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchCtrlECATE() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Ctrl-ECATE");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchCtrlEVR() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Ctrl-EVR");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchCtrlECATIO() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("Ctrl-ECATIO");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchECAT() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("ECAT");

        assertNotNull(partElements);
        assertEquals(7, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchECATLowerCase() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("ecat");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchECATStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("^ECAT");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchECATEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("ECAT$");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchECATC() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("ECATC");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchECATE() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("ECATE");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchECATIO() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("ECATIO");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchEVR() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("EVR");

        assertNotNull(partElements);
        assertEquals(3, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchIPC() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("IPC");

        assertNotNull(partElements);
        assertEquals(2, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBIECAT() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("PBI-ECAT");

        assertNotNull(partElements);
        assertEquals(3, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBIECATC() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("PBI-ECATC");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBIECATIO() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("PBI-ECATIO");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBIEVR() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("PBI-EVR");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchPBIIPC() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("PBI-IPC");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchTS() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("TS");

        assertNotNull(partElements);
        assertEquals(3, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchTSEVR() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("TS-EVR");

        assertNotNull(partElements);
        assertEquals(1, partElements.size());
    }

    // --------------------

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchColonMark() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch(":");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchMinus() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("-");

        assertNotNull(partElements);
        assertEquals(15, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchMinusStartsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("^-");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    /**
     * Test getAllPartsByMnemonicPathSearch method of PartsResourceImpl.
     */
    @Test
    public void getAllPartsByMnemonicPathSearchMinusEndsWith() {
        List<PartElement> partElements = partsResourceImpl.getAllPartsByMnemonicPathSearch("-$");

        assertNotNull(partElements);
        assertEquals(0, partElements.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to check PartElement for particular content, Ctrl.
     */
    private void assertPartElementCtrl(PartElement partElement) {
        assertNotNull(partElement);
        assertEquals("1", partElement.getLevel());
        assertEquals("Ctrl", partElement.getMnemonic());
        assertEquals("Ctrl", partElement.getMnemonicPath());
        assertEquals("Control System", partElement.getName());
        assertEquals("Control System", partElement.getNamePath());
        assertEquals("Approved", partElement.getStatus());
        assertEquals("Device Structure", partElement.getType());
        assertEquals(UtilityBusinessTestFixture3.UUID_NAMEPART_502_DISCIPLINE_CTRL, partElement.getUuid().toString());
    }

    /**
     * Utility method to check PartElement for particular content, FPM01.
     */
    private void assertPartElementFPM01(PartElement partElement) {
        assertNotNull(partElement);
        assertEquals("3", partElement.getLevel());
        assertEquals("FPM01", partElement.getMnemonic());
        assertEquals("PBI-FPM01", partElement.getMnemonicPath());
        assertEquals("PBI FPM-01", partElement.getName());
        assertEquals("Central Services-Proton Beam Instrumentation-PBI FPM-01", partElement.getNamePath());
        assertEquals("Approved", partElement.getStatus());
        assertEquals("System Structure", partElement.getType());
        assertEquals(UtilityBusinessTestFixture3.UUID_NAMEPART_2806_SUBSYSSTEM_FPM01, partElement.getUuid().toString());
    }

}
