/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.webservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.UtilityBusinessTestFixture4;
import org.openepics.names.jaxb.DeviceNameElement;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.NameViews;
import org.openepics.names.nameviews.NameViewProvider.NameRevisions;
import org.openepics.names.services.EssNamingConvention;
import org.powermock.reflect.Whitebox;

/**
 * Unit tests for SpecificDeviceNameResourceImpl.
 *
 * <br><br>
 * Note
 * <ul>
 * <li> JUnit integration test
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see SpecificDeviceNameResourceImpl
 * @see NameViewProvider
 * @see NameViews
 * @see NameRevisions
 * @see UtilityBusinessTestFixture4
 */
public class SpecificDeviceNameResourceImplTest3 {

    /*
        Purpose to test SpecificDeviceNameResource part of REST API.

        In order to be able to test, NameViewProvider and associated parts of NameViews and NameRevisions to be set up,
        after which REST API test may proceed.

        Note
            test fixture is handled by utility class
            different values available for status for DeviceNameElement
            name vs. equivalence class representative for name
     */

    private static final String TGT_TSS1080_CTRL_S_PROT_08  = "Tgt-TSS1080:Ctrl-SProt-08";
    private static final String PBI_BCM01_CTRL_MTCA3U_201   = "PBI-BCM01:Ctrl-MTCA3U-201";
    private static final String PBI_BCM01_PBI_MTCA_200      = "PBI-BCM01:PBI-MTCA-200";
    private static final String FEB_050_ROW_PBI_MTCA_215100 = "FEB-050Row:PBI-MTCA-215100";

    private static UtilityBusinessTestFixture4 testFixture;

    private static NameViewProvider nameViewProvider;
    private static Map<UUID, NameView> nameViewMap;

    private static SpecificDeviceNameResourceImpl specificDeviceNameResourceImpl;

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        testFixture = UtilityBusinessTestFixture4.getInstance();
        testFixture.setUp();

        setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        tearDown();

        testFixture.tearDown();
        testFixture = null;
    }

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @SuppressWarnings("unchecked")
    private static void setUp() throws Exception {
        nameViewProvider = new NameViewProvider();

        Whitebox.setInternalState(nameViewProvider, "namingConvention", new EssNamingConvention());
        Whitebox.setInternalState(nameViewProvider, "nameViews", new NameViews());
        Whitebox.setInternalState(nameViewProvider, "nameRevisions", nameViewProvider.new NameRevisions());

        nameViewMap = (Map<UUID, NameView>) Whitebox.getField(nameViewProvider.getNameViews().getClass(), "nameViewMap")
                .get(nameViewProvider.getNameViews());

        specificDeviceNameResourceImpl = new SpecificDeviceNameResourceImpl();
        Whitebox.setInternalState(specificDeviceNameResourceImpl, "nameViewProvider", nameViewProvider);

        // update name view provider with test fixture content

        for (NameRevision revision : testFixture.getNameRevisions()) {
            nameViewProvider.getNameViews().update(revision);

            // update nameRevisionMap with nameRevision in NameRevisions in NameViewProvider
            Whitebox.invokeMethod(nameViewProvider.getNameRevisions(), "update", revision);
        }
    }

    /**
     * One-time cleanup code.
     */
    private static void tearDown() {
        specificDeviceNameResourceImpl = null;

        if (nameViewMap != null) {
            nameViewMap.clear();
            nameViewMap = null;
        }

        nameViewProvider = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test content after setUp.
     */
    @Test
    public void setUpOk() {
        // check content after setUp
        assertNotNull(nameViewProvider);

        // check content of NameViews - system root
        assertEquals(18, nameViewProvider.getNameViews().getSystemRoot().getAllChildren().size());

        // check content of NameViews - device root
        assertEquals(3, nameViewProvider.getNameViews().getDeviceRoot().getAllChildren().size());

        // check content of NameViews - nameViewMap
        assertEquals(19, nameViewMap.size());

        // check content of NameRevisions - nameRevisionMap
        //     conventionName
        //     conventionNameEqClass
        //     (uuid)
        assertEquals(10, nameViewProvider.getNameRevisions().keySet().size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameConventionName4108Index1() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceName(FEB_050_ROW_PBI_MTCA_215100);

        assertNotNull(deviceNameElement);
        assertEquals(FEB_050_ROW_PBI_MTCA_215100, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.OBSOLETE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameConventionName4108Index7() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName(PBI_BCM01_PBI_MTCA_200);

        assertNotNull(deviceNameElement);
        assertEquals(PBI_BCM01_PBI_MTCA_200, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.OBSOLETE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameConventionName4108Index16() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName(PBI_BCM01_CTRL_MTCA3U_201);

        assertNotNull(deviceNameElement);
        assertEquals(PBI_BCM01_CTRL_MTCA3U_201, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameUuid4108() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceName(
                        UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050);

        assertNotNull(deviceNameElement);
        assertEquals(PBI_BCM01_CTRL_MTCA3U_201, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameConventionName70025Index1() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceName("Tgt-TSS1080:Ctrl-SProt-8");

        assertNull(deviceNameElement);
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameConventionName70025Index3() {
        DeviceNameElement deviceNameElement = specificDeviceNameResourceImpl.getDeviceName(TGT_TSS1080_CTRL_S_PROT_08);

        assertNotNull(deviceNameElement);
        assertEquals(TGT_TSS1080_CTRL_S_PROT_08, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.DELETED, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceName method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameUuid70025() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceName(
                        UtilityBusinessTestFixture4.UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08);

        assertNotNull(deviceNameElement);
        assertEquals(TGT_TSS1080_CTRL_S_PROT_08, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.DELETED, deviceNameElement.getStatus());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementConventionName4108Index1() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceNameElement(FEB_050_ROW_PBI_MTCA_215100);

        assertNotNull(deviceNameElement);
        assertEquals(FEB_050_ROW_PBI_MTCA_215100, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.OBSOLETE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementConventionName4108Index7() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceNameElement(PBI_BCM01_PBI_MTCA_200);

        assertNotNull(deviceNameElement);
        assertEquals(PBI_BCM01_PBI_MTCA_200, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.OBSOLETE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementConventionName4108Index16() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceNameElement(PBI_BCM01_CTRL_MTCA3U_201);

        assertNotNull(deviceNameElement);
        assertEquals(PBI_BCM01_CTRL_MTCA3U_201, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementUuid4108() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceNameElement(
                        UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050);

        assertNotNull(deviceNameElement);
        assertEquals(PBI_BCM01_CTRL_MTCA3U_201, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.ACTIVE, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementConventionName70025Index1() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceNameElement("Tgt-TSS1080:Ctrl-SProt-8");

        assertNotNull(deviceNameElement);
        assertEquals(TGT_TSS1080_CTRL_S_PROT_08, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.DELETED, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementConventionName70025Index3() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceNameElement(TGT_TSS1080_CTRL_S_PROT_08);

        assertNotNull(deviceNameElement);
        assertEquals(TGT_TSS1080_CTRL_S_PROT_08, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.DELETED, deviceNameElement.getStatus());
    }

    /**
     * Test getDeviceNameElement method of SpecificDeviceNameResourceImpl.
     */
    @Test
    public void getDeviceNameElementUuid70025() {
        DeviceNameElement deviceNameElement =
                specificDeviceNameResourceImpl.getDeviceNameElement(
                        UtilityBusinessTestFixture4.UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08);

        assertNotNull(deviceNameElement);
        assertEquals(TGT_TSS1080_CTRL_S_PROT_08, deviceNameElement.getName());
        assertEquals(
                UtilityBusinessTestFixture4.UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08,
                deviceNameElement.getUuid().toString());
        assertEquals(SpecificDeviceNameResourceImpl.DELETED, deviceNameElement.getStatus());
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getNameViewUuid4108() {
        NameView nameView =
                nameViewProvider.getNameViews().get(UUID.fromString(
                        UtilityBusinessTestFixture4.UUID_DEVICE_4108_INSTANCEINDEX_FEB_050ROW_PBI_MTCA_050));

        assertNotNull(nameView);
    }

    @Test
    public void getNameViewUuid70025() {
        NameView nameView =
                nameViewProvider.getNameViews().get(UUID.fromString(
                        UtilityBusinessTestFixture4.UUID_DEVICE_70025_INSTANCEINDEX_TGT_TSS1080_CTRL_SPROT_08));

        assertNotNull(nameView);
    }
}
