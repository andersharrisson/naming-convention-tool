/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.webservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.UtilityBusinessTestFixture3;
import org.openepics.names.jaxb.DeviceNameElement;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.NameViews;
import org.openepics.names.nameviews.NameViewProvider.NameRevisions;
import org.openepics.names.services.EssNamingConvention;
import org.openepics.names.services.NamingConvention;
import org.powermock.reflect.Whitebox;

/**
 * Unit tests for DeviceNamesResourceImpl.
 *
 * <br><br>
 * Note
 * <ul>
 * <li> JUnit integration test
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see DeviceNamesResourceImpl
 * @see NameViewProvider
 * @see NameViews
 * @see NameRevisions
 * @see UtilityBusinessTestFixture3
 */
public class DeviceNamesResourceImplTest2 {

    /*
       Purpose to test DeviceNamesResource part of REST API.

       In order to be able to test, NameViewProvider and associated parts of NameViews and NameRevisions to be set up,
       after which REST API test may proceed.

       Note
           test fixture is handled by utility class
           different values available for status for DeviceNameElement
           name vs. equivalence class representative for name
    */

    private static final String ACTIVE   = "ACTIVE";
    private static final String OBSOLETE = "OBSOLETE";

    private static final String GET_ALL  = "getAll";
    private static final String SEARCH   = "search";

    private static UtilityBusinessTestFixture3 testFixture;

    private static NameViewProvider nameViewProvider;
    private static Map<UUID, NameView> nameViewMap;

    private static DeviceNamesResourceImpl deviceNamesResourceImpl;
    private static SpecificDeviceNameResourceImpl specificDeviceNameResourceImpl;

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        testFixture = UtilityBusinessTestFixture3.getInstance();
        testFixture.setUp();

        setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        tearDown();

        testFixture.tearDown();
        testFixture = null;
    }

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @SuppressWarnings("unchecked")
    private static void setUp() throws Exception {
        nameViewProvider = new NameViewProvider();

        NamingConvention namingConvention = new EssNamingConvention();

        Whitebox.setInternalState(nameViewProvider, "namingConvention", namingConvention);
        Whitebox.setInternalState(nameViewProvider, "nameViews", new NameViews());
        Whitebox.setInternalState(nameViewProvider, "nameRevisions", nameViewProvider.new NameRevisions());

        nameViewMap = (Map<UUID, NameView>) Whitebox.getField(nameViewProvider.getNameViews().getClass(), "nameViewMap")
                .get(nameViewProvider.getNameViews());

        specificDeviceNameResourceImpl = new SpecificDeviceNameResourceImpl();
        Whitebox.setInternalState(specificDeviceNameResourceImpl, "nameViewProvider", nameViewProvider);

        deviceNamesResourceImpl = new DeviceNamesResourceImpl();
        Whitebox.setInternalState(
                deviceNamesResourceImpl, "specificDeviceNameSubresource", specificDeviceNameResourceImpl);
        Whitebox.setInternalState(deviceNamesResourceImpl, "nameViewProvider", nameViewProvider);

        // update name view provider with test fixture content

        for (NameRevision revision : testFixture.getNameRevisions()) {
            nameViewProvider.getNameViews().update(revision);

            // update nameRevisionMap with nameRevision in NameRevisions in NameViewProvider
            Whitebox.invokeMethod(nameViewProvider.getNameRevisions(), "update", revision);
        }
    }

    /**
     * One-time cleanup code.
     */
    private static void tearDown() {
        specificDeviceNameResourceImpl = null;
        deviceNamesResourceImpl = null;

        if (nameViewMap != null) {
            nameViewMap.clear();
            nameViewMap = null;
        }

        nameViewProvider = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test content after setUp.
     */
    @Test
    public void setUpOk() {
        // check content after setUp

        // check content of NameViews - system root
        assertEquals(17, nameViewProvider.getNameViews().getSystemRoot().getAllChildren().size());

        // check content of NameViews - device root
        assertEquals(33, nameViewProvider.getNameViews().getDeviceRoot().getAllChildren().size());

        // check content of NameViews - nameViewMap
        assertEquals(39, nameViewMap.size());

        // check content of NameRevisions - nameRevisionMap
        //     conventionName
        //     conventionNameEqClass
        //     (uuid)
        assertEquals(68, nameViewProvider.getNameRevisions().keySet().size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNames method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNames() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNames();

        assertNotNull(deviceNames);
        assertEquals(68, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(11, countActive);
        assertEquals(57, countObsolete);
        assertEquals(0,  countOther);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getSpecificDeviceNameSubresource method of DeviceNamesResourceImpl.
     */
    @Test
    public void getSpecificDeviceNameSubresource() {
        assertNotNull(deviceNamesResourceImpl.getSpecificDeviceNameSubresource());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesSearch(null);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("");

        assertNotNull(deviceNames);
        assertEquals(68, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchAcc() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("Acc");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchAccUpperCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("ACC");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchAccLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("acc");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchFEB() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("FEB");

        assertNotNull(deviceNames);
        assertEquals(44, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(0,  countActive);
        assertEquals(44, countObsolete);
        assertEquals(0,  countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchFEBStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^FEB");

        assertNotNull(deviceNames);
        assertEquals(44, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(0,  countActive);
        assertEquals(44, countObsolete);
        assertEquals(0,  countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchFEBEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("FEB$");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchFEBLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("feb");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchLMinusE() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("l-E");

        assertNotNull(deviceNames);
        assertEquals(10, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(9, countActive);
        assertEquals(1, countObsolete);
        assertEquals(0, countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchLMinusEStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^l-E");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchLMinusEEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("l-E$");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchLMinusEUpperCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("L-E");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchLMinusELowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("l-e");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch1MinusE() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("1-E");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch1MinusELowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("1-e");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPBI() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("PBI");

        assertNotNull(deviceNames);
        assertEquals(68, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(11, countActive);
        assertEquals(57, countObsolete);
        assertEquals(0,  countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPBIStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^PBI");

        assertNotNull(deviceNames);
        assertEquals(24, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(11, countActive);
        assertEquals(13, countObsolete);
        assertEquals(0,  countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPBIEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("PBI$");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPBILowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("pbi");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPB1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("PB1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPB1LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("pb1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPM() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("PM");

        assertNotNull(deviceNames);
        assertEquals(24, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(11, countActive);
        assertEquals(13, countObsolete);
        assertEquals(0,  countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPMStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^PM");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPMEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("PM$");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPMLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("pm");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch20() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("20");

        assertNotNull(deviceNames);
        assertEquals(11, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(2, countActive);
        assertEquals(9, countObsolete);
        assertEquals(0, countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch20StartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^20");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch20EndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("20$");

        assertNotNull(deviceNames);
        assertEquals(4, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(0, countActive);
        assertEquals(4, countObsolete);
        assertEquals(0, countOther);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemNull() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem(null);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem(GET_ALL);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemFEB() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("FEB");

        assertNotNull(deviceNames);
        assertEquals(44, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemFEBLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("feb");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemPBI() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("PBI");

        assertNotNull(deviceNames);
        assertEquals(24, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemPBILowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("pbi");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemPB1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("PB1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemPB1LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("pb1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesBySystemSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch(null);
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("");

        assertNotNull(deviceNames);
        assertEquals(68, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchI() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("I");

        assertNotNull(deviceNames);
        assertEquals(24, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchIStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("^I");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchIEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("I$");

        assertNotNull(deviceNames);
        assertEquals(24, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchILowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("i");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearch1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemNull() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem(null);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem(GET_ALL);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemFPM01() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("FPM01");

        assertNotNull(deviceNames);
        assertEquals(24, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemFPM01LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("fpm01");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemFPM1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("FPM1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemFPM1LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("fpm1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystem030Row() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("030Row");

        assertNotNull(deviceNames);
        assertEquals(44, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystem030RowUpperCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("030ROW");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystem030RowLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("030row");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesBySubsystemSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch(null);
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("");

        assertNotNull(deviceNames);
        assertEquals(68, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchM01() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("M01");

        assertNotNull(deviceNames);
        assertEquals(24, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchM01StartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("^M01");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchM01EndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("M01$");

        assertNotNull(deviceNames);
        assertEquals(24, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchM01LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("m01");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchM1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("M1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchM1LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("m1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineNull() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline(null);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline(GET_ALL);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineCtrl() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("Ctrl");

        assertNotNull(deviceNames);
        assertEquals(12, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineCtrlUpperCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("CTRL");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineCtrlLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("ctrl");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineCTR1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("CTR1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineCTR1LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("ctr1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplinePBI() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("PBI");

        assertNotNull(deviceNames);
        assertEquals(54, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplinePBILowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("pbi");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesByDisciplineSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch(null);
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("");

        assertNotNull(deviceNames);
        assertEquals(68, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchB1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("B1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchB1LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("b1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchRL() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("RL");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchRLLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("rl");

        assertNotNull(deviceNames);
        assertEquals(12, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchRLLowerCaseStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("^rl");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchRLLowerCaseEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("rl$");

        assertNotNull(deviceNames);
        assertEquals(12, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchR1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("R1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchR1LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("r1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeNull() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType(null);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType("");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType(GET_ALL);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeECATIO() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType("ECATIO");

        assertNotNull(deviceNames);
        assertEquals(34, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeECATIOLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType("ecatio");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeECAT10() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType("ECAT10");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeECAT10LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType("ecat10");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesByDeviceTypeSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch(null);
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("");

        assertNotNull(deviceNames);
        assertEquals(68, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchECATI() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("ECATI");

        assertNotNull(deviceNames);
        assertEquals(34, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchECATIStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("^ECATI");

        assertNotNull(deviceNames);
        assertEquals(34, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchECATIEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("ECATI$");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchECATILowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("ecati");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchECAT1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("ECAT1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchECAT1LowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("ecat1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

}
