/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.webservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.UtilityBusinessTestFixture2;
import org.openepics.names.jaxb.DeviceNameElement;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.NameViews;
import org.openepics.names.nameviews.NameViewProvider.NameRevisions;
import org.openepics.names.services.EssNamingConvention;
import org.openepics.names.services.NamingConvention;
import org.powermock.reflect.Whitebox;

/**
 * Unit tests for DeviceNamesResourceImpl.
 *
 * <br><br>
 * Note
 * <ul>
 * <li> JUnit integration test
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see DeviceNamesResourceImpl
 * @see NameViewProvider
 * @see NameViews
 * @see NameRevisions
 * @see UtilityBusinessTestFixture2
 */
public class DeviceNamesResourceImplTest {

    /*
       Purpose to test DeviceNamesResource part of REST API.

       In order to be able to test, NameViewProvider and associated parts of NameViews and NameRevisions to be set up,
       after which REST API test may proceed.

       Note
           test fixture is handled by utility class
           different values available for status for DeviceNameElement
           name vs. equivalence class representative for name
    */

    private static final String ACTIVE   = "ACTIVE";
    private static final String OBSOLETE = "OBSOLETE";

    private static final String GET_ALL  = "getAll";
    private static final String SEARCH   = "search";

    private static final String RFQ_010_EMR_TT_012  = "RFQ-010:EMR-TT-012";
    private static final String RFQ_010_EMR_TT2_012 = "RFQ-010:EMR-TT2-012";

    private static UtilityBusinessTestFixture2 testFixture;

    private static NameViewProvider nameViewProvider;
    private static Map<UUID, NameView> nameViewMap;

    private static DeviceNamesResourceImpl deviceNamesResourceImpl;
    private static SpecificDeviceNameResourceImpl specificDeviceNameResourceImpl;

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        testFixture = UtilityBusinessTestFixture2.getInstance();
        testFixture.setUp();

        setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        tearDown();

        testFixture.tearDown();
        testFixture = null;
    }

    /**
     * One-time initialization code.
     *
     * @throws Exception e.g. ReflectiveOperationException (NoSuchFieldException, IllegalAccessException)
     */
    @SuppressWarnings("unchecked")
    private static void setUp() throws Exception {
        nameViewProvider = new NameViewProvider();

        NamingConvention namingConvention = new EssNamingConvention();

        Whitebox.setInternalState(nameViewProvider, "namingConvention", namingConvention);
        Whitebox.setInternalState(nameViewProvider, "nameViews", new NameViews());
        Whitebox.setInternalState(nameViewProvider, "nameRevisions", nameViewProvider.new NameRevisions());

        nameViewMap = (Map<UUID, NameView>) Whitebox.getField(nameViewProvider.getNameViews().getClass(), "nameViewMap")
                .get(nameViewProvider.getNameViews());

        specificDeviceNameResourceImpl = new SpecificDeviceNameResourceImpl();
        Whitebox.setInternalState(specificDeviceNameResourceImpl, "nameViewProvider", nameViewProvider);

        deviceNamesResourceImpl = new DeviceNamesResourceImpl();
        Whitebox.setInternalState(
                deviceNamesResourceImpl, "specificDeviceNameSubresource", specificDeviceNameResourceImpl);
        Whitebox.setInternalState(deviceNamesResourceImpl, "nameViewProvider", nameViewProvider);

        // update name view provider with test fixture content

        for (NameRevision revision : testFixture.getNameRevisions()) {
            nameViewProvider.getNameViews().update(revision);

            // update nameRevisionMap with nameRevision in NameRevisions in NameViewProvider
            Whitebox.invokeMethod(nameViewProvider.getNameRevisions(), "update", revision);
        }
    }

    /**
     * One-time cleanup code.
     */
    private static void tearDown() {
        specificDeviceNameResourceImpl = null;
        deviceNamesResourceImpl = null;

        if (nameViewMap != null) {
            nameViewMap.clear();
            nameViewMap = null;
        }

        nameViewProvider = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test content after setUp.
     */
    @Test
    public void setUpOk() {
        // check content after setUp

        // check content of NameViews - system root
        assertEquals(14, nameViewProvider.getNameViews().getSystemRoot().getAllChildren().size());

        // check content of NameViews - device root
        assertEquals(16, nameViewProvider.getNameViews().getDeviceRoot().getAllChildren().size());

        // check content of NameViews - nameViewMap
        assertEquals(19, nameViewMap.size());

        // check content of NameRevisions - nameRevisionMap
        //     conventionName
        //     conventionNameEqClass
        //     (uuid)
        assertEquals(22, nameViewProvider.getNameRevisions().keySet().size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNames method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNames() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNames();

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(11, countActive);
        assertEquals(11, countObsolete);
        assertEquals(0,  countOther);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getSpecificDeviceNameSubresource method of DeviceNamesResourceImpl.
     */
    @Test
    public void getSpecificDeviceNameSubresource() {
        assertNotNull(deviceNamesResourceImpl.getSpecificDeviceNameSubresource());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesSearch(null);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchSpace() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch(" ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchFQ() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("FQ");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(11, countActive);
        assertEquals(11, countObsolete);
        assertEquals(0,  countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchFQStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^FQ");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchFQEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("FQ$");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchFQLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("fq");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchMR() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("MR");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(11, countActive);
        assertEquals(11, countObsolete);
        assertEquals(0,  countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchMRStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^MR");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchMREndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("MR$");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearchMRLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("mr");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch010() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("010");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(11, countActive);
        assertEquals(11, countObsolete);
        assertEquals(0,  countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch010StartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^010");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch010EndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("010$");

        assertNotNull(deviceNames);
        assertEquals(2, deviceNames.size());

        int countActive = 0;
        int countObsolete = 0;
        int countOther = 0;
        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getStatus()) {
                case ACTIVE :
                    countActive++;
                    break;
                case OBSOLETE :
                    countObsolete++;
                    break;
                default:
                    countOther++;
                    break;
            }
        }

        assertEquals(1, countActive);
        assertEquals(1, countObsolete);
        assertEquals(0,  countOther);
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch012() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("012");

        assertNotNull(deviceNames);
        assertEquals(2, deviceNames.size());

        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getName()) {
                case RFQ_010_EMR_TT2_012:
                    assertDeviceNameElementTT2(deviceNameElement);
                    break;
                case RFQ_010_EMR_TT_012:
                    assertDeviceNameElementTT(deviceNameElement);
                    break;
                default:
                    fail();
            }
        }
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch012StartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^012");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch012EndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("012$");

        assertNotNull(deviceNames);
        assertEquals(2, deviceNames.size());

        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getName()) {
                case RFQ_010_EMR_TT2_012:
                    assertDeviceNameElementTT2(deviceNameElement);
                    break;
                case RFQ_010_EMR_TT_012:
                    assertDeviceNameElementTT(deviceNameElement);
                    break;
                default:
                    fail();
            }
        }
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch12() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("12");

        assertNotNull(deviceNames);
        assertEquals(2, deviceNames.size());

        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getName()) {
                case RFQ_010_EMR_TT2_012:
                    assertDeviceNameElementTT2(deviceNameElement);
                    break;
                case RFQ_010_EMR_TT_012:
                    assertDeviceNameElementTT(deviceNameElement);
                    break;
                default:
                    fail();
            }
        }
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch12StartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("^12");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesSearch12EndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesSearch("12$");

        assertNotNull(deviceNames);
        assertEquals(2, deviceNames.size());

        for (DeviceNameElement deviceNameElement : deviceNames) {
            switch (deviceNameElement.getName()) {
                case RFQ_010_EMR_TT2_012:
                    assertDeviceNameElementTT2(deviceNameElement);
                    break;
                case RFQ_010_EMR_TT_012:
                    assertDeviceNameElementTT(deviceNameElement);
                    break;
                default:
                    fail();
            }
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemNull() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem(null);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem(GET_ALL);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemRFQ() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("RFQ");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemRFQLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystem("rfq");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesBySystemSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch(null);
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchQ() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("Q");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchQStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("^Q");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchQEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("Q$");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySystemSearchQLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySystemSearch("q");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemNull() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem(null);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem(GET_ALL);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystem method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystem010() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystem("010");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesBySubsystemSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch(null);
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearch1() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("1");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearch1StartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("^1");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesBySubsystemSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesBySubsystemSearch1EndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesBySubsystemSearch("1$");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineNull() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline(null);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline(GET_ALL);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineEMR() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("EMR");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDiscipline method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineEMRLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDiscipline("emr");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesByDisciplineSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch(null);
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchE() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("E");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchEStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("^E");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchEEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("E$");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDisciplineSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDisciplineSearchELowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDisciplineSearch("e");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeNull() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType(null);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType("");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType(GET_ALL);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeTT() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType("TT");

        assertNotNull(deviceNames);
        assertEquals(11, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceType method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeTTLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceType("tt");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchPatternSyntaxException() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("\\");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test(expected = NullPointerException.class)
    public void getAllDeviceNamesByDeviceTypeSearchNull() {
        deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch(null);
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchEmpty() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchUnknown() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch(SEARCH);

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchT() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("T");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchTStartsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("^T");

        assertNotNull(deviceNames);
        assertEquals(22, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchTEndsWith() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("T$");

        assertNotNull(deviceNames);
        assertEquals(11, deviceNames.size());
    }

    /**
     * Test getAllDeviceNamesByDeviceTypeSearch method of DeviceNamesResourceImpl.
     */
    @Test
    public void getAllDeviceNamesByDeviceTypeSearchTLowerCase() {
        List<DeviceNameElement> deviceNames = deviceNamesResourceImpl.getAllDeviceNamesByDeviceTypeSearch("t");

        assertNotNull(deviceNames);
        assertEquals(0, deviceNames.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to check DeviceNameElement for particular content, RFQ-010:EMR-TT2-012.
     */
    private void assertDeviceNameElementTT2(DeviceNameElement deviceNameElement) {
        assertNotNull(deviceNameElement);
        assertNull(deviceNameElement.getDeviceType());
        assertNull(deviceNameElement.getDiscipline());
        assertNull(deviceNameElement.getInstanceIndex());
        assertEquals(RFQ_010_EMR_TT2_012, deviceNameElement.getName());
        assertEquals(OBSOLETE, deviceNameElement.getStatus());
        assertNull(deviceNameElement.getSubsystem());
        assertNull(deviceNameElement.getSystem());
        assertNull(deviceNameElement.getSystemGroup());
        assertEquals(
                UtilityBusinessTestFixture2.UUID_DEVICE_12803_INSTANCEINDEX,
                deviceNameElement.getUuid().toString());
    }

    /**
     * Utility method to check DeviceNameElement for particular content, RFQ-010:EMR-TT-012.
     */
    private void assertDeviceNameElementTT(DeviceNameElement deviceNameElement) {
        assertNotNull(deviceNameElement);
        assertEquals("TT", deviceNameElement.getDeviceType());
        assertEquals("EMR", deviceNameElement.getDiscipline());
        assertEquals("012", deviceNameElement.getInstanceIndex());
        assertEquals(RFQ_010_EMR_TT_012, deviceNameElement.getName());
        assertEquals(ACTIVE, deviceNameElement.getStatus());
        assertEquals("010", deviceNameElement.getSubsystem());
        assertEquals("RFQ", deviceNameElement.getSystem());
        assertEquals("Acc", deviceNameElement.getSystemGroup());
        assertEquals(
                UtilityBusinessTestFixture2.UUID_DEVICE_12803_INSTANCEINDEX,
                deviceNameElement.getUuid().toString());
    }

}
