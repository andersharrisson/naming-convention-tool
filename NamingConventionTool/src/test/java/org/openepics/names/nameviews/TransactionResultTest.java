/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Unit tests for TransactionResult class.
 *
 * @author Lars Johansson
 *
 * @see TransactionResult
 * @see TransactionResult.Result
 */
public class TransactionResultTest {

    private static final String EMPTY = "";

    private static final String ASDF  = "asdf";
    private static final String ZXCV  = "zxcv";

    /**
     * Test success method of TransactionResult.
     */
    @Test
    public void success() {
        TransactionResult transactionResultSuccess1 = TransactionResult.success(null);
        TransactionResult transactionResultSuccess2 = TransactionResult.success(EMPTY);
        TransactionResult transactionResultSuccess3 = TransactionResult.success(ASDF);

        assertNotNull(transactionResultSuccess1);
        assertEquals(TransactionResult.Result.SUCCESS, transactionResultSuccess1.getResult());
        assertNull(transactionResultSuccess1.getSummary());
        assertNull(transactionResultSuccess1.getDetail());

        assertNotNull(transactionResultSuccess2);
        assertEquals(TransactionResult.Result.SUCCESS, transactionResultSuccess2.getResult());
        assertEquals(EMPTY, transactionResultSuccess2.getSummary());
        assertEquals(EMPTY, transactionResultSuccess2.getDetail());

        assertNotNull(transactionResultSuccess3);
        assertEquals(TransactionResult.Result.SUCCESS, transactionResultSuccess3.getResult());
        assertEquals(ASDF, transactionResultSuccess3.getSummary());
        assertEquals(ASDF, transactionResultSuccess3.getDetail());
    }

    /**
     * Test noChanges method of TransactionResult.
     */
    @Test
    public void noChanges() {
        TransactionResult transactionResult = TransactionResult.noChanges();

        assertNotNull(transactionResult);
        assertEquals(TransactionResult.Result.NO_CHANGES, transactionResult.getResult());
        assertNull(transactionResult.getSummary());
        assertNull(transactionResult.getDetail());
    }

    /**
     * Test error method of TransactionResult.
     */
    @Test
    public void error() {
        TransactionResult transactionResultError1 = TransactionResult.error(null);
        TransactionResult transactionResultError2 = TransactionResult.error(EMPTY);
        TransactionResult transactionResultError3 = TransactionResult.error(ASDF);

        assertNotNull(transactionResultError1);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError1.getResult());
        assertNull(transactionResultError1.getSummary());
        assertNull(transactionResultError1.getDetail());

        assertNotNull(transactionResultError2);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError2.getResult());
        assertEquals(EMPTY, transactionResultError2.getSummary());
        assertEquals(EMPTY, transactionResultError2.getDetail());

        assertNotNull(transactionResultError3);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError3.getResult());
        assertEquals(ASDF, transactionResultError3.getSummary());
        assertEquals(ASDF, transactionResultError3.getDetail());
    }

    /**
     * Test error method of TransactionResult.
     */
    @Test
    public void error2() {
        TransactionResult transactionResultError11 = TransactionResult.error(null, null);
        TransactionResult transactionResultError12 = TransactionResult.error(null, EMPTY);
        TransactionResult transactionResultError13 = TransactionResult.error(null, ASDF);

        TransactionResult transactionResultError21 = TransactionResult.error(EMPTY, null);
        TransactionResult transactionResultError22 = TransactionResult.error(EMPTY, EMPTY);
        TransactionResult transactionResultError23 = TransactionResult.error(EMPTY, ASDF);

        TransactionResult transactionResultError31 = TransactionResult.error(ASDF,  null);
        TransactionResult transactionResultError32 = TransactionResult.error(ASDF,  EMPTY);
        TransactionResult transactionResultError33 = TransactionResult.error(ASDF,  ZXCV);

        assertNotNull(transactionResultError11);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError11.getResult());
        assertNull(transactionResultError11.getSummary());
        assertNull(transactionResultError11.getDetail());

        assertNotNull(transactionResultError12);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError12.getResult());
        assertNull(transactionResultError12.getSummary());
        assertEquals(EMPTY, transactionResultError12.getDetail());

        assertNotNull(transactionResultError13);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError13.getResult());
        assertNull(transactionResultError13.getSummary());
        assertEquals(ASDF, transactionResultError13.getDetail());

        assertNotNull(transactionResultError21);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError21.getResult());
        assertEquals(EMPTY, transactionResultError21.getSummary());
        assertNull(transactionResultError21.getDetail());

        assertNotNull(transactionResultError22);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError22.getResult());
        assertEquals(EMPTY, transactionResultError22.getSummary());
        assertEquals(EMPTY, transactionResultError22.getDetail());

        assertNotNull(transactionResultError23);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError23.getResult());
        assertEquals(EMPTY, transactionResultError23.getSummary());
        assertEquals(ASDF, transactionResultError23.getDetail());

        assertNotNull(transactionResultError31);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError31.getResult());
        assertEquals(ASDF, transactionResultError31.getSummary());
        assertNull(transactionResultError31.getDetail());

        assertNotNull(transactionResultError32);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError32.getResult());
        assertEquals(ASDF, transactionResultError32.getSummary());
        assertEquals(EMPTY, transactionResultError32.getDetail());

        assertNotNull(transactionResultError33);
        assertEquals(TransactionResult.Result.ERROR, transactionResultError33.getResult());
        assertEquals(ASDF, transactionResultError33.getSummary());
        assertEquals(ZXCV, transactionResultError33.getDetail());
    }

}
