/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.business.NameType;
import org.openepics.names.business.UtilityBusinessTestFixture3;
import org.openepics.names.nameviews.NameViewProvider.NameRevisions;
import org.openepics.names.services.EssNamingConvention;
import org.powermock.reflect.Whitebox;

/**
 * Unit tests for {@link NameViewProvider}.
 * Concerns both inner class {@link NameRevisions} as well as {@link NameViews}.
 *
 * <br><br>
 * Note
 * <ul>
 * <li> JUnit integration test
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see NameViewProvider
 * @see NameViews
 * @see NameRevisions
 * @see NameRevisionTest
 * @see UtilityBusinessTestFixture3
 */
public class NameViewProviderTest3 {

    /*
       Purpose to test update method in NameViewProvider and associated parts of NameViews and NameRevisions
       classes, methods and variables.

       Details, test the inner workings of
           NameViews
               NameView systemRoot
               NameView deviceRoot
               Map<UUID, NameView> nameViewMap
           NameRevisions
               Map<String, NameRevision> nameRevisionMap

       Note
           test fixture is handled by utility class
    */

    private static UtilityBusinessTestFixture3 testFixture;

    private NameViewProvider nameViewProvider;
    private Map<UUID, NameView> nameViewMap;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        testFixture = UtilityBusinessTestFixture3.getInstance();
        testFixture.setUp();
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        testFixture.tearDown();
        testFixture = null;
    }

    /**
     * Initialization code before each test.
     *
     * @throws ReflectiveOperationException NoSuchFieldException, IllegalAccessException
     */
    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws ReflectiveOperationException {
        nameViewProvider = new NameViewProvider();

        Whitebox.setInternalState(nameViewProvider, "namingConvention", new EssNamingConvention());
        Whitebox.setInternalState(nameViewProvider, "nameViews", new NameViews());
        Whitebox.setInternalState(nameViewProvider, "nameRevisions", nameViewProvider.new NameRevisions());

        nameViewMap = (Map<UUID, NameView>) Whitebox.getField(nameViewProvider.getNameViews().getClass(), "nameViewMap")
                .get(nameViewProvider.getNameViews());
    }

    /**
     * Cleanup code after each test.
     */
    @After
    public void tearDown() {
        if (nameViewMap != null) {
            nameViewMap.clear();
            nameViewMap = null;
        }

        nameViewProvider = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test {@link NameViewProvider} fields and methods.
     * Concerns both inner class {@link NameRevisions} as well as {@link NameViews}.
     * Concerns fields and maps in concerned classes.
     * Complex.
     *
     * @see NameViewProvider
     * @see NameViews
     * @see NameRevisions
     */
    @Test
    public void initUpdateGet() {
        // check content before update

        assertNotNull(nameViewProvider.getNameViews().getSystemRoot());
        assertEquals(NameType.SYSTEM_STRUCTURE, nameViewProvider.getNameViews().getSystemRoot().getNameType());
        assertEquals(0, nameViewProvider.getNameViews().getSystemRoot().getAllChildren().size());
        assertNotNull(nameViewProvider.getNameViews().getDeviceRoot());
        assertEquals(NameType.DEVICE_STRUCTURE, nameViewProvider.getNameViews().getDeviceRoot().getNameType());
        assertEquals(0, nameViewProvider.getNameViews().getDeviceRoot().getAllChildren().size());
        assertNotNull(nameViewMap);
        assertEquals(0, nameViewMap.size());

        assertTrue(nameViewProvider.getNameRevisions().keySet().isEmpty());

        // update name view provider with test fixture content

        nameViewProvider.update(testFixture.getNameRevisions());

        // check content after update

        // check content of NameViews - system root
        assertEquals(17, nameViewProvider.getNameViews().getSystemRoot().getAllChildren().size());

        // check content of NameViews - device root
        assertEquals(33, nameViewProvider.getNameViews().getDeviceRoot().getAllChildren().size());

        // check content of NameViews - nameViewMap
        assertEquals(39, nameViewMap.size());

        // check content of NameRevisions - nameRevisionMap
        //     conventionName
        //     conventionNameEqClass
        //     (uuid)
        assertEquals(68, nameViewProvider.getNameRevisions().keySet().size());
    }

}
