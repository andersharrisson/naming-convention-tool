/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit tests for NameRevisionStatus class.
 *
 * @author Lars Johansson
 *
 * @see NameRevisionStatus
 */
public class NameRevisionStatusTest {

    /**
     * Test isPending method of NameRevisionStatus.
     */
    @Test
    public void isPending() {
        assertFalse(NameRevisionStatus.APPROVED.isPending());
        assertFalse(NameRevisionStatus.CANCELLED.isPending());
        assertTrue (NameRevisionStatus.PENDING.isPending());
        assertFalse(NameRevisionStatus.REJECTED.isPending());
    }

    /**
     * Test isApproved method of NameRevisionStatus.
     */
    @Test
    public void isApproved() {
        assertTrue (NameRevisionStatus.APPROVED.isApproved());
        assertFalse(NameRevisionStatus.CANCELLED.isApproved());
        assertFalse(NameRevisionStatus.PENDING.isApproved());
        assertFalse(NameRevisionStatus.REJECTED.isApproved());
    }

    /**
     * Test isCancelled method of NameRevisionStatus.
     */
    @Test
    public void isCancelled() {
        assertFalse(NameRevisionStatus.APPROVED.isCancelled());
        assertTrue (NameRevisionStatus.CANCELLED.isCancelled());
        assertFalse(NameRevisionStatus.PENDING.isCancelled());
        assertTrue (NameRevisionStatus.REJECTED.isCancelled());
    }

    /**
     * Test isRejected method of NameRevisionStatus.
     */
    @Test
    public void isRejected() {
        assertFalse(NameRevisionStatus.APPROVED.isRejected());
        assertFalse(NameRevisionStatus.CANCELLED.isRejected());
        assertFalse(NameRevisionStatus.PENDING.isRejected());
        assertTrue (NameRevisionStatus.REJECTED.isRejected());
    }

}
