/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.model.UserAccount;
import org.openepics.names.model.UtilityModel;

/**
 * Unit tests for UserAction class.
 *
 * @author Lars Johansson
 *
 * @see UserAction
 */
public class UserActionTest {

    private static final Date DATE1 = new Date();

    private static final String ASDF = "asdf";
    private static final String ZXCV = "zxcv";

    private static UserAccount userAccountEditor;
    private static UserAccount userAccountSuperUser;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        userAccountEditor    = UtilityModel.createUserAccountEditor   (ASDF);
        userAccountSuperUser = UtilityModel.createUserAccountSuperUser(ZXCV);
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        userAccountEditor    = null;
        userAccountSuperUser = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test constructor of UserAction.
     */
    @Test
    public void constructorUserActionEditor() {
        UserAction userAction = new UserAction(DATE1, ZXCV, userAccountEditor);

        assertNotNull(userAction);
    }

    /**
     * Test constructor of UserAction.
     */
    @Test
    public void constructorUserActionSuperUser() {
        UserAction userAction = new UserAction(null,  ASDF, userAccountSuperUser);

        assertNotNull(userAction);
    }

}
