/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for NameElement class.
 *
 * @author Lars Johansson
 *
 * @see NameElement
 */
public class NameElementTest {

    private static final String EMPTY = "";
    private static final String ONE   = "1";
    private static final String ZERO  = "0";

    private static final String FULLNAME_0_MNEMONIC_0 = "0 (0)";
    private static final String FULLNAME_0_MNEMONIC_1 = "0 (1)";
    private static final String FULLNAME_1_MNEMONIC_0 = "1 (0)";
    private static final String FULLNAME_1_MNEMONIC_1 = "1 (1)";
    private static final String MNEMONIC_0 = "(0)";

    private static NameElement nameElement_000;
    private static NameElement nameElement_001;
    private static NameElement nameElement_010;
    private static NameElement nameElement_011;
    private static NameElement nameElement_100;
    private static NameElement nameElement_101;
    private static NameElement nameElement_110;
    private static NameElement nameElement_111;

    private static NameElement nameElement;
    private static NameElement nameElementCopy;

    private static NameElement nameElementNull_000;
    private static NameElement nameElementNull_001;
    private static NameElement nameElementNull_010;
    private static NameElement nameElementNull_011;
    private static NameElement nameElementNull_100;
    private static NameElement nameElementNull_101;
    private static NameElement nameElementNull_110;
    private static NameElement nameElementNull_111;

    private static NameElement nameElementEmpty_000;
    private static NameElement nameElementEmpty_001;
    private static NameElement nameElementEmpty_010;
    private static NameElement nameElementEmpty_011;
    private static NameElement nameElementEmpty_100;
    private static NameElement nameElementEmpty_101;
    private static NameElement nameElementEmpty_110;
    private static NameElement nameElementEmpty_111;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        nameElement_000 = new NameElement(ZERO, ZERO, ZERO);
        nameElement_001 = new NameElement(ZERO, ZERO, ONE);
        nameElement_010 = new NameElement(ZERO, ONE,  ZERO);
        nameElement_011 = new NameElement(ZERO, ONE,  ONE);
        nameElement_100 = new NameElement(ONE,  ZERO, ZERO);
        nameElement_101 = new NameElement(ONE,  ZERO, ONE);
        nameElement_110 = new NameElement(ONE,  ONE,  ZERO);
        nameElement_111 = new NameElement(ONE,  ONE,  ONE);

        nameElement     = new NameElement("fullName", "mnemonic", "description");
        nameElementCopy = new NameElement(ONE, ONE, ZERO);

        nameElementNull_000  = new NameElement(ZERO, ZERO, ZERO);
        nameElementNull_001  = new NameElement(ZERO, ZERO, null);
        nameElementNull_010  = new NameElement(ZERO, null, ZERO);
        nameElementNull_011  = new NameElement(ZERO, null, null);
        nameElementNull_100  = new NameElement(null, ZERO, ZERO);
        nameElementNull_101  = new NameElement(null, ZERO, null);
        nameElementNull_110  = new NameElement(null, null, ZERO);
        nameElementNull_111  = new NameElement(null, null, null);

        nameElementEmpty_000 = new NameElement(ZERO,  ZERO,  ZERO);
        nameElementEmpty_001 = new NameElement(ZERO,  ZERO,  EMPTY);
        nameElementEmpty_010 = new NameElement(ZERO,  EMPTY, ZERO);
        nameElementEmpty_011 = new NameElement(ZERO,  EMPTY, EMPTY);
        nameElementEmpty_100 = new NameElement(EMPTY, ZERO,  ZERO);
        nameElementEmpty_101 = new NameElement(EMPTY, ZERO,  EMPTY);
        nameElementEmpty_110 = new NameElement(EMPTY, EMPTY, ZERO);
        nameElementEmpty_111 = new NameElement(EMPTY, EMPTY, EMPTY);
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        nameElement_000 = null;
        nameElement_001 = null;
        nameElement_010 = null;
        nameElement_011 = null;
        nameElement_100 = null;
        nameElement_101 = null;
        nameElement_110 = null;
        nameElement_111 = null;

        nameElement     = null;
        nameElementCopy = null;

        nameElementNull_000 = null;
        nameElementNull_001 = null;
        nameElementNull_010 = null;
        nameElementNull_011 = null;
        nameElementNull_100 = null;
        nameElementNull_101 = null;
        nameElementNull_110 = null;
        nameElementNull_111 = null;

        nameElementEmpty_000 = null;
        nameElementEmpty_001 = null;
        nameElementEmpty_010 = null;
        nameElementEmpty_011 = null;
        nameElementEmpty_100 = null;
        nameElementEmpty_101 = null;
        nameElementEmpty_110 = null;
        nameElementEmpty_111 = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test constructor of NameElement.
     */
    @Test
    public void constructor() {
        NameElement nameElement1 = new NameElement(nameElement);

        assertEquals(nameElement.getFullName(),    nameElement1.getFullName());
        assertEquals(nameElement.getMnemonic(),    nameElement1.getMnemonic());
        assertEquals(nameElement.getDescription(), nameElement1.getDescription());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test setFullName method of NameElement.
     */
    @Test
    public void setFullName() {
        NameElement nameElement1 = new NameElement("a", "b", "c");

        assertEquals("a", nameElement1.getFullName());
        assertEquals("b", nameElement1.getMnemonic());
        assertEquals("c", nameElement1.getDescription());

        nameElement1.setFullName(null);
        assertNull(nameElement1.getFullName());

        nameElement1.setFullName("aa");
        assertEquals("aa", nameElement1.getFullName());

        nameElement1.setFullName(EMPTY);
        assertNull(nameElement1.getFullName());
    }

    /**
     * Test setMnemonic method of NameElement.
     */
    @Test
    public void setMnemonic() {
        NameElement nameElement1 = new NameElement("a", "b", "c");

        assertEquals("a", nameElement1.getFullName());
        assertEquals("b", nameElement1.getMnemonic());
        assertEquals("c", nameElement1.getDescription());

        nameElement1.setMnemonic(null);
        assertNull(nameElement1.getMnemonic());

        nameElement1.setMnemonic("bb");
        assertEquals("bb", nameElement1.getMnemonic());

        nameElement1.setMnemonic(EMPTY);
        assertNull(nameElement1.getMnemonic());

    }

    /**
     * Test setDescription method of NameElement.
     */
    @Test
    public void setDescription() {
        NameElement nameElement1 = new NameElement("a", "b", "c");

        assertEquals("a", nameElement1.getFullName());
        assertEquals("b", nameElement1.getMnemonic());
        assertEquals("c", nameElement1.getDescription());

        nameElement1.setDescription(null);
        assertNull(nameElement1.getDescription());

        nameElement1.setDescription("cc");
        assertEquals("cc", nameElement1.getDescription());

        nameElement1.setDescription(EMPTY);
        assertNull(nameElement1.getDescription());
    }

    /**
     * Test toString method of NameElement.
     */
    @Test
    public void toStringTest() {
        assertEquals(FULLNAME_0_MNEMONIC_0, nameElement_000.toString());
        assertEquals(FULLNAME_0_MNEMONIC_0, nameElement_001.toString());
        assertEquals(FULLNAME_0_MNEMONIC_1, nameElement_010.toString());
        assertEquals(FULLNAME_0_MNEMONIC_1, nameElement_011.toString());
        assertEquals(FULLNAME_1_MNEMONIC_0, nameElement_100.toString());
        assertEquals(FULLNAME_1_MNEMONIC_0, nameElement_101.toString());
        assertEquals(FULLNAME_1_MNEMONIC_1, nameElement_110.toString());
        assertEquals(FULLNAME_1_MNEMONIC_1, nameElement_111.toString());

        assertEquals("fullName (mnemonic)", nameElement.toString());
        assertEquals(FULLNAME_1_MNEMONIC_1, nameElementCopy.toString());

        assertEquals(FULLNAME_0_MNEMONIC_0, nameElementNull_000.toString());
        assertEquals(FULLNAME_0_MNEMONIC_0, nameElementNull_001.toString());
        assertEquals(ZERO,                  nameElementNull_010.toString());
        assertEquals(ZERO,                  nameElementNull_011.toString());
        assertEquals(MNEMONIC_0,            nameElementNull_100.toString());
        assertEquals(MNEMONIC_0,            nameElementNull_101.toString());
        assertNull(nameElementNull_110.toString());
        assertNull(nameElementNull_111.toString());

        assertEquals(FULLNAME_0_MNEMONIC_0, nameElementEmpty_000.toString());
        assertEquals(FULLNAME_0_MNEMONIC_0, nameElementEmpty_001.toString());
        assertEquals(ZERO,                  nameElementEmpty_010.toString());
        assertEquals(ZERO,                  nameElementEmpty_011.toString());
        assertEquals(MNEMONIC_0,            nameElementEmpty_100.toString());
        assertEquals(MNEMONIC_0,            nameElementEmpty_101.toString());
        assertNull(nameElementEmpty_110.toString());
        assertNull(nameElementEmpty_111.toString());
    }

    /**
     * Test toStringIndexStyle method of NameElement.
     */
    @Test
    public void toStringIndexStyle() {
        assertEquals(FULLNAME_0_MNEMONIC_0, nameElement_000.toString(null));
        assertEquals(FULLNAME_0_MNEMONIC_0, nameElement_000.toString(EMPTY));
        assertEquals("0 (0) [asdf]",        nameElement_000.toString("asdf"));

        assertEquals(ZERO,                  nameElementNull_010.toString(null));
        assertEquals(ZERO,                  nameElementNull_010.toString(EMPTY));
        assertEquals(ZERO,                  nameElementNull_010.toString("asdf"));

        assertEquals(MNEMONIC_0,            nameElementNull_100.toString(null));
        assertEquals(MNEMONIC_0,            nameElementNull_100.toString(EMPTY));
        assertEquals("(0) [asdf]",          nameElementNull_100.toString("asdf"));

        assertNull(nameElementEmpty_111.toString(null));
        assertNull(nameElementEmpty_111.toString(EMPTY));
        assertNull(nameElementEmpty_111.toString("asdf"));
    }

    /**
     * Test equals method of NameElement.
     */
    @Test
    public void equals() {
        assertFalse(nameElementCopy.equals(null));

        assertFalse(nameElementCopy.equals(nameElement_000));
        assertFalse(nameElementCopy.equals(nameElement_001));
        assertFalse(nameElementCopy.equals(nameElement_010));
        assertFalse(nameElementCopy.equals(nameElement_011));
        assertFalse(nameElementCopy.equals(nameElement_100));
        assertFalse(nameElementCopy.equals(nameElement_101));
        assertTrue (nameElementCopy.equals(nameElement_110));
        assertFalse(nameElementCopy.equals(nameElement_111));

        assertFalse(nameElementCopy.equals(nameElement));
        assertTrue (nameElementCopy.equals(nameElementCopy));
    }

    /**
     * Test hasEqualFullNameAs method of NameElement.
     */
    @Test
    public void hasEqualFullNameAs() {
        assertTrue (nameElement_000.hasEqualFullNameAs(nameElement_000));
        assertTrue (nameElement_000.hasEqualFullNameAs(nameElement_001));
        assertTrue (nameElement_000.hasEqualFullNameAs(nameElement_010));
        assertTrue (nameElement_000.hasEqualFullNameAs(nameElement_011));
        assertFalse(nameElement_000.hasEqualFullNameAs(nameElement_100));
        assertFalse(nameElement_000.hasEqualFullNameAs(nameElement_101));
        assertFalse(nameElement_000.hasEqualFullNameAs(nameElement_110));
        assertFalse(nameElement_000.hasEqualFullNameAs(nameElement_111));

        assertFalse(nameElement_000.hasEqualFullNameAs(nameElement));
        assertFalse(nameElement_000.hasEqualFullNameAs(nameElementCopy));
    }

    /**
     * Test hasEqualMnemonicAs method of NameElement.
     */
    @Test
    public void hasEqualMnemonicAs() {
        assertFalse(nameElement_010.hasEqualMnemonicAs(nameElement_000));
        assertFalse(nameElement_010.hasEqualMnemonicAs(nameElement_001));
        assertTrue (nameElement_010.hasEqualMnemonicAs(nameElement_010));
        assertTrue (nameElement_010.hasEqualMnemonicAs(nameElement_011));
        assertFalse(nameElement_010.hasEqualMnemonicAs(nameElement_100));
        assertFalse(nameElement_010.hasEqualMnemonicAs(nameElement_101));
        assertTrue (nameElement_010.hasEqualMnemonicAs(nameElement_110));
        assertTrue (nameElement_010.hasEqualMnemonicAs(nameElement_111));

        assertFalse(nameElement_010.hasEqualMnemonicAs(nameElement));
        assertTrue (nameElement_010.hasEqualMnemonicAs(nameElementCopy));
    }

    /**
     * Test hasEqualDescriptionAs method of NameElement.
     */
    @Test
    public void hasEqualDescriptionAs() {
        assertFalse(nameElement_101.hasEqualDescriptionAs(nameElement_000));
        assertTrue (nameElement_101.hasEqualDescriptionAs(nameElement_001));
        assertFalse(nameElement_101.hasEqualDescriptionAs(nameElement_010));
        assertTrue (nameElement_101.hasEqualDescriptionAs(nameElement_011));
        assertFalse(nameElement_101.hasEqualDescriptionAs(nameElement_100));
        assertTrue (nameElement_101.hasEqualDescriptionAs(nameElement_101));
        assertFalse(nameElement_101.hasEqualDescriptionAs(nameElement_110));
        assertTrue (nameElement_101.hasEqualDescriptionAs(nameElement_111));

        assertFalse(nameElement_101.hasEqualDescriptionAs(nameElement));
        assertFalse(nameElement_101.hasEqualDescriptionAs(nameElementCopy));
    }

}
