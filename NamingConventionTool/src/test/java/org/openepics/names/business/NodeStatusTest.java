/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for NodeStatus class.
 *
 * @author Lars Johansson
 *
 * @see NodeStatus
 */
public class NodeStatusTest {

    private static NodeStatus nodeStatus_000;
    private static NodeStatus nodeStatus_001;
    private static NodeStatus nodeStatus_010;
    private static NodeStatus nodeStatus_011;
    private static NodeStatus nodeStatus_100;
    private static NodeStatus nodeStatus_101;
    private static NodeStatus nodeStatus_110;
    private static NodeStatus nodeStatus_111;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        nodeStatus_000 = new NodeStatus(false, false, false);
        nodeStatus_001 = new NodeStatus(false, false, true);
        nodeStatus_010 = new NodeStatus(false, true,  false);
        nodeStatus_011 = new NodeStatus(false, true,  true);
        nodeStatus_100 = new NodeStatus(true,  false, false);
        nodeStatus_101 = new NodeStatus(true,  false, true);
        nodeStatus_110 = new NodeStatus(true,  true,  false);
        nodeStatus_111 = new NodeStatus(true,  true,  true);
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        nodeStatus_000 = null;
        nodeStatus_001 = null;
        nodeStatus_010 = null;
        nodeStatus_011 = null;
        nodeStatus_100 = null;
        nodeStatus_101 = null;
        nodeStatus_110 = null;
        nodeStatus_111 = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test isAcceptedAndFiltered method of NodeStatus.
     */
    @Test
    public void isAcceptedAndFiltered() {
        assertFalse(nodeStatus_000.isAcceptedAndFiltered());
        assertFalse(nodeStatus_001.isAcceptedAndFiltered());
        assertFalse(nodeStatus_010.isAcceptedAndFiltered());
        assertTrue (nodeStatus_011.isAcceptedAndFiltered());
        assertFalse(nodeStatus_100.isAcceptedAndFiltered());
        assertFalse(nodeStatus_101.isAcceptedAndFiltered());
        assertFalse(nodeStatus_110.isAcceptedAndFiltered());
        assertTrue (nodeStatus_111.isAcceptedAndFiltered());
    }

}
