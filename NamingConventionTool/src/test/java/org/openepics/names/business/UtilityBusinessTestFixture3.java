/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import java.util.ArrayList;
import java.util.List;

import org.openepics.names.model.Device;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePart;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.model.NamePartRevisionStatus;
import org.openepics.names.model.UserAccount;
import org.openepics.names.model.UtilityModel;
import org.powermock.reflect.Whitebox;

/**
 * Purpose to provide test fixture that can be used in multiple test classes and tests.
 *
 * <br>
 * Intended usage is
 * <ol>
 * <li> <tt>getInstance</tt> method followed by </tt>setUp</tt> method to set up test fixture.
 * <li> <tt>get</tt> methods for tests
 * <li> <tt>tearDown</tt> method to tear down test fixture.
 * </ol>
 *
 * @author Lars Johansson
 *
 * @see UtilityBusiness
 * @see UtilityModel
 */
public class UtilityBusinessTestFixture3 {

    /*
       Test fixture for section, device type, device based on database backup Sep 10 2020.
           -----------------------------------------
           Central Services
           -----------------------------------------
           conventionNameEqClass        conventionName
           -----------------------------------------
           PB1-FPM1:CTR1-ECAT-100       PBI-FPM01:Ctrl-ECAT-100
           PB1-FPM1:CTR1-ECATC-101      PBI-FPM01:Ctrl-ECATC-101
           PB1-FPM1:CTR1-ECATE-101      PBI-FPM01:Ctrl-ECATE-101
           PB1-FPM1:CTR1-ECAT10-101     PBI-FPM01:Ctrl-ECATIO-101
           PB1-FPM1:CTR1-ECAT10-102     PBI-FPM01:Ctrl-ECATIO-102
           PB1-FPM1:CTR1-ECAT10-103     PBI-FPM01:Ctrl-ECATIO-103
           PB1-FPM1:CTR1-ECAT10-104     PBI-FPM01:Ctrl-ECATIO-104
           PB1-FPM1:CTR1-EVR-101        PBI-FPM01:Ctrl-EVR-101
           PB1-FPM1:CTR1-EVR-201        PBI-FPM01:Ctrl-EVR-201
           PB1-FPM1:CTR1-1PC-100        PBI-FPM01:Ctrl-IPC-100
           PB1-FPM1:CTR1-1PC-200        PBI-FPM01:Ctrl-IPC-200
           -----------------------------------------
           mnemonic path
           -----------------------------------------
           Acc
           Acc-FEB
           Acc-FEB-030Row
                          (empty)
           PBI
           PBI-FPM01
           Ctrl
           Ctrl
           Ctrl-ECATC
           Ctrl
           Ctrl-ECATIO
           Ctrl
           Ctrl-ECAT
           Ctrl-IPC
           Ctrl
           Ctrl-ECATE
           Ctrl
           Ctrl-EVR
           PBI
           PBI
           PBI-ECATC
           PBI-ECAT
           PBI-ECATIO
           PBI-IPC
           PBI-EVR
           TS
           TS
           TS-EVR
           -----------------------------------------
           names have history, have been moved from Accelerator to Central Services

       Note
           includes name revision hierarchy/history for section, device type, device.
           section, device type, device are associated
           purpose for selection of names
               include name, eq name, move of name in system structure and device structure, history
    */

    private static final String TIMING_SYSTEM2              = "Timing System.";
    private static final String TIMING_SYSTEM               = "Timing System";
    private static final String PROTON_BEAM_INSTRUMENTATION = "Proton Beam Instrumentation";

    private static final String PBI_NPM_02_LEBT_NPM2_IPC                = "PBI NPM-02 - LEBT NPM2 IPC";
    private static final String PBI_NPM_02_LEBT_NPM2_IPC_EVENT_RECEIVER = "PBI NPM-02 - LEBT NPM2 IPC event receiver";
    private static final String PBI_NPM_01_LEBT_NPM1_IPC                = "PBI NPM-01 - LEBT NPM1 IPC";
    private static final String PBI_NPM_01_LEBT_NPM1_IPC_EVENT_RECEIVER = "PBI NPM-01 - LEBT NPM1 IPC event receiver";
    private static final String PBI_FPM_01_LEBT_NPM2_IPC_EVENT_RECEIVER = "PBI FPM-01 - LEBT NPM2 IPC event receiver";
    private static final String PBI_FPM_01_LEBT_NPM2_ETHERCAT_MODULE    = "PBI FPM-01 - LEBT NPM2 Ethercat module";
    private static final String PBI_FPM_01_LEBT_NPM1_IPC_EVENT_RECEIVER = "PBI FPM-01 - LEBT NPM1 IPC event receiver";
    private static final String PBI_FPM_01_LEBT_NPM1_ETHERCAT_MODULE    = "PBI FPM-01 - LEBT NPM1 Ethercat module";
    private static final String PBI_FPM_01_LEBT_NPM_ETHERCAT_CRATE      = "PBI FPM-01 - LEBT NPM Ethercat crate";
    private static final String PBI_FPM_01_LEBT_NPM_ETHERCAT_COUPLER    = "PBI FPM-01 - LEBT NPM Ethercat coupler";

    private static final String PBI_ETHERCAT_CRATE          = "PBI Ethercat crate";
    private static final String PBI_ETHERCAT_I_O            = "PBI Ethercat I/O";
    private static final String ONCE_THE_TD_INSTANCES_ARE_CHANGED_FROM_TS_TO_CTRL =
            "Once the TD instances are changed from TS to Ctrl, "
            + "then this discipline is not valid anymore and can be deleted.";
    private static final String NOT_NECESSARY_ANYMORE       = "Not necessary anymore.";
    private static final String MOVED_TO_CTRL_DISCIPLINE    = "Moved to Ctrl discipline";
    private static final String MNEMONICS_FOR_SUPER_SECTION_IS_NOT_PART_OF_NAMES_AND_HAVE_BEEN_REMOVED =
            "Mnemonics for Super section is not part of names and have been removed "
            + "in order not to confuse users of the naming convention";
    private static final String MRF_EVENT_RECEIVER          = "MRF Event Receiver";
    private static final String MRF_EVENT_RECEIVER_AMC_SYNTHESIZES_TRIGGER_EVENTS_AND_DATA_BUFFER =
            "MRF Event Receiver (AMC) synthesizes trigger events and data buffer from the timing network.";
    private static final String JN_20200331                 = "JN 20200331";
    private static final String INITIAL_DATA                = "Initial data";
    private static final String INFRASTRUCTURE              = "Infrastructure";
    private static final String INFRASTRUCTURE_COMPONENTS   = "Infrastructure Components.";
    private static final String INDUSTRIAL_PC               = "Industrial PC";

    private static final String FEB_30R0W_PB1_EVR_9         = "FEB-30R0W:PB1-EVR-9";
    private static final String FEB_30R0W_PB1_EVR_13        = "FEB-30R0W:PB1-EVR-13";
    private static final String FEB_30R0W_PB1_ECAT10_24     = "FEB-30R0W:PB1-ECAT10-24";
    private static final String FEB_30R0W_PB1_ECAT10_23     = "FEB-30R0W:PB1-ECAT10-23";
    private static final String FEB_30R0W_PB1_ECAT10_22     = "FEB-30R0W:PB1-ECAT10-22";
    private static final String FEB_30R0W_PB1_ECAT10_21     = "FEB-30R0W:PB1-ECAT10-21";
    private static final String FEB_30R0W_PB1_ECAT_20       = "FEB-30R0W:PB1-ECAT-20";
    private static final String FEB_30R0W_PB1_1PC_3         = "FEB-30R0W:PB1-1PC-3";
    private static final String FEB_30R0W_PB1_1PC_2         = "FEB-30R0W:PB1-1PC-2";
    private static final String FEB_030_ROW_PBI_IPC_003     = "FEB-030Row:PBI-IPC-003";
    private static final String FEB_030_ROW_PBI_IPC_002     = "FEB-030Row:PBI-IPC-002";
    private static final String FEB_030_ROW_PBI_EVR_013     = "FEB-030Row:PBI-EVR-013";
    private static final String FEB_030_ROW_PBI_EVR_009     = "FEB-030Row:PBI-EVR-009";
    private static final String FEB_030_ROW_PBI_ECATIO_024  = "FEB-030Row:PBI-ECATIO-024";
    private static final String FEB_030_ROW_PBI_ECATIO_023  = "FEB-030Row:PBI-ECATIO-023";
    private static final String FEB_030_ROW_PBI_ECATIO_022  = "FEB-030Row:PBI-ECATIO-022";
    private static final String FEB_030_ROW_PBI_ECATIO_021  = "FEB-030Row:PBI-ECATIO-021";
    private static final String FEB_030_ROW_PBI_ECAT_020    = "FEB-030Row:PBI-ECAT-020";

    private static final String EVENT_RECEIVER              = "Event Receiver";
    private static final String ETHERCAT_INPUT_OUTPUT_NODE  = "EtherCAT Input / Output Node";
    private static final String ETHERCAT_CRATE              = "EtherCAT Crate";
    private static final String ETHERCAT_COUPLER            = "EtherCAT Coupler";
    private static final String ECATIO                      = "ECATIO";
    private static final String ECATC                       = "ECATC";
    private static final String ECAT10                      = "ECAT10";

    private static final String DATE_2020_04_08_09_21_41    = "2020-04-08 09:21:41";
    private static final String DATE_2020_04_03_14_46_01    = "2020-04-03 14:46:01";
    private static final String DATE_2020_04_03_10_17_30    = "2020-04-03 10:17:30";
    private static final String DATE_2019_05_03_15_43_14    = "2019-05-03 15:43:14";
    private static final String DATE_2018_03_13_15_01_05    = "2018-03-13 15:01:05";
    private static final String DATE_2017_08_31_09_24_22    = "2017-08-31 09:24:22";
    private static final String DATE_2016_12_16_16_28_54    = "2016-12-16 16:28:54";
    private static final String DATE_2014_04_04_17_52_59    = "2014-04-04 17:52:59";

    private static final String CONTROL_SYSTEM              = "Control System";
    private static final String CENTRAL_SERVICES            = "Central Services";
    private static final String AUTOMATICALLY_CANCELLED_BEFORE_MODIFY = "Automatically cancelled before modify";
    private static final String APPROVED_BY_DANIEL_PISO     = "Approved by Daniel Piso";
    private static final String APPROVED_BY_ALFIO2          = "approved by alfio";
    private static final String APPROVED_BY_ALFIO           = "Approved by Alfio";
    private static final String APPROVED_AFTER_MOVING_TO_CTRL_DSICPLINE_ALFIO =
            "Approved after moving to Ctrl Dsicpline. Alfio";
    private static final String ACCELERATOR                 = "Accelerator";

    private static final String NAME_30R0W                  = "30R0W";
    private static final String NAME_03_RACK_ROW            = "03 Rack Row";

    // ---

    public static final String ASDF = "asdf";
    public static final String ZXCV = "zxcv";

    private static final String ID = "id";

    // ----------------------------------------------------------------------------------------------------

    private static UtilityBusinessTestFixture3 instance;

    /**
     * Returns the singleton instance of this class.
     *
     * @return the singletone instance
     */
    public static synchronized UtilityBusinessTestFixture3 getInstance() {
        if (instance == null) {
            instance = new UtilityBusinessTestFixture3();
        }
        return instance;
    }

    /**
     * Constructor to be used only from within this class.
     */
    private UtilityBusinessTestFixture3() {
    }

    /**
     * One-time initialization code.
     */
    public void setUp() {
        userAccountEditor    = UtilityModel.createUserAccountEditor   (ASDF);
        userAccountSuperUser = UtilityModel.createUserAccountSuperUser(ZXCV);

        setupNameRevisionSection();
        setupNameRevisionDeviceType();
        setupNameRevisionDevice();
    }

    /**
     * One-time cleanup code.
     */
    public void tearDown() {
        userAccountEditor    = null;
        userAccountSuperUser = null;

        tearDownNameRevisionSection();
        tearDownNameRevisionDeviceType();
        tearDownNameRevisionDevice();
    }

    // ----------------------------------------------------------------------------------------------------

    private UserAccount userAccountEditor;
    private UserAccount userAccountSuperUser;

    public UserAccount getUserAccountEditor()    { return userAccountEditor;    }
    public UserAccount getUserAccountSuperUser() { return userAccountSuperUser; }

    // ----------------------------------------------------------------------------------------------------
    // section
    //     System Group        Accelerator                       ACC        name part id 1        3 name part revisions
    //     System              Front End Building                FEB        name part id 1052     1 name part revision
    //     Subsystem           03 Rack Row                       030Row     name part id 1187     3 name part revisions
    //     ----------
    //     FEB-030Row
    public static final String UUID_NAMEPART_1_SYSTEMGROUP_ACC              = "4262e1e7-2444-412e-83d7-aeabf58262c6";
    public static final String UUID_NAMEPART_1052_SYSTEM_FEB                = "08e4b66b-d503-4888-b9ae-4d6b0524231b";
    public static final String UUID_NAMEPART_1187_SUBSYSSTEM_030ROW         = "a15f3d27-9dc5-464b-90cb-c9731fe97be8";

    private List<NamePartRevision> namePartRevisionsSystemGroupAcc1;
    private List<NameRevision>     nameRevisionsSystemGroupAcc1;

    private List<NamePartRevision> namePartRevisionsSystemFEB1052;
    private List<NameRevision>     nameRevisionsSystemFEB1052;

    private List<NamePartRevision> namePartRevisionsSubsystem030ROW1187;
    private List<NameRevision>     nameRevisionsSubsystem030ROW1187;

    // section
    //     System Group        Central Services                  ---        name part id 4        3 name part revisions
    //     System              Proton Beam Instrumentation       PBI        name part id 2774     2 name part revisions
    //     Subsystem           PBI FPM-01 - PBI control group    FPM01      name part id 2806     1 name part revision
    //     ----------
    //     PBI-FPM01
    public static final String UUID_NAMEPART_4_SYSTEMGROUP_CENTRAL_SERVICES = "a796360b-2634-4499-944b-8bd5c46aea16";
    public static final String UUID_NAMEPART_2774_SYSTEM_PBI                = "b33b67d8-c518-4b98-9b62-312114747a66";
    public static final String UUID_NAMEPART_2806_SUBSYSSTEM_FPM01          = "651be684-df5d-434c-b24b-159429108725";

    private List<NamePartRevision> namePartRevisionsSystemGroupCentralServices4;
    private List<NameRevision>     nameRevisionsSystemGroupCentralServices4;

    private List<NamePartRevision> namePartRevisionsSystemPBI2774;
    private List<NameRevision>     nameRevisionsSystemPBI2774;

    private List<NamePartRevision> namePartRevisionsSubsystemFPM012806;
    private List<NameRevision>     nameRevisionsSubsystemFPM012806;

    // ----------------------------------------------------------------------------------------------------
    // device type
    //     Discipline          Proton Beam Instrumentation       PBI        name part id 237      1 name part revision
    //     Discipline          Timing System                     TS         name part id 1524     7 name part revisions
    //     Device Group        Control electronics                          name part id 1435     4 name part revisions
    //     Device Group        Infrastructure                               name part id 1989     6 name part revisions
    //     Device Type         Industrial PC                     IPC        name part id 1454     1 name part revision
    //     Device Type         EtherCAT Input / Output Node      ECATIO     name part id 1455     4 name part revisions
    //     Device Type         EtherCAT Crate                    ECAT       name part id 1471     3 name part revisions
    //     Device Type         Timing receiver                   EVR        name part id 1808     2 name part revisions
    //     Device Type         Event Receiver                    EVR        name part id 1912     8 name part revisions
    //     Device Type         EtherCAT Coupler                  ECATC      name part id 2066     2 name part revisions
    //     ----------
    public static final String UUID_NAMEPART_237_DISCIPLINE_PBI                      = "bd8e2820-b42e-4ec7-97f9-b0b898af8f1f";
    public static final String UUID_NAMEPART_1524_DISCIPLINE_TS                      = "6a2d1943-4a1a-45ef-a831-728ae27d0d11";

    public static final String UUID_NAMEPART_1435_DEVICEGROUP_CONTROL_ELECTRONICS    = "5fa9dcf1-2f6f-4805-99b7-e7327735f0a6";
    public static final String UUID_NAMEPART_1909_DEVICEGROUP_INFRASTRUCTURE         = "14f21253-29ae-45b6-9055-2db829d152e6";

    public static final String UUID_NAMEPART_1454_DEVICETYPE_IPC                     = "22dc39c7-e298-4e0d-8a66-7af653ed3f8c";
    public static final String UUID_NAMEPART_1455_DEVICETYPE_ECATIO                  = "a5477e15-a0c5-4030-89af-9684f1387682";
    public static final String UUID_NAMEPART_1471_DEVICETYPE_ECAT                    = "355e1fd7-8094-42a7-ac5a-d43297e5ae31";
    public static final String UUID_NAMEPART_1808_DEVICETYPE_EVR                     = "3282adb9-4968-44c8-b5eb-5c9c190f651a";
    public static final String UUID_NAMEPART_1912_DEVICETYPE_EVR                     = "6108c715-955b-48cc-8b73-941616036335";
    public static final String UUID_NAMEPART_2066_DEVICETYPE_ECATC                   = "1f2b1097-5735-4a77-bd0d-0030a97d66b8";

    private List<NamePartRevision> namePartRevisionsDisciplinePBI237;
    private List<NameRevision>     nameRevisionsDisciplinePBI237;
    private List<NamePartRevision> namePartRevisionsDisciplineTS1524;
    private List<NameRevision>     nameRevisionsDisciplineTS1524;

    private List<NamePartRevision> namePartRevisionsDeviceGroupControlElectronics1435;
    private List<NameRevision>     nameRevisionsDeviceGroupControllerElectronics1435;
    private List<NamePartRevision> namePartRevisionsDeviceGroupInfrastructure1909;
    private List<NameRevision>     nameRevisionsDeviceGroupInfrastructure1909;

    private List<NamePartRevision> namePartRevisionsDeviceTypeIPC1454;
    private List<NameRevision>     nameRevisionsDeviceTypeIPC1454;
    private List<NamePartRevision> namePartRevisionsDeviceTypeECATIO1455;
    private List<NameRevision>     nameRevisionsDeviceTypeECATIO1455;
    private List<NamePartRevision> namePartRevisionsDeviceTypeECAT1471;
    private List<NameRevision>     nameRevisionsDeviceTypeECAT1471;
    private List<NamePartRevision> namePartRevisionsDeviceTypeEVR1808;
    private List<NameRevision>     nameRevisionsDeviceTypeEVR1808;
    private List<NamePartRevision> namePartRevisionsDeviceTypeEVR1912;
    private List<NameRevision>     nameRevisionsDeviceTypeEVR1912;
    private List<NamePartRevision> namePartRevisionsDeviceTypeECATC2066;
    private List<NameRevision>     nameRevisionsDeviceTypeECATC2066;

    // device type
    //     Discipline          Control Systems                   Ctrl       name part id 502      3 name part revisions
    //     Device Group        Controller electronics                       name part id 2868     1 name part revision
    //     Device Type         EtherCAT Coupler                  ECATC      name part id 2874     1 name part revision
    //     Device Group        Controller I/O modules                       name part id 2869     1 name part revision
    //     Device Type         EtherCAT Input / Output Node      ECATIO     name part id 2883     1 name part revision
    //     Device Group        Controller systems                           name part id 2867     1 name part revision
    //     Device Type         EtherCAT System                   ECAT       name part id 2892     3 name part revisions
    //     Device Type         Industrial PC                     IPC        name part id 2879     1 name part revision
    //     Device Group        Enclosures                                   name part id 2870     2 name part revisions
    //     Device Type         EtherCAT enclosure                ECATE      name part id 2877     1 name part revision
    //     Device Group        Timing System                                name part id 2893     1 name part revision
    //     Device Type         Event Receiver                    EVR        name part id 2898     1 name part revision
    //     ----------
    public static final String UUID_NAMEPART_502_DISCIPLINE_CTRL                     = "a4abce9e-9a3d-4483-8296-ec9fc1845fe0";
    public static final String UUID_NAMEPART_2867_DEVICEGROUP_CONTROLLER_SYSTEMS     = "daec4c1d-4aa0-44f1-a768-56b2cf629591";
    public static final String UUID_NAMEPART_2868_DEVICEGROUP_CONTROLLER_ELECTRONICS = "70caa068-119e-485e-87b5-c5b7f9dd335f";
    public static final String UUID_NAMEPART_2869_DEVICEGROUP_CONTROLLER_IO_MODULES  = "0ca15d3b-28e1-4b4b-9e06-60ed433f1297";
    public static final String UUID_NAMEPART_2870_DEVICEGROUP_ENCLOSURES             = "f1a6825a-2dd4-4ed2-9ad4-80eb708152d2";
    public static final String UUID_NAMEPART_2893_DEVICEGROUP_TIMING_SYSTEM          = "3020a95c-b24e-4fa6-9a81-3a79111d454c";
    public static final String UUID_NAMEPART_2874_DEVICETYPE_ECATC                   = "2623fe07-1492-490c-af96-d79474ae3570";
    public static final String UUID_NAMEPART_2877_DEVICETYPE_ECATE                   = "f5e77d90-030c-438d-95d7-6cc29f83fb10";
    public static final String UUID_NAMEPART_2879_DEVICETYPE_IPC                     = "0a8e0d8f-a32a-48fd-bdb7-8ea74cf0c373";
    public static final String UUID_NAMEPART_2883_DEVICETYPE_ECATIO                  = "eb368790-0764-4eca-a04f-810d63f3c26c";
    public static final String UUID_NAMEPART_2892_DEVICETYPE_ECAT                    = "d1c2a49e-23f1-4f53-9382-4a5472208709";
    public static final String UUID_NAMEPART_2898_DEVICETYPE_EVR                     = "05a2db55-1f92-4f16-9285-7d5c0551519d";

    private List<NamePartRevision> namePartRevisionsDisciplineCtrl502;
    private List<NameRevision>     nameRevisionsDisciplineCtrl502;

    private List<NamePartRevision> namePartRevisionDeviceGroupControllerSystems2867;
    private List<NameRevision>     nameRevisionDeviceGroupControllerSystems2867;
    private List<NamePartRevision> namePartRevisionDeviceGroupControllerElectronics2868;
    private List<NameRevision>     nameRevisionDeviceGroupControllerElectronics2868;
    private List<NamePartRevision> namePartRevisionDeviceGroupControllerIOModules2869;
    private List<NameRevision>     nameRevisionDeviceGroupControllerIOModules2869;
    private List<NamePartRevision> namePartRevisionDeviceGroupEnclosures2870;
    private List<NameRevision>     nameRevisionDeviceGroupEnclosures2870;
    private List<NamePartRevision> namePartRevisionDeviceGroupTimingSystem2893;
    private List<NameRevision>     nameRevisionDeviceGroupTimingSystem2893;

    private List<NamePartRevision> namePartRevisionsDeviceTypeECATC2874;
    private List<NameRevision>     nameRevisionsDeviceTypeECATC2874;
    private List<NamePartRevision> namePartRevisionsDeviceTypeECATE2877;
    private List<NameRevision>     nameRevisionsDeviceTypeECATE2877;
    private List<NamePartRevision> namePartRevisionsDeviceTypeIPC2879;
    private List<NameRevision>     nameRevisionsDeviceTypeIPC2879;
    private List<NamePartRevision> namePartRevisionsDeviceTypeECATIO2883;
    private List<NameRevision>     nameRevisionsDeviceTypeECATIO2883;
    private List<NamePartRevision> namePartRevisionsDeviceTypeECAT2892;
    private List<NameRevision>     nameRevisionsDeviceTypeECAT2892;
    private List<NamePartRevision> namePartRevisionsDeviceTypeEVR2898;
    private List<NameRevision>     nameRevisionsDeviceTypeEVR2898;

    // ----------------------------------------------------------------------------------------------------
    // device
    //     Instance Index
    //     ----------
    //     conventionNameEqClass        conventionName
    //     -----------------------------------------
    //     PB1-FPM1:CTR1-1PC-200        PBI-FPM01:Ctrl-IPC-200              device id 4219        14 name part revisions
    //     PB1-FPM1:CTR1-1PC-100        PBI-FPM01:Ctrl-IPC-100              device id 4251        13 name part revisions
    //     PB1-FPM1:CTR1-ECAT10-102     PBI-FPM01:Ctrl-ECATIO-102           device id 9546        11 name part revisions
    //     PB1-FPM1:CTR1-ECAT10-103     PBI-FPM01:Ctrl-ECATIO-103           device id 9555        11 name part revisions
    //     PB1-FPM1:CTR1-ECATC-101      PBI-FPM01:Ctrl-ECATC-101            device id 9567        10 name part revisions
    //     PB1-FPM1:CTR1-ECATE-101      PBI-FPM01:Ctrl-ECATE-101            device id 9590        11 name part revisions
    //     PB1-FPM1:CTR1-ECAT10-104     PBI-FPM01:Ctrl-ECATIO-104           device id 9638        11 name part revisions
    //     PB1-FPM1:CTR1-ECAT10-101     PBI-FPM01:Ctrl-ECATIO-101           device id 17422       10 name part revisions
    //     PB1-FPM1:CTR1-EVR-101        PBI-FPM01:Ctrl-EVR-101              device id 25958        9 name part revisions
    //     PB1-FPM1:CTR1-EVR-201        PBI-FPM01:Ctrl-EVR-201              device id 25965       11 name part revisions
    //     PB1-FPM1:CTR1-ECAT-100       PBI-FPM01:Ctrl-ECAT-100             device id 66031        2 name part revisions
    //     ----------
    public static final String UUID_DEVICE_4219_INSTANCEINDEX_PBI_FPM01_CTRL_IPC_200     = "0e96fdca-055a-4893-bc5b-090cfa8a69a3";
    public static final String UUID_DEVICE_4251_INSTANCEINDEX_PBI_FPM01_CTRL_IPC_100     = "97c987b5-a96f-4219-b73e-14a3107fc718";
    public static final String UUID_DEVICE_9546_INSTANCEINDEX_PBI_FPM01_CTRL_ECATIO_102  = "f1d750d1-49b6-4a13-baec-aaa41906b444";
    public static final String UUID_DEVICE_9555_INSTANCEINDEX_PBI_FPM01_CTRL_ECATIO_103  = "06ac95eb-c602-48b0-bbb8-6bdd52ebc0a8";
    public static final String UUID_DEVICE_9567_INSTANCEINDEX_PBI_FPM01_CTRL_ECATC_101   = "ae3c2fc3-9bd1-482d-a693-06488f61a52a";
    public static final String UUID_DEVICE_9590_INSTANCEINDEX_PBI_FPM01_CTRL_ECATE_101   = "8890db4b-3eac-48df-aa4c-87e2eac835b2";
    public static final String UUID_DEVICE_9638_INSTANCEINDEX_PBI_FPM01_CTRL_ECATIO_104  = "f557ea65-a0b6-4ea8-bde9-16b16088813e";
    public static final String UUID_DEVICE_17422_INSTANCEINDEX_PBI_FPM01_CTRL_ECATIO_101 = "11ea835d-0d64-48b0-b28a-9737b0e226ad";
    public static final String UUID_DEVICE_25958_INSTANCEINDEX_PBI_FPM01_CTRL_EVR_101    = "cdfe9a59-f5bc-4287-9a15-c96c3ffb9f2f";
    public static final String UUID_DEVICE_25965_INSTANCEINDEX_PBI_FPM01_CTRL_EVR_201    = "4353f277-f3c8-4816-a64c-21f03c84f69e";
    public static final String UUID_DEVICE_66031_INSTANCEINDEX_PBI_FPM01_CTRL_ECAT_100   = "c20c96dc-e7ed-4150-993f-33bd2521a909";

    private List<DeviceRevision> deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200;
    private List<NameRevision>   nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200;
    private List<DeviceRevision> deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100;
    private List<NameRevision>   nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100;
    private List<DeviceRevision> deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102;
    private List<NameRevision>   nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102;
    private List<DeviceRevision> deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103;
    private List<NameRevision>   nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103;
    private List<DeviceRevision> deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101;
    private List<NameRevision>   nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101;
    private List<DeviceRevision> deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101;
    private List<NameRevision>   nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101;
    private List<DeviceRevision> deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104;
    private List<NameRevision>   nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104;
    private List<DeviceRevision> deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101;
    private List<NameRevision>   nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101;
    private List<DeviceRevision> deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101;
    private List<NameRevision>   nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101;
    private List<DeviceRevision> deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201;
    private List<NameRevision>   nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201;
    private List<DeviceRevision> deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100;
    private List<NameRevision>   nameRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100;

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return list of name revisions for test fixture.
     *
     * @return list of name revisions for test fixture
     */
    public List<NameRevision> getNameRevisions() {
        List<NameRevision> nameRevisions = new ArrayList<>();

        // section
        nameRevisions.addAll(nameRevisionsSystemGroupAcc1);
        nameRevisions.addAll(nameRevisionsSystemFEB1052);
        nameRevisions.addAll(nameRevisionsSubsystem030ROW1187);
        // ---
        nameRevisions.addAll(nameRevisionsSystemGroupCentralServices4);
        nameRevisions.addAll(nameRevisionsSystemPBI2774);
        nameRevisions.addAll(nameRevisionsSubsystemFPM012806);

        // device type
        nameRevisions.addAll(nameRevisionsDisciplinePBI237);
        nameRevisions.addAll(nameRevisionsDisciplineTS1524);
        nameRevisions.addAll(nameRevisionsDeviceGroupControllerElectronics1435);
        nameRevisions.addAll(nameRevisionsDeviceGroupInfrastructure1909);
        nameRevisions.addAll(nameRevisionsDeviceTypeIPC1454);
        nameRevisions.addAll(nameRevisionsDeviceTypeECATIO1455);
        nameRevisions.addAll(nameRevisionsDeviceTypeECAT1471);
        nameRevisions.addAll(nameRevisionsDeviceTypeEVR1808);
        nameRevisions.addAll(nameRevisionsDeviceTypeEVR1912);
        nameRevisions.addAll(nameRevisionsDeviceTypeECATC2066);
        // ---
        nameRevisions.addAll(nameRevisionsDisciplineCtrl502);
        nameRevisions.addAll(nameRevisionDeviceGroupControllerSystems2867);
        nameRevisions.addAll(nameRevisionDeviceGroupControllerElectronics2868);
        nameRevisions.addAll(nameRevisionDeviceGroupControllerIOModules2869);
        nameRevisions.addAll(nameRevisionDeviceGroupEnclosures2870);
        nameRevisions.addAll(nameRevisionDeviceGroupTimingSystem2893);
        nameRevisions.addAll(nameRevisionsDeviceTypeECATC2874);
        nameRevisions.addAll(nameRevisionsDeviceTypeECATE2877);
        nameRevisions.addAll(nameRevisionsDeviceTypeIPC2879);
        nameRevisions.addAll(nameRevisionsDeviceTypeECATIO2883);
        nameRevisions.addAll(nameRevisionsDeviceTypeECAT2892);
        nameRevisions.addAll(nameRevisionsDeviceTypeEVR2898);

        // device
        nameRevisions.addAll(nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200);
        nameRevisions.addAll(nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100);
        nameRevisions.addAll(nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102);
        nameRevisions.addAll(nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103);
        nameRevisions.addAll(nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101);
        nameRevisions.addAll(nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101);
        nameRevisions.addAll(nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104);
        nameRevisions.addAll(nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101);
        nameRevisions.addAll(nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101);
        nameRevisions.addAll(nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201);
        nameRevisions.addAll(nameRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100);

        return nameRevisions;
    }

    /**
     * Set up test structure - section - name part revisions, name revisions.
     */
    private void setupNameRevisionSection() {
        namePartRevisionsSystemGroupAcc1     = new ArrayList<>();
        nameRevisionsSystemGroupAcc1         = new ArrayList<>();

        namePartRevisionsSystemFEB1052       = new ArrayList<>();
        nameRevisionsSystemFEB1052           = new ArrayList<>();

        namePartRevisionsSubsystem030ROW1187 = new ArrayList<>();
        nameRevisionsSubsystem030ROW1187     = new ArrayList<>();

        // ---

        namePartRevisionsSystemGroupCentralServices4 = new ArrayList<>();
        nameRevisionsSystemGroupCentralServices4     = new ArrayList<>();

        namePartRevisionsSystemPBI2774               = new ArrayList<>();
        nameRevisionsSystemPBI2774                   = new ArrayList<>();

        namePartRevisionsSubsystemFPM012806          = new ArrayList<>();
        nameRevisionsSubsystemFPM012806              = new ArrayList<>();

        // ---

        NamePart namePartSystemGroupACC  = UtilityModel.createNamePartSection(UUID_NAMEPART_1_SYSTEMGROUP_ACC);
        NamePart namePartSystemFEB       = UtilityModel.createNamePartSection(UUID_NAMEPART_1052_SYSTEM_FEB);
        NamePart namePartSubsystem030ROW = UtilityModel.createNamePartSection(UUID_NAMEPART_1187_SUBSYSSTEM_030ROW);

        Whitebox.setInternalState(namePartSystemGroupACC,             ID, 1L);
        Whitebox.setInternalState(namePartSystemFEB,                  ID, 1052L);
        Whitebox.setInternalState(namePartSubsystem030ROW,            ID, 1187L);

        // 3, 1, 3 - name part revisions

        namePartRevisionsSystemGroupAcc1.add(    UtilityModel.createNamePartRevision(namePartSystemGroupACC, UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), null, INITIAL_DATA, false, null, ACCELERATOR, "Acc", null, "ACC", UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemGroupAcc1.add(    UtilityModel.createNamePartRevision(namePartSystemGroupACC, UtilityBusiness.parseDateOrNewDate("2015-04-30 10:47:54"), userAccountSuperUser, MNEMONICS_FOR_SUPER_SECTION_IS_NOT_PART_OF_NAMES_AND_HAVE_BEEN_REMOVED, false, null, ACCELERATOR, null, null, null, UtilityBusiness.parseDateOrNewDate("2015-05-06 14:24:11"), "Mnemonics for Super Section are not part of device names and have been removed to avoid confusion. ", NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemGroupAcc1.add(    UtilityModel.createNamePartRevision(namePartSystemGroupACC, UtilityBusiness.parseDateOrNewDate("2020-07-15 10:34:28"), userAccountSuperUser, "The mnemonic was added accordging to the new naming convention. Alfio", false, null, ACCELERATOR, "Acc", "The ESS Linear Accelerator", "ACC", UtilityBusiness.parseDateOrNewDate("2020-07-15 10:36:45"), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemFEB1052.add(      UtilityModel.createNamePartRevision(namePartSystemFEB, UtilityBusiness.parseDateOrNewDate("2016-04-22 10:39:46"), userAccountSuperUser, "Added FEB, needed for the Rack Row subsections", false, namePartSystemGroupACC, "Front End Building", "FEB", null, "FEB", UtilityBusiness.parseDateOrNewDate("2016-06-28 09:57:15"), APPROVED_BY_DANIEL_PISO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsSubsystem030ROW1187.add(UtilityModel.createNamePartRevision(namePartSubsystem030ROW, UtilityBusiness.parseDateOrNewDate("2016-07-04 15:19:40"), userAccountSuperUser, null, false, namePartSystemFEB, NAME_03_RACK_ROW, "030ROW", null, NAME_30R0W, UtilityBusiness.parseDateOrNewDate("2016-07-06 10:26:55"), APPROVED_BY_DANIEL_PISO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsSubsystem030ROW1187.add(UtilityModel.createNamePartRevision(namePartSubsystem030ROW, UtilityBusiness.parseDateOrNewDate("2017-05-03 10:48:13"), userAccountSuperUser, null, false, namePartSystemFEB, NAME_03_RACK_ROW, "030Row", "Changing to CamelCase according to ESS Naming Convention", NAME_30R0W, UtilityBusiness.parseDateOrNewDate("2017-05-03 10:48:19"), null, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsSubsystem030ROW1187.add(UtilityModel.createNamePartRevision(namePartSubsystem030ROW, UtilityBusiness.parseDateOrNewDate("2017-05-03 10:48:37"), userAccountSuperUser, "Changing to CamelCase according to ESS Naming Convention", false, namePartSystemFEB, NAME_03_RACK_ROW, "030Row", null, NAME_30R0W, UtilityBusiness.parseDateOrNewDate("2017-08-21 15:15:51"), APPROVED_BY_DANIEL_PISO, NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsSystemGroupAcc1.get(0),     ID, 1L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupAcc1.get(1),     ID, 1050L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupAcc1.get(2),     ID, 5007L);
        Whitebox.setInternalState(namePartRevisionsSystemFEB1052.get(0),       ID, 1720L);
        Whitebox.setInternalState(namePartRevisionsSubsystem030ROW1187.get(0), ID, 2071L);
        Whitebox.setInternalState(namePartRevisionsSubsystem030ROW1187.get(1), ID, 2574L);
        Whitebox.setInternalState(namePartRevisionsSubsystem030ROW1187.get(2), ID, 2575L);

        nameRevisionsSystemGroupAcc1.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupAcc1.get(0)));
        nameRevisionsSystemGroupAcc1.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupAcc1.get(1)));
        nameRevisionsSystemGroupAcc1.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupAcc1.get(2)));
        nameRevisionsSystemFEB1052.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemFEB1052.get(0)));
        nameRevisionsSubsystem030ROW1187.add(UtilityBusiness.createNameRevision(namePartRevisionsSubsystem030ROW1187.get(0)));
        nameRevisionsSubsystem030ROW1187.add(UtilityBusiness.createNameRevision(namePartRevisionsSubsystem030ROW1187.get(1)));
        nameRevisionsSubsystem030ROW1187.add(UtilityBusiness.createNameRevision(namePartRevisionsSubsystem030ROW1187.get(2)));

        // ----------

        NamePart namePartSystemGroupCentralServices = UtilityModel.createNamePartSection(UUID_NAMEPART_4_SYSTEMGROUP_CENTRAL_SERVICES);
        NamePart namePartSystemPBI                  = UtilityModel.createNamePartSection(UUID_NAMEPART_2774_SYSTEM_PBI);
        NamePart namePartSubsystemFPM01             = UtilityModel.createNamePartSection(UUID_NAMEPART_2806_SUBSYSSTEM_FPM01);

        Whitebox.setInternalState(namePartSystemGroupCentralServices, ID, 4L);
        Whitebox.setInternalState(namePartSystemPBI,                  ID, 2774L);
        Whitebox.setInternalState(namePartSubsystemFPM01,             ID, 2806L);

        // 3, 2, 1 - name part revisions

        namePartRevisionsSystemGroupCentralServices4.add(UtilityModel.createNamePartRevision(namePartSystemGroupCentralServices, UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), null, INITIAL_DATA, false, null, CENTRAL_SERVICES, "CS", null, "CS", UtilityBusiness.parseDateOrNewDate(DATE_2014_04_04_17_52_59), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemGroupCentralServices4.add(UtilityModel.createNamePartRevision(namePartSystemGroupCentralServices, UtilityBusiness.parseDateOrNewDate("2015-04-30 10:48:09"), userAccountSuperUser, MNEMONICS_FOR_SUPER_SECTION_IS_NOT_PART_OF_NAMES_AND_HAVE_BEEN_REMOVED, false, null, CENTRAL_SERVICES, "CS", null, "CS", UtilityBusiness.parseDateOrNewDate("2015-04-30 10:48:33"), null, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsSystemGroupCentralServices4.add(UtilityModel.createNamePartRevision(namePartSystemGroupCentralServices, UtilityBusiness.parseDateOrNewDate("2015-04-30 10:48:33"), userAccountSuperUser, MNEMONICS_FOR_SUPER_SECTION_IS_NOT_PART_OF_NAMES_AND_HAVE_BEEN_REMOVED, false, null, CENTRAL_SERVICES, null, null, null, UtilityBusiness.parseDateOrNewDate("2015-04-30 11:44:13"), "Mnemonics for Super Section is not part of names and have been removed in order not to confuse users of the naming convention", NamePartRevisionStatus.APPROVED));
        namePartRevisionsSystemPBI2774.add(              UtilityModel.createNamePartRevision(namePartSystemPBI, UtilityBusiness.parseDateOrNewDate("2020-02-26 13:06:21"), userAccountSuperUser, "Placeholder for the section \"PBI\" so subsections can be entered. The name will be changed to PBI once Jira NT-256 is resolved. Wait with approval until the correct name is in place.", false, namePartSystemGroupCentralServices, PROTON_BEAM_INSTRUMENTATION, "PBItmp", "Placeholder for the section \"PBI\". The name will be changed to PBI once Jira NT-256 is resolved.", "PB1TMP", UtilityBusiness.parseDateOrNewDate("2020-03-27 14:03:15"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsSystemPBI2774.add(              UtilityModel.createNamePartRevision(namePartSystemPBI, UtilityBusiness.parseDateOrNewDate("2020-03-27 14:03:15"), userAccountSuperUser, "Change according to ICSHWI-3839", false, namePartSystemGroupCentralServices, PROTON_BEAM_INSTRUMENTATION, "PBI", "Proton beam instrumentation", "PB1", UtilityBusiness.parseDateOrNewDate("2020-03-27 14:14:32"), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsSubsystemFPM012806.add(         UtilityModel.createNamePartRevision(namePartSubsystemFPM01, UtilityBusiness.parseDateOrNewDate("2020-02-26 13:29:40"), userAccountSuperUser, null, false, namePartSystemPBI, "PBI FPM-01", "FPM01", "PBI FPM-01 - PBI control group", "FPM1", UtilityBusiness.parseDateOrNewDate("2020-03-27 14:14:45"), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsSystemGroupCentralServices4.get(0), ID, 4L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupCentralServices4.get(1), ID, 1051L);
        Whitebox.setInternalState(namePartRevisionsSystemGroupCentralServices4.get(2), ID, 1052L);
        Whitebox.setInternalState(namePartRevisionsSystemPBI2774.get(0),               ID, 4723L);
        Whitebox.setInternalState(namePartRevisionsSystemPBI2774.get(1),               ID, 4826L);
        Whitebox.setInternalState(namePartRevisionsSubsystemFPM012806.get(0),          ID, 4755L);

        nameRevisionsSystemGroupCentralServices4.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupCentralServices4.get(0)));
        nameRevisionsSystemGroupCentralServices4.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupCentralServices4.get(1)));
        nameRevisionsSystemGroupCentralServices4.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemGroupCentralServices4.get(2)));
        nameRevisionsSystemPBI2774.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemPBI2774.get(0)));
        nameRevisionsSystemPBI2774.add(UtilityBusiness.createNameRevision(namePartRevisionsSystemPBI2774.get(1)));
        nameRevisionsSubsystemFPM012806.add(UtilityBusiness.createNameRevision(namePartRevisionsSubsystemFPM012806.get(0)));
    }

    /**
     * Set up test structure - device type - name part revisions, name revisions.
     */
    private void setupNameRevisionDeviceType() {
        namePartRevisionsDisciplinePBI237 = new ArrayList<>();
        nameRevisionsDisciplinePBI237     = new ArrayList<>();
        namePartRevisionsDisciplineTS1524 = new ArrayList<>();
        nameRevisionsDisciplineTS1524     = new ArrayList<>();

        namePartRevisionsDeviceGroupControlElectronics1435 = new ArrayList<>();
        nameRevisionsDeviceGroupControllerElectronics1435   = new ArrayList<>();
        namePartRevisionsDeviceGroupInfrastructure1909     = new ArrayList<>();
        nameRevisionsDeviceGroupInfrastructure1909         = new ArrayList<>();

        namePartRevisionsDeviceTypeIPC1454    = new ArrayList<>();
        nameRevisionsDeviceTypeIPC1454        = new ArrayList<>();
        namePartRevisionsDeviceTypeECATIO1455 = new ArrayList<>();
        nameRevisionsDeviceTypeECATIO1455     = new ArrayList<>();
        namePartRevisionsDeviceTypeECAT1471   = new ArrayList<>();
        nameRevisionsDeviceTypeECAT1471       = new ArrayList<>();
        namePartRevisionsDeviceTypeEVR1808    = new ArrayList<>();
        nameRevisionsDeviceTypeEVR1808        = new ArrayList<>();
        namePartRevisionsDeviceTypeEVR1912    = new ArrayList<>();
        nameRevisionsDeviceTypeEVR1912        = new ArrayList<>();
        namePartRevisionsDeviceTypeECATC2066  = new ArrayList<>();
        nameRevisionsDeviceTypeECATC2066      = new ArrayList<>();

        // ---

        namePartRevisionsDisciplineCtrl502 = new ArrayList<>();
        nameRevisionsDisciplineCtrl502     = new ArrayList<>();

        namePartRevisionDeviceGroupControllerSystems2867     = new ArrayList<>();
        nameRevisionDeviceGroupControllerSystems2867         = new ArrayList<>();
        namePartRevisionDeviceGroupControllerElectronics2868 = new ArrayList<>();
        nameRevisionDeviceGroupControllerElectronics2868     = new ArrayList<>();
        namePartRevisionDeviceGroupControllerIOModules2869   = new ArrayList<>();
        nameRevisionDeviceGroupControllerIOModules2869       = new ArrayList<>();
        namePartRevisionDeviceGroupEnclosures2870            = new ArrayList<>();
        nameRevisionDeviceGroupEnclosures2870                = new ArrayList<>();
        namePartRevisionDeviceGroupTimingSystem2893          = new ArrayList<>();
        nameRevisionDeviceGroupTimingSystem2893              = new ArrayList<>();

        namePartRevisionsDeviceTypeECATC2874  = new ArrayList<>();
        nameRevisionsDeviceTypeECATC2874      = new ArrayList<>();
        namePartRevisionsDeviceTypeECATE2877  = new ArrayList<>();
        nameRevisionsDeviceTypeECATE2877      = new ArrayList<>();
        namePartRevisionsDeviceTypeIPC2879    = new ArrayList<>();
        nameRevisionsDeviceTypeIPC2879        = new ArrayList<>();
        namePartRevisionsDeviceTypeECATIO2883 = new ArrayList<>();
        nameRevisionsDeviceTypeECATIO2883     = new ArrayList<>();
        namePartRevisionsDeviceTypeECAT2892   = new ArrayList<>();
        nameRevisionsDeviceTypeECAT2892       = new ArrayList<>();
        namePartRevisionsDeviceTypeEVR2898    = new ArrayList<>();
        nameRevisionsDeviceTypeEVR2898        = new ArrayList<>();

        // ---

        NamePart namePartDisciplinePBI237                  = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_237_DISCIPLINE_PBI);
        NamePart namePartDisciplineTS1524                  = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1524_DISCIPLINE_TS);
        NamePart namePartDeviceGroupControlElectronics1435 = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1435_DEVICEGROUP_CONTROL_ELECTRONICS);
        NamePart namePartDeviceGroupInfrastructure1909     = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1909_DEVICEGROUP_INFRASTRUCTURE);
        NamePart namePartDeviceTypeIPC1454                 = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1454_DEVICETYPE_IPC);
        NamePart namePartDeviceTypeECATIO1455              = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1455_DEVICETYPE_ECATIO);
        NamePart namePartDeviceTypeECAT1471                = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1471_DEVICETYPE_ECAT);
        NamePart namePartDeviceTypeEVR1808                 = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1808_DEVICETYPE_EVR);
        NamePart namePartDeviceTypeEVR1912                 = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_1912_DEVICETYPE_EVR);
        NamePart namePartDeviceTypeECATC2066               = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2066_DEVICETYPE_ECATC);

        Whitebox.setInternalState(namePartDisciplinePBI237,                   ID, 237L);
        Whitebox.setInternalState(namePartDisciplineTS1524,                   ID, 1524L);
        Whitebox.setInternalState(namePartDeviceGroupControlElectronics1435,  ID, 1435L);
        Whitebox.setInternalState(namePartDeviceGroupInfrastructure1909,      ID, 1909L);
        Whitebox.setInternalState(namePartDeviceTypeIPC1454,                  ID, 1454L);
        Whitebox.setInternalState(namePartDeviceTypeECATIO1455,               ID, 1455L);
        Whitebox.setInternalState(namePartDeviceTypeECAT1471,                 ID, 1471L);
        Whitebox.setInternalState(namePartDeviceTypeEVR1808,                  ID, 1808L);
        Whitebox.setInternalState(namePartDeviceTypeEVR1912,                  ID, 1912L);
        Whitebox.setInternalState(namePartDeviceTypeECATC2066,                ID, 2066L);

        // ---

        namePartRevisionsDisciplinePBI237.add(UtilityModel.createNamePartRevision(namePartDisciplinePBI237, UtilityBusiness.parseDateOrNewDate("2014-04-04 17:53:02"), null, INITIAL_DATA, false, null, PROTON_BEAM_INSTRUMENTATION, "PBI", null, "PB1", UtilityBusiness.parseDateOrNewDate("2014-04-04 17:53:02"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDisciplineTS1524.add(UtilityModel.createNamePartRevision(namePartDisciplineTS1524, UtilityBusiness.parseDateOrNewDate("2017-01-19 09:28:53"), userAccountSuperUser, "Adding device structure for timing system.\n" +
                "Approved letter codes by Remy & Timo.", false, null, TIMING_SYSTEM, "TmgS", null, "TMGS", UtilityBusiness.parseDateOrNewDate("2017-01-23 20:29:36"), APPROVED_BY_DANIEL_PISO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDisciplineTS1524.add(UtilityModel.createNamePartRevision(namePartDisciplineTS1524, UtilityBusiness.parseDateOrNewDate("2017-12-06 15:44:42"), userAccountSuperUser, "Fix Timing discipline", false, null, TIMING_SYSTEM, "TS", "Timing discipline", "TS", UtilityBusiness.parseDateOrNewDate("2017-12-06 15:47:47"), ":)", NamePartRevisionStatus.APPROVED));
        namePartRevisionsDisciplineTS1524.add(UtilityModel.createNamePartRevision(namePartDisciplineTS1524, UtilityBusiness.parseDateOrNewDate("2020-01-17 10:16:50"), userAccountSuperUser, "Update", false, null, TIMING_SYSTEM, "TS", "Timing System - Discipline", "TS", UtilityBusiness.parseDateOrNewDate("2020-01-22 10:16:45"), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDisciplineTS1524.add(UtilityModel.createNamePartRevision(namePartDisciplineTS1524, UtilityBusiness.parseDateOrNewDate("2020-01-27 09:48:54"), userAccountSuperUser, "Update.", false, null, TIMING_SYSTEM, "TS", TIMING_SYSTEM2, "TS", UtilityBusiness.parseDateOrNewDate("2020-01-27 10:06:43"), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDisciplineTS1524.add(UtilityModel.createNamePartRevision(namePartDisciplineTS1524, UtilityBusiness.parseDateOrNewDate("2020-04-16 11:48:45"), userAccountSuperUser, ONCE_THE_TD_INSTANCES_ARE_CHANGED_FROM_TS_TO_CTRL, true,  null, TIMING_SYSTEM, "TS", TIMING_SYSTEM2, "TS", UtilityBusiness.parseDateOrNewDate("2020-04-16 20:51:31"), "otherwise the device cannot be renamed, Alfio", NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDisciplineTS1524.add(UtilityModel.createNamePartRevision(namePartDisciplineTS1524, UtilityBusiness.parseDateOrNewDate("2020-04-17 09:25:26"), userAccountSuperUser, NOT_NECESSARY_ANYMORE, true,  null, TIMING_SYSTEM, "TS", TIMING_SYSTEM2, "TS", UtilityBusiness.parseDateOrNewDate("2020-04-17 10:15:14"), ".", NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDisciplineTS1524.add(UtilityModel.createNamePartRevision(namePartDisciplineTS1524, UtilityBusiness.parseDateOrNewDate("2020-04-20 10:58:46"), userAccountSuperUser, "deleted discipline. Alfio", true,  null, TIMING_SYSTEM, "TS", TIMING_SYSTEM2, "TS", UtilityBusiness.parseDateOrNewDate("2020-04-20 11:00:05"), APPROVED_AFTER_MOVING_TO_CTRL_DSICPLINE_ALFIO, NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsDisciplinePBI237.get(0), ID, 237L);
        Whitebox.setInternalState(namePartRevisionsDisciplineTS1524.get(0), ID, 2480L);
        Whitebox.setInternalState(namePartRevisionsDisciplineTS1524.get(1), ID, 3133L);
        Whitebox.setInternalState(namePartRevisionsDisciplineTS1524.get(2), ID, 4628L);
        Whitebox.setInternalState(namePartRevisionsDisciplineTS1524.get(3), ID, 4712L);
        Whitebox.setInternalState(namePartRevisionsDisciplineTS1524.get(4), ID, 4917L);
        Whitebox.setInternalState(namePartRevisionsDisciplineTS1524.get(5), ID, 4929L);
        Whitebox.setInternalState(namePartRevisionsDisciplineTS1524.get(6), ID, 4936L);

        nameRevisionsDisciplinePBI237.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplinePBI237.get(0)));
        nameRevisionsDisciplineTS1524.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineTS1524.get(0)));
        nameRevisionsDisciplineTS1524.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineTS1524.get(1)));
        nameRevisionsDisciplineTS1524.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineTS1524.get(2)));
        nameRevisionsDisciplineTS1524.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineTS1524.get(3)));
        nameRevisionsDisciplineTS1524.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineTS1524.get(4)));
        nameRevisionsDisciplineTS1524.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineTS1524.get(5)));
        nameRevisionsDisciplineTS1524.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineTS1524.get(6)));

        // ---

        namePartRevisionsDeviceGroupControlElectronics1435.add(UtilityModel.createNamePartRevision(namePartDeviceGroupControlElectronics1435, UtilityBusiness.parseDateOrNewDate("2016-10-14 16:01:40"), userAccountSuperUser, null, false, namePartDisciplinePBI237, "Control", null, null, null, UtilityBusiness.parseDateOrNewDate("2016-10-20 11:21:09"), APPROVED_BY_DANIEL_PISO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceGroupControlElectronics1435.add(UtilityModel.createNamePartRevision(namePartDeviceGroupControlElectronics1435, UtilityBusiness.parseDateOrNewDate("2017-10-12 12:30:56"), userAccountSuperUser, null, false, namePartDisciplinePBI237, "Control electronics", null, null, null, UtilityBusiness.parseDateOrNewDate("2017-10-12 13:07:45"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceGroupControlElectronics1435.add(UtilityModel.createNamePartRevision(namePartDeviceGroupControlElectronics1435, UtilityBusiness.parseDateOrNewDate("2017-10-12 13:07:45"), userAccountSuperUser, null, false, namePartDisciplinePBI237, "BPM front end unit", null, "BPMFE", null, UtilityBusiness.parseDateOrNewDate("2017-10-12 13:08:40"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceGroupControlElectronics1435.add(UtilityModel.createNamePartRevision(namePartDeviceGroupControlElectronics1435, UtilityBusiness.parseDateOrNewDate("2017-10-12 13:08:40"), userAccountSuperUser, null, false, namePartDisciplinePBI237, "Control electronics", null, null, null, UtilityBusiness.parseDateOrNewDate("2017-10-23 14:35:13"), "Approve requests from Inigo Alonso.", NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceGroupInfrastructure1909.add(    UtilityModel.createNamePartRevision(namePartDeviceGroupInfrastructure1909, UtilityBusiness.parseDateOrNewDate("2017-12-06 15:51:35"), userAccountSuperUser, "We are working to create the properly device name for TS", false, namePartDisciplineTS1524, "Hardware", null, "TS Hardware", null, UtilityBusiness.parseDateOrNewDate("2017-12-06 15:51:42"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceGroupInfrastructure1909.add(    UtilityModel.createNamePartRevision(namePartDeviceGroupInfrastructure1909, UtilityBusiness.parseDateOrNewDate("2020-01-17 10:18:42"), userAccountSuperUser, "It is clear it is hardware as \"Device Structure\" tab defines. The new name is \"Generic\" as it will be used globally.", false, namePartDisciplineTS1524, "Generic", null, "Generic Components.", null, UtilityBusiness.parseDateOrNewDate("2020-01-22 10:27:13"), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceGroupInfrastructure1909.add(    UtilityModel.createNamePartRevision(namePartDeviceGroupInfrastructure1909, UtilityBusiness.parseDateOrNewDate("2020-01-28 09:02:08"), userAccountSuperUser, "Better name than Generic.", false, namePartDisciplineTS1524, INFRASTRUCTURE, null, INFRASTRUCTURE_COMPONENTS, null, UtilityBusiness.parseDateOrNewDate("2020-01-28 18:50:40"), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceGroupInfrastructure1909.add(    UtilityModel.createNamePartRevision(namePartDeviceGroupInfrastructure1909, UtilityBusiness.parseDateOrNewDate("2020-04-16 11:48:47"), userAccountSuperUser, ONCE_THE_TD_INSTANCES_ARE_CHANGED_FROM_TS_TO_CTRL, true, namePartDisciplineTS1524, INFRASTRUCTURE, null, INFRASTRUCTURE_COMPONENTS, null, UtilityBusiness.parseDateOrNewDate("2020-04-16 20:52:34"), "otherwise the devices cannot be renamed. Alfio", NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceGroupInfrastructure1909.add(    UtilityModel.createNamePartRevision(namePartDeviceGroupInfrastructure1909, UtilityBusiness.parseDateOrNewDate("2020-04-17 09:25:27"), userAccountSuperUser, NOT_NECESSARY_ANYMORE, true, namePartDisciplineTS1524, INFRASTRUCTURE, null, INFRASTRUCTURE_COMPONENTS, null, UtilityBusiness.parseDateOrNewDate("2020-04-17 10:15:28"), ".", NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceGroupInfrastructure1909.add(    UtilityModel.createNamePartRevision(namePartDeviceGroupInfrastructure1909, UtilityBusiness.parseDateOrNewDate("2020-04-20 10:58:23"), userAccountSuperUser, "delete group disicpline. Alfio", true, namePartDisciplineTS1524, INFRASTRUCTURE, null, INFRASTRUCTURE_COMPONENTS, null, UtilityBusiness.parseDateOrNewDate("2020-04-20 11:00:08 "), APPROVED_AFTER_MOVING_TO_CTRL_DSICPLINE_ALFIO, NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsDeviceGroupControlElectronics1435.get(0), ID, 2370L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupControlElectronics1435.get(1), ID, 2957L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupControlElectronics1435.get(2), ID, 2993L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupControlElectronics1435.get(3), ID, 2994L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupInfrastructure1909.get(0),     ID, 3137L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupInfrastructure1909.get(1),     ID, 4629L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupInfrastructure1909.get(2),     ID, 4713L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupInfrastructure1909.get(3),     ID, 4925L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupInfrastructure1909.get(4),     ID, 4933L);
        Whitebox.setInternalState(namePartRevisionsDeviceGroupInfrastructure1909.get(5),     ID, 4935L);

        nameRevisionsDeviceGroupControllerElectronics1435.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupControlElectronics1435.get(0)));
        nameRevisionsDeviceGroupControllerElectronics1435.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupControlElectronics1435.get(1)));
        nameRevisionsDeviceGroupControllerElectronics1435.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupControlElectronics1435.get(2)));
        nameRevisionsDeviceGroupControllerElectronics1435.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupControlElectronics1435.get(3)));
        nameRevisionsDeviceGroupInfrastructure1909.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupInfrastructure1909.get(0)));
        nameRevisionsDeviceGroupInfrastructure1909.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupInfrastructure1909.get(1)));
        nameRevisionsDeviceGroupInfrastructure1909.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupInfrastructure1909.get(2)));
        nameRevisionsDeviceGroupInfrastructure1909.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupInfrastructure1909.get(3)));
        nameRevisionsDeviceGroupInfrastructure1909.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupInfrastructure1909.get(4)));
        nameRevisionsDeviceGroupInfrastructure1909.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceGroupInfrastructure1909.get(5)));

        // ---

        namePartRevisionsDeviceTypeIPC1454.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeIPC1454, UtilityBusiness.parseDateOrNewDate("2016-11-08 13:55:02"), userAccountSuperUser, null, false, namePartDeviceGroupControlElectronics1435, INDUSTRIAL_PC, "IPC", null, "1PC", UtilityBusiness.parseDateOrNewDate("2016-11-09 13:31:07"), "Additions are consistent with naming policy agreed by HW&I group.", NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECATIO1455.add(UtilityModel.createNamePartRevision(namePartDeviceTypeECATIO1455, UtilityBusiness.parseDateOrNewDate("2016-11-08 13:55:51"), userAccountSuperUser, null, false, namePartDeviceGroupControlElectronics1435, ETHERCAT_INPUT_OUTPUT_NODE, ECATIO, "EtherCAT Input and Output Node", ECAT10, UtilityBusiness.parseDateOrNewDate("2016-11-09 13:31:07"), "Additions are consistent with naming policy agreed by HW&I group.", NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECATIO1455.add(UtilityModel.createNamePartRevision(namePartDeviceTypeECATIO1455, UtilityBusiness.parseDateOrNewDate("2018-04-17 09:51:53"), userAccountSuperUser, "Description edit", false, namePartDeviceGroupControlElectronics1435, ETHERCAT_INPUT_OUTPUT_NODE, ECATIO, "Ethercat Input and Output Node", ECAT10, UtilityBusiness.parseDateOrNewDate("2018-04-18 09:16:00"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceTypeECATIO1455.add(UtilityModel.createNamePartRevision(namePartDeviceTypeECATIO1455, UtilityBusiness.parseDateOrNewDate("2018-04-18 09:16:00"), userAccountSuperUser, null, false, namePartDeviceGroupControlElectronics1435, ETHERCAT_INPUT_OUTPUT_NODE, ECATIO, ETHERCAT_INPUT_OUTPUT_NODE, ECAT10, UtilityBusiness.parseDateOrNewDate("2018-04-18 10:21:39"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECATIO1455.add(UtilityModel.createNamePartRevision(namePartDeviceTypeECATIO1455, UtilityBusiness.parseDateOrNewDate("2020-04-07 10:43:47"), userAccountSuperUser, MOVED_TO_CTRL_DISCIPLINE, true, namePartDeviceGroupControlElectronics1435, ETHERCAT_INPUT_OUTPUT_NODE, ECATIO, ETHERCAT_INPUT_OUTPUT_NODE, ECAT10, UtilityBusiness.parseDateOrNewDate(DATE_2020_04_08_09_21_41), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECAT1471.add(  UtilityModel.createNamePartRevision(namePartDeviceTypeECAT1471, UtilityBusiness.parseDateOrNewDate("2016-12-01 09:19:56"), userAccountSuperUser, null, false, namePartDeviceGroupControlElectronics1435, "EtherCat Crate", "ECAT", "Ethercat Crate", "ECAT", UtilityBusiness.parseDateOrNewDate("2016-12-01 13:08:58"), APPROVED_BY_DANIEL_PISO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECAT1471.add(  UtilityModel.createNamePartRevision(namePartDeviceTypeECAT1471, UtilityBusiness.parseDateOrNewDate("2018-04-18 09:16:41"), userAccountSuperUser, "Fixed EtherCAT spelling", false, namePartDeviceGroupControlElectronics1435, ETHERCAT_CRATE, "ECAT", ETHERCAT_CRATE, "ECAT", UtilityBusiness.parseDateOrNewDate("2018-04-18 10:21:39"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECAT1471.add(  UtilityModel.createNamePartRevision(namePartDeviceTypeECAT1471, UtilityBusiness.parseDateOrNewDate("2020-04-07 10:43:23"), userAccountSuperUser, MOVED_TO_CTRL_DISCIPLINE, true, namePartDeviceGroupControlElectronics1435, ETHERCAT_CRATE, "ECAT", ETHERCAT_CRATE, "ECAT", UtilityBusiness.parseDateOrNewDate(DATE_2020_04_08_09_21_41), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeEVR1808.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1808, UtilityBusiness.parseDateOrNewDate("2017-10-12 13:04:38"), userAccountSuperUser, null, false, namePartDeviceGroupControlElectronics1435, "Timing receiver", "EVR", null, "EVR", UtilityBusiness.parseDateOrNewDate("2017-10-23 14:35:14"), "Approve requests from Inigo Alonso.", NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeEVR1808.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1808, UtilityBusiness.parseDateOrNewDate("2020-04-07 10:46:27"), userAccountSuperUser, "Moved to TS discipline", true, namePartDeviceGroupControlElectronics1435, "Timing receiver", "EVR", null, "EVR", UtilityBusiness.parseDateOrNewDate(DATE_2020_04_08_09_21_41), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeEVR1912.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1912, UtilityBusiness.parseDateOrNewDate("2017-12-06 15:54:00"), userAccountSuperUser, null, false, namePartDeviceGroupInfrastructure1909, MRF_EVENT_RECEIVER, "EVR", null, "EVR", UtilityBusiness.parseDateOrNewDate("2017-12-06 15:54:51"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeEVR1912.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1912, UtilityBusiness.parseDateOrNewDate("2020-01-15 14:49:57"), userAccountSuperUser, "Update.", false, namePartDeviceGroupInfrastructure1909, MRF_EVENT_RECEIVER, "EVR", "Event Receiver - synthesizes the timing network packets into triggers and data records.", "EVR", UtilityBusiness.parseDateOrNewDate("2020-01-15 15:17:02"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceTypeEVR1912.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1912, UtilityBusiness.parseDateOrNewDate("2020-01-15 15:17:02"), userAccountSuperUser, "Update", false, namePartDeviceGroupInfrastructure1909, EVENT_RECEIVER, "EVR", "MRF Event Receiver (AMC) - synthesizes the timing network packets into triggers and data records.", "EVR", UtilityBusiness.parseDateOrNewDate("2020-01-20 10:24:24"), "proposal automatically cancelled before delete", NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceTypeEVR1912.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1912, UtilityBusiness.parseDateOrNewDate("2020-01-20 10:24:24"), userAccountSuperUser, "EVR is not a part of the timing system.", true, namePartDeviceGroupInfrastructure1909, MRF_EVENT_RECEIVER, "EVR", null, "EVR", UtilityBusiness.parseDateOrNewDate("2020-01-20 12:44:10"), ".", NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceTypeEVR1912.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1912, UtilityBusiness.parseDateOrNewDate("2020-01-20 12:47:20"), userAccountSuperUser, "Agreed that EVR stays withing TS", false, namePartDeviceGroupInfrastructure1909, EVENT_RECEIVER, "EVR", MRF_EVENT_RECEIVER_AMC_SYNTHESIZES_TRIGGER_EVENTS_AND_DATA_BUFFER, "EVR", UtilityBusiness.parseDateOrNewDate("2020-01-22 10:26:56"), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeEVR1912.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1912, UtilityBusiness.parseDateOrNewDate("2020-04-16 11:48:46"), userAccountSuperUser, ONCE_THE_TD_INSTANCES_ARE_CHANGED_FROM_TS_TO_CTRL, true, namePartDeviceGroupInfrastructure1909, EVENT_RECEIVER, "EVR", MRF_EVENT_RECEIVER_AMC_SYNTHESIZES_TRIGGER_EVENTS_AND_DATA_BUFFER, "EVR", UtilityBusiness.parseDateOrNewDate("2020-04-16 20:53:04"), "otherwise the devices cannot be renamed. Alfio", NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceTypeEVR1912.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1912, UtilityBusiness.parseDateOrNewDate("2020-04-17 09:25:27"), userAccountSuperUser, NOT_NECESSARY_ANYMORE, true, namePartDeviceGroupInfrastructure1909, EVENT_RECEIVER, "EVR", MRF_EVENT_RECEIVER_AMC_SYNTHESIZES_TRIGGER_EVENTS_AND_DATA_BUFFER, "EVR", UtilityBusiness.parseDateOrNewDate("2020-04-17 10:15:34"), ".", NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceTypeEVR1912.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR1912, UtilityBusiness.parseDateOrNewDate("2020-04-20 10:57:50"), userAccountSuperUser, "moved to Ctrl. Alfio", true, namePartDeviceGroupInfrastructure1909, EVENT_RECEIVER, "EVR", MRF_EVENT_RECEIVER_AMC_SYNTHESIZES_TRIGGER_EVENTS_AND_DATA_BUFFER, "EVR", UtilityBusiness.parseDateOrNewDate("2020-04-20 11:00:08"), APPROVED_AFTER_MOVING_TO_CTRL_DSICPLINE_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECATC2066.add( UtilityModel.createNamePartRevision(namePartDeviceTypeECATC2066, UtilityBusiness.parseDateOrNewDate("2018-06-11 16:55:12"), userAccountSuperUser, "New component name", false, namePartDeviceGroupControlElectronics1435, ETHERCAT_COUPLER, ECATC, "EtherCAT coupler module", ECATC, UtilityBusiness.parseDateOrNewDate("2018-06-13 14:17:05"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECATC2066.add( UtilityModel.createNamePartRevision(namePartDeviceTypeECATC2066, UtilityBusiness.parseDateOrNewDate("2020-04-07 10:43:04"), userAccountSuperUser, MOVED_TO_CTRL_DISCIPLINE, true, namePartDeviceGroupControlElectronics1435, ETHERCAT_COUPLER, ECATC, "EtherCAT coupler module", ECATC, UtilityBusiness.parseDateOrNewDate("2020-04-08 09:21:40"), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsDeviceTypeIPC1454.get(0),    ID, 2392L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATIO1455.get(0), ID, 2393L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATIO1455.get(1), ID, 3387L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATIO1455.get(2), ID, 3401L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATIO1455.get(3), ID, 4897L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECAT1471.get(0),   ID, 2413L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECAT1471.get(1),   ID, 3402L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECAT1471.get(2),   ID, 4896L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1808.get(0),    ID, 2990L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1808.get(1),    ID, 4904L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1912.get(0),    ID, 3140L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1912.get(1),    ID, 4511L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1912.get(2),    ID, 4520L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1912.get(3),    ID, 4630L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1912.get(4),    ID, 4632L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1912.get(5),    ID, 4923L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1912.get(6),    ID, 4932L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR1912.get(7),    ID, 4934L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATC2066.get(0),  ID, 3412L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATC2066.get(1),  ID, 4895L);

        nameRevisionsDeviceTypeIPC1454.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeIPC1454.get(0)));
        nameRevisionsDeviceTypeECATIO1455.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATIO1455.get(0)));
        nameRevisionsDeviceTypeECATIO1455.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATIO1455.get(1)));
        nameRevisionsDeviceTypeECATIO1455.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATIO1455.get(2)));
        nameRevisionsDeviceTypeECATIO1455.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATIO1455.get(3)));
        nameRevisionsDeviceTypeECAT1471.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECAT1471.get(0)));
        nameRevisionsDeviceTypeECAT1471.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECAT1471.get(1)));
        nameRevisionsDeviceTypeECAT1471.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECAT1471.get(2)));
        nameRevisionsDeviceTypeEVR1808.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1808.get(0)));
        nameRevisionsDeviceTypeEVR1808.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1808.get(1)));
        nameRevisionsDeviceTypeEVR1912.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1912.get(0)));
        nameRevisionsDeviceTypeEVR1912.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1912.get(1)));
        nameRevisionsDeviceTypeEVR1912.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1912.get(2)));
        nameRevisionsDeviceTypeEVR1912.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1912.get(3)));
        nameRevisionsDeviceTypeEVR1912.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1912.get(4)));
        nameRevisionsDeviceTypeEVR1912.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1912.get(5)));
        nameRevisionsDeviceTypeEVR1912.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1912.get(6)));
        nameRevisionsDeviceTypeEVR1912.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR1912.get(7)));
        nameRevisionsDeviceTypeECATC2066.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATC2066.get(0)));
        nameRevisionsDeviceTypeECATC2066.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATC2066.get(1)));

        // --------------------------------------------------

        NamePart namePartDisciplineCTRL                   = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_502_DISCIPLINE_CTRL);
        NamePart namePartDeviceGroupControllerSystems     = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2867_DEVICEGROUP_CONTROLLER_SYSTEMS);
        NamePart namePartDeviceGroupControllerElectronics = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2868_DEVICEGROUP_CONTROLLER_ELECTRONICS);
        NamePart namePartDeviceGroupControllerIOModules   = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2869_DEVICEGROUP_CONTROLLER_IO_MODULES);
        NamePart namePartDeviceGroupEnclosures            = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2870_DEVICEGROUP_ENCLOSURES);
        NamePart namePartDeviceGroupTimingSystem          = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2893_DEVICEGROUP_TIMING_SYSTEM);
        NamePart namePartDeviceTypeECATC                  = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2874_DEVICETYPE_ECATC);
        NamePart namePartDeviceTypeECATE                  = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2877_DEVICETYPE_ECATE);
        NamePart namePartDeviceTypeIPC                    = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2879_DEVICETYPE_IPC);
        NamePart namePartDeviceTypeECATIO                 = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2883_DEVICETYPE_ECATIO);
        NamePart namePartDeviceTypeECAT                   = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2892_DEVICETYPE_ECAT);
        NamePart namePartDeviceTypeEVR                    = UtilityModel.createNamePartDeviceType(UUID_NAMEPART_2898_DEVICETYPE_EVR);

        Whitebox.setInternalState(namePartDisciplineCTRL,                   ID, 502L);
        Whitebox.setInternalState(namePartDeviceGroupControllerSystems,     ID, 2867L);
        Whitebox.setInternalState(namePartDeviceGroupControllerElectronics, ID, 2868L);
        Whitebox.setInternalState(namePartDeviceGroupControllerIOModules,   ID, 2869L);
        Whitebox.setInternalState(namePartDeviceGroupEnclosures,            ID, 2870L);
        Whitebox.setInternalState(namePartDeviceGroupTimingSystem,          ID, 2893L);
        Whitebox.setInternalState(namePartDeviceTypeECATC,                  ID, 2874L);
        Whitebox.setInternalState(namePartDeviceTypeECATE,                  ID, 2877L);
        Whitebox.setInternalState(namePartDeviceTypeIPC,                    ID, 2879L);
        Whitebox.setInternalState(namePartDeviceTypeECATIO,                 ID, 2883L);
        Whitebox.setInternalState(namePartDeviceTypeECAT,                   ID, 2892L);
        Whitebox.setInternalState(namePartDeviceTypeEVR,                    ID, 2898L);

        // ---

        namePartRevisionsDisciplineCtrl502.add(UtilityModel.createNamePartRevision(namePartDisciplineCTRL, UtilityBusiness.parseDateOrNewDate("2014-11-19 11:36:10"), userAccountSuperUser, "Discipline for control system devices", false, null, CONTROL_SYSTEM, "CTR", null, "CTR", UtilityBusiness.parseDateOrNewDate("2014-11-19 11:39:25"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDisciplineCtrl502.add(UtilityModel.createNamePartRevision(namePartDisciplineCTRL, UtilityBusiness.parseDateOrNewDate("2015-01-19 11:39:27"), userAccountSuperUser, "Renamed ", false, null, CONTROL_SYSTEM, "CSys", null, "CSYS", UtilityBusiness.parseDateOrNewDate("2015-01-19 11:39:54"), null, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDisciplineCtrl502.add(UtilityModel.createNamePartRevision(namePartDisciplineCTRL, UtilityBusiness.parseDateOrNewDate("2015-01-22 11:00:46"), userAccountSuperUser, null, false, null, CONTROL_SYSTEM, "Ctrl", null, "CTR1", UtilityBusiness.parseDateOrNewDate("2015-01-22 11:00:59"), null, NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsDisciplineCtrl502.get(0), ID, 584L);
        Whitebox.setInternalState(namePartRevisionsDisciplineCtrl502.get(1), ID, 677L);
        Whitebox.setInternalState(namePartRevisionsDisciplineCtrl502.get(2), ID, 799L);

        nameRevisionsDisciplineCtrl502.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineCtrl502.get(0)));
        nameRevisionsDisciplineCtrl502.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineCtrl502.get(1)));
        nameRevisionsDisciplineCtrl502.add(UtilityBusiness.createNameRevision(namePartRevisionsDisciplineCtrl502.get(2)));

        // ---

        namePartRevisionDeviceGroupControllerSystems2867.add(    UtilityModel.createNamePartRevision(namePartDeviceGroupControllerSystems, UtilityBusiness.parseDateOrNewDate("2020-03-31 21:29:40"), userAccountSuperUser, "JN proposal 20200331", false, namePartDisciplineCTRL, "Controller systems", null, "To be used when addressing the system as an abstract entity instead of the physical crate.", null, UtilityBusiness.parseDateOrNewDate("2020-04-03 10:17:28"), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionDeviceGroupControllerElectronics2868.add(UtilityModel.createNamePartRevision(namePartDeviceGroupControllerElectronics, UtilityBusiness.parseDateOrNewDate("2020-03-31 21:31:22"), userAccountSuperUser, JN_20200331, false, namePartDisciplineCTRL, "Controller electronics", null, "The electronics that make up a controller system", null, UtilityBusiness.parseDateOrNewDate(DATE_2020_04_03_10_17_30), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionDeviceGroupControllerIOModules2869.add(  UtilityModel.createNamePartRevision(namePartDeviceGroupControllerIOModules, UtilityBusiness.parseDateOrNewDate("2020-03-31 21:34:19"), userAccountSuperUser, JN_20200331, false, namePartDisciplineCTRL, "Controller I/O modules", null, "Optional.AMC,RTM,ECATIO etc...  This could be merged into electronics, but perhaps it is better with a separate group for the different types of I/O modules.", null, UtilityBusiness.parseDateOrNewDate("2020-04-03 10:17:29"), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionDeviceGroupEnclosures2870.add(           UtilityModel.createNamePartRevision(namePartDeviceGroupEnclosures, UtilityBusiness.parseDateOrNewDate("2020-03-31 21:35:04"), userAccountSuperUser, JN_20200331, false, namePartDisciplineCTRL, "Enclosures", null, "Racks, enclosures, crates, etc...", null, UtilityBusiness.parseDateOrNewDate("2020-04-03 10:17:29"), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionDeviceGroupTimingSystem2893.add(         UtilityModel.createNamePartRevision(namePartDeviceGroupTimingSystem, UtilityBusiness.parseDateOrNewDate("2020-04-16 11:33:53"), userAccountSuperUser, "Merge of TS into Ctrl.", false, namePartDisciplineCTRL, TIMING_SYSTEM, null, "Timing System Components.", null, UtilityBusiness.parseDateOrNewDate("2020-04-16 17:41:09"), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionDeviceGroupControllerSystems2867.get(0),     ID, 4827L);
        Whitebox.setInternalState(namePartRevisionDeviceGroupControllerElectronics2868.get(0), ID, 4828L);
        Whitebox.setInternalState(namePartRevisionDeviceGroupControllerIOModules2869.get(0),   ID, 4829L);
        Whitebox.setInternalState(namePartRevisionDeviceGroupEnclosures2870.get(0),            ID, 4830L);
        Whitebox.setInternalState(namePartRevisionDeviceGroupTimingSystem2893.get(0),          ID, 4908L);

        nameRevisionDeviceGroupControllerSystems2867.add(UtilityBusiness.createNameRevision(namePartRevisionDeviceGroupControllerSystems2867.get(0)));
        nameRevisionDeviceGroupControllerElectronics2868.add(UtilityBusiness.createNameRevision(namePartRevisionDeviceGroupControllerElectronics2868.get(0)));
        nameRevisionDeviceGroupControllerIOModules2869.add(UtilityBusiness.createNameRevision(namePartRevisionDeviceGroupControllerIOModules2869.get(0)));
        nameRevisionDeviceGroupEnclosures2870.add(UtilityBusiness.createNameRevision(namePartRevisionDeviceGroupEnclosures2870.get(0)));
        nameRevisionDeviceGroupTimingSystem2893.add(UtilityBusiness.createNameRevision(namePartRevisionDeviceGroupTimingSystem2893.get(0)));

        // ---

        namePartRevisionsDeviceTypeECATC2874.add( UtilityModel.createNamePartRevision(namePartDeviceTypeECATC, UtilityBusiness.parseDateOrNewDate("2020-04-01 08:52:06"), userAccountSuperUser, null, false, namePartDeviceGroupControllerElectronics, "EtherCAT", "ECAT", "Ethercat Control System", "ECAT", UtilityBusiness.parseDateOrNewDate("2020-04-01 10:53:00"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceTypeECATC2874.add( UtilityModel.createNamePartRevision(namePartDeviceTypeECATC, UtilityBusiness.parseDateOrNewDate("2020-04-01 10:53:00"), userAccountSuperUser, null, false, namePartDeviceGroupControllerElectronics, "EtherCAT system", "ECAT", "Ethercat Control System", "ECAT", UtilityBusiness.parseDateOrNewDate(DATE_2020_04_03_10_17_30), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECATC2874.add( UtilityModel.createNamePartRevision(namePartDeviceTypeECATC, UtilityBusiness.parseDateOrNewDate("2020-04-03 14:35:12"), userAccountSuperUser, null, false, namePartDeviceGroupControllerElectronics, ETHERCAT_COUPLER, ECATC, "Ethercat Coupler module", ECATC, UtilityBusiness.parseDateOrNewDate(DATE_2020_04_03_14_46_01), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECATE2877.add( UtilityModel.createNamePartRevision(namePartDeviceTypeECATE, UtilityBusiness.parseDateOrNewDate("2020-04-01 08:59:11"), userAccountSuperUser, null, false, namePartDeviceGroupEnclosures, "EtherCAT enclosure", "ECATE", "EtherCAT enclosure", "ECATE", UtilityBusiness.parseDateOrNewDate(DATE_2020_04_03_10_17_30), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeIPC2879.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeIPC, UtilityBusiness.parseDateOrNewDate("2020-04-03 14:29:24"), userAccountSuperUser, null, false, namePartDeviceGroupControllerSystems, INDUSTRIAL_PC, "IPC", INDUSTRIAL_PC,        "1PC", UtilityBusiness.parseDateOrNewDate("2020-04-03 14:30:12"), AUTOMATICALLY_CANCELLED_BEFORE_MODIFY, NamePartRevisionStatus.CANCELLED));
        namePartRevisionsDeviceTypeIPC2879.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeIPC, UtilityBusiness.parseDateOrNewDate("2020-04-03 14:30:12"), userAccountSuperUser, null, false, namePartDeviceGroupControllerSystems, INDUSTRIAL_PC, "IPC", "Industrial PC System", "1PC", UtilityBusiness.parseDateOrNewDate(DATE_2020_04_03_14_46_01), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECATIO2883.add(UtilityModel.createNamePartRevision(namePartDeviceTypeECATIO, UtilityBusiness.parseDateOrNewDate("2020-04-03 14:32:00"), userAccountSuperUser, null, false, namePartDeviceGroupControllerIOModules, ETHERCAT_INPUT_OUTPUT_NODE, ECATIO, "EtherCAT Input and Output Node", ECAT10, UtilityBusiness.parseDateOrNewDate(DATE_2020_04_03_14_46_01), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeECAT2892.add(  UtilityModel.createNamePartRevision(namePartDeviceTypeECAT, UtilityBusiness.parseDateOrNewDate("2020-04-03 14:46:50"), userAccountSuperUser, null, false, namePartDeviceGroupControllerSystems, "EtherCAT System", "ECAT", "EtherCAT control system", "ECAT", UtilityBusiness.parseDateOrNewDate("2020-04-03 14:47:37"), APPROVED_BY_ALFIO, NamePartRevisionStatus.APPROVED));
        namePartRevisionsDeviceTypeEVR2898.add(   UtilityModel.createNamePartRevision(namePartDeviceTypeEVR, UtilityBusiness.parseDateOrNewDate("2020-04-16 11:44:27"), userAccountSuperUser, ".", false, namePartDeviceGroupTimingSystem, EVENT_RECEIVER, "EVR", "MRF Event Receiver - synthesizes trigger events and data buffer from the timing network.", "EVR", UtilityBusiness.parseDateOrNewDate("2020-04-16 17:41:08"), APPROVED_BY_ALFIO2, NamePartRevisionStatus.APPROVED));

        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATC2874.get(0),  ID, 4834L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATC2874.get(1),  ID, 4838L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATC2874.get(2),  ID, 4872L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATE2877.get(0),  ID, 4837L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeIPC2879.get(0),    ID, 4864L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeIPC2879.get(1),    ID, 4866L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECATIO2883.get(0), ID, 4869L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeECAT2892.get(0),   ID, 4879L);
        Whitebox.setInternalState(namePartRevisionsDeviceTypeEVR2898.get(0),    ID, 4913L);

        nameRevisionsDeviceTypeECATC2874.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATC2874.get(0)));
        nameRevisionsDeviceTypeECATC2874.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATC2874.get(1)));
        nameRevisionsDeviceTypeECATC2874.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATC2874.get(2)));
        nameRevisionsDeviceTypeECATE2877.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATE2877.get(0)));
        nameRevisionsDeviceTypeIPC2879.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeIPC2879.get(0)));
        nameRevisionsDeviceTypeIPC2879.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeIPC2879.get(1)));
        nameRevisionsDeviceTypeECATIO2883.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECATIO2883.get(0)));
        nameRevisionsDeviceTypeECAT2892.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeECAT2892.get(0)));
        nameRevisionsDeviceTypeEVR2898.add(UtilityBusiness.createNameRevision(namePartRevisionsDeviceTypeEVR2898.get(0)));
    }

    /**
     * Set up test structure - device - name part revisions, name revisions.
     */
    private void setupNameRevisionDevice() {
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200     = new ArrayList<>();
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200       = new ArrayList<>();
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100     = new ArrayList<>();
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100       = new ArrayList<>();
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102  = new ArrayList<>();
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102    = new ArrayList<>();
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103  = new ArrayList<>();
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103    = new ArrayList<>();
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101   = new ArrayList<>();
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101     = new ArrayList<>();
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101   = new ArrayList<>();
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101     = new ArrayList<>();
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104  = new ArrayList<>();
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104    = new ArrayList<>();
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101 = new ArrayList<>();
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101   = new ArrayList<>();
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101    = new ArrayList<>();
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101      = new ArrayList<>();
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201    = new ArrayList<>();
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201      = new ArrayList<>();
        deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100   = new ArrayList<>();
        nameRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100     = new ArrayList<>();

        // ----------

        Device device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200     = UtilityModel.createDevice(UUID_DEVICE_4219_INSTANCEINDEX_PBI_FPM01_CTRL_IPC_200);
        Device device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100     = UtilityModel.createDevice(UUID_DEVICE_4251_INSTANCEINDEX_PBI_FPM01_CTRL_IPC_100);
        Device device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102  = UtilityModel.createDevice(UUID_DEVICE_9546_INSTANCEINDEX_PBI_FPM01_CTRL_ECATIO_102);
        Device device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103  = UtilityModel.createDevice(UUID_DEVICE_9555_INSTANCEINDEX_PBI_FPM01_CTRL_ECATIO_103);
        Device device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101   = UtilityModel.createDevice(UUID_DEVICE_9567_INSTANCEINDEX_PBI_FPM01_CTRL_ECATC_101);
        Device device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101   = UtilityModel.createDevice(UUID_DEVICE_9590_INSTANCEINDEX_PBI_FPM01_CTRL_ECATE_101);
        Device device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104  = UtilityModel.createDevice(UUID_DEVICE_9638_INSTANCEINDEX_PBI_FPM01_CTRL_ECATIO_104);
        Device device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101 = UtilityModel.createDevice(UUID_DEVICE_17422_INSTANCEINDEX_PBI_FPM01_CTRL_ECATIO_101);
        Device device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101    = UtilityModel.createDevice(UUID_DEVICE_25958_INSTANCEINDEX_PBI_FPM01_CTRL_EVR_101);
        Device device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201    = UtilityModel.createDevice(UUID_DEVICE_25965_INSTANCEINDEX_PBI_FPM01_CTRL_EVR_201);
        Device device66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100   = UtilityModel.createDevice(UUID_DEVICE_66031_INSTANCEINDEX_PBI_FPM01_CTRL_ECAT_100);

        Whitebox.setInternalState(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200,     ID, 4219L);
        Whitebox.setInternalState(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100,     ID, 4251L);
        Whitebox.setInternalState(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102,  ID, 9546L);
        Whitebox.setInternalState(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103,  ID, 9555L);
        Whitebox.setInternalState(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101,   ID, 9567L);
        Whitebox.setInternalState(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101,   ID, 9590L);
        Whitebox.setInternalState(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104,  ID, 9638L);
        Whitebox.setInternalState(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, ID, 17422L);
        Whitebox.setInternalState(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101,    ID, 25958L);
        Whitebox.setInternalState(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201,    ID, 25965L);
        Whitebox.setInternalState(device66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100,   ID, 66031L);

        NamePart namePartDeviceType1454 = namePartRevisionsDeviceTypeIPC1454.get(0).getNamePart();
        NamePart namePartDeviceType1455 = namePartRevisionsDeviceTypeECATIO1455.get(0).getNamePart();
        NamePart namePartDeviceType1471 = namePartRevisionsDeviceTypeECAT1471.get(0).getNamePart();
        NamePart namePartDeviceType1808 = namePartRevisionsDeviceTypeEVR1808.get(0).getNamePart();
        NamePart namePartDeviceType1912 = namePartRevisionsDeviceTypeEVR1912.get(0).getNamePart();
        NamePart namePartDeviceType2066 = namePartRevisionsDeviceTypeECATC2066.get(0).getNamePart();
        NamePart namePartDeviceType2874 = namePartRevisionsDeviceTypeECATC2874.get(0).getNamePart();
        NamePart namePartDeviceType2877 = namePartRevisionsDeviceTypeECATE2877.get(0).getNamePart();
        NamePart namePartDeviceType2879 = namePartRevisionsDeviceTypeIPC2879.get(0).getNamePart();
        NamePart namePartDeviceType2883 = namePartRevisionsDeviceTypeECATIO2883.get(0).getNamePart();
        NamePart namePartDeviceType2892 = namePartRevisionsDeviceTypeECAT2892.get(0).getNamePart();
        NamePart namePartDeviceType2898 = namePartRevisionsDeviceTypeEVR2898.get(0).getNamePart();
        NamePart namePartSubsystem1187  = namePartRevisionsSubsystem030ROW1187.get(0).getNamePart();
        NamePart namePartSubsystem2806  = namePartRevisionsSubsystemFPM012806.get(0).getNamePart();

        // ----------

        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2016-11-09 13:58:18"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "240300", "FEB-030ROW:PBI-IPC-240300", "FEB-30R0W:PB1-1PC-240300", "LEBT-010:PBI-NPM-002H", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2017-08-31 09:24:23"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "240300", "FEB-030Row:PBI-IPC-240300", "FEB-30R0W:PB1-1PC-240300", "LEBT-010:PBI-NPM-002H", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2017-11-28 08:12:31"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "003", FEB_030_ROW_PBI_IPC_003, FEB_30R0W_PB1_1PC_3, "PBI LEBT NPM (1H)", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2017-11-28 10:14:56"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "003", FEB_030_ROW_PBI_IPC_003, FEB_30R0W_PB1_1PC_3, "PBI NPM-02", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2017-11-29 16:54:49"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "003", FEB_030_ROW_PBI_IPC_003, FEB_30R0W_PB1_1PC_3, "PBI NPM-02, LEBT NPM 2 IPC", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2018-03-07 16:33:51"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "003", FEB_030_ROW_PBI_IPC_003, FEB_30R0W_PB1_1PC_3, PBI_NPM_02_LEBT_NPM2_IPC, null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:35:45"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "9003", "FEB-030Row:PBI-IPC-9003", "FEB-30R0W:PB1-1PC-9003", PBI_NPM_02_LEBT_NPM2_IPC, null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:53:39"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "003", FEB_030_ROW_PBI_IPC_003, FEB_30R0W_PB1_1PC_3, PBI_NPM_02_LEBT_NPM2_IPC, null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2018-03-29 12:35:33"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "003", FEB_030_ROW_PBI_IPC_003, FEB_30R0W_PB1_1PC_3, "PBI BIF-02 - LEBT NPM2 IPC", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2018-08-23 09:04:24"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "003", FEB_030_ROW_PBI_IPC_003, FEB_30R0W_PB1_1PC_3, "PBI BIF-01 - LEBT NPM2 IPC", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:22"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "003", FEB_030_ROW_PBI_IPC_003, FEB_30R0W_PB1_1PC_3, "PBI FPM-01 - LEBT NPM2 IPC", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:49:26"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1454, "200", "PBI-FPM01:PBI-IPC-200", "PB1-FPM1:PB1-1PC-200", "PBI FPM-01 - LEBT NPM2 IPC", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2020-04-09 07:47:06"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2879, "200", "PBI-FPM01:Ctrl-IPC-200", "PB1-FPM1:CTR1-1PC-200", "PBI FPM-01 - LEBT NPM2 IPC system", null));
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityModel.createDeviceRevision(device4219InstanceIndex_PBI_FPM01_CTRL_IPC_200, UtilityBusiness.parseDateOrNewDate("2020-04-15 09:29:02"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2879, "200", "PBI-FPM01:Ctrl-IPC-200", "PB1-FPM1:CTR1-1PC-200", "PBI FPM-01 - IPC Control System 2", null));

        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(0),  ID, 6654L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(1),  ID, 23520L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(2),  ID, 36601L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(3),  ID, 36686L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(4),  ID, 36954L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(5),  ID, 72496L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(6),  ID, 85633L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(7),  ID, 88563L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(8),  ID, 91073L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(9),  ID, 93576L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(10), ID, 103302L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(11), ID, 110554L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(12), ID, 113189L);
        Whitebox.setInternalState(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(13), ID, 113369L);

        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(0)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(1)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(2)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(3)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(4)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(5)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(6)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(7)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(8)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(9)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(10)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(11)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(12)));
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.add(UtilityBusiness.createNameRevision(deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.get(13)));

        // ----------

        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2016-11-09 13:58:19"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "240200", "FEB-030ROW:PBI-IPC-240200", "FEB-30R0W:PB1-1PC-240200", "LEBT-010:PBI-NPM-001V", null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate(DATE_2017_08_31_09_24_22), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "240200", "FEB-030Row:PBI-IPC-240200", "FEB-30R0W:PB1-1PC-240200", "LEBT-010:PBI-NPM-001V", null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2017-11-28 08:12:31"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "002", FEB_030_ROW_PBI_IPC_002, FEB_30R0W_PB1_1PC_2, "PBI LEBT NPM (1V)", null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2017-11-28 10:14:56"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "002", FEB_030_ROW_PBI_IPC_002, FEB_30R0W_PB1_1PC_2, "PBI NPM-01", null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2017-11-29 16:54:50"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "002", FEB_030_ROW_PBI_IPC_002, FEB_30R0W_PB1_1PC_2, "PBI NPM-01, LEBT NPM 1 IPC", null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2018-03-07 16:34:49"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "002", FEB_030_ROW_PBI_IPC_002, FEB_30R0W_PB1_1PC_2, PBI_NPM_01_LEBT_NPM1_IPC, null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:35:59"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "9002", "FEB-030Row:PBI-IPC-9002", "FEB-30R0W:PB1-1PC-9002", PBI_NPM_01_LEBT_NPM1_IPC, null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:53:27"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "002", FEB_030_ROW_PBI_IPC_002, FEB_30R0W_PB1_1PC_2, PBI_NPM_01_LEBT_NPM1_IPC, null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2018-03-29 12:35:34"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "002", FEB_030_ROW_PBI_IPC_002, FEB_30R0W_PB1_1PC_2, "PBI BIF-01 - LEBT NPM1 IPC", null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:15"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1454, "002", FEB_030_ROW_PBI_IPC_002, FEB_30R0W_PB1_1PC_2, "PBI FPM-01 - LEBT NPM1 IPC", null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:48:45"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1454, "100", "PBI-FPM01:PBI-IPC-100", "PB1-FPM1:PB1-1PC-100", "PBI FPM-01 - LEBT NPM1 IPC", null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2020-04-09 07:47:07"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2879, "100", "PBI-FPM01:Ctrl-IPC-100", "PB1-FPM1:CTR1-1PC-100", "PBI FPM-01 - LEBT NPM1 IPC system", null));
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityModel.createDeviceRevision(device4251InstanceIndex_PBI_FPM01_CTRL_IPC_100, UtilityBusiness.parseDateOrNewDate("2020-04-15 09:28:54"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2879, "100", "PBI-FPM01:Ctrl-IPC-100", "PB1-FPM1:CTR1-1PC-100", "PBI FPM-01 - IPC Control System 1", null));

        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(0),  ID, 6686L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(1),  ID, 23413L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(2),  ID, 36591L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(3),  ID, 36688L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(4),  ID, 36962L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(5),  ID, 72964L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(6),  ID, 85730L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(7),  ID, 88472L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(8),  ID, 91078L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(9),  ID, 103194L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(10), ID, 110251L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(11), ID, 113197L);
        Whitebox.setInternalState(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(12), ID, 113255L);

        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(0)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(1)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(2)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(3)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(4)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(5)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(6)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(7)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(8)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(9)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(10)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(11)));
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.add(UtilityBusiness.createNameRevision(deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.get(12)));

        // ----------

        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate(DATE_2016_12_16_16_28_54), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "250112", "FEB-030ROW:PBI-ECATIO-250112", "FEB-30R0W:PB1-ECAT10-250112", PBI_ETHERCAT_I_O, null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate(DATE_2017_08_31_09_24_22), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "250112", "FEB-030Row:PBI-ECATIO-250112", "FEB-30R0W:PB1-ECAT10-250112", PBI_ETHERCAT_I_O, null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate("2017-11-24 15:38:50"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "002", "FEB-030Row:PBI-ECATIO-002", "FEB-30R0W:PB1-ECAT10-2", PBI_ETHERCAT_I_O, null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate("2018-03-13 12:49:46"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "9002", "FEB-030Row:PBI-ECATIO-9002", "FEB-30R0W:PB1-ECAT10-9002", PBI_ETHERCAT_I_O, null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate("2018-03-13 15:01:03"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "012", "FEB-030Row:PBI-ECATIO-012", "FEB-30R0W:PB1-ECAT10-12", "PBI ECAT-01 - LEBT NPM1 horizontal camera actuator", null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate("2018-08-23 11:58:52"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "A022", "FEB-030Row:PBI-ECATIO-A022", "FEB-30R0W:PB1-ECAT10-A22", "PBI BIF-01 - LEBT NPM1 horizontal camera actuator", null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate("2018-08-23 12:02:36"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "022", FEB_030_ROW_PBI_ECATIO_022, FEB_30R0W_PB1_ECAT10_22, "PBI BIF-01 - LEBT NPM1 horizontal camera actuator", null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate(DATE_2019_05_03_15_43_14), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "022", FEB_030_ROW_PBI_ECATIO_022, FEB_30R0W_PB1_ECAT10_22, "PBI BIF-01 - LEBT NPM1 Ethercat module", null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:22"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "022", FEB_030_ROW_PBI_ECATIO_022, FEB_30R0W_PB1_ECAT10_22, PBI_FPM_01_LEBT_NPM1_ETHERCAT_MODULE, null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:48:08"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1455, "102", "PBI-FPM01:PBI-ECATIO-102", "PB1-FPM1:PB1-ECAT10-102", PBI_FPM_01_LEBT_NPM1_ETHERCAT_MODULE, null));
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityModel.createDeviceRevision(device9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102, UtilityBusiness.parseDateOrNewDate("2020-04-06 08:57:50"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2883, "102", "PBI-FPM01:Ctrl-ECATIO-102", "PB1-FPM1:CTR1-ECAT10-102", PBI_FPM_01_LEBT_NPM1_ETHERCAT_MODULE, null));

        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(0),  ID, 13547L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(1),  ID, 23442L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(2),  ID, 36547L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(3),  ID, 90656L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(4),  ID, 90709L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(5),  ID, 93681L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(6),  ID, 93752L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(7),  ID, 98612L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(8),  ID, 103296L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(9),  ID, 109910L);
        Whitebox.setInternalState(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(10), ID, 112981L);

        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(0)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(1)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(2)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(3)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(4)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(5)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(6)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(7)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(8)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(9)));
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.add(UtilityBusiness.createNameRevision(deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.get(10)));

        // ----------

        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate(DATE_2016_12_16_16_28_54), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "250113", "FEB-030ROW:PBI-ECATIO-250113", "FEB-30R0W:PB1-ECAT10-250113", PBI_ETHERCAT_I_O, null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate(DATE_2017_08_31_09_24_22), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "250113", "FEB-030Row:PBI-ECATIO-250113", "FEB-30R0W:PB1-ECAT10-250113", PBI_ETHERCAT_I_O, null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate("2017-11-24 15:38:48"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "003", "FEB-030Row:PBI-ECATIO-003", "FEB-30R0W:PB1-ECAT10-3", PBI_ETHERCAT_I_O, null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate("2018-03-13 12:49:45"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "9003", "FEB-030Row:PBI-ECATIO-9003", "FEB-30R0W:PB1-ECAT10-9003", PBI_ETHERCAT_I_O, null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate("2018-03-13 15:01:09"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "013", "FEB-030Row:PBI-ECATIO-013", "FEB-30R0W:PB1-ECAT10-13", "PBI ECAT-01 - LEBT NPM2 vertical camera actuator", null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate("2018-08-23 11:58:45"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "A023", "FEB-030Row:PBI-ECATIO-A023", "FEB-30R0W:PB1-ECAT10-A23", "PBI BIF-01 - LEBT NPM2 vertical camera actuator", null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate("2018-08-23 12:02:37"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "023", FEB_030_ROW_PBI_ECATIO_023, FEB_30R0W_PB1_ECAT10_23, "PBI BIF-01 - LEBT NPM2 vertical camera actuator", null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate(DATE_2019_05_03_15_43_14), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "023", FEB_030_ROW_PBI_ECATIO_023, FEB_30R0W_PB1_ECAT10_23, "PBI BIF-01 - LEBT NPM2 Ethercat module", null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:20"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "023", FEB_030_ROW_PBI_ECATIO_023, FEB_30R0W_PB1_ECAT10_23, PBI_FPM_01_LEBT_NPM2_ETHERCAT_MODULE, null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:47:35"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1455, "103", "PBI-FPM01:PBI-ECATIO-103", "PB1-FPM1:PB1-ECAT10-103", PBI_FPM_01_LEBT_NPM2_ETHERCAT_MODULE, null));
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityModel.createDeviceRevision(device9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103, UtilityBusiness.parseDateOrNewDate("2020-04-06 08:57:52"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2883, "103", "PBI-FPM01:Ctrl-ECATIO-103", "PB1-FPM1:CTR1-ECAT10-103", PBI_FPM_01_LEBT_NPM2_ETHERCAT_MODULE, null));

        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(0),  ID, 13556L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(1),  ID, 23443L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(2),  ID, 36499L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(3),  ID, 90654L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(4),  ID, 90763L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(5),  ID, 93607L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(6),  ID, 93761L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(7),  ID, 98616L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(8),  ID, 103272L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(9),  ID, 109516L);
        Whitebox.setInternalState(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(10), ID, 112922L);

        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(0)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(1)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(2)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(3)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(4)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(5)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(6)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(7)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(8)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(9)));
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.add(UtilityBusiness.createNameRevision(deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.get(10)));

        // ----------

        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate(DATE_2016_12_16_16_28_54), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "250311", "FEB-030ROW:PBI-ECATIO-250311", "FEB-30R0W:PB1-ECAT10-250311", PBI_ETHERCAT_I_O, null));
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate(DATE_2017_08_31_09_24_22), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "250311", "FEB-030Row:PBI-ECATIO-250311", "FEB-30R0W:PB1-ECAT10-250311", PBI_ETHERCAT_I_O, null));
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate("2017-11-24 15:38:48"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "013", "FEB-030Row:PBI-ECATIO-013", "FEB-30R0W:PB1-ECAT10-13", PBI_ETHERCAT_I_O, null));
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate("2018-03-13 12:49:39"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "9013", "FEB-030Row:PBI-ECATIO-9013", "FEB-30R0W:PB1-ECAT10-9013", PBI_ETHERCAT_I_O, null));
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate(DATE_2018_03_13_15_01_05), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "031", "FEB-030Row:PBI-ECATIO-031", "FEB-30R0W:PB1-ECAT10-31", "PBI ECAT-03 - MEBT NPM1 vertical camera actuator theta", null));
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate("2018-08-23 11:58:53"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType2066, "A020", "FEB-030Row:PBI-ECATC-A020", "FEB-30R0W:PB1-ECATC-A20", "PBI BIF-01 - LEBT NPM Ethercat coupler", null));
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate("2018-08-23 12:02:37"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType2066, "020", "FEB-030Row:PBI-ECATC-020", "FEB-30R0W:PB1-ECATC-20", "PBI BIF-01 - LEBT NPM Ethercat coupler", null));
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:18"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType2066, "020", "FEB-030Row:PBI-ECATC-020", "FEB-30R0W:PB1-ECATC-20", PBI_FPM_01_LEBT_NPM_ETHERCAT_COUPLER, null));
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:47:26"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2066, "101", "PBI-FPM01:PBI-ECATC-101", "PB1-FPM1:PB1-ECATC-101", PBI_FPM_01_LEBT_NPM_ETHERCAT_COUPLER, null));
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityModel.createDeviceRevision(device9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101, UtilityBusiness.parseDateOrNewDate("2020-04-06 08:57:48"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2874, "101", "PBI-FPM01:Ctrl-ECATC-101", "PB1-FPM1:CTR1-ECATC-101", PBI_FPM_01_LEBT_NPM_ETHERCAT_COUPLER, null));

        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(0), ID, 13568L);
        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(1), ID, 23440L);
        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(2), ID, 36489L);
        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(3), ID, 90580L);
        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(4), ID, 90726L);
        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(5), ID, 93692L);
        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(6), ID, 93767L);
        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(7), ID, 103241L);
        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(8), ID, 109397L);
        Whitebox.setInternalState(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(9), ID, 112862L);

        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(0)));
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(1)));
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(2)));
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(3)));
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(4)));
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(5)));
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(6)));
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(7)));
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(8)));
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.add(UtilityBusiness.createNameRevision(deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.get(9)));

        // ----------

        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate(DATE_2016_12_16_16_28_54), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1471, "250200", "FEB-030ROW:PBI-ECAT-250200", "FEB-30R0W:PB1-ECAT-250200", PBI_ETHERCAT_CRATE, null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate("2017-08-31 09:24:21"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1471, "250200", "FEB-030Row:PBI-ECAT-250200", "FEB-30R0W:PB1-ECAT-250200", PBI_ETHERCAT_CRATE, null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate("2017-11-24 15:38:47"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1471, "002", "FEB-030Row:PBI-ECAT-002", "FEB-30R0W:PB1-ECAT-2", PBI_ETHERCAT_CRATE, null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate("2018-03-13 12:49:38"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1471, "9002", "FEB-030Row:PBI-ECAT-9002", "FEB-30R0W:PB1-ECAT-9002", PBI_ETHERCAT_CRATE, null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate(DATE_2018_03_13_15_01_05), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1471, "020", FEB_030_ROW_PBI_ECAT_020, FEB_30R0W_PB1_ECAT_20, "PBI ECAT-02 - Ethercat crate MEBT NPM", null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate("2018-08-23 11:58:50"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1471, "A020", "FEB-030Row:PBI-ECAT-A020", "FEB-30R0W:PB1-ECAT-A20", "PBI BIF-01 - LEBT NPM Ethercat crate", null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate("2018-08-23 12:02:38"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1471, "020", FEB_030_ROW_PBI_ECAT_020, FEB_30R0W_PB1_ECAT_20, "PBI BIF-01 - LEBT NPM Ethercat crate", null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:20"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1471, "020", FEB_030_ROW_PBI_ECAT_020, FEB_30R0W_PB1_ECAT_20, PBI_FPM_01_LEBT_NPM_ETHERCAT_CRATE, null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:48:02"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1471, "100", "PBI-FPM01:PBI-ECAT-100", "PB1-FPM1:PB1-ECAT-100", PBI_FPM_01_LEBT_NPM_ETHERCAT_CRATE, null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate("2020-04-06 08:57:47"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2877, "100", "PBI-FPM01:Ctrl-ECATE-100", "PB1-FPM1:CTR1-ECATE-100", PBI_FPM_01_LEBT_NPM_ETHERCAT_CRATE, null));
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityModel.createDeviceRevision(device9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101, UtilityBusiness.parseDateOrNewDate("2020-04-07 09:42:57"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2877, "101", "PBI-FPM01:Ctrl-ECATE-101", "PB1-FPM1:CTR1-ECATE-101", PBI_FPM_01_LEBT_NPM_ETHERCAT_CRATE, null));

        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(0),  ID, 13591L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(1),  ID, 23370L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(2),  ID, 36486L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(3),  ID, 90570L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(4),  ID, 90725L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(5),  ID, 93668L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(6),  ID, 93771L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(7),  ID, 103270L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(8),  ID, 109845L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(9),  ID, 112838L);
        Whitebox.setInternalState(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(10), ID, 113083L);

        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(0)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(1)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(2)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(3)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(4)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(5)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(6)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(7)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(8)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(9)));
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.add(UtilityBusiness.createNameRevision(deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.get(10)));

        // ----------

        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate("2016-12-16 16:28:55"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "250114", "FEB-030ROW:PBI-ECATIO-250114", "FEB-30R0W:PB1-ECAT10-250114", PBI_ETHERCAT_I_O, null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate("2017-08-31 09:24:24"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "250114", "FEB-030Row:PBI-ECATIO-250114", "FEB-30R0W:PB1-ECAT10-250114", PBI_ETHERCAT_I_O, null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate("2017-11-24 15:38:50"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "004", "FEB-030Row:PBI-ECATIO-004", "FEB-30R0W:PB1-ECAT10-4", PBI_ETHERCAT_I_O, null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate("2018-03-13 12:49:43"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "9004", "FEB-030Row:PBI-ECATIO-9004", "FEB-30R0W:PB1-ECAT10-9004", PBI_ETHERCAT_I_O, null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate(DATE_2018_03_13_15_01_05), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "014", "FEB-030Row:PBI-ECATIO-014", "FEB-30R0W:PB1-ECAT10-14", "PBI ECAT-01 - LEBT NPM2 horizontal camera actuator", null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate("2018-08-23 11:58:52"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "A024", "FEB-030Row:PBI-ECATIO-A024", "FEB-30R0W:PB1-ECAT10-A24", "PBI BIF-01 - LEBT NPM2 horizontal camera actuator", null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate("2018-08-23 12:02:42"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "024", FEB_030_ROW_PBI_ECATIO_024, FEB_30R0W_PB1_ECAT10_24, "PBI BIF-01 - LEBT NPM2 horizontal camera actuator", null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate(DATE_2019_05_03_15_43_14), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "024", FEB_030_ROW_PBI_ECATIO_024, FEB_30R0W_PB1_ECAT10_24, "PBI BIF-01 - LEBT NPM2 Ethercat module", null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:17"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "024", FEB_030_ROW_PBI_ECATIO_024, FEB_30R0W_PB1_ECAT10_24, PBI_FPM_01_LEBT_NPM2_ETHERCAT_MODULE, null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:47:35"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1455, "104", "PBI-FPM01:PBI-ECATIO-104", "PB1-FPM1:PB1-ECAT10-104", PBI_FPM_01_LEBT_NPM2_ETHERCAT_MODULE, null));
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityModel.createDeviceRevision(device9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104, UtilityBusiness.parseDateOrNewDate("2020-04-06 08:57:47"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2883, "104", "PBI-FPM01:Ctrl-ECATIO-104", "PB1-FPM1:CTR1-ECAT10-104", PBI_FPM_01_LEBT_NPM2_ETHERCAT_MODULE, null));

        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(0),  ID, 13639L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(1),  ID, 23577L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(2),  ID, 36544L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(3),  ID, 90628L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(4),  ID, 90721L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(5),  ID, 93686L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(6),  ID, 93809L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(7),  ID, 98601L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(8),  ID, 103218L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(9),  ID, 109517L);
        Whitebox.setInternalState(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(10), ID, 112849L);

        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(0)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(1)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(2)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(3)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(4)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(5)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(6)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(7)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(8)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(9)));
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.add(UtilityBusiness.createNameRevision(deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.get(10)));

        // ----------

        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate("2017-08-29 10:36:47"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "001", "FEB-030Row:PBI-ECATIO-001", "FEB-30R0W:PB1-ECAT10-1", null, null));
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate("2017-11-24 15:38:51"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "001", "FEB-030Row:PBI-ECATIO-001", "FEB-30R0W:PB1-ECAT10-1", PBI_ETHERCAT_I_O, null));
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate("2018-03-13 12:49:43"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "9001", "FEB-030Row:PBI-ECATIO-9001", "FEB-30R0W:PB1-ECAT10-9001", PBI_ETHERCAT_I_O, null));
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate("2018-03-13 15:01:11"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "011", "FEB-030Row:PBI-ECATIO-011", "FEB-30R0W:PB1-ECAT10-11", "PBI ECAT-01 - LEBT NPM1 vertical camera actuator", null));
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate("2018-08-23 11:58:49"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "A021", "FEB-030Row:PBI-ECATIO-A021", "FEB-30R0W:PB1-ECAT10-A21", "PBI BIF-01 - LEBT NPM1 vertical camera actuator", null));
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate("2018-08-23 12:02:38"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "021", FEB_030_ROW_PBI_ECATIO_021, FEB_30R0W_PB1_ECAT10_21, "PBI BIF-01 - LEBT NPM1 vertical camera actuator", null));
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate(DATE_2019_05_03_15_43_14), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "021", FEB_030_ROW_PBI_ECATIO_021, FEB_30R0W_PB1_ECAT10_21, "PBI BIF-01 - LEBT NPM1 Ethercat module", null));
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:17"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1455, "021", FEB_030_ROW_PBI_ECATIO_021, FEB_30R0W_PB1_ECAT10_21, PBI_FPM_01_LEBT_NPM1_ETHERCAT_MODULE, null));
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:48:49"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1455, "101", "PBI-FPM01:PBI-ECATIO-101", "PB1-FPM1:PB1-ECAT10-101", PBI_FPM_01_LEBT_NPM1_ETHERCAT_MODULE, null));
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityModel.createDeviceRevision(device17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101, UtilityBusiness.parseDateOrNewDate("2020-04-06 08:57:51"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2883, "101", "PBI-FPM01:Ctrl-ECATIO-101", "PB1-FPM1:CTR1-ECAT10-101", PBI_FPM_01_LEBT_NPM1_ETHERCAT_MODULE, null));

        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(0), ID, 22983L);
        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(1), ID, 36561L);
        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(2), ID, 90630L);
        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(3), ID, 90779L);
        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(4), ID, 93649L);
        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(5), ID, 93770L);
        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(6), ID, 98611L);
        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(7), ID, 103216L);
        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(8), ID, 110280L);
        Whitebox.setInternalState(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(9), ID, 112901L);

        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(0)));
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(1)));
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(2)));
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(3)));
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(4)));
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(5)));
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(6)));
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(7)));
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(8)));
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.add(UtilityBusiness.createNameRevision(deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.get(9)));

        // ----------

        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityModel.createDeviceRevision(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101, UtilityBusiness.parseDateOrNewDate("2017-12-01 09:50:04"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "009", FEB_030_ROW_PBI_EVR_009, FEB_30R0W_PB1_EVR_9, "PBI NPM-01,  LEBT NPM 1 IPC event receiver", null));
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityModel.createDeviceRevision(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101, UtilityBusiness.parseDateOrNewDate("2018-03-07 16:34:52"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "009", FEB_030_ROW_PBI_EVR_009, FEB_30R0W_PB1_EVR_9, PBI_NPM_01_LEBT_NPM1_IPC_EVENT_RECEIVER, null));
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityModel.createDeviceRevision(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:35:08"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "9009", "FEB-030Row:PBI-EVR-9009", "FEB-30R0W:PB1-EVR-9009", PBI_NPM_01_LEBT_NPM1_IPC_EVENT_RECEIVER, null));
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityModel.createDeviceRevision(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:53:51"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "009", FEB_030_ROW_PBI_EVR_009, FEB_30R0W_PB1_EVR_9, PBI_NPM_01_LEBT_NPM1_IPC_EVENT_RECEIVER, null));
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityModel.createDeviceRevision(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101, UtilityBusiness.parseDateOrNewDate("2018-03-29 12:35:29"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "009", FEB_030_ROW_PBI_EVR_009, FEB_30R0W_PB1_EVR_9, "PBI BIF-01 - LEBT NPM1 IPC event receiver", null));
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityModel.createDeviceRevision(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:14"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "009", FEB_030_ROW_PBI_EVR_009, FEB_30R0W_PB1_EVR_9, PBI_FPM_01_LEBT_NPM1_IPC_EVENT_RECEIVER, null));
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityModel.createDeviceRevision(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:48:44"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1808, "101", "PBI-FPM01:PBI-EVR-101", "PB1-FPM1:PB1-EVR-101", PBI_FPM_01_LEBT_NPM1_IPC_EVENT_RECEIVER, null));
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityModel.createDeviceRevision(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101, UtilityBusiness.parseDateOrNewDate("2020-04-03 11:19:58"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1912, "101", "PBI-FPM01:TS-EVR-101", "PB1-FPM1:TS-EVR-101", PBI_FPM_01_LEBT_NPM1_IPC_EVENT_RECEIVER, null));
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityModel.createDeviceRevision(device25958InstanceIndex_PBI_FPM01_CTRL_EVR_101, UtilityBusiness.parseDateOrNewDate("2020-04-17 08:19:12"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2898, "101", "PBI-FPM01:Ctrl-EVR-101", "PB1-FPM1:CTR1-EVR-101", PBI_FPM_01_LEBT_NPM1_IPC_EVENT_RECEIVER, null));

        Whitebox.setInternalState(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(0), ID, 40865L);
        Whitebox.setInternalState(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(1), ID, 72984L);
        Whitebox.setInternalState(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(2), ID, 85392L);
        Whitebox.setInternalState(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(3), ID, 88644L);
        Whitebox.setInternalState(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(4), ID, 91040L);
        Whitebox.setInternalState(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(5), ID, 103177L);
        Whitebox.setInternalState(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(6), ID, 110237L);
        Whitebox.setInternalState(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(7), ID, 111144L);
        Whitebox.setInternalState(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(8), ID, 113780L);

        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityBusiness.createNameRevision(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(0)));
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityBusiness.createNameRevision(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(1)));
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityBusiness.createNameRevision(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(2)));
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityBusiness.createNameRevision(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(3)));
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityBusiness.createNameRevision(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(4)));
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityBusiness.createNameRevision(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(5)));
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityBusiness.createNameRevision(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(6)));
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityBusiness.createNameRevision(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(7)));
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.add(UtilityBusiness.createNameRevision(deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.get(8)));

        // ----------

        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2017-12-01 09:50:04"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "010", "FEB-030Row:PBI-EVR-010", "FEB-30R0W:PB1-EVR-10", "PBI NPM-02,  LEBT NPM 2 IPC event receiver", null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2017-12-04 09:38:30"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "013", FEB_030_ROW_PBI_EVR_013, FEB_30R0W_PB1_EVR_13, "PBI NPM-02,  LEBT NPM 2 IPC event receiver", null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2018-03-07 16:33:45"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "013", FEB_030_ROW_PBI_EVR_013, FEB_30R0W_PB1_EVR_13, PBI_NPM_02_LEBT_NPM2_IPC_EVENT_RECEIVER, null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:35:13"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "9013", "FEB-030Row:PBI-EVR-9013", "FEB-30R0W:PB1-EVR-9013", PBI_NPM_02_LEBT_NPM2_IPC_EVENT_RECEIVER, null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2018-03-09 13:53:35"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "013", FEB_030_ROW_PBI_EVR_013, FEB_30R0W_PB1_EVR_13, PBI_NPM_02_LEBT_NPM2_IPC_EVENT_RECEIVER, null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2018-03-29 12:35:39"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "013", FEB_030_ROW_PBI_EVR_013, FEB_30R0W_PB1_EVR_13, "PBI BIF-02 - LEBT NPM2 IPC event receiver", null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2018-08-23 09:04:22"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "013", FEB_030_ROW_PBI_EVR_013, FEB_30R0W_PB1_EVR_13, "PBI BIF-01 - LEBT NPM2 IPC event receiver", null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2019-10-31 10:39:19"), userAccountEditor, false, namePartSubsystem1187, namePartDeviceType1808, "013", FEB_030_ROW_PBI_EVR_013, FEB_30R0W_PB1_EVR_13, PBI_FPM_01_LEBT_NPM2_IPC_EVENT_RECEIVER, null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2020-03-31 10:49:29"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1808, "201", "PBI-FPM01:PBI-EVR-201", "PB1-FPM1:PB1-EVR-201", PBI_FPM_01_LEBT_NPM2_IPC_EVENT_RECEIVER, null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2020-04-03 11:19:58"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType1912, "201", "PBI-FPM01:TS-EVR-201", "PB1-FPM1:TS-EVR-201", PBI_FPM_01_LEBT_NPM2_IPC_EVENT_RECEIVER, null));
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityModel.createDeviceRevision(device25965InstanceIndex_PBI_FPM01_CTRL_EVR_201, UtilityBusiness.parseDateOrNewDate("2020-04-17 08:19:14"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2898, "201", "PBI-FPM01:Ctrl-EVR-201", "PB1-FPM1:CTR1-EVR-201", PBI_FPM_01_LEBT_NPM2_IPC_EVENT_RECEIVER, null));

        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(0),  ID, 40872L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(1),  ID, 42690L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(2),  ID, 72424L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(3),  ID, 85418L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(4),  ID, 88538L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(5),  ID, 91112L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(6),  ID, 93558L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(7),  ID, 103262L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(8),  ID, 110572L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(9),  ID, 111145L);
        Whitebox.setInternalState(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(10), ID, 113814L);

        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(0)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(1)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(2)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(3)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(4)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(5)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(6)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(7)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(8)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(9)));
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.add(UtilityBusiness.createNameRevision(deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.get(10)));

        // ----------

        deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.add(UtilityModel.createDeviceRevision(device66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100, UtilityBusiness.parseDateOrNewDate("2020-04-06 08:58:33"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2892, "100", "PBI-FPM01:Ctrl-ECAT-100", "PB1-FPM1:CTR1-ECAT-100", "PBI FPM-01 - LEBT NPM Ethercat system", null));
        deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.add(UtilityModel.createDeviceRevision(device66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100, UtilityBusiness.parseDateOrNewDate("2020-04-15 09:29:01"), userAccountEditor, false, namePartSubsystem2806, namePartDeviceType2892, "100", "PBI-FPM01:Ctrl-ECAT-100", "PB1-FPM1:CTR1-ECAT-100", "PBI FPM-01 - Ethercat control system", null));

        Whitebox.setInternalState(deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.get(0), ID, 112952L);
        Whitebox.setInternalState(deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.get(1), ID, 113350L);

        nameRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.add(UtilityBusiness.createNameRevision(deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.get(0)));
        nameRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.add(UtilityBusiness.createNameRevision(deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.get(1)));
    }

    /**
     * Tear down test structure - section - name part revisions, name revisions.
     */
    private void tearDownNameRevisionSection() {
        namePartRevisionsSystemGroupAcc1.clear();
        namePartRevisionsSystemGroupAcc1 = null;
        nameRevisionsSystemGroupAcc1.clear();
        nameRevisionsSystemGroupAcc1 = null;

        namePartRevisionsSystemFEB1052.clear();
        namePartRevisionsSystemFEB1052 = null;
        nameRevisionsSystemFEB1052.clear();
        nameRevisionsSystemFEB1052 = null;

        namePartRevisionsSubsystem030ROW1187.clear();
        namePartRevisionsSubsystem030ROW1187 = null;
        nameRevisionsSubsystem030ROW1187.clear();
        nameRevisionsSubsystem030ROW1187 = null;
        // ----------
        namePartRevisionsSystemGroupCentralServices4.clear();
        namePartRevisionsSystemGroupCentralServices4 = null;
        nameRevisionsSystemGroupCentralServices4.clear();
        nameRevisionsSystemGroupCentralServices4 = null;

        namePartRevisionsSystemPBI2774.clear();
        namePartRevisionsSystemPBI2774 = null;
        nameRevisionsSystemPBI2774.clear();
        nameRevisionsSystemPBI2774 = null;

        namePartRevisionsSubsystemFPM012806.clear();
        namePartRevisionsSubsystemFPM012806 = null;
        nameRevisionsSubsystemFPM012806.clear();
        nameRevisionsSubsystemFPM012806 = null;
}

    /**
     * Tear down test structure - device type - name part revisions, name revisions.
     */
    private void tearDownNameRevisionDeviceType() {
        namePartRevisionsDisciplinePBI237.clear();
        namePartRevisionsDisciplinePBI237 = null;
        nameRevisionsDisciplinePBI237.clear();
        nameRevisionsDisciplinePBI237 = null;

        namePartRevisionsDisciplineTS1524.clear();
        namePartRevisionsDisciplineTS1524 = null;
        nameRevisionsDisciplineTS1524.clear();
        nameRevisionsDisciplineTS1524 = null;

        namePartRevisionsDeviceGroupControlElectronics1435.clear();
        namePartRevisionsDeviceGroupControlElectronics1435 = null;
        nameRevisionsDeviceGroupControllerElectronics1435.clear();
        nameRevisionsDeviceGroupControllerElectronics1435 = null;

        namePartRevisionsDeviceGroupInfrastructure1909.clear();
        namePartRevisionsDeviceGroupInfrastructure1909 = null;
        nameRevisionsDeviceGroupInfrastructure1909.clear();
        nameRevisionsDeviceGroupInfrastructure1909 = null;

        namePartRevisionsDeviceTypeIPC1454.clear();
        namePartRevisionsDeviceTypeIPC1454 = null;
        nameRevisionsDeviceTypeIPC1454.clear();
        nameRevisionsDeviceTypeIPC1454 = null;

        namePartRevisionsDeviceTypeECATIO1455.clear();
        namePartRevisionsDeviceTypeECATIO1455 = null;
        nameRevisionsDeviceTypeECATIO1455.clear();
        nameRevisionsDeviceTypeECATIO1455 = null;

        namePartRevisionsDeviceTypeECAT1471.clear();
        namePartRevisionsDeviceTypeECAT1471 = null;
        nameRevisionsDeviceTypeECAT1471.clear();
        nameRevisionsDeviceTypeECAT1471 = null;

        namePartRevisionsDeviceTypeEVR1808.clear();
        namePartRevisionsDeviceTypeEVR1808 = null;
        nameRevisionsDeviceTypeEVR1808.clear();
        nameRevisionsDeviceTypeEVR1808 = null;

        namePartRevisionsDeviceTypeEVR1912.clear();
        namePartRevisionsDeviceTypeEVR1912 = null;
        nameRevisionsDeviceTypeEVR1912.clear();
        nameRevisionsDeviceTypeEVR1912 = null;

        namePartRevisionsDeviceTypeECATC2066.clear();
        namePartRevisionsDeviceTypeECATC2066 = null;
        nameRevisionsDeviceTypeECATC2066.clear();
        nameRevisionsDeviceTypeECATC2066 = null;
        // --------------------------------------------------
        namePartRevisionsDisciplineCtrl502.clear();
        namePartRevisionsDisciplineCtrl502 = null;
        nameRevisionsDisciplineCtrl502.clear();
        nameRevisionsDisciplineCtrl502 = null;
        // ----------
        namePartRevisionDeviceGroupControllerSystems2867.clear();
        namePartRevisionDeviceGroupControllerSystems2867 = null;
        nameRevisionDeviceGroupControllerSystems2867.clear();
        nameRevisionDeviceGroupControllerSystems2867 = null;

        namePartRevisionDeviceGroupControllerElectronics2868.clear();
        namePartRevisionDeviceGroupControllerElectronics2868 = null;
        nameRevisionDeviceGroupControllerElectronics2868.clear();
        nameRevisionDeviceGroupControllerElectronics2868 = null;

        namePartRevisionDeviceGroupControllerIOModules2869.clear();
        namePartRevisionDeviceGroupControllerIOModules2869 = null;
        nameRevisionDeviceGroupControllerIOModules2869.clear();
        nameRevisionDeviceGroupControllerIOModules2869 = null;

        namePartRevisionDeviceGroupEnclosures2870.clear();
        namePartRevisionDeviceGroupEnclosures2870 = null;
        nameRevisionDeviceGroupEnclosures2870.clear();
        nameRevisionDeviceGroupEnclosures2870 = null;

        namePartRevisionDeviceGroupTimingSystem2893.clear();
        namePartRevisionDeviceGroupTimingSystem2893 = null;
        nameRevisionDeviceGroupTimingSystem2893.clear();
        nameRevisionDeviceGroupTimingSystem2893 = null;
        // ----------
        namePartRevisionsDeviceTypeECATC2874.clear();
        namePartRevisionsDeviceTypeECATC2874 = null;
        nameRevisionsDeviceTypeECATC2874.clear();
        nameRevisionsDeviceTypeECATC2874 = null;

        namePartRevisionsDeviceTypeECATE2877.clear();
        namePartRevisionsDeviceTypeECATE2877 = null;
        nameRevisionsDeviceTypeECATE2877.clear();
        nameRevisionsDeviceTypeECATE2877 = null;

        namePartRevisionsDeviceTypeIPC2879.clear();
        namePartRevisionsDeviceTypeIPC2879 = null;
        nameRevisionsDeviceTypeIPC2879.clear();
        nameRevisionsDeviceTypeIPC2879 = null;

        namePartRevisionsDeviceTypeECATIO2883.clear();
        namePartRevisionsDeviceTypeECATIO2883 = null;
        nameRevisionsDeviceTypeECATIO2883.clear();
        nameRevisionsDeviceTypeECATIO2883 = null;

        namePartRevisionsDeviceTypeECAT2892.clear();
        namePartRevisionsDeviceTypeECAT2892 = null;
        nameRevisionsDeviceTypeECAT2892.clear();
        nameRevisionsDeviceTypeECAT2892 = null;

        namePartRevisionsDeviceTypeEVR2898.clear();
        namePartRevisionsDeviceTypeEVR2898 = null;
        nameRevisionsDeviceTypeEVR2898.clear();
        nameRevisionsDeviceTypeEVR2898 = null;
    }

    /**
     * Tear down test structure - device - device revisions, name revisions.
     */
    private void tearDownNameRevisionDevice() {
        deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.clear();
        deviceRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100 = null;
        nameRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100.clear();
        nameRevisions66031InstanceIndex_PBI_FPM01_CTRL_ECAT_100 = null;

        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.clear();
        deviceRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101 = null;
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101.clear();
        nameRevisions9567InstanceIndex_PBI_FPM01_CTRL_ECATC_101 = null;

        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.clear();
        deviceRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101 = null;
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101.clear();
        nameRevisions9590InstanceIndex_PBI_FPM01_CTRL_ECATE_101 = null;

        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.clear();
        deviceRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101 = null;
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101.clear();
        nameRevisions17422InstanceIndex_PBI_FPM01_CTRL_ECATIO_101 = null;

        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.clear();
        deviceRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102 = null;
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102.clear();
        nameRevisions9546InstanceIndex_PBI_FPM01_CTRL_ECATIO_102 = null;

        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.clear();
        deviceRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103 = null;
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103.clear();
        nameRevisions9555InstanceIndex_PBI_FPM01_CTRL_ECATIO_103 = null;

        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.clear();
        deviceRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104 = null;
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104.clear();
        nameRevisions9638InstanceIndex_PBI_FPM01_CTRL_ECATIO_104 = null;

        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.clear();
        deviceRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101 = null;
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101.clear();
        nameRevisions25958InstanceIndex_PBI_FPM01_CTRL_EVR_101 = null;

        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.clear();
        deviceRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201 = null;
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201.clear();
        nameRevisions25965InstanceIndex_PBI_FPM01_CTRL_EVR_201 = null;

        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.clear();
        deviceRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100 = null;
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100.clear();
        nameRevisions4251InstanceIndex_PBI_FPM01_CTRL_IPC_100 = null;

        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.clear();
        deviceRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200 = null;
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200.clear();
        nameRevisions4219InstanceIndex_PBI_FPM01_CTRL_IPC_200 = null;
    }

}
