/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openepics.names.model.Device;
import org.openepics.names.model.NamePart;
import org.openepics.names.model.UtilityModel;
import org.powermock.reflect.Whitebox;

/**
 * Unit tests for NameArtifact class.
 *
 * @author Lars Johansson
 *
 * @see NameArtifact
 */
public class NameArtifactTest {

    private static final String NAME_ARTIFACT_MAP = "NAME_ARTIFACT_MAP";

    private static final String UUID1 = "4262e1e7-2444-412e-83d7-aeabf58262c6";
    private static final String UUID2 = "c6e84e09-29d7-49a0-8ecd-92a6eff86da4";
    private static final String UUID3 = "abc14850-594e-4924-b5e8-aba7aa7865e5";

    private static final String UUID4 = "a796360b-2634-4499-944b-8bd5c46aea16";
    private static final String UUID5 = "ce97227b-7102-436a-b119-5810eaeb1683";
    private static final String UUID6 = "6290cf1d-d697-4757-acbf-5b27ea15ffb7";
    private static final String UUID7 = "121958b6-0592-41d3-8cec-0f05f9c01db8";
    private static final String UUID8 = "aed0256a-a2a1-4e51-90b1-3fe204ccbeb1";
    private static final String UUID9 = "30e3a796-8f39-48d5-a87c-566fefeba5aa";

    private static NameArtifact nameArtifactNamePartSection;
    private static NameArtifact nameArtifactNamePartDeviceType;
    private static NameArtifact nameArtifactDevice;

    private static NamePart namePartSection1;
    private static NamePart namePartSection2;
    private static NamePart namePartDeviceType1;
    private static NamePart namePartDeviceType2;

    private static Device device1;
    private static Device device2;

    /**
     * One-time initialization code.
     *
     * @throws ReflectiveOperationException NoSuchFieldException, IllegalAccessException
     */
    @BeforeClass
    public static void oneTimeSetUp() throws ReflectiveOperationException {
        nameArtifactNamePartSection    = UtilityBusiness.createNameArtifactSystemStructure(UUID1);
        nameArtifactNamePartDeviceType = UtilityBusiness.createNameArtifactDeviceStructure(UUID2);
        nameArtifactDevice             = UtilityBusiness.createNameArtifactDeviceRegistry (UUID3);

        namePartSection1    = UtilityModel.createNamePartSection   (UUID4);
        namePartSection2    = UtilityModel.createNamePartSection   (UUID5);
        namePartDeviceType1 = UtilityModel.createNamePartDeviceType(UUID6);
        namePartDeviceType2 = UtilityModel.createNamePartDeviceType(UUID7);

        device1 = UtilityModel.createDevice(UUID8);
        device2 = UtilityModel.createDevice(UUID9);

        clearNameArtifactMap();
    }

    /**
     * One-time cleanup code.
     *
     * @throws ReflectiveOperationException NoSuchFieldException, IllegalAccessException
     */
    @AfterClass
    public static void oneTimeTearDown() throws ReflectiveOperationException {
        clearNameArtifactMap();

        nameArtifactNamePartSection    = null;
        nameArtifactNamePartDeviceType = null;
        nameArtifactDevice             = null;

        namePartSection1    = null;
        namePartSection2    = null;
        namePartDeviceType1 = null;
        namePartDeviceType2 = null;
        device1 = null;
        device2 = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test constructor of NameArtifact.
     */
    @Test(expected = NullPointerException.class)
    public void constructorUuidNull() {
        new NameArtifact(null, NameType.SYSTEM_STRUCTURE);
    }

    /**
     * Test constructor of NameArtifact.
     */
    @Test(expected = NullPointerException.class)
    public void constructorNameTypeNull() {
        new NameArtifact(UUID.fromString(UUID1), null);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test equals method of NameArtifact.
     */
    @Test
    public void equals() {
        NameArtifact nameArtifact1 = UtilityBusiness.createNameArtifactSystemStructure(UUID1);
        NameArtifact nameArtifact2 = UtilityBusiness.createNameArtifactDeviceStructure(UUID1);
        NameArtifact nameArtifact3 = UtilityBusiness.createNameArtifactDeviceRegistry (UUID1);

        assertEquals   (nameArtifact1, nameArtifactNamePartSection);
        assertNotEquals(nameArtifact1, nameArtifactNamePartDeviceType);
        assertNotEquals(nameArtifact1, nameArtifactDevice);

        assertEquals   (nameArtifact2, nameArtifactNamePartSection);
        assertNotEquals(nameArtifact2, nameArtifactNamePartDeviceType);
        assertNotEquals(nameArtifact2, nameArtifactDevice);

        assertEquals   (nameArtifact2, nameArtifactNamePartSection);
        assertNotEquals(nameArtifact3, nameArtifactNamePartDeviceType);
        assertNotEquals(nameArtifact3, nameArtifactDevice);
    }

    /**
     * Test hashCode method of NameArtifact.
     */
    @Test
    public void hashCodeTest() {
        NameArtifact nameArtifact1 = UtilityBusiness.createNameArtifactSystemStructure(UUID1);
        NameArtifact nameArtifact2 = UtilityBusiness.createNameArtifactDeviceStructure(UUID1);
        NameArtifact nameArtifact3 = UtilityBusiness.createNameArtifactDeviceRegistry (UUID1);
        NameArtifact nameArtifact4 = UtilityBusiness.createNameArtifactSystemStructure(UUID2);
        NameArtifact nameArtifact5 = UtilityBusiness.createNameArtifactDeviceStructure(UUID2);
        NameArtifact nameArtifact6 = UtilityBusiness.createNameArtifactDeviceRegistry (UUID2);
        NameArtifact nameArtifact7 = UtilityBusiness.createNameArtifactSystemStructure(UUID3);
        NameArtifact nameArtifact8 = UtilityBusiness.createNameArtifactDeviceStructure(UUID3);
        NameArtifact nameArtifact9 = UtilityBusiness.createNameArtifactDeviceRegistry (UUID3);

        assertEquals   (nameArtifact1.hashCode(), nameArtifactNamePartSection.hashCode());
        assertNotEquals(nameArtifact1.hashCode(), nameArtifactNamePartDeviceType.hashCode());
        assertNotEquals(nameArtifact1.hashCode(), nameArtifactDevice.hashCode());

        assertEquals   (nameArtifact2.hashCode(), nameArtifactNamePartSection.hashCode());
        assertNotEquals(nameArtifact2.hashCode(), nameArtifactNamePartDeviceType.hashCode());
        assertNotEquals(nameArtifact2.hashCode(), nameArtifactDevice.hashCode());

        assertEquals   (nameArtifact3.hashCode(), nameArtifactNamePartSection.hashCode());
        assertNotEquals(nameArtifact3.hashCode(), nameArtifactNamePartDeviceType.hashCode());
        assertNotEquals(nameArtifact3.hashCode(), nameArtifactDevice.hashCode());

        assertNotEquals(nameArtifact4.hashCode(), nameArtifactNamePartSection.hashCode());
        assertEquals   (nameArtifact4.hashCode(), nameArtifactNamePartDeviceType.hashCode());
        assertNotEquals(nameArtifact4.hashCode(), nameArtifactDevice.hashCode());

        assertNotEquals(nameArtifact5.hashCode(), nameArtifactNamePartSection.hashCode());
        assertEquals   (nameArtifact5.hashCode(), nameArtifactNamePartDeviceType.hashCode());
        assertNotEquals(nameArtifact5.hashCode(), nameArtifactDevice.hashCode());

        assertNotEquals(nameArtifact6.hashCode(), nameArtifactNamePartSection.hashCode());
        assertEquals   (nameArtifact6.hashCode(), nameArtifactNamePartDeviceType.hashCode());
        assertNotEquals(nameArtifact6.hashCode(), nameArtifactDevice.hashCode());

        assertNotEquals(nameArtifact7.hashCode(), nameArtifactNamePartSection.hashCode());
        assertNotEquals(nameArtifact7.hashCode(), nameArtifactNamePartDeviceType.hashCode());
        assertEquals   (nameArtifact7.hashCode(), nameArtifactDevice.hashCode());

        assertNotEquals(nameArtifact8.hashCode(), nameArtifactNamePartSection.hashCode());
        assertNotEquals(nameArtifact8.hashCode(), nameArtifactNamePartDeviceType.hashCode());
        assertEquals   (nameArtifact8.hashCode(), nameArtifactDevice.hashCode());

        assertNotEquals(nameArtifact9.hashCode(), nameArtifactNamePartSection.hashCode());
        assertNotEquals(nameArtifact9.hashCode(), nameArtifactNamePartDeviceType.hashCode());
        assertEquals   (nameArtifact9.hashCode(), nameArtifactDevice.hashCode());
    }

    /**
     * Test asNamePart method of NameArtifact.
     */
    @Test(expected = NullPointerException.class)
    public void asNamePartSection() {
        nameArtifactNamePartSection.asNamePart();
    }

    /**
     * Test asNamePart method of NameArtifact.
     */
    @Test(expected = NullPointerException.class)
    public void asNamePartDeviceType() {
        nameArtifactNamePartDeviceType.asNamePart();
    }

    /**
     * Test asNamePart method of NameArtifact.
     */
    @Test(expected = NullPointerException.class)
    public void asNamePartDevice() {
        nameArtifactDevice.asNamePart();
    }

    /**
     * Test asDevice method of NameArtifact.
     */
    @Test(expected = NullPointerException.class)
    public void asDeviceSection() {
        nameArtifactNamePartSection.asDevice();
    }

    /**
     * Test asDevice method of NameArtifact.
     */
    @Test(expected = NullPointerException.class)
    public void asDeviceDeviceType() {
        nameArtifactNamePartDeviceType.asDevice();
    }

    /**
     * Test asDevice method of NameArtifact.
     */
    @Test(expected = NullPointerException.class)
    public void asDeviceDevice() {
        nameArtifactDevice.asDevice();
    }

    /**
     * Test asNamePart method of NameArtifact.
     */
    @Test
    public void asNamePart() {
        assertEquals(namePartSection1,    NameArtifact.getInstance(namePartSection1).asNamePart());
        assertEquals(namePartSection2,    NameArtifact.getInstance(namePartSection2).asNamePart());
        assertEquals(namePartDeviceType1, NameArtifact.getInstance(namePartDeviceType1).asNamePart());
        assertEquals(namePartDeviceType2, NameArtifact.getInstance(namePartDeviceType2).asNamePart());
    }

    /**
     * Test asDevice method of NameArtifact.
     */
    @Test
    public void asDevice() {
        assertEquals(device1, NameArtifact.getInstance(device1).asDevice());
        assertEquals(device2, NameArtifact.getInstance(device2).asDevice());
    }

    /**
     * Test getInstance method of NameArtifact.
     */
    @Test
    public void getInstanceNamePart() throws ReflectiveOperationException {
        assertEquals(UUID4, NameArtifact.getInstance(namePartSection1).getUuid().toString());
        assertEquals(UUID5, NameArtifact.getInstance(namePartSection2).getUuid().toString());
        assertEquals(UUID6, NameArtifact.getInstance(namePartDeviceType1).getUuid().toString());
        assertEquals(UUID7, NameArtifact.getInstance(namePartDeviceType2).getUuid().toString());

        @SuppressWarnings("unchecked")
        Map<UUID, NameArtifact> nameArtifactMap = (Map<UUID, NameArtifact>)
                Whitebox.getField(NameArtifact.class, NAME_ARTIFACT_MAP).get(null);

        // map size depend on calls to NameArtifact.getInstance
        // order of methods in test class not known, therefore map size not known

        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);

        // ------------------------------------------------------------

        // sending in new objects with uuid that already exists/is used
        // should not be added to map

        // attempt to reuse uuid, provoke conflict
        // should return existing information/object for uuid

        NamePart namePartA = UtilityModel.createNamePartSection   (UUID4);
        NamePart namePartB = UtilityModel.createNamePartSection   (UUID5);
        NamePart namePartC = UtilityModel.createNamePartSection   (UUID6);
        NamePart namePartD = UtilityModel.createNamePartSection   (UUID7);

        NamePart namePartE = UtilityModel.createNamePartDeviceType(UUID4);
        NamePart namePartF = UtilityModel.createNamePartDeviceType(UUID5);
        NamePart namePartG = UtilityModel.createNamePartDeviceType(UUID6);
        NamePart namePartH = UtilityModel.createNamePartDeviceType(UUID7);

        Device deviceA = UtilityModel.createDevice(UUID4);
        Device deviceB = UtilityModel.createDevice(UUID5);
        Device deviceC = UtilityModel.createDevice(UUID6);
        Device deviceD = UtilityModel.createDevice(UUID7);

        NameArtifact nameArtifactTest = NameArtifact.getInstance(namePartA);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID4, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.SYSTEM_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartSection1, NameArtifact.getInstance(namePartA).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(namePartB);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID5, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.SYSTEM_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartSection2, NameArtifact.getInstance(namePartB).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(namePartC);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID6, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartDeviceType1, NameArtifact.getInstance(namePartC).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(namePartD);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID7, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartDeviceType2, NameArtifact.getInstance(namePartD).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(namePartE);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID4, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.SYSTEM_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartSection1, NameArtifact.getInstance(namePartE).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(namePartF);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID5, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.SYSTEM_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartSection2, NameArtifact.getInstance(namePartF).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(namePartG);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID6, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartDeviceType1, NameArtifact.getInstance(namePartG).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(namePartH);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID7, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartDeviceType2, NameArtifact.getInstance(namePartH).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(deviceA);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID4, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.SYSTEM_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartSection1, NameArtifact.getInstance(deviceA).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(deviceB);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID5, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.SYSTEM_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartSection2, NameArtifact.getInstance(deviceB).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(deviceC);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID6, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartDeviceType1, NameArtifact.getInstance(deviceC).asNamePart());

        nameArtifactTest = NameArtifact.getInstance(deviceD);
        assertTrue(nameArtifactMap.size() >= 4);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID7, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_STRUCTURE, nameArtifactTest.getNameType());
        assertEquals(namePartDeviceType2, NameArtifact.getInstance(deviceD).asNamePart());
    }

    /**
     * Test getInstance method of NameArtifact.
     */
    @Test
    public void getInstanceDevice() throws ReflectiveOperationException {
        assertEquals(UUID8, NameArtifact.getInstance(device1).getUuid().toString());
        assertEquals(UUID9, NameArtifact.getInstance(device2).getUuid().toString());

        @SuppressWarnings("unchecked")
        Map<UUID, NameArtifact> nameArtifactMap = (Map<UUID, NameArtifact>)
                Whitebox.getField(NameArtifact.class, NAME_ARTIFACT_MAP).get(null);

        // map size depend on calls to NameArtifact.getInstance
        // order of methods in test class not known, therefore map size not known

        assertTrue(nameArtifactMap.size() >= 2);
        assertTrue(nameArtifactMap.size() <= 6);

        // ------------------------------------------------------------

        // sending in new objects with uuid that already exists/is used
        // should not be added to map

        // attempt to reuse uuid, provoke conflict
        // should return existing information/object for uuid

        NamePart namePartA = UtilityModel.createNamePartSection   (UUID8);
        NamePart namePartB = UtilityModel.createNamePartSection   (UUID9);

        NamePart namePartE = UtilityModel.createNamePartDeviceType(UUID8);
        NamePart namePartF = UtilityModel.createNamePartDeviceType(UUID9);

        Device deviceA = UtilityModel.createDevice(UUID8);
        Device deviceB = UtilityModel.createDevice(UUID9);

        NameArtifact nameArtifactTest = NameArtifact.getInstance(namePartA);
        assertTrue(nameArtifactMap.size() >= 2);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID8, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_REGISTRY, nameArtifactTest.getNameType());
        assertEquals(device1, NameArtifact.getInstance(namePartA).asDevice());

        nameArtifactTest = NameArtifact.getInstance(namePartB);
        assertTrue(nameArtifactMap.size() >= 2);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID9, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_REGISTRY, nameArtifactTest.getNameType());
        assertEquals(device2, NameArtifact.getInstance(namePartB).asDevice());

        nameArtifactTest = NameArtifact.getInstance(namePartE);
        assertTrue(nameArtifactMap.size() >= 2);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID8, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_REGISTRY, nameArtifactTest.getNameType());
        assertEquals(device1, NameArtifact.getInstance(namePartE).asDevice());

        nameArtifactTest = NameArtifact.getInstance(namePartF);
        assertTrue(nameArtifactMap.size() >= 2);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID9, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_REGISTRY, nameArtifactTest.getNameType());
        assertEquals(device2, NameArtifact.getInstance(namePartF).asDevice());

        nameArtifactTest = NameArtifact.getInstance(deviceA);
        assertTrue(nameArtifactMap.size() >= 2);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID8, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_REGISTRY, nameArtifactTest.getNameType());
        assertEquals(device1, NameArtifact.getInstance(deviceA).asDevice());

        nameArtifactTest = NameArtifact.getInstance(deviceB);
        assertTrue(nameArtifactMap.size() >= 2);
        assertTrue(nameArtifactMap.size() <= 6);
        assertEquals(UUID9, nameArtifactTest.getUuid().toString());
        assertEquals(NameType.DEVICE_REGISTRY, nameArtifactTest.getNameType());
        assertEquals(device2, NameArtifact.getInstance(deviceB).asDevice());
    }

    // ----------------------------------------------------------------------------------------------------

    // potentially dangerous since name artifact map is cleared
    // recommend not to make publically available,
    //     e.g. not in utility class since risk of misuse, or assure test use only
    private static void clearNameArtifactMap() throws ReflectiveOperationException {
        @SuppressWarnings("unchecked")
         Map<UUID, NameArtifact> nameArtifactMap = (Map<UUID, NameArtifact>)
                 Whitebox.getField(NameArtifact.class, NAME_ARTIFACT_MAP).get(null);

        // clear map
        nameArtifactMap.clear();
    }

}
