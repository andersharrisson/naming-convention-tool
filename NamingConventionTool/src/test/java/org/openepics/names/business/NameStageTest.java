/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit tests for NameStage class.
 *
 * @author Lars Johansson
 *
 * @see NameStage
 */
public class NameStageTest {

    /**
     * Test isInitial method of NameStage.
     */
    @Test
    public void isInitial() {
        assertTrue (NameStage.INITIAL.isInitial());
        assertFalse(NameStage.INITIAL_PROPOSED.isInitial());
        assertFalse(NameStage.INITIAL_CANCELLED.isInitial());
        assertFalse(NameStage.STABLE.isInitial());
        assertFalse(NameStage.ACTIVE_MODIFICATION_PROPOSED.isInitial());
        assertFalse(NameStage.ACTIVE_DELETION_PROPOSED.isInitial());
        assertFalse(NameStage.ACTIVE_CANCELLED.isInitial());
        assertFalse(NameStage.DELETED_APPROVED.isInitial());
    }

    /**
     * Test isAdded method of NameStage.
     */
    @Test
    public void isAdded() {
        assertFalse(NameStage.INITIAL.isAdded());
        assertTrue (NameStage.INITIAL_PROPOSED.isAdded());
        assertFalse(NameStage.INITIAL_CANCELLED.isAdded());
        assertFalse(NameStage.STABLE.isAdded());
        assertFalse(NameStage.ACTIVE_MODIFICATION_PROPOSED.isAdded());
        assertFalse(NameStage.ACTIVE_DELETION_PROPOSED.isAdded());
        assertFalse(NameStage.ACTIVE_CANCELLED.isAdded());
        assertFalse(NameStage.DELETED_APPROVED.isAdded());
    }

    /**
     * Test isModified method of NameStage.
     */
    @Test
    public void isModified() {
        assertFalse(NameStage.INITIAL.isModified());
        assertFalse(NameStage.INITIAL_PROPOSED.isModified());
        assertFalse(NameStage.INITIAL_CANCELLED.isModified());
        assertFalse(NameStage.STABLE.isModified());
        assertTrue (NameStage.ACTIVE_MODIFICATION_PROPOSED.isModified());
        assertFalse(NameStage.ACTIVE_DELETION_PROPOSED.isModified());
        assertFalse(NameStage.ACTIVE_CANCELLED.isModified());
        assertFalse(NameStage.DELETED_APPROVED.isModified());
    }

    /**
     * Test isDeleted method of NameStage.
     */
    @Test
    public void isDeleted() {
        assertFalse(NameStage.INITIAL.isDeleted());
        assertFalse(NameStage.INITIAL_PROPOSED.isDeleted());
        assertFalse(NameStage.INITIAL_CANCELLED.isDeleted());
        assertFalse(NameStage.STABLE.isDeleted());
        assertFalse(NameStage.ACTIVE_MODIFICATION_PROPOSED.isDeleted());
        assertTrue (NameStage.ACTIVE_DELETION_PROPOSED.isDeleted());
        assertFalse(NameStage.ACTIVE_CANCELLED.isDeleted());
        assertFalse(NameStage.DELETED_APPROVED.isDeleted());
    }

    /**
     * Test isArchived method of NameStage.
     */
    @Test
    public void isArchived() {
        assertFalse(NameStage.INITIAL.isArchived());
        assertFalse(NameStage.INITIAL_PROPOSED.isArchived());
        assertFalse(NameStage.INITIAL_CANCELLED.isArchived());
        assertFalse(NameStage.STABLE.isArchived());
        assertFalse(NameStage.ACTIVE_MODIFICATION_PROPOSED.isArchived());
        assertFalse(NameStage.ACTIVE_DELETION_PROPOSED.isArchived());
        assertFalse(NameStage.ACTIVE_CANCELLED.isArchived());
        assertTrue (NameStage.DELETED_APPROVED.isArchived());
    }

    /**
     * Test isObsolete method of NameStage.
     */
    @Test
    public void isObsolete() {
        assertFalse(NameStage.INITIAL.isObsolete());
        assertFalse(NameStage.INITIAL_PROPOSED.isObsolete());
        assertTrue (NameStage.INITIAL_CANCELLED.isObsolete());
        assertFalse(NameStage.STABLE.isObsolete());
        assertFalse(NameStage.ACTIVE_MODIFICATION_PROPOSED.isObsolete());
        assertFalse(NameStage.ACTIVE_DELETION_PROPOSED.isObsolete());
        assertFalse(NameStage.ACTIVE_CANCELLED.isObsolete());
        assertTrue (NameStage.DELETED_APPROVED.isObsolete());
    }

    /**
     * Test isRemoved method of NameStage.
     */
    @Test
    public void isRemoved() {
        assertFalse(NameStage.INITIAL.isRemoved());
        assertFalse(NameStage.INITIAL_PROPOSED.isRemoved());
        assertTrue (NameStage.INITIAL_CANCELLED.isRemoved());
        assertFalse(NameStage.STABLE.isRemoved());
        assertFalse(NameStage.ACTIVE_MODIFICATION_PROPOSED.isRemoved());
        assertFalse(NameStage.ACTIVE_DELETION_PROPOSED.isRemoved());
        assertFalse(NameStage.ACTIVE_CANCELLED.isRemoved());
        assertFalse(NameStage.DELETED_APPROVED.isRemoved());
    }

    /**
     * Test isActive method of NameStage.
     */
    @Test
    public void isActive() {
        assertFalse(NameStage.INITIAL.isActive());
        assertFalse(NameStage.INITIAL_PROPOSED.isActive());
        assertFalse(NameStage.INITIAL_CANCELLED.isActive());
        assertTrue (NameStage.STABLE.isActive());
        assertTrue (NameStage.ACTIVE_MODIFICATION_PROPOSED.isActive());
        assertTrue (NameStage.ACTIVE_DELETION_PROPOSED.isActive());
        assertTrue (NameStage.ACTIVE_CANCELLED.isActive());
        assertFalse(NameStage.DELETED_APPROVED.isActive());
    }

    /**
     * Test isPending method of NameStage.
     */
    @Test
    public void isPending() {
        assertFalse(NameStage.INITIAL.isPending());
        assertTrue (NameStage.INITIAL_PROPOSED.isPending());
        assertFalse(NameStage.INITIAL_CANCELLED.isPending());
        assertFalse(NameStage.STABLE.isPending());
        assertTrue (NameStage.ACTIVE_MODIFICATION_PROPOSED.isPending());
        assertTrue (NameStage.ACTIVE_DELETION_PROPOSED.isPending());
        assertFalse(NameStage.ACTIVE_CANCELLED.isPending());
        assertFalse(NameStage.DELETED_APPROVED.isPending());
    }

    /**
     * Test isApproved method of NameStage.
     */
    @Test
    public void isApproved() {
        assertFalse(NameStage.INITIAL.isApproved());
        assertFalse(NameStage.INITIAL_PROPOSED.isApproved());
        assertFalse(NameStage.INITIAL_CANCELLED.isApproved());
        assertTrue (NameStage.STABLE.isApproved());
        assertTrue (NameStage.ACTIVE_MODIFICATION_PROPOSED.isApproved());
        assertTrue (NameStage.ACTIVE_DELETION_PROPOSED.isApproved());
        assertTrue (NameStage.ACTIVE_CANCELLED.isApproved());
        assertTrue (NameStage.DELETED_APPROVED.isApproved());
    }

    /**
     * Test isCancelled method of NameStage.
     */
    @Test
    public void isCancelled() {
        assertFalse(NameStage.INITIAL.isCancelled());
        assertFalse(NameStage.INITIAL_PROPOSED.isCancelled());
        assertTrue (NameStage.INITIAL_CANCELLED.isCancelled());
        assertFalse(NameStage.STABLE.isCancelled());
        assertFalse(NameStage.ACTIVE_MODIFICATION_PROPOSED.isCancelled());
        assertFalse(NameStage.ACTIVE_DELETION_PROPOSED.isCancelled());
        assertTrue (NameStage.ACTIVE_CANCELLED.isCancelled());
        assertFalse(NameStage.DELETED_APPROVED.isCancelled());
    }

    /**
     * Test isPendingDeleted method of NameStage.
     */
    @Test
    public void isPendingDeleted() {
        assertFalse(NameStage.INITIAL.isPendingDeleted());
        assertFalse(NameStage.INITIAL_PROPOSED.isPendingDeleted());
        assertFalse(NameStage.INITIAL_CANCELLED.isPendingDeleted());
        assertFalse(NameStage.STABLE.isPendingDeleted());
        assertFalse(NameStage.ACTIVE_MODIFICATION_PROPOSED.isPendingDeleted());
        assertTrue (NameStage.ACTIVE_DELETION_PROPOSED.isPendingDeleted());
        assertFalse(NameStage.ACTIVE_CANCELLED.isPendingDeleted());
        assertFalse(NameStage.DELETED_APPROVED.isPendingDeleted());
    }

    /**
     * Test isApprovedDeleted method of NameStage.
     */
    @Test
    public void isApprovedDeleted() {
        assertFalse(NameStage.INITIAL.isApprovedDeleted());
        assertFalse(NameStage.INITIAL_PROPOSED.isApprovedDeleted());
        assertFalse(NameStage.INITIAL_CANCELLED.isApprovedDeleted());
        assertFalse(NameStage.STABLE.isApprovedDeleted());
        assertFalse(NameStage.ACTIVE_MODIFICATION_PROPOSED.isApprovedDeleted());
        assertFalse(NameStage.ACTIVE_DELETION_PROPOSED.isApprovedDeleted());
        assertFalse(NameStage.ACTIVE_CANCELLED.isApprovedDeleted());
        assertTrue (NameStage.DELETED_APPROVED.isApprovedDeleted());
    }

}
