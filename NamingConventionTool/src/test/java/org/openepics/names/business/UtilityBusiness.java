/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePartRevision;

/**
 * Utility class for test purposes, in particular of org.openepics.names.business package classes.
 *
 * @author Lars Johansson
 */
public class UtilityBusiness {

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * This class is not to be instantiated.
     */
    private UtilityBusiness() {
        throw new IllegalStateException("Utility class");
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Constructs a new name artifact entity with {@link NameType#SYSTEM_STRUCTURE} from a UUID identifier.
     *
     * @param uuid  the universally unique identifier
     * @return      name artifact
     */
    public static NameArtifact createNameArtifactSystemStructure(String uuid) {
        return new NameArtifact(UUID.fromString(uuid), NameType.SYSTEM_STRUCTURE);
    }
    /**
     * Constructs a new name artifact entity with {@link NameType#DEVICE_STRUCTURE} from a UUID identifier.
     *
     * @param uuid  the universally unique identifier
     * @return      name artifact
     */
    public static NameArtifact createNameArtifactDeviceStructure(String uuid) {
        return new NameArtifact(UUID.fromString(uuid), NameType.DEVICE_STRUCTURE);
    }
    /**
     * Constructs a new name artifact entity with {@link NameType#DEVICE_REGISTRY} from a UUID identifier.
     *
     * @param uuid  the universally unique identifier
     * @return      name artifact
     */
    public static NameArtifact createNameArtifactDeviceRegistry(String uuid) {
        return new NameArtifact(UUID.fromString(uuid), NameType.DEVICE_REGISTRY);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Constructs a new name revision entity (representing database entry).
     *
     * @param nameArtifact              the name instance
     * @param systemStructureParent     the parent nameArtifact in the system structure
     * @param deviceStructureParent     the parent nameArtifact in the device structure
     * @param nameElement               the name element containing full name, mnemonic and description
     * @param request                   the user request
     * @param process                   the user process
     * @param deleted                   indicates if the revision is either pending deletion or is archived (approved)
     * @param status                    the status of the revision, pending, approved, cancelled or rejected
     * @return                          name revision
     */
    public static NameRevision createNameRevision(
            NameArtifact nameArtifact,
            NameArtifact systemStructureParent,
            NameArtifact deviceStructureParent,
            NameElement nameElement,
            UserAction request,
            UserAction process,
            boolean deleted,
            NameRevisionStatus status) {
        return new NameRevision(
                nameArtifact, systemStructureParent, deviceStructureParent,
                nameElement, request, process, deleted, status);
    }
    /**
     * Constructor to create a name revision entity from a current database entity.
     *
     * @param revision  the device revision
     * @return          name revision
     */
    public static NameRevision createNameRevision(DeviceRevision revision) {
        return new NameRevision(revision);
    }
    /**
     * Constructor to create a nameRevision from a current database entity.
     *
     * @param revision  the name part revision
     * @return          name revision
     */
    public static NameRevision createNameRevision(NamePartRevision revision) {
        return new NameRevision(revision);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return parsed date according to {@link UtilityBusiness#SDF} or new date if given date can not be parsed.
     *
     * @param date  given date to be parsed
     * @return      parsed date according to {@link UtilityBusiness#SDF} or new date if given date can not be parsed
     */
    public static Date parseDateOrNewDate(String date) {
        try {
            return SDF.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

}
