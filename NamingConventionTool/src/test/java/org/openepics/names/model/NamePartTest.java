/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for NamePart class.
 *
 * @author Lars Johansson
 *
 * @see NamePart
 */
public class NamePartTest {

    private static final String UUID1 = "4262e1e7-2444-412e-83d7-aeabf58262c6";
    private static final String UUID2 = "c6e84e09-29d7-49a0-8ecd-92a6eff86da4";
    private static final String UUID3 = "abc14850-594e-4924-b5e8-aba7aa7865e5";
    private static final String UUID4 = "a796360b-2634-4499-944b-8bd5c46aea16";

    private static NamePart namePartSection1;
    private static NamePart namePartSection2;
    private static NamePart namePartDeviceType1;
    private static NamePart namePartDeviceType2;

    /**
     * One-time initialization code.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        namePartSection1    = UtilityModel.createNamePartSection   (UUID1);
        namePartSection2    = UtilityModel.createNamePartSection   (UUID2);
        namePartDeviceType1 = UtilityModel.createNamePartDeviceType(UUID3);
        namePartDeviceType2 = UtilityModel.createNamePartDeviceType(UUID4);
    }

    /**
     * One-time cleanup code.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        namePartSection1    = null;
        namePartSection2    = null;
        namePartDeviceType1 = null;
        namePartDeviceType2 = null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test constructor of NamePart.
     */
    @Test
    public void constructorEmpty() {
        NamePart namePart = new NamePart();

        assertNotNull(namePart);
    }

    /**
     * Test constructor of NamePart.
     */
    @Test(expected = NullPointerException.class)
    public void constructorUuidNull() {
        new NamePart(null, NamePartType.SECTION);
    }

    /**
     * Test constructor of NamePart.
     */
    @Test(expected = NullPointerException.class)
    public void constructorNamePartTypeNull() {
        new NamePart(UUID.fromString(UUID1), null);
    }

    /**
     * Test constructor of NamePart.
     */
    @Test
    public void constructor() {
        NamePart namePart1 = UtilityModel.createNamePartDeviceType(UUID2);
        NamePart namePart2 = UtilityModel.createNamePartSection   (UUID3);

        assertNotNull(namePart1);
        assertNotNull(namePart2);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test equals method of UserAccount.
     */
    @Test
    public void equals() {
        NamePart namePart1 = UtilityModel.createNamePartSection   (UUID1);
        NamePart namePart2 = UtilityModel.createNamePartSection   (UUID2);
        NamePart namePart3 = UtilityModel.createNamePartDeviceType(UUID3);
        NamePart namePart4 = UtilityModel.createNamePartDeviceType(UUID4);

        assertNotNull  (namePart1);
        assertEquals   (namePart1, namePart1);
        assertNotEquals(namePart1, namePart2);
        assertNotEquals(namePart1, namePart3);
        assertNotEquals(namePart1, namePart4);
        assertEquals   (namePart1, namePartSection1);
        assertNotEquals(namePart1, namePartSection2);
        assertNotEquals(namePart1, namePartDeviceType1);
        assertNotEquals(namePart1, namePartDeviceType2);

        assertEquals   (namePart2, namePart2);
        assertNotEquals(namePart2, namePart3);
        assertNotEquals(namePart2, namePart4);
        assertNotEquals(namePart2, namePartSection1);
        assertEquals   (namePart2, namePartSection2);
        assertNotEquals(namePart2, namePartDeviceType1);
        assertNotEquals(namePart2, namePartDeviceType2);

        assertEquals   (namePart3, namePart3);
        assertNotEquals(namePart3, namePart4);
        assertNotEquals(namePart3, namePartSection1);
        assertNotEquals(namePart3, namePartSection2);
        assertEquals   (namePart3, namePartDeviceType1);
        assertNotEquals(namePart3, namePartDeviceType2);

        assertEquals   (namePart4, namePart4);
        assertNotEquals(namePart4, namePartSection1);
        assertNotEquals(namePart4, namePartSection2);
        assertNotEquals(namePart4, namePartDeviceType1);
        assertEquals   (namePart4, namePartDeviceType2);

        assertEquals   (namePartSection1, namePartSection1);
        assertNotEquals(namePartSection1, namePartSection2);
        assertNotEquals(namePartSection1, namePartDeviceType1);
        assertNotEquals(namePartSection1, namePartDeviceType2);

        assertEquals   (namePartSection2, namePartSection2);
        assertNotEquals(namePartSection2, namePartDeviceType1);
        assertNotEquals(namePartSection2, namePartDeviceType2);

        assertEquals   (namePartDeviceType1, namePartDeviceType1);
        assertNotEquals(namePartDeviceType1, namePartDeviceType2);

        assertEquals   (namePartDeviceType2, namePartDeviceType2);
    }

    /**
     * Test hashcode method of UserAccount.
     */
    @Test
    public void hashCodeTest() {
        NamePart namePart1 = UtilityModel.createNamePartSection   (UUID1);
        NamePart namePart2 = UtilityModel.createNamePartSection   (UUID2);
        NamePart namePart3 = UtilityModel.createNamePartDeviceType(UUID3);
        NamePart namePart4 = UtilityModel.createNamePartDeviceType(UUID4);

        assertEquals(namePart1.hashCode(), namePartSection1.hashCode());
        assertEquals(namePart2.hashCode(), namePartSection2.hashCode());
        assertEquals(namePart3.hashCode(), namePartDeviceType1.hashCode());
        assertEquals(namePart4.hashCode(), namePartDeviceType2.hashCode());

        assertEquals   (namePartSection1.hashCode(),    namePartSection1.hashCode());
        assertNotEquals(namePartSection1.hashCode(),    namePartSection2.hashCode());
        assertNotEquals(namePartSection1.hashCode(),    namePartDeviceType1.hashCode());
        assertNotEquals(namePartSection1.hashCode(),    namePartDeviceType2.hashCode());

        assertEquals   (namePartSection2.hashCode(),    namePartSection2.hashCode());
        assertNotEquals(namePartSection2.hashCode(),    namePartDeviceType1.hashCode());
        assertNotEquals(namePartSection2.hashCode(),    namePartDeviceType2.hashCode());

        assertEquals   (namePartDeviceType1.hashCode(), namePartDeviceType1.hashCode());
        assertNotEquals(namePartDeviceType1.hashCode(), namePartDeviceType2.hashCode());

        assertEquals   (namePartDeviceType2.hashCode(), namePartDeviceType2.hashCode());
    }

}
