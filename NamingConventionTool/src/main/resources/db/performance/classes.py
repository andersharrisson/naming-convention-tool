#
#Version: 0.01a
# Helper script for creating and cleaning database 
#
#Requirements to run this script : python and pg8000
#To install pg8000 : pip install pg8000
#
from datetime import datetime
import random
import uuid

def createString(word, i):
    ''' returns a substring of word appended with a number according to index i'''
    return word[:(i % len(word)) + 1] + str(i//len(word))   

def getFromList(aList, idx):
    ''' return element with index idx from aList if idx is greater that length of a list it returns idx - k*length'''
    if len(aList) != 0:
        return str(aList[idx % len(aList)]) 
    
class Base():
      
    def insert(self, cursor):
        ''' Inserts object data into databse through connection provided by cursor and returns id'''
        return self.insertIntoDatabase(self.__class__.table , self.__class__.columns, self.getValue(), cursor)
    
    def insertIntoDatabase(self, table, columns, value, cursor):
        '''Inserts "value" data as "columns" into database "table" through connection provided by cursor and returns id'''
        cursor.execute("INSERT INTO " + table  + " (" + ", ".join(columns) + ") " + "VALUES " + "('" + "', '".join(value) + "')"  + " RETURNING id")
        self.id =  [id[0] for id in cursor.fetchall()][0]
        return self.id

class NamePart(Base):
    columns = ["version", "nameparttype", "uuid"]
    table = "namepart"
        
    def __init__(self, namepartType):
        '''params:
            namepartType - str "DEVICE_TYPE" or "SECTION"'''
        self.version = str(0)
        self.namepartType = namepartType
        self.UUID = str(uuid.uuid4())
        self.id = -1
        
    def getValue(self):
        return [self.version, self.namepartType, self.UUID]

class NamePartRevision(Base):
    statuses = ["APPROVED" , "CANCELLED", "PENDING" ,"REJECTED"]
    columns = ["version", "deleted", "mnemonic", "name", "processdate", "processorcomment", "requestdate",
                            "requestercomment", "status", "namepart_id", "processedby_id", "requestedby_id", "mnemoniceqclass", "description" , "parent_id",]
    table = "namepartrevision"
    
    def __init__(self, i, version, name,  mnemonic , namepartId , ownerId,  parent = None):
        '''params:
            i - int used for data generation
            version - int version of revision
            name - str name of the namepart revision
            mnemonic - str of length 3 mnemonic of the namepart revison  
            namepartId - int database id on the namepart this revision belongs to
            ownerId - int database id of the user this revision belongs to
            parent - NamerPartRevison the parent of this revision. Default None
             '''
        self.version = str(version)
        self.deleted  = "f"
        self.mnemonic = mnemonic
        self.name = mnemonic + ":" + name + "_"
        self.processdate = str(datetime.now())
        self.processorcomment = createString(NamePartRevision.columns[5], i)
        self.requestdate = str(datetime.now())
        self.requestcomment = createString(NamePartRevision.columns[7], i)
        self.status = NamePartRevision.statuses[0]
        self.namepart_id = str(namepartId)
        self.parent = parent 
        if self.parent != None:
            self.parent_id = str(parent.namepart_id)
        else:
            self.parent_id = "0"
        self.processedby_id = str(ownerId)
        self.requestedby_id = str(ownerId)
        self.mnemoniceqclass =  mnemonic.upper()
        self.description = createString(NamePartRevision.columns[-2], i) 
        self.id = -1
        
    def getValue(self):
        return (self.version, self.deleted, self.mnemonic, self.name, self.processdate, self.processorcomment, self.requestdate, 
            self.requestcomment, self.status, self.namepart_id, self.processedby_id, self.requestedby_id, self.mnemoniceqclass, self.description, self.parent_id)
    
    def insert(self, cursor):
        if self.parent_id != "0":
            Base.insert(self, cursor)
        else:
            return Base.insertIntoDatabase(self, self.table, self.columns[:-1], self.getValue()[:-1], cursor)

class Device(Base):
    columns = ["version", "uuid"]
    table = "device"
        
    def __init__(self):
        self.version = str(0)
        self.UUID = str(uuid.uuid4())
        self.id = -1
         
    def getValue(self):
        return (self.version, self.UUID)

class DeviceRevision(Base):
    columns = ["version", "conventionname", "conventionnameeqclass", "deleted", "instanceindex", "requestdate", "device_id", "devicetype_id",
                            "requestedby_id", "section_id" , "additionalinfo"]
    table = "devicerevision"
    def __init__(self, i, version , deviceId, deviceTypeParent, sectionParent, ownerId):
        '''params:
            i - int used for data generation
            version - int version of revision
            deviceId - int database id on the device this revision belongs to
            parent - deviceTypeParent the deviceType parent of the revision
            parent - sectionParent the section parent of this revision
            ownerId - int database id of the user this revision belongs to
        '''
        self.version = str(version)
        self.idx = str(i).zfill(6)
        self.parents = sectionParent.parent.mnemonic + "-" + sectionParent.mnemonic + ":" + deviceTypeParent.parent.parent.mnemonic + "-" + deviceTypeParent.mnemonic
        self.conventionname = self.parents + "-"+ self.idx
        self.conventionnameeqclass = self.conventionname.upper()
        self.deleted  = "f"
        self.requestdate = str(datetime.now()) 
        self.device_id = str(deviceId)
        self.devicetype_id = str(deviceTypeParent.namepart_id)
        self.requestedby_id = str(ownerId)
        self.section_id = str(sectionParent.namepart_id)
        self.additionainfo = createString(DeviceRevision.columns[-1], i) 
        self.id = -1
        
    def getValue(self):
        return (self.version, self.conventionname, self.conventionnameeqclass, self.deleted, self.idx, self.requestdate, self.device_id, self.devicetype_id,
                                  self.requestedby_id, self.section_id, self.additionainfo)
