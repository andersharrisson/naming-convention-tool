/*
 * Copyright (c) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

/**
 * Helper class to have information for namesakes for a mnemonic.
 *
 * @author Lars Johansson
 *
 * @see TransactionBean#namesakes(String)
 */
public class NameSake {

    private NameView nameView;
    private boolean isLatestRevision;
    private boolean isBaseRevision;

    /**
     * Constructor
     *
     * @param nameView name view
     * @param isLatestRevision if information is considered latest revision
     * @param isBaseRevision if revision is considered base revision
     */
    public NameSake(NameView nameView, boolean isLatestRevision, boolean isBaseRevision) {
        this.nameView = nameView;
        this.isLatestRevision = isLatestRevision;
        this.isBaseRevision = isBaseRevision;
    }

    /**
     * Return name view of <tt>NameSake</tt>.
     *
     * @return name view of <tt>NameSake</tt>
     */
    public NameView getNameView() {
        return nameView;
    }

    /**
     * Return if information is considered latest revision.
     *
     * @return if information is considered latest revision
     */
    public boolean isLatestRevision() {
        return isLatestRevision;
    }

    /**
     * Return if revision is considered base revision.
     *
     * @return if revision is considered base revision
     */
    public boolean isBaseRevision() {
        return isBaseRevision;
    }

}
