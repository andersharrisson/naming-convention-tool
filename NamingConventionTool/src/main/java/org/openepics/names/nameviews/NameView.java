/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.openepics.names.util.As;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.NameType;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * A view of a name with latest approved and pending revisions, parent child relations etc.
 *
 * In effect, a data object with relations.
 *
 * @author Karin Rathsman
 */
public class NameView {

    private NameRevisionPair revisionPair;
    private List<NameView> children;
    private NameView systemStructureParent;
    private NameView deviceStructureParent;
    private NameType nameType;
    private int level;

    /**
     * Constructs a name view from an existing name view.
     *
     * @param nameView existing name view
     */
    public NameView(NameView nameView) {
        this(nameView.getNameType());
        this.revisionPair = new NameRevisionPair(nameView.getRevisionPair());
    }

    /**
     * Constructs an empty top level name view.
     *
     * @param nameType name type, i.e. type of structure
     */
    public NameView(NameType nameType) {
        Preconditions.checkArgument(nameType != null);
        this.nameType = nameType;
        this.level = 0;
        this.children = Lists.newArrayList();
        this.revisionPair = new NameRevisionPair();
        this.systemStructureParent = null;
        this.deviceStructureParent = null;
    }

    /**
     *
     * @return the name type.
     */
    public NameType getNameType() {
        return nameType;
    }

    /**
     *
     * @return the latest approved and latest unapproved revision pair
     */
    public NameRevisionPair getRevisionPair() {
        return revisionPair;
    }

    /**
     * Return the name view parent of NameType type.
     *
     * @param type the parent type
     * @return the name view parent of NameType type
     */
    public NameView getParent(NameType type) {
        Preconditions.checkArgument(!type.isDeviceRegistry());
        return type.isDeviceStructure() ? deviceStructureParent : systemStructureParent;
    }

    /**
     * Set the parent and update the parent child relationship.
     *
     * @param parent the parent name view
     * @param type the root type.
     */
    protected void setParent(NameView parent, NameType type) {
        Preconditions.checkArgument(type != null && !type.isDeviceRegistry());
        updateParentChild(getParent(type), parent);
        if (type.isDeviceStructure()) {
            this.deviceStructureParent = parent;
        } else {
            this.systemStructureParent = parent;
        }
        updateLevel();
    }

    /**
     * update the parent child relationship
     *
     * @param source the current nameView parent
     * @param destination the updated nameView parent
     */
    private void updateParentChild(NameView source, NameView destination) {
        if (source != null) {
            source.removeChild(this);
        }
        if (destination != null) {
            destination.addChild(this);
        }
    }

    /**
     * adds child nameViews and sorts children
     *
     * @param child the child to add
     */
    private void addChild(NameView child) {
        if (child != null) {
            children.add(child);
        }
        if (children.size() > 1) {
            sortChildren();
        }
    }

    /**
     * remove child nameViews
     *
     * @param child
     */
    private void removeChild(NameView child) {
        children.remove(child);
    }

    /**
     * Sorts children
     */
    private void sortChildren() {
        Collections.sort(children, new Comparator<NameView>() {
            @Override
            public int compare(NameView left, NameView right) {
                String str1 = left.getRevisionPair().getBaseRevision().getNameElement().getFullName();
                String str2 = right.getRevisionPair().getBaseRevision().getNameElement().getFullName();
                int n = String.CASE_INSENSITIVE_ORDER.compare(str1, str2);
                if (n == 0) {
                    n = str1.compareTo(str2);
                }
                return n;
            }
        });
    }

    /**
     * @return the children
     */
    public List<NameView> getChildren() {
        return children;
    }

    /**
     * Return all children.
     *
     * @return all children
     */
    public List<NameView> getAllChildren() {
        return getAllChildren(null);
    }

    /**
     * Return all children for given {@link NameType}.
     *
     * @param nameType system structure, device structure, device registry or <tt>null</tt> if not specified
     * @return all children for given {@link NameType}
     */
    public List<NameView> getAllChildren(NameType nameType) {
        // recursive
        List<NameView> allChildren = Lists.newArrayList();
        for (NameView child : children) {
            if (nameType != null) {
                if (nameType.equals(child.getNameType())) {
                    allChildren.add(child);
                    List<NameView> allChildrenForChild = child.getAllChildren(nameType);
                    for (NameView nameView : allChildrenForChild) {
                        allChildren.add(nameView);
                    }
                }
            } else {
                allChildren.add(child);
                List<NameView> allChildrenForChild = child.getAllChildren();
                for (NameView nameView : allChildrenForChild) {
                    allChildren.add(nameView);
                }
            }
        }
        return allChildren;
    }

    /**
     * Updates the level as the parent child level is updated.
     */
    private void updateLevel() {
        if (nameType.isDeviceRegistry()) {
            this.level = 4;
        } else {
            this.level = getParent(nameType) != null ? getParent(nameType).getLevel() + 1 : 0;
            for (NameView child : getChildren()) {
                child.updateLevel();
            }
        }
    }

    /**
     *
     * @return the level in the hierarchy, starting with zero at the root
     */
    public int getLevel() {
        return level;
    }

    /**
     *
     * @return  true if the nameView is root,
     *          i.e. stage is initial, device type is system or device structure and level is null.
     */
    public boolean isRoot() {
        return getLevel() == 0
                && getRevisionPair().getNameStage().isInitial()
                && (nameType.equals(NameType.SYSTEM_STRUCTURE) || nameType.equals(NameType.DEVICE_STRUCTURE));
    }

    /**
     * Return if name view is in structure (i.e. not root or device instance) of type nameType.
     *
     * @param nameType the structure name type
     * @return true if name view is in structure (i.e. not root or device instance) of type nameType
     */
    public boolean isInStructure(NameType nameType) {
        return isInStructure() && this.nameType.equals(nameType);
    }

    /**
     *
     * @return true if the name view is between level 1 and 3 and nameType is system or device structure.
     */
    public boolean isInStructure() {
        return getLevel() > 0
                && getLevel() < 4
                && !getRevisionPair().getNameStage().isInitial()
                && (nameType.equals(NameType.SYSTEM_STRUCTURE) || nameType.equals(NameType.DEVICE_STRUCTURE));
    }

    /**
     *
     * @return true if the nameView is a device
     */
    public boolean isInDeviceRegistry() {
        return getLevel() == 4
                && getRevisionPair().getBaseRevision() != null
                && nameType.equals(NameType.DEVICE_REGISTRY);
    }

    /**
     *
     * @return true if children are in the system or device structure
     */
    public boolean isChildInStructure() {
        return (isRoot() || isInStructure()) && getLevel() < 3;
    }

    /**
     *
     * @return true if children are devices
     */
    public boolean isChildInDeviceRegistry() {
        // note
        //     child is     in device registry if level is 3
        //     child may be in device registry if level is 1 or 2 (system structure)

        return isInStructure() && getLevel() == 3;
    }

    /**
     * @return true if children can be devices
     */
    public boolean canChildBeInDeviceRegistry() {
        if (isInStructure()) {
            if (getLevel() == 3) {
                return true;
            } else if (NameType.SYSTEM_STRUCTURE.equals(getNameType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return list of name part full names starting from the root of the hierarchy
     * to the child that is be added to this name part.
     *
     * @param fullName full name of the name part
     * @return list of name part full names starting from the root of the hierarchy
     *         to the child that is be added to this name part
     */
    public List<String> getFullnamePathWithChild(String fullName) {
        Preconditions.checkArgument(isInStructure() || isRoot());
        final ImmutableList.Builder<String> pathElements = ImmutableList.builder();
        pathElements.add(As.emptyIfNull(fullName));
        for (NameView pathElement = this;
                !pathElement.isRoot();
                pathElement = pathElement.getParent(pathElement.getNameType())) {
            pathElements.add(
                    pathElement.getRevisionPair().getBaseRevision().getNameElement().getFullName() != null
                    ? pathElement.getRevisionPair().getBaseRevision().getNameElement().getFullName()
                            : "");
        }
        return pathElements.build().reverse();
    }

    /**
     * Return list of name part full names starting from the root of the hierarchy to this name part.
     *
     * @return list of name part full names starting from the root of the hierarchy to this name part
     */
    public List<String> getFullnamePath() {
        Preconditions.checkArgument(isInStructure());
        return getParent(getNameType())
                .getFullnamePathWithChild(getRevisionPair().getBaseRevision().getNameElement().getFullName());
    }

    /**
     * Return the list of name part mnemonic names starting from the root of the hierarchy
     * to the child that is be added to this name part.
     *
     * @param mnemonic  the mnemonic of the name part
     * @return          the list of name part mnemonic names starting from the root of the hierarchy
     *                  to the child that is be added to this name part
     */
    public List<String> getMnemonicPathWithChild(String mnemonic) {
        Preconditions.checkArgument(isInStructure() || isRoot());
        final ImmutableList.Builder<String> pathElements = ImmutableList.builder();
        pathElements.add(As.emptyIfNull(mnemonic));
        for (NameView pathElement = this;
                !pathElement.isRoot();
                pathElement = pathElement.getParent(pathElement.getNameType())) {
            pathElements.add(
                    pathElement.getRevisionPair().getBaseRevision().getNameElement().getMnemonic() != null
                            ? pathElement.getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : "");
        }
        return pathElements.build().reverse();
    }

    /**
     * Return list of name part mnemonic names starting from the root of the hierarchy to this name part,
     * using base revision.
     *
     * @return list of name part mnemonic names starting from the root of the hierarchy to this name part,
     *         using base revision
     */
    public List<String> getMnemonicPath() {
        Preconditions.checkArgument(isInStructure());
        return getParent(getNameType())
                .getMnemonicPathWithChild(getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
    }

    /**
     * Return list of name part mnemonic names starting from the root of the hierarchy to this name part,
     * using latest revision.
     *
     * @return list of name part mnemonic names starting from the root of the hierarchy to this name part,
     *         using latest revision
     */
    public List<String> getMnemonicPathLatestRevision() {
        Preconditions.checkArgument(isInStructure());
        return getParent(getNameType())
                .getMnemonicPathWithChild(getRevisionPair().getLatestRevision().getNameElement().getMnemonic());
    }

    /**
     *
     * @return the set of devices in the registry descending from this nameView.
     */
    public List<NameView> getDevices() {
        List<NameView> devices = Lists.newArrayList();
        for (NameView child : getChildren()) {
            if (child.isInDeviceRegistry()) {
                devices.add(child);
            } else {
                devices.addAll(child.getDevices());
            }
        }
        return devices;
    }
    /**
    *
    * @return the set of devices in the registry descending one level from this nameView, i.e. descendants one level.
    */
   public List<NameView> getDevicesOneLevel() {
       List<NameView> devices = Lists.newArrayList();
       for (NameView child : getChildren()) {
           if (child.isInDeviceRegistry()) {
               devices.add(child);
           }
       }
       return devices;
   }

    /**
     *
     * @return the set of active device parents descending from this nameView.
     */
    public Set<NameView> getAllowedDeviceParents() {
        Set<NameView> deviceParents = Sets.newHashSet();
        if (canChildBeInDeviceRegistry()
                && getRevisionPair().getNameStage().isAllowedAsParentOf(NameStage.STABLE)
                && !StringUtils.isEmpty(getRevisionPair().getBaseRevision().getNameElement().getMnemonic())) {
            deviceParents.add(this);
        }
        for (NameView child : getChildren()) {
            deviceParents.addAll(child.getAllowedDeviceParents());
        }
        return deviceParents;
    }

    /**
     * Return text description of {@link NameView} structure.
     *
     * @return text description of {@link NameView} structure
     */
    public String toStringStructure() {
        // description for current level and upwards
        //     0   name type
        //     1-4 full name [mnemonic]
        // --------------------------------------------------------------------------------
        // e.g. Device Structure, Cryogenics [Cryo], Switches, Temperature Differential Switch [TDS].
        // --------------------------------------------------------------------------------
        // note recursive

        if (level < 0 || level > 4) {
            return toString();
        }

        if (isRoot()) {
            return (getNameType().getName());
        } else {
            NameElement nameElement = getRevisionPair().getBaseRevision().getNameElement();
            String mnemonic = StringUtils.trimToEmpty(nameElement.getMnemonic());
            if (!StringUtils.isEmpty(mnemonic)) {
                mnemonic = " [" + mnemonic + "]";
            }

            StringBuilder sb = new StringBuilder();
            sb.append(getParent(getNameType()).toStringStructure());
            sb.append(", ");
            sb.append(nameElement.getFullName());
            sb.append(mnemonic);

            return sb.toString();
        }
    }

}
