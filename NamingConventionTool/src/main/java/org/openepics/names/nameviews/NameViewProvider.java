/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.openepics.names.business.NameArtifact;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.business.NameRevisionService;
import org.openepics.names.business.NameType;
import org.openepics.names.business.RowData;
import org.openepics.names.operation.SingleNameOperation;
import org.openepics.names.services.NamingConvention;
import org.openepics.names.ui.common.TreeNodeManager;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.util.Set;

/**
 * Been that generates the nameView tree and updates name views.
 *
 * @author karinrathsman
 *
 */
@ApplicationScoped
public class NameViewProvider implements Serializable {

    // Note revision history of file in repository.

    /**
     *
     */
    private static final long serialVersionUID = 6916728601880286405L;
    @Inject
    private NamingConvention namingConvention;
    @Inject
    private NameRevisionService nameRevisionService;
    private NameRevisions nameRevisions;
    private NameViews nameViews;

    /**
     * Initializes the name view provider.
     */
    public void init() {
        nameViews = new NameViews();
        nameRevisions = new NameRevisions();
        update(nameRevisionService.revisions());
    }

    /**
     *
     * @return the nameViews
     */
    public NameViews getNameViews() {
        return nameViews;
    }

    protected synchronized void update(List<NameRevision> newRevisions) {
        for (NameRevision revision : newRevisions) {
            nameViews.update(revision);
            nameRevisions.update(revision);
        }
    }

    /**
     * Return the nameView for the specified uuid.
     *
     * @param uuid the unique identifier for the name instance
     * @return the nameView for the specified uuid
     */
    public NameView nameView(UUID uuid) {
        return nameViews.get(uuid);
    }

    /**
     * Return the root of the name structure of type nameType.
     *
     * @param nameType the type of the structure (system or device structure)
     * @return the root of the name structure of type nameType
     */
    public NameView nameStructure(NameType nameType) {
        return nameViews.getRoot(nameType);
    }

    /**
     * Return the name element definition of the name part type to be used in dialog header and menus.
     *
     * @param nameView the name view
     * @return the name element definition of the name part type to be used in dialog header and menus.
     * Example: "Modify mnemonic for nameElement.getFullName()" where nameElement can be system, subsystem,
     * discipline etc...
     */
    public NameElement nameElementDefinitionForChild(NameView nameView) {
        Preconditions.checkArgument(nameView != null);
        final List<String> path = nameView.getMnemonicPathWithChild("");
        return namingConvention.getNameElementDefinition(path, nameView.getNameType());
    }

    /**
     * Generate all revisions in history for a device
     *
     * @param nameView the nameView
     * @return List of revisions in history.
     */
    public List<NameRevision> getHistoryRevisions(NameView nameView) {
        return nameRevisionService.historyRevisions(nameView.getRevisionPair().getBaseRevision().getNameArtifact());
    }

    /**
     * find the artifact
     *
     * @param nameView the artifact of the specified nameView
     * @return the name artifact
     */
    private static NameArtifact artifact(NameView nameView) {
        return nameView != null && !nameView.isRoot()
                ? nameView.getRevisionPair().getBaseRevision().getNameArtifact()
                : null;
    }

    /**
     * Generates the Header
     *
     * @param nameOperation the single name operation
     * @return title to be used in headers
     */
    public String getHeader(SingleNameOperation nameOperation) {
        NameView parentView = nameOperation.getParentView();
        return nameOperation.getTitle() + " " + nameElementDefinitionForChild(parentView).getFullName();
    }

    /**
     * Generate the history revisions
     *
     * @param nameView the nameView
     * @return all the revision of the name as RowData.
     */
    public List<RowData> getHistoryRevisionsAsRowData(NameView nameView) {
        List<RowData> historyRevisionsAsRowData = Lists.newArrayList();
        if (nameView != null && !nameView.getRevisionPair().getNameStage().isInitial()) {
            NameRevisionPair revisionPair = new NameRevisionPair();
            List<NameRevision> historyRevisions = nameRevisionService.historyRevisions(artifact(nameView));
            for (NameRevision revision : historyRevisions) {
                if (nameView.isInStructure()
                        && !revision.getStatus().isPending()
                        && revision.getUserAction().getUser() != null) {
                    // TODO : update nameRevisions with only one user action and remove this...
                    boolean approved = revisionPair.getNameStage().isApproved();
                    NameElement approvedElement = approved ? revisionPair.getApprovedRevision().getNameElement() : null;
                    historyRevisionsAsRowData.add(
                            new RowData(
                                    revision.getNameElement(),
                                    approvedElement,
                                    revision.getUserAction(),
                                    true,
                                    revision.isDeleted(),
                                    approved,
                                    false,
                                    false,
                                    false));
                }
                revisionPair.update(revision);
                historyRevisionsAsRowData.add(TreeNodeManager.rowData(revisionPair, true, true));
            }
        }
        return historyRevisionsAsRowData;
    }

    /**
     * @return the restfulNameRevisionMap that maps a given device name with the
     * latest revision with that name.
     */
    public NameRevisions getNameRevisions() {
        return nameRevisions;
    }

    /**
     * This class handles map of key-value with equivalence class representative of convention name as key
     * and name revision as value. It is used to build structure of ESS name data.
     *
     * <p>
     * Equivalence class representative of convention name is used as key as it ensure uniqueness of names
     * when treating similar looking names. Name revision represents database entry.
     */
    public class NameRevisions {

        private Map<String, NameRevision> nameRevisionMap;

        /**
         * Constructs new map for equivalence class representative of convention name and name revision objects.
         */
        public NameRevisions() {
            nameRevisionMap = new HashMap<>();
        }

        private String key(String name) {
            return namingConvention.equivalenceClassRepresentative(name);
        }

        /**
         * Retrieves content from nameRevisionMap for given parameter as <tt>uuid</tt> or <tt>name</tt>.
         * Among usage is REST API and its methods.
         *
         * @param string uuid or name
         * @return name revision
         */
        public NameRevision get(String string) {
            // note
            //     equivalenceClassRepresentative of name as key in nameRevisionMap
            //
            //     method not to call key method with input string directly since that would mean
            //     calling equivalenceClassRepresentative twice, which would potentially strip
            //     information from input string, e.g. CRY0 would become CRY
            //
            //     method works with
            //     - name
            //     - equivalenceClassRepresentative of name
            //     - uuid
            //
            //     1. consider input equivalenceClassRepresentative
            //     2. consider input name (make equivalenceClassRepresentative)
            //     3. consider input uuid

            String key = string;
            if (nameRevisionMap.containsKey(key)) {
                return nameRevisionMap.get(key);
            } else if (nameRevisionMap.containsKey(key(key))) {
                return nameRevisionMap.get(key(key));
            } else {
                try {
                    UUID uuid = UUID.fromString(string);
                    NameView nameView = nameView(uuid);
                    return nameView.getRevisionPair().getApprovedRevision();
                } catch (Exception e) {
                    return null;
                }
            }
        }

        /**
         * Updates the nameRevisionMap if the specified revision supersedes the
         * existing revision with the same name
         *
         * @param revision
         */
        protected void update(NameRevision revision) {
            // note that equivalenceClassRepresentative of name as key in nameRevisionMap
            if (revision.getNameArtifact().getNameType().isDeviceRegistry()) {
                String key = key(revision.getNameElement().getFullName());
                NameRevision otherRevision = nameRevisionMap.get(key);
                if (revision.supersede(otherRevision)) {
                    nameRevisionMap.put(key, revision);
                }
            }
        }

        /**
         * Return key set for name revision map.
         *
         * @return key set for name revision map
         *
         * @see NameRevisions
         */
        public Set<String> keySet() {
            return nameRevisionMap.keySet();
        }

        /**
         * Return entry set for name revision map.
         *
         * @return entry set for name revision map
         *
         * @see NameRevisions
         */
        public Set<Map.Entry<String, NameRevision>> entrySet() {
            return nameRevisionMap.entrySet();
        }

        /**
         * Return if name revision map contains given key.
         *
         * @param name key for name revision map
         * @return if name revision map contains given key
         *
         * @see NameRevisions
         */
        public boolean contains(String name) {
            return nameRevisionMap.containsKey(key(name));
        }

    }

}
