/*
 * Copyright (c) 2015 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.nameviews;

import org.openepics.names.business.NameType;

/**
 * View for instances of device records in table
 *
 * @author karinrathsman
 *
 */
public class DeviceRecordView {

    private final NameView device;

    /**
     * Constructs a new device record view base on a name view representing a device.
     *
     * @param deviceView name view, data object with relations, representing a device
     */
    public DeviceRecordView(NameView deviceView) {
        this.device = deviceView;
    }

    /**
     * @return the systemGroup
     */
    public NameView getSystemGroup() {
        NameView parent = device != null ? device.getParent(NameType.SYSTEM_STRUCTURE) : null;
        if (parent != null) {
            if (parent.getLevel() == 3) {
                return parent.getParent(NameType.SYSTEM_STRUCTURE).getParent(NameType.SYSTEM_STRUCTURE);
            } else if (parent.getLevel() == 2) {
                return parent.getParent(NameType.SYSTEM_STRUCTURE);
            } else if (parent.getLevel() == 1) {
                return parent;
            }
        }
        return null;
    }

    /**
     * @return the system
     */
    public NameView getSystem() {
        NameView parent = device != null ? device.getParent(NameType.SYSTEM_STRUCTURE) : null;
        if (parent != null) {
            if (parent.getLevel() == 3) {
                return parent.getParent(NameType.SYSTEM_STRUCTURE);
            } else if (parent.getLevel() == 2) {
                return parent;
            }
        }
        return null;
    }

    /**
     * @return the view of the subsystem containing the device.
     */
    public NameView getSubsystem() {
        NameView parent = device != null ? device.getParent(NameType.SYSTEM_STRUCTURE) : null;
        return parent != null && parent.getLevel() == 3
                ? parent
                : null;
    }

    /**
     * @return the discipline
     */
    public NameView getDiscipline() {
        NameView deviceGroup = getDeviceGroup();
        return deviceGroup != null ? deviceGroup.getParent(NameType.DEVICE_STRUCTURE) : null;
    }

    /**
     * @return the deviceGroup
     */
    public NameView getDeviceGroup() {
        NameView deviceType = getDeviceType();
        return deviceType != null ? deviceType.getParent(NameType.DEVICE_STRUCTURE) : null;    }

    /**
     * @return the view of the device type containing the device.
     */
    public NameView getDeviceType() {
        return device != null ? device.getParent(NameType.DEVICE_STRUCTURE) : null;
    }

    /**
     *
     * @return the device
     */
    public NameView getDevice() {
        return device;
    }

}
