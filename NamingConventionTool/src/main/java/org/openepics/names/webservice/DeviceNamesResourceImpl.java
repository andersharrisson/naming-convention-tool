/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.webservice;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameRevision;
import org.openepics.names.jaxb.DeviceNameElement;
import org.openepics.names.jaxb.DeviceNamesResource;
import org.openepics.names.jaxb.SpecificDeviceNameResource;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.util.NamingConventionUtility;
import com.google.common.collect.Lists;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * This is implementation of {@link DeviceNamesResource} interface.
 *
 * @author Andraz Pozar
 * @author Banafsheh Hajinasab
 * @author Lars Johansson
 *
 * @see DeviceNamesResource
 * @see SpecificDeviceNameResource
 */
@Stateless
public class DeviceNamesResourceImpl implements DeviceNamesResource {

    private static final Logger LOGGER = Logger.getLogger(DeviceNamesResourceImpl.class.getName());

    // ----------------------------------------------------------------------------------------------------
    // table of content
    // ----------------------------------------------------------------------------------------------------
    // overall (list, specific, search)
    // name structure (specific, search)
    //     system
    //         system
    //         subsystem
    //     device
    //         discipline
    //         device type
    // ----------------------------------------------------------------------------------------------------
    // note
    //     - case sensitive
    //     - NamingConventionUtility.extract methods may return null, if part of convention name not found
    // ----------------------------------------------------------------------------------------------------

    @Inject
    private SpecificDeviceNameResource specificDeviceNameSubresource;
    @Inject
    private NameViewProvider nameViewProvider;

    // ----------------------------------------------------------------------------------------------------
    // overall (list, specific, search)
    // ----------------------------------------------------------------------------------------------------

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNames()}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNames() {
        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (String name : nameViewProvider.getNameRevisions().keySet()) {
            DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
            deviceNames.add(deviceData);
        }
        return deviceNames;
    }

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getSpecificDeviceNameSubresource()}
     */
    @Override
    public SpecificDeviceNameResource getSpecificDeviceNameSubresource() {
        return specificDeviceNameSubresource;
    }

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNamesSearch(String)}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNamesSearch(String searchText) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        try {
            Pattern pattern = Pattern.compile(searchText);

            for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
                NameRevision nameRevision = entry.getValue();
                String name = nameRevision.getNameElement().getFullName();
                if (pattern.matcher(name).find()) {
                    DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
                    deviceNames.add(deviceData);
                }
            }
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return deviceNames;
    }

    // ----------------------------------------------------------------------------------------------------
    // name structure (specific, search)
    //     system
    //         system
    //         subsystem
    //     device
    //         discipline
    //         device type
    // ----------------------------------------------------------------------------------------------------

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNamesBySystem(String)}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNamesBySystem(String system) {
        // note
        //     exact match
        //     case sensitive

        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
            NameRevision nameRevision = entry.getValue();
            String name = nameRevision.getNameElement().getFullName();
            String sub = NamingConventionUtility.extractSystem(name);
            if (StringUtils.equals(system, sub) ) {
                DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
                deviceNames.add(deviceData);
            }
        }
        return deviceNames;
    }

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNamesBySystemSearch(String)}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNamesBySystemSearch(String system) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        try {
            Pattern pattern = Pattern.compile(system);

            for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
                NameRevision nameRevision = entry.getValue();
                String name = nameRevision.getNameElement().getFullName();
                String sub = NamingConventionUtility.extractSystem(name);
                if (sub != null && pattern.matcher(sub).find()) {
                    DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
                    deviceNames.add(deviceData);
                }
            }
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return deviceNames;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNamesBySubsystem(String)}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNamesBySubsystem(String subsystem) {
        // note
        //     exact match
        //     case sensitive

        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
            NameRevision nameRevision = entry.getValue();
            String name = nameRevision.getNameElement().getFullName();
            String sub = NamingConventionUtility.extractSubsystem(name);
            if (StringUtils.equals(subsystem, sub) ) {
                DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
                deviceNames.add(deviceData);
            }
        }
        return deviceNames;
    }

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNamesBySubsystemSearch(String)}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNamesBySubsystemSearch(String subsystem) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        try {
            Pattern pattern = Pattern.compile(subsystem);

            for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
                NameRevision nameRevision = entry.getValue();
                String name = nameRevision.getNameElement().getFullName();
                String sub = NamingConventionUtility.extractSubsystem(name);
                if (sub != null && pattern.matcher(sub).find()) {
                    DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
                    deviceNames.add(deviceData);
                }
            }
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return deviceNames;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNamesByDiscipline(String)}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNamesByDiscipline(String discipline) {
        // note
        //     exact match
        //     case sensitive

        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
            NameRevision nameRevision = entry.getValue();
            String name = nameRevision.getNameElement().getFullName();
            String sub = NamingConventionUtility.extractDiscipline(name);
            if (StringUtils.equals(discipline, sub) ) {
                DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
                deviceNames.add(deviceData);
            }
        }
        return deviceNames;
    }

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNamesByDisciplineSearch(String)}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNamesByDisciplineSearch(String discipline) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        try {
            Pattern pattern = Pattern.compile(discipline);

            for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
                NameRevision nameRevision = entry.getValue();
                String name = nameRevision.getNameElement().getFullName();
                String sub = NamingConventionUtility.extractDiscipline(name);
                if (sub != null && pattern.matcher(sub).find()) {
                    DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
                    deviceNames.add(deviceData);
                }
            }
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return deviceNames;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNamesByDeviceType(String)}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNamesByDeviceType(String deviceType) {
        // note
        //     exact match
        //     case sensitive

        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
            NameRevision nameRevision = entry.getValue();
            String name = nameRevision.getNameElement().getFullName();
            String sub = NamingConventionUtility.extractDeviceType(name);
            if (StringUtils.equals(deviceType, sub) ) {
                DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
                deviceNames.add(deviceData);
            }
        }
        return deviceNames;
    }

    /**
     * see {@link org.openepics.names.jaxb.DeviceNamesResource#getAllDeviceNamesByDeviceTypeSearch(String)}
     */
    @Override
    public List<DeviceNameElement> getAllDeviceNamesByDeviceTypeSearch(String deviceType) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNames = Lists.newArrayList();
        try {
            Pattern pattern = Pattern.compile(deviceType);

            for (Map.Entry<String, NameRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
                NameRevision nameRevision = entry.getValue();
                String name = nameRevision.getNameElement().getFullName();
                String sub = NamingConventionUtility.extractDeviceType(name);
                if (sub != null && pattern.matcher(sub).find()) {
                    DeviceNameElement deviceData = specificDeviceNameSubresource.getDeviceNameElement(name);
                    deviceNames.add(deviceData);
                }
            }
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return deviceNames;
    }

}
