/*-
* Copyright (c) 2014 European Spallation Source
* Copyright (c) 2014 Cosylab d.d.
*
* This file is part of Naming Service.
* Naming Service is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or any newer version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.services;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameType;

/**
 * Definitions used in GUIs headers
 *
 * @author karinrathsman
 *
 */
@ManagedBean
@ApplicationScoped
public class NamingConventionDefinition {

    private final NameElement superSection = new NameElement("Super Section", "", "High level area of the facility restricted to a particular use. Used for filtering and sorting purposes. Not part of the convention names except for offsite systems,which do not follow the naming convention");
    private final NameElement section = new NameElement("Section", "Sec", "Level 2 of Area Structure");
    private final NameElement subsection = new NameElement("Subsection", "Sub", "Level 3 of Area Structure");
    private final NameElement discipline = new NameElement("Discipline", "Dis", "Branch of knowledge indicating the context in which a device is used");
    private final NameElement deviceGroup = new NameElement("Device Group", "", "Introdued to allow certain device types to be grouped together in lists. Not part of the convention names. Default is Miscellaneous");
    private final NameElement deviceType = new NameElement("Device Type", "Dev", "Two devices of the same (generic) type provide the same function");
    private final NameElement device = new NameElement("Device Name", "Sec-Sub:Dis-Dev-Idx", "Name of a single instance of a device");

    /**
     *
     * @param level the level
     * @param nameType the name type
     * @return the name definition for the level and nameType
     */
    public NameElement type(int level, NameType nameType) {
        return nameType.isAreaStructure() ? areaStructureLevel(level) : deviceStructureLevel(level);
    }

    /**
     *
     * @param level the level
     * @return the area structure definition for the level
     */
    public NameElement areaStructureLevel(int level) {
        switch (level) {
            case 1:
                return getSuperSection();
            case 2:
                return getSection();
            case 3:
                return getSubsection();
            case 4:
                return getDevice();
            default:
                return null;
        }
    }

    /**
     *
     * @param level the level
     * @return the device structure definition for the level
     */
    public NameElement deviceStructureLevel(int level) {
        switch (level) {
            case 1:
                return getDiscipline();
            case 2:
                return getDeviceGroup();
            case 3:
                return getDeviceType();
            case 4:
                return getDevice();
            default:
                return null;
        }
    }

    /**
     * @return the superSection
     */
    public NameElement getSuperSection() {
        return superSection;
    }

    /**
     * @return the section
     */
    public NameElement getSection() {
        return section;
    }

    /**
     * @return the subsection
     */
    public NameElement getSubsection() {
        return subsection;
    }

    /**
     * @return the discipline
     */
    public NameElement getDiscipline() {
        return discipline;
    }

    /**
     * @return the deviceGroup
     */
    public NameElement getDeviceGroup() {
        return deviceGroup;
    }

    /**
     * @return the deviceType
     */
    public NameElement getDeviceType() {
        return deviceType;
    }

    /**
     * @return the device
     */
    public NameElement getDevice() {
        return device;
    }
}
