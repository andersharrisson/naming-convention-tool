/*
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.services;

import javax.annotation.Nullable;

import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameType;
import org.openepics.names.util.NamingConventionUtility;
import java.util.List;

/**
 * An interface defining the naming convention to be used by the Naming application.
 * It includes:
 * <ul>
 * <li> name validation rules
 * <li> name uniqueness rules
 * <li> form of composite names
 * </ul>
 *
 * <p>
 * The used naming convention is configured through beans.xml using the CDI alternatives mechanism.
 *
 * <p>
 * A device name consists of  elements having general structure
 * <ul>
 * <li> <pre>Sys-Sub:Dis-Dev-Idx</pre>
 * <li> <pre>Sup-Sys-Sub:Dis-Dev-Idx</pre>
 * </ul>
 * It consists of of <br/><br/>
 * <ul>
 * <li> System structure
 * <ul>
 * <li> System group (Sup)(optional)
 * <li> System (Sys)
 * <li> Subsystem (Sub)
 * </ul>
 * <li> Device structure
 * <ul>
 * <li> Discipline (Dis)
 * <li> Device type (Dev)
 * </ul>
 * <li> Device
 * <ul>
 * <li> Instance index (Idx)
 * </ul>
 * </ul>
 *
 * A device name thus consists of an system structure element, a device structure element and an instance index.
 * Together, elements make a device name. In addition, there is intermediate level in device structure
 * (device group, between discipline and device type) for grouping purposes.<br/><br/>
 *
 * <p>
 * An element in the name structure is typically handled through its mnemonic path and type.
 * <ul>
 * <li> mnemonic path is a list of mnemonics starting from the root of the hierarchy to the intended mnemonic
 * <li> root of hierarchy is system group (system structure) or discipline (device structure)
 * <li> type is structure that mnemonic path belongs, system or device structure
 * </ul>
 *
 * <p>
 * Elements consist of full name, mnemonic, description and follow rules, individually and together
 * (system structure, device structure, device). Among rules:
 * <ul>
 * <li> empty
 * <li> space
 * <li> value
 * <li> alphanumeric
 * <li> letter case
 * <li> length
 * <li> equivalence class representative
 * </ul>
 *
 * <p>
 * Key concepts
 * <ul>
 * <li> mnemonic required or not
 * <li> mnemonic valid or not
 * <li> if mnemonic can coexist with other mnemonic
 * <li> equivalence, uniqueness of names when treating similar looking names
 * </ul>
 *
 * <p>
 * Note
 * <ul>
 * <li> system structure is logical structure
 * <li> depth of system and device structures is 3
 * <li> names typically handled and referred to through mnemonics
 * <li> naming convention also referred to as convention
 * </ul>
 *
 * @author Marko Kolar
 * @author Karin Rathsman
 * @author Lars Johansson
 *
 * @see <a href="https://confluence.esss.lu.se/display/NC/ESS+Naming+Convention"/>
 *               https://confluence.esss.lu.se/display/NC/ESS+Naming+Convention</a>
 * @see <a href="https://chess.esss.lu.se/enovia/tvc-action/showObject/dmg_TechnicalSpecification/ESS-0000757/valid">
 *               https://chess.esss.lu.se/enovia/tvc-action/showObject/dmg_TechnicalSpecification/ESS-0000757/valid</a>
 *
 * @see NamingConventionUtility
 */
public interface NamingConvention {

    /**
     * Return if the mnemonic is required or if it can be <tt>null</tt>, i.e. mnemonic not part of the name.
     *
     * @param mnemonicPath      list of mnemonics starting from the root of the hierarchy to the mnemonic to be tested
     * @param nameType          type specifying whether mnemonic belongs to the system structure or the device structure
     * @return                  <tt>true</tt> if the mnemonic is required or if it can be <tt>null</tt>, i.e. mnemonic
     *                          not part of the name
     */
    boolean isMnemonicRequired(List<String> mnemonicPath, NameType nameType);

    /**
     * Return validation for given mnemonic path and type, according to convention rules.
     *
     * @param mnemonicPath      list of mnemonics starting from the root of the hierarchy to the mnemonic to be tested
     * @param nameType          type specifying whether mnemonic belongs to the system structure or the device structure
     * @return                  validation for given mnemonic path and type, according to convention rules
     */
    MnemonicValidation validateMnemonic(List<String> mnemonicPath, NameType nameType);

    /**
     * Return if device's instance index is valid according to convention rules,
     * in the context of device's system and device type.
     *
     * @param systemPath        list of system names (system structure)
     *                          starting from the root of the hierarchy to the specific system containing the device
     * @param deviceTypePath    list of device type names (device structure)
     *                          starting from the root of the hierarchy to the specific subtype of the device
     * @param instanceIndex     the device instance index to test for validity, or null if no instance index
     *                          is assigned to the device, in which case this is also checked for validity
     * @return                  <tt>true</tt> if the device's instance index is valid according to convention rules,
     *                          in the context of device's system and device type
     */
    boolean isInstanceIndexValid(
            List<String> systemPath, List<String> deviceTypePath, @Nullable String instanceIndex);

    /**
     * Return if device's instance index is valid according to convention rules,
     * in the context of device's system and device type.
     *
     * @param systemPath        list of system names (system structure)
     *                          starting from the root of the hierarchy to the specific system containing the device
     * @param deviceTypePath    list of device type names (device structure)
     *                          starting from the root of the hierarchy to the specific subtype of the device
     * @param instanceIndex     the device instance index to test for validity, or null if no instance index
     *                          is assigned to the device, in which case this is also checked for validity
     * @param overrideRuleset   if ruleset for instance index is to be overridden, e.g. for super user
     * @return                  <tt>true</tt> if the device's instance index is valid according to convention rules,
     *                          in the context of device's system and device type
     */
    boolean isInstanceIndexValid(
            List<String> systemPath, List<String> deviceTypePath, @Nullable String instanceIndex,
            boolean overrideRuleset);

    /**
     * Return if two mnemonics with given mnemonic paths and types can coexist within the application at the same time
     * according to the convention rules.
     *
     * @param mnemonicPath1     list of mnemonics starting from the root of the hierarchy to the mnemonic to be tested
     * @param nameType1         type specifying whether mnemonic belongs to the system structure or the device structure
     * @param mnemonicPath2     list of mnemonics starting from the root of the hierarchy to the mnemonic to be tested
     * @param nameType2         type specifying whether mnemonic belongs to the system structure or the device structure
     * @return                  <tt>true</tt> if two mnemonics with given mnemonic paths and types can coexist
     *                          within the application at the same time according to the convention rules
     */
    boolean canMnemonicsCoexist(
            List<String> mnemonicPath1, NameType nameType1,
            List<String> mnemonicPath2, NameType nameType2);

    /**
     * Return if the name part can be moved from <tt>mnemonicPathSource</tt> to <tt>mnemonicPathDestination</tt>.
     *
     * @param mnemonicPathSource        (source) list of mnemonics starting from the root of the hierarchy
     *                                  to the parent of the name part which is to be moved,
     *                                  <tt>null</tt> if the parent is root
     * @param nameTypeSource            (source) type specifying whether mnemonic belongs
     *                                  to the system structure or the device structure
     * @param mnemonicPathDestination   (destination) list of mnemonics starting from the root of the hierarchy
     *                                  to the parent into which the name part is to be moved,
     *                                  <tt>null</tt> if the parent is root
     * @param nameTypeDestination       (destination) type specifying whether mnemonic belongs
     *                                  to the system structure or the device structure
     * @return                          <tt>true</tt> if the name part can be moved from
     *                                  <tt>mnemonicPathSource</tt> to <tt>mnemonicPathDestination</tt>
     */
    boolean canNamePartMove(
            List<String> mnemonicPathSource, NameType nameTypeSource,
            List<String> mnemonicPathDestination, NameType nameTypeDestination);

    /**
     * Return equivalence class representative for given name. This is used to ensure uniqueness of names
     * when treating similar looking names, e.g. 0 vs. O, 1 vs. l treated as as equal.
     *
     * @param name      name of which to determine the equivalence class representative
     * @return          equivalence class representative for given name
     */
    String equivalenceClassRepresentative(String name);

    /**
     * Return name element type name key.
     *
     * @param mnemonicPath  list of mnemonics starting from the root of the hierarchy
     * @param nameType      type specifying whether mnemonic belongs to the system structure or the device structure
     * @return              name element type name key
     */
    String conventionKey(List<String> mnemonicPath, NameType nameType);

    /**
     * Return composite name of a name part including name part strings and according to convention rules.
     * E.g. (sys-sub) for a subsystem or (dis-dev) for a deviceType.
     *
     * @param mnemonicPath  list of mnemonics starting from the root of the hierarchy
     * @param nameType      type specifying whether mnemonic belongs to the system structure or the device structure
     * @return              composite name of a name part including name part strings
     *                      and according to convention rules
     */
    String conventionName(List<String> mnemonicPath, NameType nameType);

    /**
     * Return convention name of the device defined by system, device type and instance index.
     *
     * @param systemPath        list of system names starting from the root of the hierarchy
     *                          to the specific system containing the device
     * @param deviceTypePath    list of device type names starting from the root of the hierarchy
     *                          to the specific subtype of the device
     * @param instanceIndex     device instance index, <tt>null</tt> if omitted
     * @return                  convention name of the device defined by system, device type and instance index
     */
    String conventionName(List<String> systemPath, List<String> deviceTypePath, @Nullable String instanceIndex);

    /**
     * Return name element type name, e.g. used in dialog headers and menus.
     * E.g. 'Add new namePartTypeName' where namePartTypeName can be subsystem, deviceType etc.
     *
     * @param mnemonicPath      list of mnemonics starting from the root of the hierarchy
     * @param nameType          type specifying whether mnemonic belongs to the system structure or the device structure
     * @return                  name element type name used in e.g. dialog headers and menus
     */
    String getNamePartTypeName(List<String> mnemonicPath, NameType nameType);

    /**
     * Return name element type mnemonic, e.g. used as watermarks in dialogs.
     * E.g. 'Add Mnemonic: namePartTypeMnemonic' where namePartTypeMnemonic can be sub, dev, etc.
     *
     * @param mnemonicPath      list of mnemonics starting from the root of the hierarchy
     * @param nameType          type specifying whether mnemonic belongs to the system structure or the device structure
     * @return                  name element type mnemonic used e.g. as watermarks in dialogs
     */
    String getNamePartTypeMnemonic(List<String> mnemonicPath, NameType nameType);

    /**
     * Return definition of name element.
     *
     * @param mnemonicPath  list of mnemonics starting from the root of the hierarchy
     * @param nameType      type specifying whether mnemonic belongs to the system structure or the device structure
     * @return              definition of name element
     */
    NameElement getNameElementDefinition(List<String> mnemonicPath, NameType nameType);

}
