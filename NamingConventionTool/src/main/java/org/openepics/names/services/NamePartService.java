/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.services;

import org.apache.commons.codec.binary.StringUtils;
import org.openepics.names.model.*;
import org.openepics.names.util.As;
import org.openepics.names.util.ConversionUtil;
import org.openepics.names.util.JpaHelper;

import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.NoResultException;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * A service bean managing NamePart and Device entities.
 *
 * @author Marko Kolar
 * @author Karin Rahtsman
 */
@Stateless
public class NamePartService {

    private static final Logger LOGGER = Logger.getLogger(NamePartService.class.getName());

    private static final String APPROVED = "approved";
    private static final String DEVICE = "device";
    private static final String NAME_PART = "namePart";
    private static final String PENDING = "pending";
    private static final String STATUS = "status";
    private static final String USER_NAME = "userName";

    @Inject
    private NamingConvention namingConvention;
    @PersistenceContext
    private EntityManager em;

    /**
     * @return The list of all revisions
     */
    public List<NamePartRevision> allNamePartRevisions() {
        List<NamePartRevision> revisions =
                em.createQuery("SELECT r FROM NamePartRevision r", NamePartRevision.class).getResultList();
        updateEqNameParts(revisions);
        return revisions;
    }

    /**
     * updates the name equivalence classes after changes to the naming rules...
     * @param revisions revisions to be updated.
     */
    private void updateEqNameParts(List<NamePartRevision> revisions) {
        for (NamePartRevision revision : revisions) {
            String name = revision.getMnemonic();
            if (name != null && !name.isEmpty()) {
                String eqName = namingConvention.equivalenceClassRepresentative(name);
                if (!eqName.equals(revision.getMnemonicEqClass())) {
                    revision.setMnemonicEqClass(eqName);
                    em.merge(revision);
                }
            }
        }
    }

    /**
     * updates the name equivalence classes after changes to the naming rules...
     * @param revisions revisions to be updated.
     */
    private void updateEqDeviceNames(List<DeviceRevision> revisions) {
        for (DeviceRevision revision : revisions) {
            String name = revision.getConventionName();
            String eqName = namingConvention.equivalenceClassRepresentative(name);
            if (!eqName.equals(revision.getConventionNameEqClass())) {
                revision.setConventionNameEqClass(eqName);
                em.merge(revision);
            }
        }
    }

    /**
     * @return The list of all revisions
     */
    public List<DeviceRevision> allDeviceRevisions() {
        List<DeviceRevision> revisions =
                em.createQuery("SELECT r FROM DeviceRevision r", DeviceRevision.class).getResultList();
        updateEqDeviceNames(revisions);
        return revisions;
    }

    /**
     * Return the list of all revisions of the given name part,
     * including approved, pending, canceled or rejected,
     * starting from the oldest to the latest.
     *
     * @param namePart  the name part
     * @return          the list of all revisions of the given name part,
     *                  including approved, pending, canceled or rejected,
     *                  starting from the oldest to the latest
     */
    public List<NamePartRevision> revisions(NamePart namePart) {
        return em.createQuery(
                    "SELECT r FROM NamePartRevision r WHERE r.namePart = :namePart ORDER BY r.id",
                    NamePartRevision.class)
                .setParameter(NAME_PART, namePart)
                .getResultList();
    }

    /**
     *
     * @param namePart  the name part
     * @return          The current, most recent approved revision of the given name part.
     *                  Null if no approved revision exists for this name part.
     */
    private @Nullable
    NamePartRevision approvedRevision(NamePart namePart) {
        return JpaHelper.getSingleResultOrNull(
                em.createQuery(
                        "SELECT r FROM NamePartRevision r WHERE r.namePart = :namePart "
                                + "AND r.status = :status ORDER BY r.id DESC",
                        NamePartRevision.class)
                    .setParameter(NAME_PART, namePart)
                    .setParameter(STATUS, NamePartRevisionStatus.APPROVED)
                    .setMaxResults(1));
    }

    /**
     *
     * @param namePart the name part
     * @return The revision of the given name part currently pending approval. Null if no revision is pending approval.
     */
    private @Nullable
    NamePartRevision pendingRevision(NamePart namePart) {
        final @Nullable
        NamePartRevision lastPendingOrApprovedRevision =
                JpaHelper.getSingleResultOrNull(
                        em.createQuery("SELECT r FROM NamePartRevision r WHERE r.namePart = :namePart "
                                        + "AND (r.status = :approved OR r.status = :pending) ORDER BY r.id DESC",
                                    NamePartRevision.class)
                                .setParameter(NAME_PART, namePart)
                                .setParameter(APPROVED, NamePartRevisionStatus.APPROVED)
                                .setParameter(PENDING, NamePartRevisionStatus.PENDING)
                                .setMaxResults(1));

        if (lastPendingOrApprovedRevision == null) {
            return null;
        } else if (lastPendingOrApprovedRevision.getStatus() == NamePartRevisionStatus.PENDING) {
            return lastPendingOrApprovedRevision;
        } else {
            return null;
        }
    }

    /**
     * Return the list of all revisions of the given device, starting from the oldest to the latest.
     *
     * @param device the device
     * @return the list of all revisions of the given device, starting from the oldest to the latest
     */
    public List<DeviceRevision> revisions(Device device) {
        return em.createQuery(
                    "SELECT r FROM DeviceRevision r WHERE r.device = :device ORDER BY r.id", DeviceRevision.class)
                .setParameter(DEVICE, device)
                .getResultList();
    }

    /**
     * Return the current, most recent revision of the given device.
     *
     * @param device the device
     * @return the current, most recent revision of the given device
     */
    public DeviceRevision currentRevision(Device device) {
        return em.createQuery(
                    "SELECT r FROM DeviceRevision r WHERE r.device = :device ORDER BY r.id DESC", DeviceRevision.class)
                .setParameter(DEVICE, device)
                .getResultList()
                .get(0);
    }

    /**
     * the em attached namePartRevision in the database
     *
     * @param namePartRevision the (unattached) namePartRevision
     * @return
     */
    private NamePartRevision emAttached(NamePartRevision namePartRevision) {
        if (!em.contains(namePartRevision)) {
            return As.notNull(em.find(NamePartRevision.class, namePartRevision.getId()));
        } else {
            return namePartRevision;
        }
    }

    /**
     * Return the UserAccount of the user with the given user name.
     *
     * @param userName the name of the user
     * @return the UserAccount of the user with the given user name
     */
    public UserAccount userWithName(String userName) {
        return em.createQuery("SELECT u FROM UserAccount u WHERE u.username = :userName", UserAccount.class)
                .setParameter(USER_NAME, userName)
                .getSingleResult();
    }

    /**
     * Creates new user with the given user name if not already exists.
     *
     * @param userName the user name
     * @param role the Role
     */
    public void createUser(String userName, Role role) {
        try {
            userWithName(userName);
        } catch (NoResultException e) {
            em.persist(new UserAccount(userName, role));
        }
    }

    /**
     * Return the EntityManager-attached entity corresponding to the given UserAccount entity.
     *
     * @param user the (possibly detached) UserAccount entity
     * @return the EntityManager-attached entity corresponding to the given UserAccount entity
     */
    public UserAccount emAttached(UserAccount user) {
        return As.notNull(em.find(UserAccount.class, user.getId()));
    }

    /**
     * Gets the parent for a give NamePartRevision from the DB
     *
     * @param partRev the actual element for which the parent has to be retrieved
     *                [if more node is given in response it chooses to one with max(ID)]
     * @return the parentNode for the element, or NULL, if does not have any parent (ROOT node)
     */
    public NamePartRevision getParentForId(NamePartRevision partRev) {
        NamePartRevision result = null;

        if((partRev != null) && (partRev.getParent() != null)) {
            result = em.createQuery(
                    "SELECT r FROM NamePartRevision r WHERE r.id = " +
                            " (SELECT MAX(r2.id) FROM NamePartRevision r2 WHERE r2.namePart.id = :pParentId)",
                    NamePartRevision.class)
                    .setParameter("pParentId", partRev.getParent().getId())
                    .getSingleResult();
        }

        return result;
    }

    /**
     * Gets DeviceRevisions from DB which had been modified/deleted that occurred on the previous day
     * so notification has to be sent to admins
     *
     * @param deleted <code>true</code>, if deleted DeviceRevisions are needed,
     *                <code>false</code> if not deleted DeviceRevisions are needed
     *
     * @return list of DeviceRevisions
     */
    public List<DeviceRevision> devicesToSendNotification(boolean deleted) {

        LocalDate frmDate = LocalDate.now();

        LocalDate toDate = LocalDate.now();
        frmDate = frmDate.minusDays(1);

        //creating the JPA criteria for DeviceRevision
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DeviceRevision> cr = cb.createQuery(DeviceRevision.class);
        Root<DeviceRevision> root = cr.from(DeviceRevision.class);
        cr.select(root);

        List<Predicate> predicates = new ArrayList<>();

        //setting the requestDate for the previous day
        Predicate datePred =  cb.between(root.get("requestDate"),
                ConversionUtil.convertToDateViaInstant(frmDate),
                ConversionUtil.convertToDateViaInstant(toDate));
        //search only for deleted, or non-deleted devices
        Predicate deletedPred = cb.equal(root.get("deleted"), deleted);

        Predicate finalPred = cb.and(datePred, deletedPred);

        predicates.add(finalPred);

        cr.where(predicates.toArray(new Predicate[]{}));
        cr.orderBy(cb.asc(root.get("requestDate")));

        return em.createQuery(cr)
                .getResultList();
    }

    /**
     * Gets DeviceRevision history for a specific Device order by requestDate ASC
     *
     * @param device the device that has to get the history for
     * @return history of DeviceRevision, order by requestDate ASC
     */
    public List<DeviceRevision> historyForDevice(Device device) {

        return em.createQuery("FROM DeviceRevision WHERE device = :pDevice ORDER BY requestDate ASC")
                .setParameter("pDevice", device)
                .getResultList();
    }

    /**
     * Determines if the device revision has modification.
     *
     * Freshly created device revisions and description-only changes
     * are not considered modified.
     *
     * @param deviceRevision device revision that is candidate to be considered as modified
     * @return <code>true</code> if the device revision has been modified,
     *         or <code>false</code> if the device revision was only created
     */
    public boolean isModifiedDevice(DeviceRevision deviceRevision) {
        // need to compare with previous entry for same device
        // no new (not modified if new)

        String query = "SELECT d.* FROM DeviceRevision d WHERE d.id = "
                + "(SELECT MAX(d2.id) FROM DeviceRevision d2 WHERE d2.device_id = " + deviceRevision.getDevice().getId()
                + " AND d2.id < " + deviceRevision.getId() + ")";

        DeviceRevision deviceRevisionPrevious = null;
        try {
            deviceRevisionPrevious = (DeviceRevision) em.createNativeQuery(query, DeviceRevision.class)
                    .getSingleResult();
        } catch (NoResultException e) {
            // new entry if no result
            LOGGER.log(Level.FINE, "No previous result found, " + e.getMessage());
            deviceRevisionPrevious = null;
        }

        // modified if convention name is different (which encompasses section, device type, instance index)
        if (deviceRevisionPrevious != null &&
                !StringUtils.equals(deviceRevision.getConventionName(), deviceRevisionPrevious.getConventionName())) {
            return true;
        }
        return false;
    }


}
