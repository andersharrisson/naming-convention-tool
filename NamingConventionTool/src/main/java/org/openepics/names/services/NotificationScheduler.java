/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.services;

import com.google.common.collect.Lists;

import org.openepics.names.business.NameRevisionService;
import org.openepics.names.dto.DeviceNotificationDTO;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.util.NamingHelper;
import org.openepics.names.util.NotificationService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Scheduler class to notify admins/users from ESS Name Registry changes
 *
 * @author Imre Toth <imre.toth@ess.eu>
 **/
@Singleton
@Startup
public class NotificationScheduler {

    //limit of the list size in each notification email
    private static final int BATCH_LIMIT = 30;

    private static final Logger LOGGER = Logger.getLogger(NotificationScheduler.class.getName());
    private static final String DEVICES_DELETED_SUBJECT = "Devices deleted from Naming";
    private static final String DEVICES_MODIFIED_SUBJECT = "Devices modified in Naming";

    @Inject
    private NameRevisionService nameRevisionService;
    @Inject
    private NamePartService namePartService;
    @Inject
    private NamingHelper namingHelper;
    @Inject
    private NotificationService notificationService;

    /**
     * Creates an interval timer that periodically invokes validations.
     */
    @PostConstruct
    private void init() {
        //no fields to initialize yet
    }

    /**
     * Notify admins/users about Device modification, and delete
     * Have to run once a day, and send separate emails in which device-list size is limited
     */
    @Schedule(minute = "0", hour = "08", persistent = false)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void notifyAdminsFromDevice() {
        try {

            //notify admins from device modifications
            notifyDeviceModification();
            //notify admins from device deletion
            notifyDeviceDelete();

        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Gets modified devices in batches (list size limit), send notification to admins
     */
    private void notifyDeviceModification() {
        LOGGER.log(Level.FINE, "Trying to notify admins from modified devices");

        //get all modified devices
        List<DeviceRevision> modifiedDevices = modifiedDeviceRevisions();

        if ((modifiedDevices != null) && (!modifiedDevices.isEmpty())) {

            //partition the list by size
            List<List<DeviceRevision>> listPartition = Lists.partition(modifiedDevices, BATCH_LIMIT);
            for (List<DeviceRevision> modifiedPart : listPartition) {
            //send notifications in batches
                List<DeviceNotificationDTO> devicesModified = modifiedPart
                        .stream()
                        .map(d -> new DeviceNotificationDTO(
                                d, namingHelper.previousNameForDevice(d)))
                        .collect(Collectors.toList());

                LOGGER.log(Level.FINE, "Sending batch notification from modified devices");
                boolean mailSendResult = notificationService.notifyDeviceModify(
                        devicesModified, null, DEVICES_MODIFIED_SUBJECT);

                //if sending was successful
                if (mailSendResult) {
                    LOGGER.log(Level.FINE, "Modified devices batch notification sent successful");
                } else {
                    LOGGER.log(Level.SEVERE, "Modified devices batch notification sending error");
                }
            }
        } else {
            LOGGER.log(Level.FINE, "No devices were modified, will not send notification to admins");
        }

        LOGGER.log(Level.FINE, "Admin batch notification finished for modified devices");
    }

    /**
     * Gets deleted devices in batches (list size limit), send notification to admins
     */
    private void notifyDeviceDelete() {
        LOGGER.log(Level.FINE, "Trying to notify admins from deleted devices");

        //get all deleted devices
        List<DeviceRevision> deletedDevices = nameRevisionService.deletedDevicesToNotify();

        if ((deletedDevices != null) && (!deletedDevices.isEmpty())) {

            //partition by size
            List<List<DeviceRevision>> listPartition = Lists.partition(deletedDevices, BATCH_LIMIT);
            for (List<DeviceRevision> deletePart : listPartition) {
                //send notifications in batches
                List<DeviceNotificationDTO> devicesDeleted = deletePart
                        .stream()
                        .map(d -> new DeviceNotificationDTO(d))
                        .collect(Collectors.toList());

                LOGGER.log(Level.FINE, "Sending batch notification from deleted devices");
                boolean mailSendResult = notificationService.notifyDeviceDelete(
                        devicesDeleted, null, DEVICES_DELETED_SUBJECT);

                //if sending was successful
                if (mailSendResult) {
                    LOGGER.log(Level.FINE, "Deleted devices batch notification sent successful");
                } else {
                    LOGGER.log(Level.SEVERE, "Error while sending deleted devices batch notification");
                }
            }
        } else {
            LOGGER.log(Level.FINE, "No devices were deleted, will not send notification to admins");
        }

        LOGGER.log(Level.FINE, "Admin batch notification finished for deleted devices");
    }

    /**
     * Gets all modified DeviceRevisions, for which the modification operation occurred the previous day.
     *
     * @return modified DeviceRevision for which the modification occurred the previous day.
     */
    private List<DeviceRevision> modifiedDeviceRevisions() {
        List<DeviceRevision> result = new ArrayList<>();

        // getting all modified DeviceRevisions for which the modification occurred the previous day
        List<DeviceRevision> deviceRevisions = nameRevisionService.modifiedDevicesToNotify();

        for (DeviceRevision deviceRevision : deviceRevisions) {
            // include DeviceRevisions only which already has history (new newly created)
            if (namePartService.isModifiedDevice(deviceRevision)) {
                result.add(deviceRevision);
            }
        }

        return result;
    }

}
