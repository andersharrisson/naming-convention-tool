package org.openepics.names.nameViews;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import org.openepics.names.util.As;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.NameType;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * A view of a name with latest approved and pending revisions, parent child relations etc.
 *
 * @author Karin Rathsman
 */
public class NameView {

    private NameRevisionPair revisionPair;
    private List<NameView> children;
    private NameView areaStructureParent;
    private NameView deviceStructureParent;
    private NameType nameType;
    private int level;

    public NameView(NameView nameView) {
        this(nameView.getNameType());
        this.revisionPair = new NameRevisionPair(nameView.getRevisionPair());
    }

    public NameView(NameType nameType) {
        Preconditions.checkArgument(nameType != null);
        this.nameType = nameType;
        this.level = 0;
        this.children = Lists.newArrayList();
        this.revisionPair = new NameRevisionPair();
        this.areaStructureParent = null;
        this.deviceStructureParent = null;
    }

    /**
     *
     * @return the name type.
     */
    public NameType getNameType() {
        return nameType;
    }

    /**
     *
     * @return the latest approved and latest unapproved revision pair
     */
    public NameRevisionPair getRevisionPair() {
        return revisionPair;
    }

    /**
     *
     * @param type the parent type
     * @return the parent of NameType type
     */
    public NameView getParent(NameType type) {
        Preconditions.checkArgument(!type.isDeviceRegistry());
        return type.isDeviceStructure() ? deviceStructureParent : areaStructureParent;
    }

    /**
     * Set the parent and update the parent child relationship.
     *
     * @param parent the parent name view
     * @param type the root type.
     */
    protected void setParent(NameView parent, NameType type) {
        Preconditions.checkArgument(type != null && !type.isDeviceRegistry());
        updateParentChild(getParent(type), parent);
        if (type.isDeviceStructure()) {
            this.deviceStructureParent = parent;
        } else {
            this.areaStructureParent = parent;
        }
        updateLevel();
    }

    /**
     * update the parent child relationship
     *
     * @param source the current nameView parent
     * @param destination the updated nameView parent
     */
    private void updateParentChild(NameView source, NameView destination) {
        if (source != null) {
            source.removeChild(this);
        }
        if (destination != null) {
            destination.addChild(this);
        }
    }

    /**
     * adds child nameViews and sorts children
     *
     * @param child the child to add
     */
    private void addChild(NameView child) {
        if (child != null) {
            children.add(child);
        }
        if (children.size() > 1) {
            sortChildren();
        }
    }

    /**
     * remove child nameViews
     *
     * @param child
     */
    private void removeChild(NameView child) {
        children.remove(child);
    }

    /**
     * Sorts children
     */
    private void sortChildren() {
        Collections.sort(children, new Comparator<NameView>() {
            @Override
            public int compare(NameView left, NameView right) {
                String str1 = left.getRevisionPair().getBaseRevision().getNameElement().getFullName();
                String str2 = right.getRevisionPair().getBaseRevision().getNameElement().getFullName();
                int n = String.CASE_INSENSITIVE_ORDER.compare(str1, str2);
                if (n == 0) {
                    n = str1.compareTo(str2);
                }
                return n;
            }
        });
    }

    /**
     * @return the children
     */
    public List<NameView> getChildren() {
        return children;
    }

    /**
     * Updates the level as the parent child level is updated.
     */
    private void updateLevel() {
        if (nameType.isDeviceRegistry()) {
            int areaParentLevel = areaStructureParent != null ? areaStructureParent.getLevel() : -1;
            int deviceParentLevel = deviceStructureParent != null ? deviceStructureParent.getLevel() : -1;
            this.level = areaParentLevel == deviceParentLevel ? areaParentLevel + 1 : 0;
        } else {
            this.level = getParent(nameType) != null ? getParent(nameType).getLevel() + 1 : 0;
            for (NameView child : getChildren()) {
                child.updateLevel();
            }
        }
    }

    /**
     *
     * @return the level in the hierarchy, starting with zero at the root
     */
    public int getLevel() {
        return level;
    }

    /**
     *
     * @return true if the nameView is root, i.e. stage is initial, device type is area or device structure and level is null.
     */
    public boolean isRoot() {
        return getLevel() == 0 && getRevisionPair().getNameStage().isInitial() && (nameType.equals(NameType.AREA_STRUCTURE) || nameType.equals(NameType.DEVICE_STRUCTURE));
    }

    /**
     *
     * @param nameType the structure name type
     * @return true if name view is in structure (i.e. not root or device instance) of type nameType
     */
    public boolean isInStructure(NameType nameType) {
        return isInStructure() && nameType.equals(nameType);
    }

    /**
     *
     * @return true if the name view is between level 1 and 3 and nameType is area or device structure.
     */
    public boolean isInStructure() {
        return getLevel() > 0 && getLevel() < 4 && !getRevisionPair().getNameStage().isInitial() && (nameType.equals(NameType.AREA_STRUCTURE) || nameType.equals(NameType.DEVICE_STRUCTURE));
    }

    /**
     *
     * @return true if the nameView is a device
     */
    public boolean isInDeviceRegistry() {
        return getLevel() == 4 && getRevisionPair().getBaseRevision() != null && nameType.equals(NameType.DEVICE_REGISTRY);
    }

    /**
     *
     * @return true if children are in the area or device structure
     */
    public boolean isChildInStructure() {
        return (isRoot() || isInStructure()) && getLevel() < 3;
    }

    /**
     *
     * @return true if children are devices
     */
    public boolean isChildInDeviceRegistry() {
        return isInStructure() && getLevel() == 3;
    }

    /**
     * @return The list of name part mnemonic names starting from the root of the hierarchy to the child that is be added to this name part.
     * @param mnemonic the mnemonic of the name part
     */
    public List<String> getMnemonicPathWithChild(String mnemonic) {
        Preconditions.checkArgument(isInStructure() || isRoot());
        final ImmutableList.Builder<String> pathElements = ImmutableList.builder();
        pathElements.add(As.emptyIfNull(mnemonic));
        for (NameView pathElement = this; !pathElement.isRoot(); pathElement = pathElement.getParent(pathElement.getNameType())) {
            pathElements.add(pathElement.getRevisionPair().getBaseRevision().getNameElement().getMnemonic() != null ? pathElement.getRevisionPair().getBaseRevision().getNameElement().getMnemonic() : "");
        }
        return pathElements.build().reverse();
    }

    /**
     * @return The list of name part mnemonic names starting from the root of the hierarchy to this name part.
     */
    public List<String> getMnemonicPath() {
        Preconditions.checkArgument(isInStructure());
        return getParent(getNameType()).getMnemonicPathWithChild(getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
    }

    /**
     *
     * @return the set of devices in the registry descending from this nameView.
     */
    public List<NameView> getDevices() {
        List<NameView> devices = Lists.newArrayList();
        if (!isChildInDeviceRegistry()) {
            for (NameView child : getChildren()) {
                devices.addAll(child.getDevices());
            }
        } else {
            devices.addAll(getChildren());

        }
        return devices;
    }

    /**
     *
     * @return the set of active device parents descending from this nameView.
     */
    public Set<NameView> getAllowedDeviceParents() {
        Set<NameView> deviceParents = Sets.newHashSet();
        if (isChildInDeviceRegistry() && getRevisionPair().getNameStage().isAllowedAsParentOf(NameStage.STABLE)) {
            deviceParents.add(this);
        } else {
            for (NameView child : getChildren()) {
                deviceParents.addAll(child.getAllowedDeviceParents());
            }
        }
        return deviceParents;
    }
}
