package org.openepics.names.nameViews;

import org.openepics.names.business.NameType;

/**
 * View for instances of device records in table
 *
 * @author karinrathsman
 *
 */
public class DeviceRecordView {

    private final NameView device;

    public DeviceRecordView(NameView deviceView) {
        this.device = deviceView;
    }

    /**
     * @return The view of the subsection containing the device.
     */
    public NameView getSubsection() {
        return device.getParent(NameType.AREA_STRUCTURE);
    }

    /**
     * @return The view of the device type containing the device.
     */
    public NameView getDeviceType() {
        return device.getParent(NameType.DEVICE_STRUCTURE);
    }

    /**
     * @return the section
     */
    public NameView getSection() {
        return getSubsection().getParent(NameType.AREA_STRUCTURE);
    }

    /**
     * @return the superSection
     */
    public NameView getSuperSection() {
        return getSection().getParent(NameType.AREA_STRUCTURE);
    }

    /**
     * @return the discipline
     */
    public NameView getDiscipline() {
        return getDeviceGroup().getParent(NameType.DEVICE_STRUCTURE);
    }

    /**
     * @return the deviceGroup
     */
    public NameView getDeviceGroup() {
        return getDeviceType().getParent(NameType.DEVICE_STRUCTURE);
    }

    /**
     *
     * @return the device
     */
    public NameView getDevice() {
        return device;
    }

}
