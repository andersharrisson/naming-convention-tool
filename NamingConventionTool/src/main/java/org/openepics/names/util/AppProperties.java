/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class for handling of application properties.
 *
 * @author Zoltan Runyo <zoltan.runyo@esss.se>
 */
@ManagedBean
@ApplicationScoped
public class AppProperties {

    private static final Logger LOGGER = Logger.getLogger(AppProperties.class.getCanonicalName());

    private static final String SUPPORT_EMAIL = "names.supportEmail";

    /**
     * Retrieves the support email property value.
     *
     * @return the support email
     */
    public String getSupportEmail() {
        final String supportEmail = getProperty(SUPPORT_EMAIL);
        LOGGER.log(Level.FINE, "Support email: {0}", supportEmail);
        return supportEmail;
    }

    /**
     * Retrieves the string property with the given key-name
     *
     * @param name a {@link String} key for the property
     * @return the property value, or <code>null</code> if it does not exist
     */
    private String getProperty(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }

        return System.getProperty(name, null);
    }
}
