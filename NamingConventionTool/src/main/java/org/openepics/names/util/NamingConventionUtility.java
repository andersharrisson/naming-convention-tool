/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameType;
import org.openepics.names.nameviews.NameView;

/**
 * Utility class to provide split of device name in naming convention into name parts
 * for system structure and device structure.
 *
 * <p>
 * Note delimiter characters, in and between, system structure and device structure.
 * <ul>
 * <li>-
 * <li>:
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see NamingConvention
 */
public class NamingConventionUtility {

    // Note
    //
    //     ================================================================================
    //     This class handles system structure and device structure of name convention
    //     ================================================================================
    //     ESS Naming Convention
    //         system structure:device structure:process variable
    //         ------------------------------------------------------------
    //         system structure need     to be part of ESS name
    //         device structure need not to be part of ESS name
    //         process variable      not to be part of ESS name
    //         ------------------------------------------------------------
    //         system structure
    //             system group
    //             system
    //             subsystem
    //         device structure
    //             discipline
    //             device type
    //             instance index (ess name)
    //         ------------------------------------------------------------
    //         system group is optional
    //         instance index is optional
    //     ================================================================================
    //     Kind of ESS names handled
    //         CONVENTIONNAME_010
    //         CONVENTIONNAME_011
    //         CONVENTIONNAME_010_111
    //         CONVENTIONNAME_011_110
    //         CONVENTIONNAME_011_111
    //         CONVENTIONNAME_111_110
    //         CONVENTIONNAME_111_111
    //         ------------------------------------------------------------
    //         system
    //         system-subsystem
    //         system:discipline-deviceType-instanceIndex
    //         system-subsystem:discipline-deviceType
    //         system-subsystem:discipline-deviceType-instanceIndex
    //         systemGroup-system-subsystem:discipline-deviceType
    //         systemGroup-system-subsystem:discipline-deviceType-instanceIndex
    //     ================================================================================
    //     delimiters
    //         :
    //         -
    //     ================================================================================
    //     This class to be able to handle past and present names
    //     ================================================================================

    // p&id - pipeline & instrumentation diagram
    private static final String[] DISCIPLINES_P_ID = {"Cryo", "EMR", "HVAC", "Proc", "SC", "Vac", "WtrC"};
    public static final String DISCIPLINE_P_ID = "P&ID";
    public static final String DISCIPLINE_SCIENTIFIC = "Scientific";

    public static final String[] MNEMONIC_PATH_P_ID_NUMERIC = {"SC-IOC"};

    private static final String DELIMITER_EXTRA = ":";
    private static final String DELIMITER_INTRA = "-";

    /**
     * This class is not to be instantiated.
     */
    private NamingConventionUtility() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return string array with disciplines for p & id (pipeline & instrumentation diagram).
     *
     * @return string array with disciplines for p & id (pipeline & instrumentation diagram)
     */
    public static String[] getDisciplinesPID() {
        return Arrays.copyOf(DISCIPLINES_P_ID, DISCIPLINES_P_ID.length);
    }

    /**
     * Return string array with mnemonic paths for p & id (pipeline & instrumentation diagram) numeric.
     *
     * @return string array with mnemonic paths for p & id (pipeline & instrumentation diagram) numeric
     */
    public static String[] getMnemonicPathsPIDNumeric() {
        return Arrays.copyOf(MNEMONIC_PATH_P_ID_NUMERIC, MNEMONIC_PATH_P_ID_NUMERIC.length);
    }

    /**
     * Extract system group from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return system group
     */
    public static String extractSystemGroup(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     system structure
            //         before first occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 1 || st1.countTokens() == 2 || st1.countTokens() == 3) {
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3) {
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract system from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return system
     */
    public static String extractSystem(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     system structure
            //         before first occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 1 || st1.countTokens() == 2 || st1.countTokens() == 3) {
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3) {
                    st2.nextToken();
                    return st2.nextToken();
                } else if (st2.countTokens() == 2 || st2.countTokens() == 1) {
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract subsystem from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return subsystem
     */
    public static String extractSubsystem(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     system structure
            //         before first occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 1 || st1.countTokens() == 2 || st1.countTokens() == 3) {
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3) {
                    st2.nextToken();
                    st2.nextToken();
                    return st2.nextToken();
                } else if (st2.countTokens() == 2) {
                    st2.nextToken();
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract discipline from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return discipline
     */
    public static String extractDiscipline(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     device structure
            //         after           first  occurrence of DELIMITER_EXTRA
            //         before possible second occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 2 || st1.countTokens() == 3) {
                st1.nextToken();
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3 || st2.countTokens() == 2) {
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract devicetype from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return devicetype
     */
    public static String extractDeviceType(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     device structure
            //         after           first  occurrence of DELIMITER_EXTRA
            //         before possible second occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 2 || st1.countTokens() == 3) {
                st1.nextToken();
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3 || st2.countTokens() == 2) {
                    st2.nextToken();
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract instance index from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return instance index
     */
    public static String extractInstanceIndex(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     device structure
            //         after           first  occurrence of DELIMITER_EXTRA
            //         before possible second occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 2 || st1.countTokens() == 3) {
                st1.nextToken();
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3) {
                    st2.nextToken();
                    st2.nextToken();
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Return instance index style based on given parameter.
     *
     * @param nameView          name view for which to (possibly) show instance index style
     * @return                  instance index style
     *
     * @see NamingConventionUtility#getInstanceIndexStyle(NameType, List, boolean)
     */
    public static String getInstanceIndexStyle(NameView nameView) {
        return nameView != null && nameView.isInStructure()
                ? getInstanceIndexStyle(nameView.getMnemonicPath(), nameView.getNameType(), true)
                : null;
    }

    /**
     * Return instance index style based on given parameters.
     *
     * @param mnemonicPath      list of mnemonics starting from the root of the hierarchy to the mnemonic to be tested
     * @param nameType          type specifying whether mnemonic belongs to the system structure or the device structure
     * @param show              if show instance index style given other parameters fulfill conditions
     * @return                  instance index style
     *
     * @see NamingConventionUtility#DISCIPLINE_SCIENTIFIC
     * @see NamingConventionUtility#DISCIPLINE_P_ID
     * @see NamingConventionUtility#DISCIPLINES_P_ID
     */
    public static String getInstanceIndexStyle(List<String> mnemonicPath, NameType nameType, boolean show) {
        String indexStyle = null;
        if (nameType != null
                && NameType.DEVICE_STRUCTURE.equals(nameType)
                && mnemonicPath != null
                && !mnemonicPath.isEmpty()) {
            if (show) {
                indexStyle = DISCIPLINE_SCIENTIFIC;
                String topMnemonic = mnemonicPath.get(0);
                for (String str : getDisciplinesPID()) {
                    if (StringUtils.equals(str, topMnemonic)) {
                        indexStyle = DISCIPLINE_P_ID;
                        break;
                    }
                }
            }
        }
        return indexStyle;
    }

    /**
     * Return if convention name is considered OFFSITE.
     *
     * @param conventionName ESS convention name
     * @return               if convention name is considered OFFSITE
     */
    public static boolean isOffsite(String conventionName) {
        return !StringUtils.isEmpty(extractSystemGroup(conventionName))
                && !StringUtils.isEmpty(extractSystem(conventionName))
                && !StringUtils.isEmpty(extractSubsystem(conventionName));
    }

    /**
     * Utility method to convert a mnemonic path (a list of strings) to a string
     * with {@link NamingConventionUtility#DELIMITER_INTRA} as path separator.
     *
     * @param mnemonicPath list of strings
     * @return string
     */
    public static String mnemonicPath2String(List<String> mnemonicPath) {
        if (mnemonicPath != null) {
            String value = StringUtils.join(mnemonicPath, DELIMITER_INTRA);
            value = StringUtils.replace(value, DELIMITER_INTRA + DELIMITER_INTRA, DELIMITER_INTRA);
            return StringUtils.strip(value, DELIMITER_INTRA);
        }
        return null;
    }

}
