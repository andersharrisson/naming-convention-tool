/*
 * Copyright (c) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openepics.names.nameviews.TransactionResult;
import org.openepics.names.ui.common.ErrorDialogController;
import org.primefaces.PrimeFaces;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Utility class. Among purpose is to show message to user.
 * <p>
 * Note behavior of corresponding <tt><p:messages</tt> element,
 * in particular if <tt>escape="false"></tt> attribute is supplied.
 *
 * @author Lars Johansson
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
  */
public class UiUtility {

    /**
     * Enum to help distinguish and handle different type of errors.
     */
    public enum ErrorType {
        HANDLED, UNHANDLED
    }

    /**
     * This class is not to be instantiated.
     */
    private UiUtility() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Utility method used to display an information message to the user after modified entity successfully.
     *
     * @param summary Summary of the message
     */
    public static void showInfoMessage(final String summary) {
        showMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, summary, ""));
    }

    /**
     * Show faces error message to user with given content.
     *
     * @param summary summary of message
     * @param detail detail of message
     *
     * @see FacesMessage#FacesMessage(javax.faces.application.FacesMessage.Severity, String, String)
     */
    public static void showErrorMessage(String summary, String detail) {
        showMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
    }

    /**
     * Utility method used to display result of a transaction.
     *
     * @param result transaction result
     */
    public static void showMessage(final TransactionResult result) {
        if(result != null) {
            switch (result.getResult()) {
                case SUCCESS:
                    showInfoMessage(result.getSummary());
                    break;
                case NO_CHANGES:
                    showInfoMessage("No modification performed");
                    break;
                case ERROR:
                    showErrorMessage(ErrorType.HANDLED, result.getSummary(), result.getDetail(), null);
                    break;
            }
        }
    }

    /**
     * Utility method used to display an error message to the user.
     *
     * @param message message to display
     * @param throwable cause
     */
    public static void showErrorMessage(final String message, final Throwable throwable) {
    	String stackTrace = throwable != null ? ExceptionUtils.getStackTrace(throwable) : "";
    	showErrorMessage(ErrorType.HANDLED, message, null, stackTrace);
    }

    /**
     * Utility method used to display an error message to the user.
     *
     * @param header header of message to display
     * @param message message to display
     * @param details details of message to display
     * @param throwable cause
     */
    public static void showErrorMessage(
    		final String header, String message, final String details, final Throwable throwable) {
        String stackTrace = throwable != null ? ExceptionUtils.getStackTrace(throwable) : "";
        showErrorMessage(ErrorType.HANDLED, header, message, details, stackTrace);
    }

    /**
     * Utility method used to display an error message to the user.
     *
     * @param errorType type of error
     * @param message message to display
     * @param details details of message to display
     * @param stackTrace stack trace if exception has occurred
     */
    public static void showErrorMessage(
    		final ErrorType errorType, final String message, final String details, final String stackTrace) {
        ErrorDialogController errorDialogController = getErrorDialogController();
        errorDialogController.setHeader(null);
        errorDialogController.setMessage(message);
        errorDialogController.setDetails(details);
        errorDialogController.setStackTrace(stackTrace);
        errorDialogController.setUnexpectedError(ErrorType.UNHANDLED.equals(errorType));
        PrimeFaces.current().ajax().update("errorDialogForm:errorDialog");
        PrimeFaces.current().executeScript("PF('errorDialog').show();");
    }

    /**
     * Utility method used to display an error message to the user.
     *
     * @param errorType error type
     * @param header header of message to display
     * @param message message to display
     * @param details details of message to display
     * @param stackTrace stack trace if exception has occurred
     */
    public static void showErrorMessage(final ErrorType errorType,
    		final String header, final String message, final String details, final String stackTrace) {
        ErrorDialogController errorDialogController = getErrorDialogController();
        errorDialogController.setHeader(header);
        errorDialogController.setMessage(message);
        errorDialogController.setDetails(details);
        errorDialogController.setStackTrace(stackTrace);
        errorDialogController.setUnexpectedError(ErrorType.UNHANDLED.equals(errorType));
        PrimeFaces.current().ajax().update("errorDialogForm:errorDialog");
        PrimeFaces.current().executeScript("PF('errorDialog').show();");
    }

    /**
     * Closes error dialog.
     */
    public static void dismissErrorMessage() {
        PrimeFaces.current().executeScript("PF('errorDialog').hide();");
    }

    private static void showMessage(FacesMessage message) {
        final FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, message);
    }

    private static ErrorDialogController getErrorDialogController() {
        FacesContext context = FacesContext.getCurrentInstance();
        Application application = context.getApplication();
        return application.evaluateExpressionGet(context, "#{errorDialogController}", ErrorDialogController.class);
    }
}
