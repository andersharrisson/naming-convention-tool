/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.model.NamePartType;
import org.openepics.names.services.NamePartService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Utility class for Naming.
 *
 * @author Imre Toth <imre.toth@esss.se>
 */

public class NamingHelper {

    @Inject
    private NamePartService namePartService;

    /***
     * Determines the path from node to root, and gives back it in a list
     *
     * @param name the actual node element for which the path has to be generated to the root
     * @param partType the type of the actual node element
     *
     * @return: a list consisting of nodes from root to the actual element
     *          result will be empty list, if name is NULL, or partType is mismatching
     */
    public List<String> parentListForNode(NamePartRevision name, NamePartType partType) {
        List<String> result = new ArrayList<>();

        if((name == null) || (!partType.equals(name.getNamePart().getNamePartType()))) {
            return result;
        }

        NamePartRevision tmp = name;

        do {
            result.add(mailEntryFromNamePartRev(tmp));

            tmp = namePartService.getParentForId(tmp);
        } while (tmp != null);

        Collections.reverse(result);

        return result;
    }

    /**
     * Generates the name entry from NamePartRevision. Mnemonic will be included, if exists.
     *
     * @param namePart the NamePartRevision for what the String has to be created
     *
     * @return name of the NamePartRevison included mnemonic - if it exists
     */
    private String mailEntryFromNamePartRev(NamePartRevision namePart) {
        if(StringUtils.isEmpty(namePart.getMnemonic())) {
            return namePart.getName();
        } else {
            return namePart.getName() + " (" + namePart.getMnemonic() + ")";
        }
    }

    /**
     * Gets the previous name for a device that was before the DeviceRevision parameter
     *
     * @param deviceRev the actual DeviceRevision for what the previous name has to be known
     * @return the last known name of a device, or empty string
     */
    public String previousNameForDevice(DeviceRevision deviceRev) {
        String result = "";

        //DeviceRevision is empty -> do not have to examine history
        if (deviceRev == null) {
            return result;
        }

        //get all history for a device
        List<DeviceRevision> devHistory = namePartService.historyForDevice(deviceRev.getDevice());

        if ((devHistory != null) && (!devHistory.isEmpty())) {
            result = prevDevHistoryName(deviceRev, devHistory);
        }

        return result;
    }

    /**
     * Gets the previous name for a given DeviceRevision from history
     *
     * @param deviceRev the actual DeviceRevision that's previous name has to be determined
     * @param devHistory the history of the DeviceRevision
     * @return name of the DeviceRevision that was before the deviceRev.
     * Returns empty String if there wasn't any previous name in the history.
     */
    private String prevDevHistoryName(DeviceRevision deviceRev, List<DeviceRevision> devHistory) {
        String result = "";
        DeviceRevision oldDev = null;
        boolean success = false;

        //search for the "current state"
        for (DeviceRevision d : devHistory) {
            if (d.getId().equals(deviceRev.getId())) {
                success = true;
                break;
            }
            oldDev = d;
        }

        //actual entry found
        if ((success) && (oldDev != null)) {
            result = oldDev.getConventionName();
        }
        return result;
    }

}
