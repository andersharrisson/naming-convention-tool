/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.dto.DeviceNotificationDTO;
import org.openepics.names.dto.MailNotificationDTO;
import org.openepics.names.rbac.UserDirectoryService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class to provide email notification to users.
 *
 * @author Imre Toth <imre.toth@esss.se>
 */
@Singleton
@Startup
public class NotificationService {

    private static final Logger LOGGER = Logger.getLogger(NotificationService.class.getName());

    private static String appBaseUrl = System.getProperty("names.applicationBaseURL");
    private static final String ADD_FOOTER_VAR = "addfooter";
    private static final String BASE_URL_VAR = "baseurl";

    private static final String DEVICE_DELETE           = "DEVICE_DELETE";
    private static final String DEVICE_MODIFY           = "DEVICE_MODIFY";
    private static final String SYSTEM_STRUCTURE_CHANGE = "SYSTEM_STRUCTURE_CHANGE";
    private static final String DEVICE_STRUCTURE_CHANGE = "DEVICE_STRUCTURE_CHANGE";

    @Inject
    private MailService mailService;

    @Inject
    private UserDirectoryService userDirectoryService;

    @PersistenceContext
    private EntityManager em;

    /**
     * Sends email notification to admins if someone has made an operation
     * on a node (system structure or device structure) or a leaf (device registry).
     *
     * @param modificationSystemStructure the system structure list that has been modified with an operator
     * @param modificationDeviceStructure the device structure list that has been modified with an operator
     * @param ccList the user that has applied the operator on a node/nodes
     * @param subject subject for email notification
     * @return was the mail sending successful, or not
     */
    public boolean notifyUserFromChange(List<MailNotificationDTO> modificationSystemStructure,
                                        List<MailNotificationDTO> modificationDeviceStructure,
                                        List<String> ccList,
                                        String subject) {
        LOGGER.log(Level.INFO, "Changes in Naming, trying to send email to admins");

        try {
            Set<String> administratorEmails = userDirectoryService.getAllAdministratorEmails();

            subject = subject + " - " + appBaseUrl;

            if(hasStructureModification(modificationSystemStructure)) {
                ccList = mergeLists(ccList, getCCLoginListForNotification(SYSTEM_STRUCTURE_CHANGE));
            }

            if(hasStructureModification(modificationDeviceStructure)) {
                ccList = mergeLists(ccList, getCCLoginListForNotification(DEVICE_STRUCTURE_CHANGE));
            }

            mailService.sendMail(administratorEmails, emailsForNames(ccList), subject,
                    generateChangeNotificationBody(
                            modificationSystemStructure,
                            modificationDeviceStructure,
                             true),
                    null, null, false);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while trying to send changes-notification to admins", e);
            return false;
        }
        return true;
    }

    /**
     * Sends email notification to cc if someone has made an operation
     * on a node (system structure or device structure) or a leaf (device registry).
     *
     * @param modificationSystemStructure the system structure list that has been modified with an operator
     * @param modificationDeviceStructure the device structure list that has been modified with an operator
     * @param ccList the user that has applied the operator on a node/nodes
     * @param subject subject for email notification
     * @return was the mail sending successful, or not
     */
    public boolean notifyCCFromChange(List<MailNotificationDTO> modificationSystemStructure,
                                      List<MailNotificationDTO> modificationDeviceStructure,
                                      List<String> ccList,
                                      String subject) {
        // send email notifications as cc
        //     no need to consider
        //         getCCLoginListForNotification(SYSTEM_STRUCTURE_CHANGE)
        //         getCCLoginListForNotification(DEVICE_STRUCTURE_CHANGE)
        //     handled in notifyUserFromChange

        LOGGER.log(Level.INFO, "Changes in Naming, trying to send email to cc if single non-admin cc");
        if (ccList == null
                || ccList.size() != 1
                || userDirectoryService.getAllAdministratorEmails().contains(
                        userDirectoryService.getEmail(ccList.get(0)))) {
            LOGGER.log(Level.INFO, "Changes in Naming, not send email as not single non-admin cc");
            return false;
        }

        try {
            subject = subject + " - " + appBaseUrl;

            mailService.sendMail(emailsForNames(ccList), emailsForNames(ccList), subject,
                    generateChangeNotificationBody(
                            modificationSystemStructure,
                            modificationDeviceStructure,
                             true),
                    null, null, false);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while trying to send changes-notification to cc", e);
            return false;
        }
        return true;
    }

    /**
     * Send batch-notification to all admins about devices that have been deleted
     *
     * @param deviceDelete the list of devices that have been deleted
     * @param ccList the CC list for the email
     * @param subject the subject of the email
     * @return <code>true</code> if email sending was successful, or email sending was disabled;
     * <code>false</code> if mail sending was unsuccessful
     */

    public boolean notifyDeviceDelete(List<DeviceNotificationDTO> deviceDelete,
                                        List<String> ccList,
                                        String subject) {
        LOGGER.log(Level.INFO, "Trying to notify admins from Device deletion");

        try {
            Set<String> administratorEmails = userDirectoryService.getAllAdministratorEmails();

            subject = subject + " - " + appBaseUrl;

            ccList = mergeLists(ccList, getCCLoginListForNotification(DEVICE_DELETE));

            mailService.sendMail(administratorEmails, emailsForNames(ccList), subject,
                    generateDeviceDeleteNotificationBody(deviceDelete, true),
                    null, null, false);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while trying to send device delete-notification to admins", e);
            return false;
        }
        return true;
    }

    /**
     * Send batch-notification to all admins about devices that have been modified
     *
     * @param deviceModification the list of devices that have been modified
     * @param ccList the CC list for the email
     * @param subject the subject of the email
     * @return <code>true</code> if email sending was successful, or email sending was disabled;
     * <code>false</code> if mail sending was unsuccessful
     */
    public boolean notifyDeviceModify(List<DeviceNotificationDTO> deviceModification,
                                        List<String> ccList,
                                        String subject) {
        LOGGER.log(Level.INFO, "Trying to notify admins from device modification");

        try {
            Set<String> administratorEmails = userDirectoryService.getAllAdministratorEmails();

            subject = subject + " - " + appBaseUrl;

            ccList = mergeLists(ccList, getCCLoginListForNotification(DEVICE_MODIFY));

            mailService.sendMail(administratorEmails, emailsForNames(ccList), subject,
                    generateDeviceModifyNotificationBody(deviceModification, true),
                    null, null, false);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while trying to send device changes-notification to admins", e);
            return false;
        }
        return true;
    }

    /**
     * Retrieves email addresses from RBAC for given users
     *
     * @param userNames ESS usernames
     * @return list of email addresses for users that have been given in parameter
     */
    private List<String> emailsForNames(List<String> userNames) {
        List<String> result = new ArrayList<>();

        if(CollectionUtils.isNotEmpty(userNames)) {
            for(String replyMail : userNames) {
                if(StringUtils.isNotEmpty(replyMail)) {
                    try {
                        result.add(userDirectoryService.getEmail(replyMail));
                    } catch (Exception e) {
                        LOGGER.log(Level.WARNING,
                                "Email not found in RBAC when trying to notify users from change: ".concat(replyMail));
                    }
                }
            }
        }

        return result;
    }

    /**
     * Generates Thymeleaf template and fills data with the given parameter values
     *
     * @param modificationSystemStructure modified system structure values with operation
     * @param modificationDeviceStructure modified device structure values with operation
     * @param addFooter does the email need to have a footer, or not
     * @return the HTML template body filled with data
     */
    private String generateChangeNotificationBody(List<MailNotificationDTO> modificationSystemStructure,
                                                  List<MailNotificationDTO> modificationDeviceStructure,
                                                  boolean addFooter) {
        int numberNotificationsSystemStructure = 0;
        for (MailNotificationDTO mailNotificationDTO : modificationSystemStructure) {
            if (mailNotificationDTO.getNodeList() != null && !mailNotificationDTO.getNodeList().isEmpty()) {
                numberNotificationsSystemStructure += 1;
            }
        }

        int numberNotificationsDeviceStructure = 0;
        for (MailNotificationDTO mailNotificationDTO : modificationDeviceStructure) {
            if (mailNotificationDTO.getNodeList() != null && !mailNotificationDTO.getNodeList().isEmpty()) {
                numberNotificationsDeviceStructure += 1;
            }
        }

        TemplateEngine engine = generateTemplateEngine();

        final Context ctx = new Context();
        ctx.setVariable("numberNotificationsSystemStructure", numberNotificationsSystemStructure);
        ctx.setVariable("numberNotificationsDeviceStructure", numberNotificationsDeviceStructure);
        ctx.setVariable("systemStructure", modificationSystemStructure);
        ctx.setVariable("deviceStructure", modificationDeviceStructure);
        ctx.setVariable(ADD_FOOTER_VAR, addFooter);
        ctx.setVariable(BASE_URL_VAR, appBaseUrl);

        return engine.process("emailTemplate/notification_change.html", ctx);
    }

    /**
     * Generates Thymeleaf template and fills data with the given parameter values
     *
     * @param deviceModification list of modified devices from ESS Name Registry
     * @param addFooter does the email need to have a footer, or not
     * @return the HTML template body filled with data
     */
    private String generateDeviceModifyNotificationBody(List<DeviceNotificationDTO> deviceModification,
                                                  boolean addFooter) {
        TemplateEngine engine = generateTemplateEngine();

        final Context ctx = new Context();
        ctx.setVariable("deviceModification", deviceModification);
        ctx.setVariable(ADD_FOOTER_VAR, addFooter);
        ctx.setVariable(BASE_URL_VAR, appBaseUrl);

        return engine.process("emailTemplate/notification_device_modify.html", ctx);
    }

    /**
     * Generates Thymeleaf template and fills data with the given parameter values
     *
     * @param deviceDelete list of deleted devices from ESS Name Registry
     * @param addFooter does the email need to have a footer, or not
     * @return the HTML template body filled with data
     */

    private String generateDeviceDeleteNotificationBody(List<DeviceNotificationDTO> deviceDelete,
                                                        boolean addFooter) {
        TemplateEngine engine = generateTemplateEngine();

        final Context ctx = new Context();
        ctx.setVariable("deviceDelete", deviceDelete);
        ctx.setVariable(ADD_FOOTER_VAR, addFooter);
        ctx.setVariable(BASE_URL_VAR, appBaseUrl);

        return engine.process("emailTemplate/notification_device_delete.html", ctx);
    }

    /**
     * Generates the Thymeleaf template engine
     *
     * @return generate template engine
     */
    private TemplateEngine generateTemplateEngine() {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();

        templateResolver.setTemplateMode("HTML");
        templateResolver.setCacheable(false);
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(templateResolver);
        return engine;
    }

    /**
     * Gets usernames from DB for a given notification type.
     *
     * @param notificationType the notification type for which usernames has to be found in DB
     * @return list of usernames for a given notification type;
     * or empty list, if no user found for the notification tpye
     */
    private List<String> getCCLoginListForNotification(String notificationType) {
        return em.createQuery("SELECT u.userLoginName FROM UserNotificationEntity u " +
                                " WHERE u.notificationType = :pNotificationType",
                        String.class)
                .setParameter("pNotificationType", notificationType)
                .getResultList();
    }

    /**
     * Merges two StringLists, and returns the two lists merged together.
     * If a list was empty, it will return the other list values.
     * If all lists were empty, the result will be NULL value
     *
     * @param firstList the first list that has to be merged to the second one
     * @param secondList the second list that has to be merged to the first one
     * @return the two lists merged together. If lists were empty, the return value will be NULL
     */
    private List<String> mergeLists(List<String> firstList, List<String> secondList) {
        List<String> result = null;

        if (CollectionUtils.isEmpty(firstList) && CollectionUtils.isEmpty(secondList)) {
            return result;
        }
        if (CollectionUtils.isNotEmpty(firstList)) {
            result = new ArrayList<>(firstList);
        }

        if (CollectionUtils.isNotEmpty(secondList)) {
           if (result == null) {
               result = new ArrayList<>();
           }

           result.addAll(secondList);
        }

        return result;
    }

    /**
     * Checks if the list contains any data to send notification.
     *
     * @param changeList the list that has to be checked
     * @return <code>true</code> if list contains information to send notification,
     * or <code>false</code> if no data found to send notification
     */
    private boolean hasStructureModification(List<MailNotificationDTO> changeList) {
        if(CollectionUtils.isEmpty(changeList)) {
            return false;
        }

        for(MailNotificationDTO m : changeList) {
            if(CollectionUtils.isNotEmpty(m.getNodeList())) {
                return true;
            }
        }

        return false;
    }
}
