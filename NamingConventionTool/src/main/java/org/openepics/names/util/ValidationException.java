/*
 * Copyright (c) 2017 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

/**
 * Signals that a validation exception has occurred.
 * Contains message and description.
 * Message serves as headline and brief description.
 * Description serves as (potentially) detailed description.
 *
 * @author karinrathsman
 * @author Lars Johansson
 */
public class ValidationException extends Exception {

    public static final String VALIDATION_ERROR = "Validation Error";

    /**
     *
     */
    private static final long serialVersionUID = 2059483039978124605L;

    private final String description;

    /**
     * Create ValidationException with given description.
     *
     * @param message message of the ValidationException
     */
    public ValidationException(String message) {
        this(message, null);
    }

    /**
     * Create ValidationException with given message and description.
     *
     * @param message message of the ValidationException
     * @param description description of the ValidationException
     */
    public ValidationException(String message, String description) {
        super(message);
        this.description = description;
    }

    /**
     * Return the description of the ValidationException.
     *
     * @return description of the ValidationException
     */
    public String getDescription() {
        return this.description;
    }

}
