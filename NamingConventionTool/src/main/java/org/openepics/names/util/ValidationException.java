/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openepics.names.util;

import javax.faces.application.FacesMessage;

/**
 *
 * @author karinrathsman
 */
public class ValidationException extends Exception {

    private final FacesMessage facesMessage;

    /**
     *
     * @param facesMessage The facases message of the exception
     */
    public ValidationException(FacesMessage facesMessage) {
        super(facesMessage.getDetail());
        this.facesMessage = facesMessage;
    }

    /**
     * @param message the detailed exception message
     */
    public ValidationException(String message) {
        super(message);
        this.facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", message);
    }

    /**
     *
     * @return validation message
     */
    public FacesMessage getFacesMessage() {
        return facesMessage;
    }

}
