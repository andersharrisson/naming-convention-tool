/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.util;

import com.google.common.base.Preconditions;

import javax.annotation.Nullable;
import org.openepics.names.services.MnemonicValidation;

/**
 * A static utility class for casting @Nullable values to non-@Nullable.
 *
 * @author Marko Kolar
 */
public class As {

    /**
     * This class is not to be instantiated.
     */
    private As() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return the cast of the value declared nullable to the same type that does not permit null values.
     *
     * @param value     a value declared nullable
     * @param <T>       arbitrary class
     * @return          the cast of the value declared nullable to the same type that does not permit null values.
     *                  Throws an exception if the input value is, in fact, null.
     */
    public static <T> T notNull(@Nullable T value) {
        Preconditions.checkNotNull(value);
        return value;
    }

    /**
     * Return string as null if the string is empty.
     *
     * @param string the string
     * @return string as null if the string is empty.
     */
    public static String nullIfEmpty(String string) {
        if (string == null) {
            return null;
        }
        String result = string.trim();
        return !result.isEmpty() ? result : null;
    }

    /**
     * Return string as empty if the string is null.
     *
     * @param string the string
     * @return string as empty if the string is null.
     */
    public static String emptyIfNull(String string) {
        return string != null ? string : "";

    }

    /**
     * Return true if boolean variables are equal.
     *
     * @param a argument
     * @param b argument
     * @return true if boolean variables are equal
     */
    public static boolean equals(boolean a, boolean b) {
        return a && b || !a && !b;
    }

    /**
     * Validates that statement is true.
     *
     * @param statement statement to validate
     * @param message error message
     * @throws org.openepics.names.util.ValidationException validation exception
     *
     */
    public static void validateState(boolean statement, String message) throws ValidationException {
        if (!statement) {
            throw new ValidationException(message);
        }
    }

    /**
     * Validates that statement is valid.
     *
     * @param statement validation enumeration
     * @param message error message
     * @throws ValidationException validation exception
     */
    public static void validateStateMnemonicRules(
            MnemonicValidation statement, String message) throws ValidationException {

        if (!statement.equals(MnemonicValidation.VALID)) {
            throw new ValidationException(message);
        }
    }

}
