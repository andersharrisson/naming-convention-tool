/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Utility class for helping in conversion
 * (e.g. conversion between LocalDate and java.util.date)
 *
 * @author Imre Toth <imre.toth@ess.eu>
 **/
public class ConversionUtil {

    /**
     * This class is not to be instantiated.
     */
    private ConversionUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Converts Java 8 LocalDate to java.util.Date
     *
     * @param dateToConvert the LocalDate that has to be converted to java.util.Date
     * @return the date converted to java.util.Date
     */
    public static Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
}
