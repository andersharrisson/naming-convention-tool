/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;

import com.google.common.collect.Lists;

/**
 * Bean to handle filtering and node status of tree nodes (expanded or collapsed) throughout a session.
 *
 * @author Karin Rathsman
 */
@SessionScoped
public class NameFilter implements Serializable {

    private static final long serialVersionUID = -2153127283622095245L;
    private boolean includeActive;
    private boolean includePending;
    private boolean includeArchived;
    private Options[] selectedOptions;
    private boolean includeCancelled;
    private final Map<UUID, NodeStatus> nodeMap;
    private final NodeStatus rootNodeStatus = new NodeStatus(true, true, false);

    public Map<UUID, NodeStatus> getNodeMap() {
        return nodeMap;
    }

    @Inject
    private NameViewProvider nameViewProvider;

    /**
     * Constructs a new name filter setting default options as {@link Options#ACTIVE} {@link Options#PENDING}.
     */
    @Inject
    public NameFilter() {
        Options[] defaultOptions = new Options[]{Options.ACTIVE, Options.PENDING};
        nodeMap = new HashMap<>();
        selectedOptions = defaultOptions;
    }

    /**
     * Enum to distinguish between and handle different options for name filter used in UI.
     */
    public enum Options {
        ACTIVE, ARCHIVED, PENDING, CANCELLED
    }

    /**
     * @return the options
     */
    public Options[] getOptions() {
        return Options.values();
    }

    /**
     * Sets the selected options
     *
     * @param selectedOptions the selected options to set.
     */
    public void setSelectedOptions(Options[] selectedOptions) {
        this.selectedOptions = selectedOptions != null ? Arrays.copyOf(selectedOptions, selectedOptions.length) : null;
        update();
    }

    /**
     *
     * @return the selected options
     */
    public Options[] getSelectedOptions() {
        return selectedOptions != null ? Arrays.copyOf(selectedOptions, selectedOptions.length) : null;
    }

    /**
     * Update name filter based on selected options.
     */
    public void update() {
        List<Options> filter = Lists.newArrayList(getSelectedOptions());
        includeActive = filter.contains(Options.ACTIVE);
        includeArchived = filter.contains(Options.ARCHIVED);
        includePending = filter.contains(Options.PENDING);
        includeCancelled = filter.contains(Options.CANCELLED);

        if (!nodeMap.isEmpty()) {
            for (Map.Entry<UUID, NodeStatus> entry : nodeMap.entrySet()) {
                UUID key = entry.getKey();
                NameView nameView = nameViewProvider.nameView(key);
                NodeStatus nodeStatus = entry.getValue();
                nodeStatus.setAccepted(accept(nameView));
            }
        }
    }

    /**
     *
     * @return true if the pending name stages are included
     */
    public boolean isIncludePending() {
        return includePending;
    }

    /**
     *
     * @return true if cancelled name stages are included.
     */
    public boolean isIncludeCancelled() {
        return includeCancelled;
    }

    private boolean isActive(NameStage nameStage) {
        return includeActive && nameStage.isActive();
    }

    /**
     * Return if entry is archived based on if archived entries to be included
     * and if given name stage is considered archived.
     *
     * @param nameStage given name stage
     * @return if entry is archived based on if archived entries to be included
     *         and if given name stage is considered archived
     */
    public boolean isArchived(NameStage nameStage) {
        return includeArchived && nameStage.isArchived();
    }

    private boolean isPending(NameStage nameStage) {
        return includePending && nameStage.isPending();
    }

    private boolean isCancelled(NameStage nameStage) {
        return includeCancelled && nameStage.isCancelled();
    }

    /**
     * Decides whether a nameView is accepted by the view options.
     *
     * @param nameView
     * @return true if the name view is accepted by the view options
     */
    private boolean accept(NameView nameView) {
        return acceptStatus(nameView.getRevisionPair().getNameStage()) && include(nameView);
    }

    private boolean acceptStatus(NameStage nameStage) {
        return isActive(nameStage) || isArchived(nameStage) || isPending(nameStage) || isCancelled(nameStage);
    }

    private boolean include(NameView nameView) {
        if (nameView.getNameType().isDeviceStructure()) {
            return true;
        } else {
            if (nameView.isRoot()) {
                return false;
            } else if (nameView.isInStructure(NameType.SYSTEM_STRUCTURE)
                    && nameView.getParent(NameType.SYSTEM_STRUCTURE).isRoot()) {
                return true;
            } else {
                return include(nameView.getParent(NameType.SYSTEM_STRUCTURE));
            }
        }
    }

    /**
     * Return the nodeStatus of the nameView.
     *
     * @param nameView the nameView
     * @return the nodeStatus of the nameView
     */
    public NodeStatus getNodeStatus(NameView nameView) {
        if (nameView == null || nameView.isRoot()) {
            return rootNodeStatus;
        } else {
            UUID key = nameView.getRevisionPair().getBaseRevision().getNameArtifact().getUuid();
            if (!nodeMap.containsKey(key)) {
                nodeMap.put(key, new NodeStatus(false, true, accept(nameView)));
            }
            return nodeMap.get(key);
        }
    }

    /**
     * sets the customized filter.
     *
     * @param nameView the name View
     * @param filtered is true if the nameView is customized filtered.
     */
    public void setCustomizedFiltered(NameView nameView, boolean filtered) {
        if (nameView.isInDeviceRegistry()) {
            boolean subsystemFiltered =
                    getNodeStatus(nameView.getParent(NameType.SYSTEM_STRUCTURE)).isCustomizedFiltered();
            boolean deviceTypeFiltered =
                    getNodeStatus(nameView.getParent(NameType.DEVICE_STRUCTURE)).isCustomizedFiltered();
            getNodeStatus(nameView).setCustomizedFiltered(subsystemFiltered && deviceTypeFiltered);
        } else if (nameView.isInStructure()) {
            getNodeStatus(nameView).setCustomizedFiltered(filtered);
            for (NameView child : nameView.getChildren()) {
                setCustomizedFiltered(child, filtered);
            }
        } else {
            return;
        }
    }

}
