package org.openepics.names.business;

import java.util.Date;

import org.openepics.names.model.UserAccount;

public class UserAction {

    private final Date date;
    private final String message;
    private final UserAccount user;

    /**
     * Constructor
     *
     * @param date the date
     * @param message the user message
     * @param user the user
     */
    public UserAction(Date date, String message, UserAccount user) {
        this.date = date;
        this.message = message;
        this.user = user;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the user
     */
    public UserAccount getUser() {
        return user;
    }

}
