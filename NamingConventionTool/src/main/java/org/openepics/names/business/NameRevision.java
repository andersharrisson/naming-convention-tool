/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import org.openepics.names.model.Device;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePart;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.model.NamePartRevisionStatus;
import org.openepics.names.util.As;
import com.google.common.base.Objects;
import java.util.Date;

/**
 * Name Revision, currently as an intermediate fix to combine namePartRevision and deviceRevision.
 *
 * @author karinrathsman
 *
 */
public class NameRevision {

    // Note revision history of file in repository.

    private boolean deleted;
    private NameArtifact systemStructureParent;
    private NameArtifact deviceStructureParent;
    private NameElement nameElement;
    private UserAction userAction;
    private UserAction process;
    private NameArtifact nameArtifact;
    private NameRevisionStatus status;
    private long id;

    /**
     * Constructs a new name revision entity (representing database entry).
     *
     * @param nameArtifact              the name instance
     * @param systemStructureParent     the parent nameArtifact in the system structure
     * @param deviceStructureParent     the parent nameArtifact in the device structure
     * @param nameElement               the name element containing full name, mnemonic and description
     * @param request                   the user request
     * @param process                   the user process
     * @param deleted                   indicates if the revision is either pending deletion or is archived (approved)
     * @param status                    the status of the revision, pending, approved, cancelled or rejected
     */
    public NameRevision(
            NameArtifact nameArtifact,
            NameArtifact systemStructureParent,
            NameArtifact deviceStructureParent,
            NameElement nameElement,
            UserAction request,
            UserAction process,
            boolean deleted,
            NameRevisionStatus status) {

        this.nameArtifact = nameArtifact;
        this.status = status;
        this.deleted = deleted;
        this.systemStructureParent = systemStructureParent;
        this.deviceStructureParent = deviceStructureParent;
        this.nameElement = nameElement;
        this.userAction = request;
        this.process = process;
    }

    /**
     * Constructor to create a nameRevision from the current database. (To be removed when database has been upgraded)
     *
     * @param revision the deviceRevision
     */
    public NameRevision(DeviceRevision revision) {
        this.userAction =
                new UserAction(revision.getRequestDate(), revision.getProcessorComment(), revision.getRequestedBy());
        this.process = null;
        this.deleted = revision.isDeleted();
        this.nameElement =
                new NameElement(
                        revision.getConventionName(), revision.getInstanceIndex(), revision.getAdditionalInfo());
        this.nameArtifact = nameArtifact(revision.getDevice());
        this.systemStructureParent = nameArtifact(revision.getSection());
        this.deviceStructureParent = revision.getDeviceType() != null ? nameArtifact(revision.getDeviceType()) : null;
        this.status = NameRevisionStatus.APPROVED;
        this.id = revision.getId();
    }

    /**
     * Constructor to create a nameRevision from the current database. (To be removed when database has been upgraded)
     *
     * @param revision the namePartRevision
     */
    public NameRevision(NamePartRevision revision) {
        As.notNull(revision.getNamePart());
        this.nameArtifact = nameArtifact(revision.getNamePart());
        As.notNull(nameArtifact.getNameType());
        this.deleted = revision.isDeleted();
        this.userAction =
                new UserAction(revision.getRequestDate(), revision.getRequesterComment(), revision.getRequestedBy());
        this.process =
                new UserAction(revision.getProcessDate(), revision.getProcessorComment(), revision.getProcessedBy());
        this.nameElement = new NameElement(revision.getName(), revision.getMnemonic(), revision.getDescription());

        if (nameArtifact.getNameType().isSystemStructure()) {
            this.systemStructureParent = revision.getParent() != null ? nameArtifact(revision.getParent()) : null;
            this.deviceStructureParent = null;
        } else if (nameArtifact.getNameType().isDeviceStructure()) {
            this.systemStructureParent = null;
            this.deviceStructureParent = revision.getParent() != null ? nameArtifact(revision.getParent()) : null;
        } else {
            this.systemStructureParent = null;
            this.deviceStructureParent = null;
        }
        this.status = NamePartRevisionStatus.asNameRevisionStatus(revision.getStatus());
        this.id = revision.getId();
    }

    private static NameArtifact nameArtifact(NamePart namePart) {
        return NameArtifact.getInstance(namePart);
    }

    private static NameArtifact nameArtifact(Device device) {
        return NameArtifact.getInstance(device);
    }

    /**
     * @return the nameArtifact
     */
    public NameArtifact getNameArtifact() {
        return nameArtifact;
    }

    /**
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     *
     * @return the revision number.
     */
    public long getId() {
        return id;
    }

    /**
     * Return the specified name type parent
     *
     * @param type the revision name type (system structure, device structure or device registry)
     * @return the specified name type parent
     */
    public NameArtifact getParent(NameType type) {
        return type.isDeviceStructure() ? deviceStructureParent : systemStructureParent;
    }

    /**
     * @return the systemStructureParent
     */
    private NameArtifact getSystemStructureParent() {
        return systemStructureParent;
    }

    /**
     * @return the deviceStructureParent
     */
    private NameArtifact getDeviceStructureParent() {
        return deviceStructureParent;
    }

    /**
     * @return the nameElement containing the fullName, mnemonic and description.
     */
    public NameElement getNameElement() {
        return nameElement;
    }

    /**
     * @return the userAction
     */
    public UserAction getUserAction() {
        return userAction;
    }

    /**
     * @return the process
     */
    public UserAction getProcess() {
        return process;
    }

    /**
     *
     * @return the status (approved, pending, cancelled or rejected)
     */
    public NameRevisionStatus getStatus() {
        return status;
    }

    /**
     * Return if other revision is null or if the this revision is the same as or supersede the other revision.
     *
     * @param other other revision to compare with
     * @return true if other revision is null or if the this revision is the same as or supersede the other revision
     */
    public boolean supersede(NameRevision other) {
        // TODO: fix this...(when new revision are be created instead of updating old ones)
        return other == null
                    || getId() > other.getId()
                    || getId() == other.getId() && other.getStatus().isPending() && !getStatus().isPending();
    }

    public Date getDate() {
        return status.isPending() || nameArtifact.getNameType().isDeviceRegistry()
                ? userAction.getDate()
                : process.getDate();
    }

    /**
     * Return if other revision is the same as other revision.
     *
     * @param other other revision to compare with
     * @return true if other revision is the same as other revision
     */
    public boolean isSameAs(NameRevision other) {
        // TODO: fix this...(when new revision are be created instead of updating old ones)
        return other != null && getId() == other.getId() && other.getStatus().equals(getStatus());
    }

    /**
     * Return if the other revision has the same nameArtifact.
     *
     * @param other the other revision to compare with
     * @return true if the other revision has the same nameArtifact
     */
    public boolean sameArtifact(NameRevision other) {
        return other != null && getNameArtifact().equals(other.getNameArtifact());
    }

    /**
     * Return if a new proposal specified by the arguments needs validation.
     *
     * @param deleted       true if proposal is delete
     * @param nameElement   proposed nameElement
     * @param systemParent  proposed systemParent
     * @param deviceParent  proposed deviceParent
     * @return              true if a new proposal specified by the arguments needs validation
     */
    public boolean isValidationNeededForProposedChanges(
            boolean deleted,
            NameElement nameElement,
            NameArtifact systemParent,
            NameArtifact deviceParent) {

        if (!As.equals(isDeleted(), deleted)) {
            return true;
        } else if (!getNameElement().hasEqualMnemonicAs(nameElement)) {
            return true;
        } else if (!Objects.equal(getDeviceStructureParent(), deviceParent)) {
            return true;
        } else if (!Objects.equal(getSystemStructureParent(), systemParent)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Representing if a new proposal specified by the arguments needs validation.
     *
     * @param proposedRevision  the proposed revision
     * @return                  true if a new proposal specified by the arguments needs validation
     */
    public boolean isValidationNeededForProposedChanges(NameRevision proposedRevision) {
        return proposedRevision == null
                || isValidationNeededForProposedChanges(
                        proposedRevision.isDeleted(),
                        proposedRevision.getNameElement(),
                        proposedRevision.getSystemStructureParent(),
                        proposedRevision.getDeviceStructureParent());
    }
}
