package org.openepics.names.business;

import org.openepics.names.model.Device;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePart;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.model.NamePartRevisionStatus;
import org.openepics.names.util.As;
import com.google.common.base.Objects;
import java.util.Date;

/**
 * Name Revision, currently as an intermediate fix to combine namePartRevision and deviceRevision.
 *
 * @author karinrathsman
 *
 */
public class NameRevision {

    private boolean deleted;
    private NameArtifact areaStructureParent;
    private NameArtifact deviceStructureParent;
    private NameElement nameElement;
    private UserAction userAction;
    private UserAction process;
    private NameArtifact nameArtifact;
    private NameRevisionStatus status;
    private long id;

    /**
     *
     * @param nameArtifact the name instance
     * @param areaStructureParent the parent nameArtifact in the area structure
     * @param deviceStructureParent the parent nameArtifact in the device structure
     * @param nameElement the name element containing fullname, mnemonic and description
     * @param request the user request
     * @param process the user process
     * @param deleted indicates if the revision is either pending deletion or is archived (approved)
     * @param status the status of the revision, pending, approved, cancelled or rejectd
     */
    public NameRevision(NameArtifact nameArtifact, NameArtifact areaStructureParent, NameArtifact deviceStructureParent, NameElement nameElement, UserAction request, UserAction process, boolean deleted, NameRevisionStatus status) {
        this.nameArtifact = nameArtifact;
        this.status = status;
        this.deleted = deleted;
        this.areaStructureParent = areaStructureParent;
        this.deviceStructureParent = deviceStructureParent;
        this.nameElement = nameElement;
        this.userAction = request;
        this.process = process;
    }

    /**
     * Constructor to create a nameRevision from the current database. (To be removed when database has been upgraded)
     *
     * @param revision the deviceRevision
     */
    public NameRevision(DeviceRevision revision) {
        this.userAction = new UserAction(revision.getRequestDate(), null, revision.getRequestedBy());
        this.process = null;
        this.deleted = revision.isDeleted();
        this.nameElement = new NameElement(revision.getConventionName(), revision.getInstanceIndex(), revision.getAdditionalInfo());
        this.nameArtifact = nameArtifact(revision.getDevice());
        this.areaStructureParent = nameArtifact(revision.getSection());
        this.deviceStructureParent = nameArtifact(revision.getDeviceType());
        this.status = NameRevisionStatus.APPROVED;
        this.id = revision.getId();
    }

    /**
     * Constructor to create a nameRevision from the current database. (To be removed when database has been upgraded)
     *
     * @param revision the namePartRevision
     */
    public NameRevision(NamePartRevision revision) {
        As.notNull(revision.getNamePart());
        this.nameArtifact = nameArtifact(revision.getNamePart());
        As.notNull(nameArtifact.getNameType());
        this.deleted = revision.isDeleted();
        this.userAction = new UserAction(revision.getRequestDate(), revision.getRequesterComment(), revision.getRequestedBy());
        this.process = new UserAction(revision.getProcessDate(), revision.getProcessorComment(), revision.getProcessedBy());
        this.nameElement = new NameElement(revision.getName(), revision.getMnemonic(), revision.getDescription());
        if (nameArtifact.getNameType().isAreaStructure()) {
            this.areaStructureParent = revision.getParent() != null ? nameArtifact(revision.getParent()) : null;
            this.deviceStructureParent = null;
        } else if (nameArtifact.getNameType().isDeviceStructure()) {
            this.areaStructureParent = null;
            this.deviceStructureParent = revision.getParent() != null ? nameArtifact(revision.getParent()) : null;
        } else {
            this.areaStructureParent = null;
            this.deviceStructureParent = null;
        }
        this.status = NamePartRevisionStatus.AsNameRevisionStatus(revision.getStatus());
        this.id = revision.getId();
    }

//        /**
//         * Method to update status
//         * @param processedStatus
//         * @param user
//         * @param comment 
//         */
//        private void process(NameRevisionStatus processedStatus, @Nullable UserAccount user, @Nullable String comment){
//            Preconditions.checkState(getStatus() == NameRevisionStatus.PENDING);
//            Preconditions.checkState(processedStatus != NameRevisionStatus.PENDING);
//            this.status = processedStatus;
//            this.process=new UserAction(new Date(), comment, user);
//	}                
    private static NameArtifact nameArtifact(NamePart namePart) {
        return NameArtifact.getInstance(namePart);
    }

    private static NameArtifact nameArtifact(Device device) {
        return NameArtifact.getInstance(device);
    }

    /**
     * @return the nameArtifact
     */
    public NameArtifact getNameArtifact() {
        return nameArtifact;
    }

    /**
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     *
     * @return the revision number.
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param type the revision name type (area structure, device structure or device registry)
     * @return the specified name type parent
     */
    public NameArtifact getParent(NameType type) {
        return type.isDeviceStructure() ? deviceStructureParent : areaStructureParent;
    }

    /**
     * @return the areaStructureParent
     */
    private NameArtifact getAreaStructureParent() {
        return areaStructureParent;
    }

    /**
     * @return the deviceStructureParent
     */
    private NameArtifact getDeviceStructureParent() {
        return deviceStructureParent;
    }

    /**
     * @return the nameElement containing the fullName, mnemonic and description.
     */
    public NameElement getNameElement() {
        return nameElement;
    }

    /**
     * @return the userAction
     */
    public UserAction getUserAction() {
        return userAction;
    }

    /**
     * @return the process
     */
    public UserAction getProcess() {
        return process;
    }

    /**
     *
     * @return the status (approved, pending, cancelled or rejected)
     */
    public NameRevisionStatus getStatus() {
        return status;
    }

    /**
     *
     * @param other other revision to compare with
     * @return true if other revision is null or if the this revision is the same as or supersede the other revision
     */
    public boolean supersede(NameRevision other) {
        // TODO: fix this...(when new revision are be created instead of updating old ones)
        return other == null || getId() > other.getId() || getId() == other.getId() && other.getStatus().isPending() && !getStatus().isPending();
//            return other == null || getDate().after(other.getDate());
    }

    public Date getDate() {
        return status.isPending() || nameArtifact.getNameType().isDeviceRegistry() ? userAction.getDate() : process.getDate();
    }

    /**
     *
     * @param other other revision to compare with
     * @return true if other revision is the same as other revision
     */
    public boolean isSameAs(NameRevision other) {
        // TODO: fix this...(when new revision are be created instead of updating old ones)
        return getId() == other.getId() && other.getStatus().equals(getStatus());
    }

    /**
     *
     * @param other the other revision to compare with
     * @return true if the other revision has the same nameArtifact
     */
    public boolean sameArtifact(NameRevision other) {
        return other != null && getNameArtifact().equals(other.getNameArtifact());
    }

    /**
     * @param deleted true if proposal is delete
     * @param nameElement proposed nameElement
     * @param areaParent proposed areaParent
     * @param deviceParent proposed deviceParent
     * @return true if a new proposal specified by the arguments needs validation.
     */
    public boolean isValidationNeededForProposedChanges(boolean deleted, NameElement nameElement, NameArtifact areaParent, NameArtifact deviceParent) {
        if (!As.equals(isDeleted(), deleted)) {
            return true;
        } else if (!getNameElement().hasEqualMnemonicAs(nameElement)) {
            return true;
        } else if (!Objects.equal(getDeviceStructureParent(), deviceParent)) {
            return true;
        } else if (!Objects.equal(getAreaStructureParent(), areaParent)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param proposedRevision the proposed revision
     * @return true if a new proposal specified by the arguments needs validation.
     */
    public boolean isValidationNeededForProposedChanges(NameRevision proposedRevision) {
        return isValidationNeededForProposedChanges(proposedRevision.isDeleted(), proposedRevision.getNameElement(), proposedRevision.getAreaStructureParent(), proposedRevision.getDeviceStructureParent());
    }
}
