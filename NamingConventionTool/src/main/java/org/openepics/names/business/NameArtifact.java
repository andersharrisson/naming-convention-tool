package org.openepics.names.business;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import org.openepics.names.model.Device;
import org.openepics.names.model.NamePart;
import org.openepics.names.model.NamePartType;
import org.openepics.names.util.As;

import com.google.common.base.Preconditions;

public class NameArtifact {

    private static final Map<UUID, NameArtifact> NAME_ARTIFACT_MAP = new HashMap<>();
    private NameType nameType;
    private String uuid;
    private NamePart namePart;
    private Device device;

    /**
     * @param uuid the universally unique identifier
     * @param nameType the type of the name artifact.
     */
    public NameArtifact(UUID uuid, NameType nameType) {
        Preconditions.checkNotNull(uuid);
        Preconditions.checkNotNull(nameType);
        this.uuid = uuid.toString();
        this.nameType = nameType;
    }

    /**
     * @return The universally unique identifier.
     */
    public UUID getUuid() {
        return UUID.fromString(uuid);
    }

    /**
     * @return The type of the NameArtifact.
     */
    public NameType getNameType() {
        return nameType;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof NameArtifact && ((NameArtifact) other).getUuid().equals(getUuid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUuid());
    }

    protected static NameArtifact getInstance(NamePart namePart) {
        UUID uuid = namePart.getUuid();
        if (!NAME_ARTIFACT_MAP.containsKey(uuid)) {
            NameType nameType = null;
            if (namePart.getNamePartType().equals(NamePartType.SECTION)) {
                nameType = NameType.AREA_STRUCTURE;
            } else if (namePart.getNamePartType().equals(NamePartType.DEVICE_TYPE)) {
                nameType = NameType.DEVICE_STRUCTURE;
            } else {
                throw new IllegalStateException("NameType cannot be resolved");
            }

            final NameArtifact nameArtifact = new NameArtifact(namePart.getUuid(), nameType);
            nameArtifact.namePart = namePart;
            nameArtifact.device = null;
            NAME_ARTIFACT_MAP.put(uuid, nameArtifact);
        }
        return NAME_ARTIFACT_MAP.get(uuid);
    }

    /**
     *
     * @return the name artifact as a NamePart (to be removed with the new database)
     */
    public NamePart asNamePart() {
        return As.notNull(namePart);
    }

    /**
     *
     * @return the name artifact as a Device (to be removed with the new database)
     */
    public Device asDevice() {
        return As.notNull(device);
    }

    /**
     *
     * @param device the device to convert to NameArtifact.
     * @return NameArtifact
     */
    protected static NameArtifact getInstance(Device device) {
        UUID uuid = device.getUuid();
        if (!NAME_ARTIFACT_MAP.containsKey(uuid)) {
            final NameArtifact nameArtifact = new NameArtifact(device.getUuid(), NameType.DEVICE_REGISTRY);
            nameArtifact.device = device;
            nameArtifact.namePart = null;
            NAME_ARTIFACT_MAP.put(uuid, nameArtifact);
        }
        return NAME_ARTIFACT_MAP.get(uuid);
    }
}
