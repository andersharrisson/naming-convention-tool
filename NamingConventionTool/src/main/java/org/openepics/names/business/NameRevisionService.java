/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import java.util.List;

import javax.inject.Inject;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.services.NamePartService;
import com.google.common.collect.Lists;
import javax.ejb.Stateless;

/**
 * A service bean and gateway providing revisions to and from entities.
 *
 * @author karinrathsman
 *
 */
@Stateless
public class NameRevisionService {

    @Inject
    private NamePartService namePartService;

    /**
     *
     * @return All name revisions from the database
     */
    public List<NameRevision> revisions() {
        List<NameRevision> nameRevisions = namePartRevisionsAsNameRevisions(namePartService.allNamePartRevisions());
        nameRevisions.addAll(deviceRevisionsAsNameRevisions(namePartService.allDeviceRevisions()));
        return nameRevisions;
    }

    /**
     * Converts list of namePartRevisions to NameRevisions
     *
     * @param namePartRevisions
     * @return List of NamePartRevisions converted to NameRevisions
     */
    private List<NameRevision> namePartRevisionsAsNameRevisions(List<NamePartRevision> namePartRevisions) {
        List<NameRevision> nameRevisionViews = Lists.newArrayList();
        for (NamePartRevision revision : namePartRevisions) {
            nameRevisionViews.add(new NameRevision(revision));
        }
        return nameRevisionViews;
    }

    /**
     * Converts a list of deviceRevisons to NameRevisions
     *
     * @param deviceRevisions
     * @return List of NameRevisions
     */
    private List<NameRevision> deviceRevisionsAsNameRevisions(List<DeviceRevision> deviceRevisions) {
        List<NameRevision> nameRevisions = Lists.newArrayList();
        for (DeviceRevision revision : deviceRevisions) {
            nameRevisions.add(new NameRevision(revision));
        }
        return nameRevisions;
    }

    /**
     * Return the revision in history of the specified nameArtifact.
     *
     * @param nameArtifact the nameArtifact of the device or name element
     * @return the revision in history of the specified nameArtifact
     */
    public List<NameRevision> historyRevisions(NameArtifact nameArtifact) {
        try {
            return namePartRevisionsAsNameRevisions(
                    Lists.newArrayList(namePartService.revisions(nameArtifact.asNamePart())));
        } catch (Exception e) {
            return deviceRevisionsAsNameRevisions(
                    Lists.newArrayList(namePartService.revisions(nameArtifact.asDevice())));
        }
    }

    /**
     * Lists DeviceRevisions that has been deleted and admins has to be notified from the action
     *
     * @return list of DeviceRevisions with a size limitation
     */
    public List<DeviceRevision> deletedDevicesToNotify() {

        return namePartService.devicesToSendNotification(true);
    }

    /**
     * Lists DeviceRevisions that has been modified and admins has to be notified from the action
     *
     * @return list of DeviceRevisions with a size limitation
     */
    public List<DeviceRevision> modifiedDevicesToNotify() {

        return namePartService.devicesToSendNotification(false);
    }
}
