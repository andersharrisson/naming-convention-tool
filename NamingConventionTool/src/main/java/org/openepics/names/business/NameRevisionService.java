package org.openepics.names.business;

import java.util.List;

import javax.inject.Inject;
import org.openepics.names.model.DeviceRevision;
import org.openepics.names.model.NamePartRevision;
import org.openepics.names.services.NamePartService;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import javax.ejb.Stateless;

/**
 * A service bean and gateway providing revisions to and from entities.
 *
 * @author karinrathsman
 *
 */
@Stateless
public class NameRevisionService {

    @Inject
    private NamePartService namePartService;

    /**
     *
     * @return All name revisions from the database
     */
    public List<NameRevision> revisions() {
        List<NameRevision> nameRevisions = new ArrayList<>();

        nameRevisions = namePartRevisionsAsNameRevisions(namePartService.allNamePartRevisions());
        nameRevisions.addAll(deviceRevisionsAsNameRevisions(namePartService.allDeviceRevisions()));
        return nameRevisions;
    }

    /**
     * Converts list of namePartRevisions to NameRevisions
     *
     * @param namePartRevisions
     * @return List of NamePartRevisions converted to NameRevisions
     */
    private List<NameRevision> namePartRevisionsAsNameRevisions(List<NamePartRevision> namePartRevisions) {
        List<NameRevision> nameRevisionViews = Lists.newArrayList();
        for (NamePartRevision revision : namePartRevisions) {
            nameRevisionViews.add(new NameRevision(revision));
        }
        return nameRevisionViews;
    }

    /**
     * Converts a list of deviceRevisons to NameRevisions
     *
     * @param deviceRevisions
     * @return List of NameRevisions
     */
    private List<NameRevision> deviceRevisionsAsNameRevisions(List<DeviceRevision> deviceRevisions) {
        List<NameRevision> nameRevisions = Lists.newArrayList();
        for (DeviceRevision revision : deviceRevisions) {
            nameRevisions.add(new NameRevision(revision));
        }
        return nameRevisions;
    }

    /**
     *
     * @param nameArtifact the nameArtifact of the device or name element
     * @return the revision in history of the specified nameArtifact.
     */
    public List<NameRevision> historyRevisions(NameArtifact nameArtifact) {
        try {
            return namePartRevisionsAsNameRevisions(Lists.newArrayList(namePartService.revisions(nameArtifact.asNamePart())));
        } catch (Exception e) {
            return deviceRevisionsAsNameRevisions(Lists.newArrayList(namePartService.revisions(nameArtifact.asDevice())));
        }
    }
}
