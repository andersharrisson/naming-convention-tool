package org.openepics.names.business;

import org.openepics.names.util.As;

/**
 * Enum to handle the different name stages a revision pair can have.
 *
 * @author karinrathsman
 */
public enum NameStage {
    INITIAL(false, false, false, false, false),
    INITIAL_PROPOSED(true, false, false, false, false),
    INITIAL_CANCELLED(false, false, false, false, true),
    STABLE(false, false, true, false, false),
    ACTIVE_MODIFICATION_PROPOSED(true, false, true, false, false),
    ACTIVE_DELETION_PROPOSED(true, true, true, false, false),
    ACTIVE_CANCELLED(false, false, true, false, true),
    DELETED_APPROVED(false, false, true, true, false);

    private boolean pending;
    private boolean pendingDeleted;
    private boolean approved;
    private boolean approvedDeleted;
    private boolean cancelled;

    /**
     * @param pending indicating whether proposed addition, modification, or deletion is pending approval
     * @param pendingDeleted indicates whether the pending change is a deletion.
     * @param approved indicates whether an approve revision exists in the past.
     * @param approvedDeleted indicates the approved revision is deleted (archived) or not.
     * @param cancelled indicates whether the proposal was cancelled.
     */
    NameStage(boolean pending, boolean pendingDeleted, boolean approved, boolean approvedDeleted, boolean cancelled) {
        this.pending = pending;
        this.pendingDeleted = pendingDeleted;
        this.approved = approved;
        this.approvedDeleted = approvedDeleted;
        this.cancelled = cancelled;
    }

    /**
     *
     * @return true if the nameStage is initial.
     */
    public boolean isInitial() {
        return equals(INITIAL);
    }

    /**
     *
     * @return true if the nameStage is in proposed stage (no approved revisions available)
     */
    public boolean isAdded() {
        return equals(INITIAL_PROPOSED);
    }

    /**
     *
     * @return if the nameStage has both approved and pending modification.
     */
    public boolean isModified() {
        return equals(ACTIVE_MODIFICATION_PROPOSED);
    }

    /**
     *
     * @return if the nameStage has an active revision (approved and not deleted) but is pending deletion.
     */
    public boolean isDeleted() {
        return equals(ACTIVE_DELETION_PROPOSED);
    }

    /**
     *
     * @return if the nameStage is approved deleted.
     */
    public boolean isArchived() {
        return equals(DELETED_APPROVED);
    }

    /**
     *
     * @return true if the name is archived or initially cancelled.
     */
    public boolean isObsolete() {
        return isArchived() || equals(INITIAL_CANCELLED);
    }

    /**
     *
     * @return true if the name is initially cancelled before approval.
     */
    public boolean isRemoved() {
        return equals(INITIAL_CANCELLED);
    }

    /**
     *
     * @return true if the nameStage is approved and not archived.
     */
    public boolean isActive() {
        return isApproved() && !isArchived();
    }

    /**
     *
     * @param pendingDeleted indicator whether the request is to delete.
     * @return next pending nameStage in workflow after add, modify or delete request. Null if illegal.
     */
    public NameStage nextRequestStage(boolean pendingDeleted) {
        return !(isObsolete() || isDeleted()) ? NameStage.get(true, pendingDeleted, isApproved(), isApprovedDeleted(), false) : null;
    }

    /**
     *
     * @param approve indicator whether the process is to approve request.
     * @return next nameStage in workflow after process approve, cancel or reject. Null if illegal.
     */
    public NameStage nextProcessedStage(boolean approve) {
        boolean approved = approve || isApproved();
        boolean approveDeleted = approve && isPendingDeleted() || isApprovedDeleted();

        return isPending() ? NameStage.get(false, false, approved, approveDeleted, !approve) : null;
    }

    /**
     *
     * @return if the stabe is pending
     */
    public boolean isPending() {
        return pending;
    }

    /**
     *
     * @return if the stage is approved
     */
    public boolean isApproved() {
        return approved;
    }

    /**
     *
     * @return true if the pending stage was cancelled
     */
    public boolean isCancelled() {
        return cancelled;
    }

    /**
     *
     * @return true if the stage is pending deletion
     */
    protected boolean isPendingDeleted() {
        return pendingDeleted;
    }

    /**
     *
     * @return true if the stage is archived.
     */
    protected boolean isApprovedDeleted() {
        return approvedDeleted;
    }

    /**
     *
     * @param child child stage
     * @return true if this stage is allowed as parent of the specified child stage. False otherwise
     */
    public boolean isAllowedAsParentOf(NameStage child) {
        return (isInitial() || equals(STABLE) || equals(ACTIVE_CANCELLED) || isModified()) && !child.isInitial() || equals(child) || isDeleted() && child.isArchived() || child.equals(INITIAL_CANCELLED);
    }

    /**
     *
     * @param pending boolean flag true if the name is pending approval
     * @param pendingDeleted boolean flag, true if the the name is pending deletion.
     * @param approved boolean flag true if the name has an approved revision
     * @param approvedDeleted boolean flag. True if the name has been proposal to delete the name has been approved.
     * @param cancelled Boolean flag. True if the pending revision has been cancelled or rejected
     * @return if the arguments corresponds to this nameStage
     */
    private boolean equals(boolean pending, boolean pendingDeleted, boolean approved, boolean approvedDeleted, boolean cancelled) {
        return As.equals(approved, isApproved()) && (!approved || As.equals(approvedDeleted, isApprovedDeleted()))
                && As.equals(pending, isPending()) && (!pending || As.equals(pendingDeleted, isPendingDeleted()))
                && As.equals(cancelled, isCancelled());
    }

    /**
     *
     * @param pending boolean flag true if the name is pending approval
     * @param pendingDeleted boolean flag, true if the the name is pending deletion.
     * @param approved boolean flag true if the name has an approved revision
     * @param approvedDeleted boolean flag. True if the name has been proposal to delete the name has been approved.
     * @param cancelled Boolean flag. True if the pending revision has been cancelled or rejected
     * @return The name stage in the workflow that the name has based on the arguments.
     */
    public static NameStage get(boolean pending, boolean pendingDeleted, boolean approved, boolean approvedDeleted, boolean cancelled) {
        for (NameStage nameStage : values()) {
            if (nameStage.equals(pending, pendingDeleted, approved, approvedDeleted, cancelled)) {
                return nameStage;
            }
        }
        return null;
    }

}
