package org.openepics.names.business;

import com.google.common.base.Objects;

/**
 * A class to generate information of a name element to be used in tables and trees as row and columns.
 *
 * @author karinrathsman
 *
 */
public class RowData {

    private Column fullName;
    private Column mnemonic;
    private Column description;
    private String status;
    private boolean pending;
    private boolean cancelled;
    private boolean archived;
    private boolean deleted;
    private boolean approved;
    private UserAction userAction;
    private Column combined;

    /**
     *
     * @param unapprovedElement	nameElement of the unapproved revision
     * @param approvedElement nameElement of the approved revision
     * @param userAction the action, including name time and message by the user
     * @param pending true if the name element is pending approval
     * @param deleted true if the name element is pending deletion
     * @param approved true if the name element is approved
     * @param archived true if the element has been approved to be deleted
     * @param cancelled true if the pending changes has been cancelled or rejected
     * @param rejected true if the pending changes has been rejected
     */
    public RowData(NameElement unapprovedElement, NameElement approvedElement, UserAction userAction, boolean pending, boolean deleted, boolean approved, boolean archived, boolean cancelled, boolean rejected) {
        this.approved = approved;
        this.pending = pending;
        this.deleted = deleted;
        this.cancelled = cancelled;
        this.archived = archived;
        this.mnemonic = new Column(approved ? approvedElement.getMnemonic() : null, pending || cancelled ? unapprovedElement.getMnemonic() : null);
        this.fullName = new Column(approved ? approvedElement.getFullName() : null, pending || cancelled ? unapprovedElement.getFullName() : null);
        this.description = new Column(approved ? approvedElement.getDescription() : null, pending || cancelled ? unapprovedElement.getDescription() : null);
        this.combined = new Column(approved ? approvedElement.toString() : null, pending || cancelled ? unapprovedElement.toString() : null);
        this.status = archived ? "Archived" : pending ? "Pending" : approved ? "Approved" : rejected ? "Rejected" : cancelled ? "Cancelled" : "";
        this.userAction = userAction;
    }

    /**
     * @param revisionPair the revision Pair.
     * @param includePending indicates if pending states shall be displayed
     * @param includeCancelled indicates if cancelled states shall be displayed
     * @return RowData of the specified revisionPair to be used in tables, trees and history tables.
     */
    public static RowData newRowData(NameRevisionPair revisionPair, boolean includePending, boolean includeCancelled) {
        NameStage stage = revisionPair.getNameStage();
        NameRevision unapprovedRevision = revisionPair.getUnapprovedRevision();
        NameRevision approvedRevision = revisionPair.getApprovedRevision();
        boolean cancelled = stage.isCancelled() && includeCancelled;
        boolean pending = stage.isPending() && includePending;
        boolean rejected = stage.isCancelled() && unapprovedRevision.getStatus().isRejected();
        NameElement approvedElement = approvedRevision != null ? approvedRevision.getNameElement() : null;
        NameElement unapprovedElement = unapprovedRevision != null ? unapprovedRevision.getNameElement() : null;
        UserAction userAction;
        if (stage.isPending()) {
            userAction = unapprovedRevision.getUserAction();
        } else if (stage.isCancelled()) {
            userAction = unapprovedRevision.getProcess();
        } else if (approvedRevision.getProcess() != null && approvedRevision.getProcess().getUser() != null) {
            userAction = approvedRevision.getProcess();
        } else {
            userAction = approvedRevision.getUserAction();
        }
        return new RowData(unapprovedElement, approvedElement, userAction, pending, pending && stage.isDeleted(), stage.isApproved(), stage.isArchived(), cancelled, cancelled && rejected);
    }

    /**
     *
     * @return the status to be shown in tables and trees
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return the status style (defined in CSS)
     */
    public String getStatusStyle() {
        return getStatus();
    }

    /**
     *
     * @return true if the name element is pending changes or deletion
     */
    public boolean isPending() {
        return pending;
    }

    /**
     *
     * @return true if the name element is archived
     */
    public boolean isArchived() {
        return archived;
    }

    /**
     *
     * @return true if the name element is pending deletion
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     *
     * @return true if the name element is approved
     */
    public boolean isApproved() {
        return approved;
    }

    /**
     *
     * @return true if the name element is cancelled
     */
    public boolean isCancelled() {
        return cancelled;
    }

    /**
     * @return the fullName
     */
    public Column getFullName() {
        return fullName;
    }

    /**
     * @return the mnemonic
     */
    public Column getMnemonic() {
        return mnemonic;
    }

    /**
     * @return the description
     */
    public Column getDescription() {
        return description;
    }

    /**
     * @return the userAction
     */
    public UserAction getUserAction() {
        return userAction;
    }

    /**
     * @param userAction the userAction to set
     */
    public void setUserAction(UserAction userAction) {
        this.userAction = userAction;
    }

    /**
     * @return the combined
     */
    public Column getCombined() {
        return combined;
    }

    /**
     * @param combined the combined to set
     */
    public void setCombined(Column combined) {
        this.combined = combined;
    }

    /**
     * Subclass column (full name, mnemonic or description) to be used in tables and trees.
     *
     * @author karinrathsman
     *
     */
    public class Column {

        private String approved;
        private String unapproved;

        /**
         * @param approved the approved string
         * @param unapproved the pending or cancelled string
         */
        private Column(String approved, String unapproved) {
            this.unapproved = unapproved;
            this.approved = approved;
        }

        /**
         *
         * @return true if this column is pending changes.
         */
        private boolean isColumnPending() {
            return isPending() && !Objects.equal(unapproved, approved);
        }

        /**
         *
         * @return true of previous pending changes in this column has been cancelled or rejected;
         */
        private boolean isColumnCancelled() {
            return isCancelled() && !Objects.equal(unapproved, approved);
        }

        /**
         *
         * @return a separator between approved and cancelled column strings, if necessary.
         */
        public String getCancelledSeparator() {
            return approved != null && !getCancelled().equals("") ? " " : "";
        }

        /**
         *
         * @return a seperator between approved and pending column strings, if necessary
         */
        public String getPendingSeparator() {
            return approved != null && !getPending().equals("") ? " " : "";
        }

        /**
         *
         * @return the style defined in CSS for approved string
         */
        public String getApprovedStyle() {
            if (isArchived()) {
                return "Archived";
            } else if (isColumnPending() || isDeleted()) {
                return "Deleted";
            } else {
                return "Approved";
            }
        }

        /**
         *
         * @return the style defined in CSS for pending string
         */
        public String getPendingStyle() {
            return "Proposed";
        }

        /**
         *
         * @return the style defined in CSS for cancelled string
         */
        public String getCancelledStyle() {
            return unapproved != null ? "Cancelled" : "Deleted";
        }

        /**
         *
         * @return the approved string
         */
        public String getApproved() {
            return approved != null ? approved : "";
        }

        /**
         *
         * @return the pending string
         */
        public String getPending() {
            return isColumnPending() && unapproved != null ? unapproved : "";
        }

        /**
         *
         * @return the cancelled or rejected string
         */
        public String getCancelled() {
            return isColumnCancelled() ? (unapproved != null ? unapproved : approved) : "";
        }

        /*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
         */
        public String toString() {
            return approved != null ? approved : unapproved != null ? unapproved : "";
        }
    }
}
