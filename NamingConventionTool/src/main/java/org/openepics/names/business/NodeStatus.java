/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

/**
 * subclass to save the node status for individual nameViews
 *
 * @author karinrathsman
 *
 */
public class NodeStatus {

    private boolean expanded;
    private boolean customizedFiltered;
    private boolean accepted;

    protected NodeStatus(boolean expanded, boolean filtered, boolean accepted) {
        this.expanded = expanded;
        this.customizedFiltered = filtered;
        this.accepted = accepted;
    }

    /**
     * @param filtered the filtered to set
     */
    public void setCustomizedFiltered(boolean filtered) {
        this.customizedFiltered = filtered;
    }

    /**
     *
     * @return true if filtered
     */
    public boolean isCustomizedFiltered() {
        return customizedFiltered;
    }

    /**
     * @return true if expanded
     */
    public boolean isExpanded() {
        return expanded;
    }

    /**
     * @param expanded the expanded to set
     */
    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    /**
     * @return the accepted
     */
    public boolean isAccepted() {
        return accepted;
    }

    /**
     * @param accepted the accepted to set
     */
    protected void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    /**
     *
     * @return true if the nodeStatus is accepted and filtered, false otherwise
     */
    public boolean isAcceptedAndFiltered() {
        return isAccepted() && isCustomizedFiltered();
    }

}
