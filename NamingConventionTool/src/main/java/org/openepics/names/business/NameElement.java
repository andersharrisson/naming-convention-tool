/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.business;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.util.As;

/**
 * Class handling the name elements (full name, menonic and discipline) for a revision or a new proposal.
 *
 * @author karinrathsman
 *
 */
public class NameElement {

    private String fullName;
    private String mnemonic;
    private String description;

    /**
     * Constructs a new name element entity based on given data.
     *
     * @param fullName the full name
     * @param mnemonic the mnemonic (letter code) for a name
     * @param description the description for a name
     */
    public NameElement(String fullName, String mnemonic, String description) {
        setFullName(fullName);
        setMnemonic(mnemonic);
        setDescription(description);
    }

    /**
     * Constructs a new name element through copy of name element.
     *
     * @param nameElement the name element to "clone"
     */
    public NameElement(NameElement nameElement) {
        setMnemonic(nameElement.getMnemonic());
        setFullName(nameElement.getFullName());
        setDescription(nameElement.getDescription());
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = As.nullIfEmpty(fullName);
    }

    /**
     * @return the mnemonic
     */
    public String getMnemonic() {
        return mnemonic;
    }

    /**
     * @param mnemonic the mnemonic to set.
     */
    public void setMnemonic(String mnemonic) {
        this.mnemonic = As.nullIfEmpty(mnemonic);
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = As.nullIfEmpty(description);
    }

    @Override
    public String toString() {
        String mnemonicString = mnemonic != null ? "(" + mnemonic + ")" : null;
        if (fullName != null) {
            return mnemonicString != null ? fullName + " " + mnemonicString : fullName;
        } else {
            return mnemonicString != null ? mnemonicString : null;
        }
    }

    /**
     * Returns string representation of this NameElement, with additional index style information
     * (for first level in device structure).
     *
     * @param indexStyle additional index style information (for first level in device structure)
     * @return string representation of this NameElement, with additional index style information
     * (for first level in device structure)
     */
    public String toString(String indexStyle) {
        String indexStyleString = indexStyle != null && !StringUtils.isEmpty(indexStyle) ? " [" + indexStyle + "]" : "";
        String mnemonicString = mnemonic != null ? "(" + mnemonic + ")" + indexStyleString : null;
        if (fullName != null) {
            return mnemonicString != null ? fullName + " " + mnemonicString : fullName;
        } else {
            return mnemonicString != null ? mnemonicString : null;
        }
    }

    @Override
    public boolean equals(Object otherObject) {
        NameElement other =
                otherObject != null && otherObject instanceof NameElement ? (NameElement) otherObject : null;
        return other != null
                && Objects.equals(fullName, other.getFullName())
                && Objects.equals(mnemonic, other.getMnemonic())
                && Objects.equals(description, other.getDescription());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.fullName);
        hash = 11 * hash + Objects.hashCode(this.mnemonic);
        hash = 11 * hash + Objects.hashCode(this.description);
        return hash;
    }

    /**
     * Return if other name element has same mnemonic as this name element.
     *
     * @param other other name element
     * @return if other name element has same mnemonic as this name element.
     */
    public boolean hasEqualMnemonicAs(NameElement other) {
        return Objects.equals(getMnemonic(), other.getMnemonic());
    }

    /**
     * Return if other name element has same full name as this name element.
     *
     * @param other other name element
     * @return if other name element has same full name as this name element
     */
    public boolean hasEqualFullNameAs(NameElement other) {
        return Objects.equals(getFullName(), other.getFullName());
    }

    /**
     * Return if other name element has same description as this name element.
     *
     * @param other other name element
     * @return if other name element has same description as this name element
     */
    public boolean hasEqualDescriptionAs(NameElement other) {
        return Objects.equals(getDescription(), other.getDescription());
    }

}
