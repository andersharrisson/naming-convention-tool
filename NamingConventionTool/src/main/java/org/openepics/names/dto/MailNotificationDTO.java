/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.dto;

import java.util.List;

/**
 * Utility class to store data that has to be sent in notification emails.
 *
 * @author Imre Toth <imre.toth@esss.se>
 */
public class MailNotificationDTO {

    //node list that has been modified; root node to leaf node
    private List<String> nodeList;
    //the operation that has been done on leaf node
    private String operation;
    //what was the commit message
    private String commitMessage;

    /**
     * Constructs a new data transfer object for mail notification.
     *
     * @param nodeList list of nodes
     * @param operation which operation is concerned
     * @param commitMessage commit message
     */
    public MailNotificationDTO(List<String> nodeList, String operation, String commitMessage) {
        this.nodeList = nodeList;
        this.operation = operation;
        this.commitMessage = commitMessage;
    }

    public List<String> getNodeList() {
        return nodeList;
    }

    public String getOperation() {
        return operation;
    }

    public String getCommitMessage() {
        return commitMessage;
    }

    /**
     * Return if item with given index is in last position in node list.
     *
     * @param index index in node list
     * @return if item with given index is in last position in node list
     */
    public boolean isItemLast(int index) {
        return (index >= nodeList.size() - 1);
    }

}
