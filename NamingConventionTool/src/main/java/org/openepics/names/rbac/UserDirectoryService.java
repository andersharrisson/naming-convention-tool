/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rbac;

import java.util.Set;

/**
 * An interface for querying the user directory service.
 *
 * @author Imre Toth <imre.toth@esss.se>
 */
public interface UserDirectoryService {

    /**
     * Tests that the directory service is reachable, and throws an exception if it isn't.
     */
    void validate();

    /**
     * Returns a set of all user names currently registered in the directory.
     *
     * @return the set of all user names currently registered in the directory
     */
    Set<String> getAllUsernames();

    /**
     * Returns a set of names of all users that have administer permission of the naming database.
     *
     * @return the set of names of all users that have administer permission of the naming database
     */
    Set<String> getAllAdministratorUsernames();

    /**
     * Retrieves the email address for the given user.
     *
     * @param username the user name
     * @return the user email
     */
    String getEmail(String username);

    /**
     * Retrieves fur name for the given user.
     *
     * @param username the user name
     * @return the full name of the user
     */
    String getUserFullName(String username);

    /**
     * Retrieves the full name and email of the user in the form Name Surname - mail@mail.com
     *
     * @param username the user name
     * @return the user full name and email
     */
    String getUserFullNameAndEmail(String username);

    /**
     * Returns a set of emails of all users that have administer permission of the naming database.
     *
     * @return the set of emails of all users that have administer permission of the naming database
     */
    Set<String> getAllAdministratorEmails();
}
