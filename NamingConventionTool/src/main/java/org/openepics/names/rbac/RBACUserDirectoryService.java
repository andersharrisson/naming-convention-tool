/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rbac;

import se.esss.ics.rbac.access.RBACConnector;
import se.esss.ics.rbac.access.RBACConnectorException;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;

import javax.ejb.EJB;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of user directory service.
 *
 * @author Imre Toth <imre.toth@esss.se>
 */
public class RBACUserDirectoryService implements UserDirectoryService {

    private static final Logger LOGGER = Logger.getLogger(RBACUserDirectoryService.class.getName());

    private static final String TEST_USERNAME = "nonexisting_user";
    private static boolean validated;

    @EJB
    private DirectoryServiceAccess directoryServiceAccess;

    /**
     * Tests that the directory service is reachable, and throws an exception if it isn't.
     */
    @Override
    public void validate() {
        if (validated) {
            return;
        }

        LOGGER.log(Level.INFO, "Checking for presence of user directory.");

        try {
            // test service by trying to retrieve some data
            directoryServiceAccess.getUserInfo(TEST_USERNAME.toCharArray());
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException(e);
        }
        validated = true;
    }

    /**
     * Returns a set of all user names currently registered in the directory.
     *
     * @return the set
     */
    @Override
    public Set<String> getAllUsernames() {
        final Set<String> usernames = new HashSet<String>();

        try {
            for (UserInfo userInfo : directoryServiceAccess.getUsers("*", true, false, false)) {
                usernames.add(userInfo.getUsername());
            }
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException("There was an error retriving data from the user directory.", e);
        }
        return usernames;
    }

    /**
     * Returns a set of names of all users that have administer permission of the naming database.
     *
     * @return the set
     */
    @Override
    public Set<String> getAllAdministratorUsernames() {
        final Set<String> usernames = new HashSet<String>();

        try {
            for (final String userName : RBACConnector.getInstance().getUsersWithPermission(
                    NamingRBACDefinitions.NAMING_SERVICE, NamingRBACDefinitions.ADMINISTER_NAMING_DB_PERMISSION)) {
                usernames.add(userName);
            }
        } catch (RBACConnectorException e) {
            throw new RuntimeException("There was an error retriving data from the RBAC connector.", e);
        }
        return usernames;
    }

    /**
     * Retrieves the email address for the given user.
     *
     * @param username the user name
     * @return the user email
     */
    @Override
    public String getEmail(String username) {
        try {
            return directoryServiceAccess.getUserInfo(username.toCharArray()).getEMail();
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException("There was an error retriving a user email.", e);
        }
    }

    /**
     * Retrieves fur name for the given user.
     *
     * @param username the user name
     * @return the full name of the user
     */
    @Override
    public String getUserFullName(String username) {
        try {
            UserInfo userInfo = directoryServiceAccess.getUserInfo(username.toCharArray());
            String middleName = userInfo.getMiddleName() != null ? userInfo.getMiddleName() + " " : "";
            return userInfo.getFirstName() + " " + middleName + userInfo.getLastName();
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException("There was an error retriving a user full name.", e);
        }
    }

    /**
     * Retrieves the full name and email of the user in the form Name Surname - mail@mail.com
     *
     * @param username the user name
     * @return the user full name and email
     */
    @Override
    public String getUserFullNameAndEmail(String username) {
        StringBuilder sb = new StringBuilder(100);
        sb.append(getUserFullName(username));

        final String email = getEmail(username);
        if (email != null) {
            sb.append(" - ").append(email);
        }
        return sb.toString();
    }

    /**
     * Returns a set of emails of all users that have administer permission of the naming database.
     *
     * @return the set, or empty set
     */
    @Override
    public Set<String> getAllAdministratorEmails() {
        Set<String> result = new HashSet<>();

        Set<String> allAdministrators = getAllAdministratorUsernames();

        if((allAdministrators != null) && (!allAdministrators.isEmpty())) {
            for(String name : allAdministrators) {
                result.add(getEmail(name));
            }
        }

        return result;
    }
}
