/*
 * Copyright (c) 2014 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.ui.common;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.openepics.names.business.NameType;
import org.openepics.names.services.SessionService;
import org.openepics.names.ui.parts.NamePartsController;
import org.openepics.names.util.UiUtility;

import se.esss.ics.rbac.loginmodules.service.Message;

/**
 * A UI controller bean for the login / logout button and form.
 *
 * @author Vasu V
 * @author K. Rathsman
 */
@ManagedBean
@ViewScoped
public class LoginController implements Serializable {

    private static final long serialVersionUID = 7124872676453151325L;

    private static final String FAILED_TO_SIGN_IN = "Failed to sign in";
    private static final String FAILED_TO_SIGN_OUT = "Failed to sign out";

    private static final Logger LOGGER = Logger.getLogger(LoginController.class.getName());

    @Inject
    private SessionService sessionService;
    private String inputUsername;
    private String inputPassword;

    /**
     * Initializes the bean.
     */
    @PostConstruct
    public synchronized void init() {

    }

    public String getActiveIndex() {
        String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
        switch (viewId) {
            case "/index.xhtml":
                return "0";
            case "/devices.xhtml":
                return "1";
            case "/parts.xhtml":
                String type =
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("type");
                if (type == null) {
                    FacesContext facesContext = FacesContext.getCurrentInstance();
                    NamePartsController namePartsController =
                            (NamePartsController) facesContext.getApplication().getExpressionFactory()
                                    .createValueExpression(
                                            facesContext.getELContext(), "#{namePartsController}", Object.class)
                                    .getValue(facesContext.getELContext());
                    type = namePartsController.getType();
                }
                if (type.equals(NameType.SYSTEM_STRUCTURE.toString())) {
                    return "2";
                } else if (type.equals(NameType.DEVICE_STRUCTURE.toString())) {
                    return "3";
                } else {
                    return "0";
                }
            case "/help.xhtml":
                return "4";
            default:
                return "0";
        }
    }

    /**
     * Prepares for the login popup.
     */
    public void prepareLoginPopup() {
        inputUsername = null;
        clearPassword();
    }

    /**
     * Clears the password.
     */
    public void clearPassword() {
        inputPassword = null;
    }

    /**
     * Sign in of user, session is updated.
     *
     * @throws IOException if user cannot sign in
     */
    public synchronized void signIn() throws IOException {
        try {
            Message m = sessionService.login(inputUsername, inputPassword);
            if (m.isSuccessful()) {
                LOGGER.log(Level.INFO, "Login successful for " + getUsername());
            } else {
                UiUtility.showErrorMessage(FAILED_TO_SIGN_IN, m.getMessage());
                LOGGER.log(Level.INFO, "Login failed for " + inputUsername);
            }
        } catch (Exception e) {
            UiUtility.showErrorMessage(FAILED_TO_SIGN_IN, e.getMessage());
            LOGGER.log(Level.SEVERE, "Cannot sign in", e.getMessage());

        } finally {
            clearPassword();
            sessionService.update();
        }
    }

    /**
     * Return if the user is signed in for application.
     *
     * @return if the user is signed in for application
     */
    public boolean isLoggedIn() {
        return sessionService.isLoggedIn();
    }

    /**
     * Return user name.
     * @return user name
     */
    public String getUsername() {
        return sessionService.getUsername();
    }

    /**
     * Sign in of user, session is updated.
     *
     * @return the view identifier for this view
     */
    public synchronized String signOut() {
        try {
            Message m = sessionService.logout();
            if (m.isSuccessful()) {
                LOGGER.log(Level.INFO, "Logout successful");

            } else {
                UiUtility.showErrorMessage(FAILED_TO_SIGN_OUT, m.getMessage());
                LOGGER.log(Level.SEVERE, FAILED_TO_SIGN_OUT, m.getMessage());
            }
        } catch (Exception e) {
            UiUtility.showErrorMessage(FAILED_TO_SIGN_OUT, e.getMessage());
            LOGGER.log(Level.SEVERE, FAILED_TO_SIGN_OUT, e.getMessage());
        } finally {
            prepareLoginPopup();
            sessionService.update();
        }
        return FacesContext.getCurrentInstance().getViewRoot().getViewId();

    }

    public String getInputUsername() {
        return inputUsername;
    }

    public void setInputUsername(String inputUsername) {
        this.inputUsername = inputUsername;
    }

    public String getInputPassword() {
        return inputPassword;
    }

    public void setInputPassword(String inputPassword) {
        this.inputPassword = inputPassword;
    }

}
