/*
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facility for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 */

package org.openepics.names.ui.parts;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.ByteStreams;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameFilter;
import org.openepics.names.business.NameType;
import org.openepics.names.business.RowData;
import org.openepics.names.business.NameFilter.Options;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.TransactionBean;
import org.openepics.names.nameviews.TransactionResult;
import org.openepics.names.operation.Add;
import org.openepics.names.operation.Approve;
import org.openepics.names.operation.Cancel;
import org.openepics.names.operation.Delete;
import org.openepics.names.operation.Modify;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.operation.Reject;
import org.openepics.names.operation.SingleNameOperation;
import org.openepics.names.ui.common.TreeNodeManager;
import org.openepics.names.ui.common.UIException;
import org.openepics.names.util.As;
import org.openepics.names.util.CookieUtility;
import org.openepics.names.util.EncodingUtility;
import org.openepics.names.util.UiUtility;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openepics.names.operation.CheckDevices;
import org.openepics.names.services.SessionService;
import org.openepics.names.util.ValidationException;

/**
 * A UI controller bean for System Structure and Device Structure screens.
 *
 * @author Vasu V
 * @author Karin Rathsman
 */
@ManagedBean
@ViewScoped
public class NamePartsController implements Serializable {

    // Note revision history of file in repository.

    private static final long serialVersionUID = 3723235418421156956L;

    private static final String CONTENT_TYPE_OPENXML =
            "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    @Inject
    private ExcelImportNamePart excelImportNamePart;
    @Inject
    private TreeNodeManager treeNodeManager;
    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private TransactionBean transactionBean;
    @Inject
    private SessionService sessionService;

    private byte[] importData;
    private String importFileName;

    private List<RowData> historyRevisionsAsRowData;
    private TreeNode viewRoot;
    private TreeNode[] selectedNodes;
    private NameType nameType;
    private Wizard wizard;
    private NameOperation nameOperation;

    // datatable
    //     number of view options
    //     view option visibility
    private int numberOfViewOptions;
    private List<Boolean> viewOptionVisibility;

    /**
     * Constructs the controller bean and handles one time setup.
     */
    public NamePartsController() {
        // initialize datatable
        //     number of view options
        //         according to namefilter
        //     view option visibility
        //         view options initialized as default options in namefilter, may be updated
        numberOfViewOptions = 4;
        viewOptionVisibility = new ArrayList<>();
        viewOptionVisibility.add(Boolean.TRUE);
        viewOptionVisibility.add(Boolean.FALSE);
        viewOptionVisibility.add(Boolean.TRUE);
        viewOptionVisibility.add(Boolean.FALSE);
    }

    /**
     * Return string for name type. Non-null value to be returned.
     *
     * @return the type
     */
    public String getType() {
        return nameType != null ? nameType.toString() : NameType.SYSTEM_STRUCTURE.toString();
    }

    /**
     * Initializes the bean.
     */
    @PostConstruct
    public void init() {
        try {
            String structure =
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("type");
            nameType = NameType.get(structure);

            // prepare datatable
            //     cookies
            //         view option visibility
            Cookie[] cookies =
                    ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                        .getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    switch (cookie.getName()) {
                        case CookieUtility.NT_DEVICEREGISTRY_VIEW_OPTION_VISIBILITY:
                            initViewOptionVisibility(cookie.getValue());
                            break;
                        default:
                            break;
                    }
                }
            }

            // view visibility updated above
            update();
        } catch (Exception e) {
            throw new UIException("NamesPartController initialization failed: " + e.getMessage(), e);
        }
    }

    /**
     * Init view option visibility from given value.
     *
     * @param visibility view option visibility to be interpreted
     *
     * @see org.openepics.names.business.NameFilter.Options#ACTIVE
     * @see org.openepics.names.business.NameFilter.Options#ARCHIVED
     * @see org.openepics.names.business.NameFilter.Options#PENDING
     * @see org.openepics.names.business.NameFilter.Options#CANCELLED
     */
    private void initViewOptionVisibility(String visibility) {
        // keep track of view option visibility in list
        // note position in array/list for entries
        if (!StringUtils.isEmpty(visibility)) {
            visibility = EncodingUtility.decode(visibility);
            String[] split = visibility.split(CookieUtility.DELIMITER_ENTRIES);
            if (split.length == numberOfViewOptions) {
                List<Options> selectedOptions = new ArrayList<>();
                for (int i=0; i<numberOfViewOptions; i++) {
                    viewOptionVisibility.set(i, Boolean.valueOf(split[i]));
                    switch (i) {
                        case 0:
                            if (viewOptionVisibility.get(i))
                                selectedOptions.add(Options.ACTIVE);
                            break;
                        case 1:
                            if (viewOptionVisibility.get(i))
                                selectedOptions.add(Options.ARCHIVED);
                            break;
                        case 2:
                            if (viewOptionVisibility.get(i))
                                selectedOptions.add(Options.PENDING);
                            break;
                          case 3:
                            if (viewOptionVisibility.get(i))
                                selectedOptions.add(Options.CANCELLED);
                            break;
                        default:
                            break;
                    }
                }
                Options[] selectedOptionsArray = new Options[selectedOptions.size()];
                for (int j=0; j<selectedOptions.size(); j++) {
                    selectedOptionsArray[j] = selectedOptions.get(j);
                }
                getViewFilter().setSelectedOptions(selectedOptionsArray);
            }
        }
    }

    /**
     * updates all relevant data after changes.
     */
    public void update() {
        viewRoot = As.notNull(treeNodeManager.getStructureTree(null, nameType));
        setSelectedNodes(null);
        nameOperation = null;
        wizard = null;
    }

    /**
     * Event triggered when toggling view option visibility for data table.
     * Allows keeping track of view option visibility.
     */
    public void onUpdateViewFilter() {
        // keep track of view option visibility
        // set view option visibility cookie
        //     serialize view option visibility
        //     set cookie

        viewOptionVisibility.clear();
        for (int i=0; i<numberOfViewOptions; i++) {
            viewOptionVisibility.add(Boolean.FALSE);
        }
        Options[] selectedOptions = getViewFilter().getSelectedOptions();
        for (Options options : selectedOptions) {
            if (Options.ACTIVE.equals(options))
                viewOptionVisibility.set(0, Boolean.TRUE);
            if (Options.ARCHIVED.equals(options))
                viewOptionVisibility.set(1, Boolean.TRUE);
            if (Options.PENDING.equals(options))
                viewOptionVisibility.set(2, Boolean.TRUE);
            if (Options.CANCELLED.equals(options))
                viewOptionVisibility.set(3, Boolean.TRUE);
        }

        String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                + CookieUtility.NT_DEVICEREGISTRY_VIEW_OPTION_VISIBILITY
                + "', '" + viewOptionVisibility2String()
                + "', {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
        PrimeFaces.current().executeScript(statement);
    }

    /**
     * Serializes view option visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String viewOptionVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfViewOptions; i++) {
            sb.append(viewOptionVisibility.get(i));
            if (i < (numberOfViewOptions-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * expands all nodes in the tree.
     */
    public void onExpandAll() {
        treeNodeManager.expandAll(viewRoot);
    }

    /**
     * collapses all nodes in the tree
     */
    public void onCollapseAll() {
        treeNodeManager.collapseAll(viewRoot);
    }

    /**
     * @return the view filter used in tables and trees.
     */
    public NameFilter getViewFilter() {
        return treeNodeManager.getNameFilter();
    }

    /**
     * @return the nameOperation
     */
    public NameOperation getNameOperation() {
        return nameOperation;
    }

    public boolean isSystemStructure() {
        return nameType != null && NameType.SYSTEM_STRUCTURE.equals(nameType);
    }

    public boolean isDeviceStructure() {
        return nameType != null && NameType.DEVICE_STRUCTURE.equals(nameType);
    }

    /**
     *
     * @return tru if the history can be shown for selected name
     */
    public boolean canShowHistory() {
        return selectedNodes != null && selectedNodes.length == 1;
    }

    /**
     * prepare for the history popup
     */
    public void prepareHistoryPopup() {
        NameView selectedNameView = canShowHistory() ? TreeNodeManager.nameView(selectedNodes[0]) : null;
        historyRevisionsAsRowData = nameViewProvider.getHistoryRevisionsAsRowData(selectedNameView);
        if(selectedNameView ==null || historyRevisionsAsRowData==null || historyRevisionsAsRowData.isEmpty()){
            throw new IllegalStateException("History empty");
        }
    }

    /**
     * @return the list of all revisionsPair as row Data of the selected node.
     */
    public List<RowData> getHistoryRevisionsAsRowData() {
        return historyRevisionsAsRowData;
    }

    /**
     *
     * @return the root of the name View tree.
     */
    public TreeNode getViewRoot() {
        return viewRoot;
    }

    /**
     * @return the selected tree nodes
     */
    public TreeNode[] getSelectedNodes() {
        return selectedNodes;
    }

    /**
     * sets the selected Nodes
     *
     * @param selectedNodes array containing the selected nodes
     */
    public void setSelectedNodes(TreeNode[] selectedNodes) {
        this.selectedNodes = selectedNodes != null ? selectedNodes : new TreeNode[0];
    }

    /**
     * Return if the selected names can be operated on.
     *
     * @param actionString indicating the required operation
     * @return true if the selected names can be operated on
     */
    public boolean can(String actionString) {
        try {
            nameOperation(actionString).validateOnSelect();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Return if the selected names can be operated on.
     *
     * @param actionString indicating the required operation
     * @return true if the selected names can be operated on
     */
    public boolean rendered(String actionString) {
        try {
            nameOperation(actionString).validateUser(sessionService.isEditor(), sessionService.isSuperUser());
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    /**
     *
     * @param actionString
     * @return new name operation of instance specified by the actionString
     */
    private NameOperation nameOperation(String actionString) {
        Set<NameView> selectedNameViews = Sets.newHashSet();
        for (TreeNode treeNode : selectedNodes) {
            selectedNameViews.add(TreeNodeManager.nameView(treeNode));
        }
        NameView rootView = TreeNodeManager.nameView(viewRoot);
        switch (actionString) {
            case "add":
                return new Add(selectedNameViews, rootView);
            case "approve":
                return new Approve(selectedNameViews, rootView);
            case "cancel":
                return new Cancel(selectedNameViews, rootView);
            case "delete":
                return new Delete(selectedNameViews, rootView);
            case "modify":
                return new Modify(selectedNameViews, rootView);
            case "reject":
                return new Reject(selectedNameViews, rootView);
            case "checkDevices":
                return new CheckDevices(selectedNameViews, rootView);
            default:
                 throw new UnsupportedOperationException();
        }
    }

    /**
     * initializes for import action
     *
     * @param action specify if the action is modify or add
     */
    public void prepareImportPopup(String action) {
        importData = null;
        importFileName = null;
    }

    /**
     * Handles the file upload event on import
     *
     * @param event the file upload event
     */
    public void handleFileUpload(FileUploadEvent event) {
        try (InputStream inputStream = event.getFile().getInputstream()) {
            this.importData = ByteStreams.toByteArray(inputStream);
            this.importFileName = FilenameUtils.getName(event.getFile().getFileName());
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    /**
    *
    * @return the importFileName
    */
   public String getImportFileName() {
       return importFileName;
   }

   /**
   *
   * @return the Name template excel file
   */
  public StreamedContent getDownloadableNamesTemplate() {
      return new DefaultStreamedContent(
              excelImportNamePart.templateForImport(),
              CONTENT_TYPE_OPENXML,
              "NamingImportTemplateNamePart.xlsx");
  }

  /**
   * Imports devices from excel
   */
  public void onImport() {
      try (InputStream inputStream = new ByteArrayInputStream(importData)) {
          excelImportNamePart.parseNamePartImportFile(inputStream);
          update();
      } catch (IOException e) {
          throw new RuntimeException();
      }
  }

    /**
     * Submits the name operation.
     *
     */
    public synchronized void onSubmit() {
        TransactionResult result = transactionBean.submit(nameOperation);
        if(nameOperation instanceof Add){
            TreeNode parentNode = treeNodeManager.nodeOf(viewRoot, ((Add) nameOperation).getParentView());
            if(parentNode!=null){
                treeNodeManager.setExpanded(parentNode,true);
                treeNodeManager.expandParents(parentNode);
            }
        }
        update();
        UiUtility.showMessage(result);
    }

    /**
     *
     * @return true if the name operation can be submitted.
     */
    public boolean canSubmit() {
        if (nameOperation == null
                || nameOperation.isMessageRequired() && StringUtils.isEmpty(nameOperation.getMessage())) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * To apply the Wizard form user/admin has to enter a commit message
     * @return true if wizard-data can be submitted.
     */
    public boolean canCreatePart() {
        return (wizard.singleNameOperation == null) || (StringUtils.isEmpty(wizard.singleNameOperation.getMessage()));
    }

    /**
     * Generates title/tooltip for an operation
     *
     * @return title/tooltip for Submit button for an operation
     */
    public String operationSubmitTitle() {
        if(canSubmit()) {
            return "Submit operations";
        } else {
            return "Please, enter a Commit message!";
        }
    }

    /**
     * Generates title/tooltip for creation
     *
     * @return title/tooltip for Submit button for creation
     */
    public String createPartTitle() {
        if(!canCreatePart()) {
            return "Submit creation";
        } else {
            return "Please, enter a Commit message!";
        }
    }

    /**
     * @return the wizard
     */
    public Wizard getWizard() {
        return wizard;
    }

    /**
     * prepare the operation dialog
     *
     * @param actionString string specifying the operation to be performed.
     */
    public void prepareOperationDialog(String actionString) {
        this.nameOperation = nameOperation(actionString);
        if (nameOperation instanceof SingleNameOperation) {
            this.wizard = new Wizard((SingleNameOperation) nameOperation);
        }
    }

    /**
     * Subclass to control the wizard for single name operation
     *
     * @author karinrathsman
     *
     */
    public class Wizard {

        private static final String VALIDATION_ERROR = "Validation Error";

        private TreeNode rootTree;
        private @Nullable
        TreeNode nameViewNode;
        private @Nullable
        TreeNode parentViewNode;
        private SingleNameOperation singleNameOperation;
        private final List<String> tabs = Lists.newArrayList("namePartTab", "parentTab", "descriptionTab", "finishTab");

        /**
         * constructor initialising the wizard
         *
         * @param nameOperation the single name operation data for the operaiton
         */
        public Wizard(SingleNameOperation nameOperation) {
            Preconditions.checkState(nameOperation != null, "name operation is null");
            this.singleNameOperation = nameOperation;
            this.rootTree = treeNodeManager.getStructureTree(nameOperation, nameType);
            if (nameOperation != null) {
                setNameViewNode(treeNodeManager.nodeOf(rootTree, nameOperation.getNameView()));
                setParentViewNode(treeNodeManager.nodeOf(rootTree, nameOperation.getParentView()));
            }
        }

        /**
         *
         * @return the single name operation
         */
        public SingleNameOperation getSingleNameOperation() {
            return singleNameOperation;
        }

        /**
         *
         * @return if view to to be handled is discipline
         */
        public boolean isDiscipline() {
            NameView nameView = singleNameOperation.getParentView();
            return nameView != null
                    && NameType.DEVICE_STRUCTURE.equals(nameView.getNameType())
                    && nameView.isRoot();
        }

        /**
         * @return the operationRoot
         */
        public TreeNode getOperationRoot() {
            return rootTree;
        }

        /**
         * updates wizard when selecting nameViewNode
         *
         * @param event the node selection event
         */
        public void onSelectNameViewNode(NodeSelectEvent event) {
            TreeNode node = event.getTreeNode();
            setNameViewNode(node);
        }

        /**
         * updates wizard when unselecting nameViewNode
         *
         * @param event the node unselect event
         */
        public void onUnselectNameViewNode(NodeUnselectEvent event) {
            setNameViewNode(null);
        }

        /**
         * updates wizard when selecting parentViewNode
         *
         * @param event the node select event
         */
        public void onSelectParentViewNode(NodeSelectEvent event) {
            TreeNode node = event.getTreeNode();
            setParentViewNode(node);
        }

        /**
         * updates wizard when unselecting parentViewNode
         *
         * @param event the node unselect event
         */
        public void onUnselectParentViewNode(NodeUnselectEvent event) {
            setParentViewNode(rootTree);
        }

        /**
         * Sets and updates the form for the name part
         *
         * @param nameViewNode the nameViewNode to set
         */
        private void setNameViewNode(TreeNode nameViewNode) {
            TreeNodeManager.updateSelected(this.nameViewNode, nameViewNode);
            this.nameViewNode = nameViewNode;
            NameView nameView = nameViewNode != null ? TreeNodeManager.nameView(nameViewNode) : null;
            if (nameView != null && nameView.isInStructure() && singleNameOperation != null) {
                singleNameOperation.updateNameView(nameView);
                setParentViewNode(nameViewNode.getParent());
            }
        }

        /**
         * sets and updates the form for the parent node
         *
         * @param parentViewNode the parentViewNode to set
         */
        private void setParentViewNode(TreeNode parentViewNode) {
            TreeNodeManager.updateSelected(this.parentViewNode, parentViewNode);
            this.parentViewNode = parentViewNode;
            singleNameOperation.setParentView(TreeNodeManager.nameView(parentViewNode));
        }

        /**
         *
         * @return the name definition element
         */
        public NameElement getNameElementDefinition() {
            return nameViewProvider.nameElementDefinitionForChild(singleNameOperation.getParentView());
        }

        /**
         * Return if the tab is rendered.
         *
         * @param tab number
         * @return true if the tab is rendered
         */
        public boolean isTabRendered(int tab) {
            if (tab < 0 || tab > tabs.size() - 1) {
                return false;
            }

            if (singleNameOperation instanceof Add) {
                return tab != 0;
            } else if (singleNameOperation instanceof Modify) {
                return tab != 1;
            } else {
                return true;
            }
        }

        /**
         * Return next tab to be shown.
         *
         * @param event the flow event
         * @return next tab to be shown
         */
        public String onFlowProcess(FlowEvent event) {
            final String oldStep = event.getOldStep();
            final String newStep = event.getNewStep();
            final int next = tabs.indexOf(newStep);
            final int prev = tabs.indexOf(oldStep);

            final int nextRendered = nextRendered(prev, next);
            boolean forward = next >= prev;

            if (oldStep != null && isTabRendered(prev)) {
                if (prev == 0) {
                    if (nameViewNode == null) {
                        UiUtility.showErrorMessage(VALIDATION_ERROR, "Please select from list");
                        return oldStep;
                    }
                } else if (prev == 2 && forward) {
                    try {
                        transactionBean.validateMnemonic(singleNameOperation);
                    } catch (ValidationException e) {
                        UiUtility.showErrorMessage(e.getMessage(), e.getDescription());
                        return oldStep;
                    }
                }
            }

            if (isTabRendered(nextRendered)) {
                switch (nextRendered) {
                    case 0:
                        TreeNodeManager.updateSelected(parentViewNode, nameViewNode);
                        break;
                    case 1:
                        TreeNodeManager.updateSelected(nameViewNode, parentViewNode);
                        break;
                    default:
                        break;
                }
            }

            return isTabRendered(next) ? newStep : tabs.get(next + 1);
        }

        /**
         *
         * @param prev the previous tab
         * @param next the next tab
         * @return the next rendered tab.
         */
        private int nextRendered(int prev, int next) {
            //TODO
            boolean forward = next >= prev;
            if (isTabRendered(next)) {
                return next;
            } else if (forward && next + 1 < tabs.size()) {
                return nextRendered(prev, next + 1);
            } else if (!forward && next > 0) {
                return nextRendered(prev, next - 1);
            } else {
                return nextRendered(0, 0);
            }
        }

        /**
        *
        * @return true if a non null mnemonic is required
        */
       public boolean isMnemonicRequired() {
           return transactionBean.isMnemonicRequiredForChild(singleNameOperation.getParentView());
       }

       /**
        * @return true if mnemonic available to set
        */
       public boolean isMnemonicAvailable() {
           return isMnemonicRequired()
                   || !NameType.DEVICE_STRUCTURE.equals(singleNameOperation.getParentView().getNameType());
       }

        /**
         *
         * @return title to be used in headers
         */
        public String getHeader() {
            return nameViewProvider.getHeader(singleNameOperation);
        }
    }

}
