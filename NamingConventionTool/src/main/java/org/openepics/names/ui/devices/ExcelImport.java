/*-
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.ui.devices;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.names.business.NameType;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.nameViews.NameViewProvider;
import org.openepics.names.operation.AddDevice;
import org.openepics.names.services.NamingConvention;
import org.openepics.names.util.As;
import org.openepics.names.util.ExcelCell;
import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.faces.application.FacesMessage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevision;
import org.openepics.names.nameViews.DeviceRecordView;
import org.openepics.names.nameViews.TransactionBean;
import org.openepics.names.operation.DeleteDevices;
import org.openepics.names.operation.ModifyDevice;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.util.ValidationException;

/**
 * A bean for importing devices from Excel.
 */
@Stateless
public class ExcelImport {

    @Inject
    private NamingConvention namingConvention;
    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private TransactionBean transactionBean;
    private Map<String, NameView> subsectionMap;
    private Map<String, NameView> deviceTypeMap;
    private NameView areaRoot;
    private Map<String, ImportRow> keys;
    private Map<String, ImportRow> uuids;

    private static FacesMessage failureMessage(String detail) {
        return new FacesMessage(FacesMessage.SEVERITY_ERROR, "Import failed", detail);
    }

    /**
     * Parses the input stream read from an Excel file, creating devices in the database. If the device already exists, it's silently ignored.
     *
     * @param input the input stream
     * @param importIsModify true if modify names on import, false if adding on import
     * @return a message with the outcome of the import operation
     */
    public FacesMessage parseDeviceImportFile(InputStream input, boolean importIsModify) {
        final Set<NameOperation> operations = Sets.newHashSet();

        init();

        try {
            final XSSFWorkbook workbook = new XSSFWorkbook(input);
            final XSSFSheet sheet = workbook.getSheetAt(0);
            for (Row row : sheet) {
                if (row.getRowNum() > 0) {
                    ImportRow importRow = new ImportRow(row.getRowNum());
                    int i = 0;
                    String key = null;
                    if (importIsModify) {
                        importRow.setKey(row.getCell(i++));
                        importRow.setDelete(row.getCell(i++));
                    } else {
                        importRow.setKey(null);
                        importRow.setDelete(null);
                    }
                    importRow.setSuperSection(row.getCell(i++));
                    importRow.setSection(row.getCell(i++));
                    importRow.setSubsection(row.getCell(i++));
                    importRow.setDiscipline(row.getCell(i++));
                    importRow.setDeviceType(row.getCell(i++));
                    importRow.setInstanceIndex(row.getCell(i++));
                    importRow.setDescription(row.getCell(i++));
                    NameOperation operation = importRow.operation();
                    if (operation != null) {
                        operations.add(operation);
                    }
                }
            }
        } catch (IOException e) {
            return failureMessage(e.getMessage());
        } catch (ValidationException ex) {
            return ex.getFacesMessage();
        }
        return transactionBean.submit(operations);
    }

    private void init() {
        uuids = new HashMap<>();
        areaRoot = nameViewProvider.nameStructure(NameType.AREA_STRUCTURE);
        subsectionMap = nameViewMap(areaRoot);
        NameView deviceStructure = nameViewProvider.nameStructure(NameType.DEVICE_STRUCTURE);
        deviceTypeMap = nameViewMap(deviceStructure);
        keys = new HashMap<>();
    }

    /**
     *
     * @param root
     * @return
     */
    private Map<String, NameView> nameViewMap(NameView root) {
        Set<NameView> parents = root.getAllowedDeviceParents();
        Map<String, NameView> namePartMap = new HashMap<>();
        for (NameView parentView : parents) {
            String conventionName = namingConvention.equivalenceClassRepresentative(transactionBean.conventionName(parentView));
            Preconditions.checkState(!namePartMap.containsKey(conventionName), "conventionnames for device parents is not unique. Database contains corrupted data");
            namePartMap.put(conventionName, parentView);
        }
        return namePartMap;
    }

    private List<String> mnemonicPath(String level1, String level2, String level3) {
        List<String> mnemonicPath = Lists.newArrayList();
        mnemonicPath.add(As.emptyIfNull(level1));
        mnemonicPath.add(As.emptyIfNull(level2));
        mnemonicPath.add(As.emptyIfNull(level3));
        return mnemonicPath;
    }

    /**
     * Exports devices from the database, producing a stream which can be streamed to the user over HTTP.
     *
     * @param records the list with filtered records
     * @param importIsModify specifies wheter the import is modify
     * @return an Excel input stream containing the exported data
     */
    public InputStream templateForImport(@Nullable List<DeviceRecordView> records, boolean importIsModify) {
        final XSSFWorkbook workbook = exportWorkbook(records, importIsModify);
        final InputStream inputStream;
        try {
            final File temporaryFile = File.createTempFile("temp", "xlsx");
            FileOutputStream outputStream = new FileOutputStream(temporaryFile);
            workbook.write(outputStream);
            outputStream.close();
            inputStream = new FileInputStream(temporaryFile);
            temporaryFile.delete();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return inputStream;
    }

    private XSSFWorkbook exportWorkbook(@Nullable List<DeviceRecordView> devices, boolean importIsModify) {
        final XSSFWorkbook workbook = new XSSFWorkbook();
        final XSSFSheet sheet = workbook.createSheet("ImportTemplate.csv");
        if (importIsModify) {
            sheet.protectSheet("hemligt");
        }

        final ArrayList<String> titles = new ArrayList<>();
        if (importIsModify) {
            titles.add("Id");
            titles.add("Deleted");
        }
        titles.add("Super Section");
        titles.add("Section");
        titles.add("Subsection");
        titles.add("Discipline");
        titles.add("Device Type");
        titles.add("Instance Index");
        titles.add("Description");
        XSSFFont boldFont = workbook.createFont();
        boldFont.setBold(true);
        XSSFDataFormat defaultFormat = workbook.createDataFormat();

        XSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFont(boldFont);
        headerStyle.setDataFormat(defaultFormat.getFormat("@"));
        XSSFCellStyle defaultStyle = workbook.createCellStyle();
        defaultStyle.setDataFormat(defaultFormat.getFormat("@"));
        defaultStyle.setLocked(false);

        Row head = sheet.createRow(0);
        for (int i = 0; i < titles.size(); i++) {
            if (!importIsModify) {
                sheet.getColumnHelper().setColDefaultStyle(i, defaultStyle);
            }
            Cell cell = head.createCell(i);
            cell.setCellValue(titles.get(i));
            cell.setCellStyle(headerStyle);
        }

        if (importIsModify && devices != null) {
            int rowNr = 0;
            for (DeviceRecordView record : devices) {
                if (!record.getDevice().getRevisionPair().getNameStage().isArchived()) {
                    final ArrayList<String> cells = new ArrayList<>();
                    cells.add(record.getDevice().getRevisionPair().getBaseRevision().getNameArtifact().getUuid().toString());
                    cells.add("NO");
                    cells.add(record.getSuperSection().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getSection().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getSubsection().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getDiscipline().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getDeviceType().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                    cells.add(record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getDescription());
                    final Row row = sheet.createRow(++rowNr);
                    Cell cell = row.createCell(0);
                    cell.setCellValue(cells.get(0));
                    cell.setCellStyle(headerStyle);

                    for (int i = 1; i < cells.size(); i++) {
                        cell = row.createCell(i);
                        cell.setCellValue(cells.get(i));
                        cell.setCellStyle(defaultStyle);
                    }
                }
            }
        }
        for (int i = 0; i < titles.size(); i++) {
            sheet.autoSizeColumn(i);
        }
        return workbook;
    }

    private class ImportRow {

        private final int row;
        private String superSection;
        private String section;
        private String subsection;
        private String discipline;
        private String deviceType;
        private final NameElement nameElement;
        private Action action;
        private String uuid;
        private NameView device;

        public ImportRow(int row) {
            this.row = row + 1;
            this.action = Action.ADD;
            this.uuid = null;
            this.nameElement = new NameElement(null, null, null);
        }

        public void setSuperSection(Cell cell) {
            this.superSection = As.nullIfEmpty(ExcelCell.asString(cell));
        }

        /**
         * Check statement ant throws exception if it is not true
         *
         * @param statement statement to check
         * @param detail error message
         * @throws ValidationException
         */
        private void validate(boolean statement, String detail) throws ValidationException {
            if (!statement) {
                throw new ValidationException(failureMessage(detail));
            }
        }

        private FacesMessage failureMessage(String detail) {
            String errorString = "Import failed on row " + String.valueOf(row);
            return new FacesMessage(FacesMessage.SEVERITY_ERROR, errorString, detail);
        }

        public void setAction(Action action) {
            this.action = action;
        }

        private void setDelete(Cell cell) throws ValidationException {
            String string = As.nullIfEmpty(ExcelCell.asString(cell));
            if (string == null && uuid == null) {
                this.action = Action.ADD;
            } else if (string != null && string.equalsIgnoreCase("YES")) {
                validate(uuid != null, "The ID is null");
                this.action = Action.DELETE;
            }
        }

        private void setKey(Cell cell) throws ValidationException {
            this.uuid = As.nullIfEmpty(ExcelCell.asString(cell));
            if (uuid != null) {
                if (uuids.containsKey(uuid)) {
                    validate(false, " ID " + uuid + "already exist on row " + uuids.get(uuid).getRow());
                    throw new ValidationException(failureMessage("ID " + uuid + "already exist on row " + uuids.get(uuid).getRow()));
                } else {
                    uuids.put(uuid, this);
                }
                device = nameViewProvider.getNameViews().get(UUID.fromString(uuid));
                validate(device != null && device.isInDeviceRegistry(), "ID does not correspond to a device");
                if (action != Action.DELETE) {
                    this.action = Action.MODIFY;
                }
            } else {
                device = null;
                this.action = Action.ADD;
            }
        }

        private int getRow() {
            return row;
        }

        private void setSection(Cell cell) {
            this.section = As.nullIfEmpty(ExcelCell.asString(cell));
        }

        private void setSubsection(Cell cell) {
            this.subsection = As.nullIfEmpty(ExcelCell.asString(cell));
        }

        private void setDiscipline(Cell cell) {
            this.discipline = As.nullIfEmpty(ExcelCell.asString(cell));
        }

        private void setDeviceType(Cell cell) {
            this.deviceType = As.nullIfEmpty(ExcelCell.asString(cell));
        }

        private void setInstanceIndex(Cell cell) {
            nameElement.setMnemonic(ExcelCell.asString(cell));
        }

        private void setDescription(Cell cell) {
            nameElement.setDescription(ExcelCell.asString(cell));
        }

        private NameOperation operation() throws ValidationException {
            boolean emptyRow = superSection == null && section == null && subsection == null && discipline == null && deviceType == null && nameElement.getDescription() == null && nameElement.getMnemonic() == null;
            if (emptyRow) {
                return null;
            } else {
                validate(section != null, "Section is null");
                validate(subsection != null, "Subsection is null");
                final String subsectionKey = namingConvention.conventionKey(mnemonicPath(superSection, section, subsection), NameType.AREA_STRUCTURE);
                NameView subsectionView = subsectionMap.get(subsectionKey);
                validate(subsectionView != null, " Subsection key " + subsectionKey + " was not found in the database.");

                validate(discipline != null, "Discipline is null");
                validate(deviceType != null, "Device Type is null");
                final String deviceTypeKey = namingConvention.conventionKey(mnemonicPath(discipline, null, deviceType), NameType.DEVICE_STRUCTURE);
                NameView deviceTypeView = deviceTypeMap.get(deviceTypeKey);
                validate(deviceTypeView != null, " Device type key " + deviceTypeKey + " was not found in the database.");

                String deviceName = namingConvention.conventionName(subsectionView.getMnemonicPath(), deviceTypeView.getMnemonicPath(), nameElement.getMnemonic());
                nameElement.setFullName(deviceName);

                String deviceKey = namingConvention.equivalenceClassRepresentative(deviceName);
                if (keys.containsKey(deviceKey)) {
                    int otherRow = keys.get(deviceKey).getRow();
                    validate(false, deviceName + " duplicates name on row " + String.valueOf(otherRow));
                }
                keys.put(deviceKey, this);

                if (nameViewProvider.getNameRevisions().contains(deviceName)) {
                    NameRevision revision = nameViewProvider.getNameRevisions().get(deviceName);
                    boolean equalsRevision = revision.getNameElement().equals(nameElement) && As.equals(revision.isDeleted(), action.equals(Action.DELETE));
                    if (equalsRevision) {
                        return null;
                    }
                }

                NameOperation operation = null;
                switch (action) {
                    case DELETE:
                        validate(nameElement.equals(device.getRevisionPair().getBaseRevision().getNameElement()), "Device to be deleted has changes");
                        operation = new DeleteDevices(Sets.newHashSet(device), areaRoot);
                        break;
                    case ADD:
                        Set<NameView> selectedSubsection = Sets.newHashSet(subsectionView);
                        AddDevice add = new AddDevice(selectedSubsection, areaRoot);
                        add.updateOtherParentView(deviceTypeView);
                        add.setNameElement(nameElement);
                        operation = add;
                        break;
                    case MODIFY:
                        ModifyDevice modify = new ModifyDevice(Sets.newHashSet(device), areaRoot);
                        modify.setParentView(subsectionView);
                        modify.updateOtherParentView(deviceTypeView);
                        modify.setNameElement(nameElement);
                        operation = modify;
                        break;
                    default:
                        throw new AssertionError(action.name());
                }

                try {
                    transactionBean.validate(operation);
                } catch (ValidationException e) {
                    String detail = e.getFacesMessage().getDetail();
                    throw new ValidationException(failureMessage(detail));
                }
                return operation;
            }
        }

    }

    private enum Action {
        DELETE, ADD, MODIFY;
    }

}
