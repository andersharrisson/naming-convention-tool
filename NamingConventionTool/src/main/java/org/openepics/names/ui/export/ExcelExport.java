/*
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.ui.export;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.nameviews.DeviceRecordView;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.ui.common.TreeNodeManager;
import org.openepics.names.util.As;
import org.openepics.names.util.ExcelUtility;
import org.primefaces.model.TreeNode;

import javax.annotation.Nullable;
import javax.ejb.Stateless;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.poi.ss.usermodel.CellType;

/**
 * A bean for exporting systems, device types and devices to Excel.
 */
@Stateless
public class ExcelExport {

    // Note revision history of file in repository.

    private static final String SYSTEM_GROUP = "System Group";
    private static final String SYSTEM = "System";
    private static final String SUBSYSTEM = "Subsystem";
    private static final String DISCIPLINE = "Discipline";

    /**
     * Exports the entities from the database, producing a stream which can be streamed to the user over HTTP.
     *
     * @param systemStructure the treeNode root of the system structure
     * @param deviceStructure the treeNode root of the device structure
     * @param records the list with filtered records
     * @return an Excel input stream containing the exported data
     */
    public InputStream exportFile(TreeNode systemStructure, TreeNode deviceStructure, List<DeviceRecordView> records) {
        final XSSFWorkbook workbook = exportWorkbook(systemStructure, deviceStructure, records);
        return ExcelUtility.getInputStreamForWorkbook(workbook);
    }

    private XSSFWorkbook exportWorkbook(TreeNode systemsTree, TreeNode typesTree, List<DeviceRecordView> devices) {
        final XSSFWorkbook workbook = new XSSFWorkbook();

        final XSSFSheet systemGroupSheet =
                createSheetWithHeader(
                        workbook, SYSTEM_GROUP,
                        "System Group::ID", "System Group::FullName", "System Group::Mnemonic",
                        "System Group::Description", "System Group::Date Approved");
        fillNamePartSheet(systemGroupSheet, 1, systemsTree);

        final XSSFSheet systemSheet =
                createSheetWithHeader(
                        workbook, SYSTEM,
                        SYSTEM_GROUP, "System::ID", "System::FullName", "System::Mnemonic",
                        "System::Description", "System::Date Approved");
        fillNamePartSheet(systemSheet, 2, systemsTree);

        final XSSFSheet subsystemSheet =
                createSheetWithHeader(
                        workbook, SUBSYSTEM,
                        SYSTEM_GROUP, SYSTEM, "Subsystem::ID", "Subsystem::FullName", "Subsystem::Mnemonic",
                        "Subsystem::Description", "Subsystem::Date Approved");
        fillNamePartSheet(subsystemSheet, 3, systemsTree);

        final XSSFSheet disciplineSheet =
                createSheetWithHeader(
                        workbook, DISCIPLINE,
                        "Discipline::ID", "Discipline::FullName", "Discipline::Mnemonic",
                        "Discipline::Description", "Discipline::Date Approved");
        fillNamePartSheet(disciplineSheet, 1, typesTree);

        final XSSFSheet categorySheet =
                createSheetWithHeader(
                        workbook, "Device Group",
                        DISCIPLINE, "Device Group::ID", "Device Group::FullName", "Device Group::Mnemonic",
                        "Device Group::Description", "Device Group::Date Approved");
        fillNamePartSheet(categorySheet, 2, typesTree);

        final XSSFSheet deviceTypeSheet =
                createSheetWithHeader(
                        workbook, "Device Type",
                        DISCIPLINE, "Device Group", "Device Type::ID", "Device Type::FullName",
                        "Device Type::Mnemonic", "Device Type::Description", "Device Type::Date Approved");
        fillNamePartSheet(deviceTypeSheet, 3, typesTree);

        final XSSFSheet namedDeviceSheet =
                createSheetWithHeader(
                        workbook, "Device Names",
                        "ID", SYSTEM_GROUP, SYSTEM, SUBSYSTEM, DISCIPLINE,
                        "Device Type", "Instance Index", "Device Name", "Description", "Date Modified");
        fillDeviceSheet(namedDeviceSheet, devices);

        return workbook;
    }

    private void fillNamePartSheet(XSSFSheet sheet, int maxLevel, TreeNode node) {
        fillNamePartSheet(sheet, maxLevel, 1, node, Lists.<String>newArrayList());
    }

    private void fillNamePartSheet(
            XSSFSheet sheet, int maxLevel, int currentLevel, TreeNode node, List<String> rowData) {

        for (TreeNode child : node.getChildren()) {
            final @Nullable
            NameView childView = TreeNodeManager.nameView(child);
            NameRevisionPair revisionPair = childView.getRevisionPair();
            if (revisionPair != null && revisionPair.getNameStage().isActive()) {
                NameElement element = revisionPair.getBaseRevision().getNameElement();
                if (currentLevel < maxLevel) {
                    final List<String> ancestorData =
                            ImmutableList.<String>builder().addAll(rowData).add(element.toString()).build();
                    fillNamePartSheet(sheet, maxLevel, currentLevel + 1, child, ancestorData);
                } else {
                    final Row row = appendRow(sheet);
                    for (String systemInfo : rowData) {
                        appendCell(row, systemInfo);
                    }

                    appendCell(row,
                            childView.getRevisionPair().getBaseRevision().getNameArtifact().getUuid().toString());
                    appendCell(row, element.getFullName());
                    appendCell(row, As.emptyIfNull(element.getMnemonic()));
                    appendCell(row, As.emptyIfNull(element.getDescription()));
                    appendCell(row,
                            new SimpleDateFormat("yyyy-MM-dd")
                                    .format(As.notNull(revisionPair.getApprovedRevision()).getProcess().getDate()));
                }
            } else {
                return;
            }
        }
    }

    private void fillDeviceSheet(XSSFSheet sheet, List<DeviceRecordView> records) {
        for (DeviceRecordView record : records) {
            if (!record.getDevice().getRevisionPair().getNameStage().isArchived()) {
                final Row row = appendRow(sheet);
                appendCell(row,
                        record.getDevice().getRevisionPair().getBaseRevision().getNameArtifact().getUuid().toString());
                appendCell(row,
                        record.getSystemGroup() != null
                            ? record.getSystemGroup().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                appendCell(row,
                        record.getSystem() != null
                            ? record.getSystem().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                appendCell(row,
                        record.getSubsystem() != null
                            ? record.getSubsystem().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                appendCell(row,
                        record.getDiscipline() != null
                            ? record.getDiscipline().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                appendCell(row,
                        record.getDeviceType() != null
                            ? record.getDeviceType().getRevisionPair().getBaseRevision().getNameElement().getMnemonic()
                            : null);
                appendCell(row,
                        record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                appendCell(row,
                        record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getFullName());
                appendCell(row,
                        record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getDescription());
                appendCell(row,
                        new SimpleDateFormat("yyyy-MM-dd")
                                .format(record.getDevice()
                                        .getRevisionPair().getBaseRevision().getUserAction().getDate()));
            }
        }
    }

    private XSSFSheet createSheetWithHeader(XSSFWorkbook workbook, String sheetName, String... columnNames) {
        final XSSFSheet sheet = workbook.createSheet(sheetName);
        final Row row = appendRow(sheet);
        for (String columnName : columnNames) {
            appendCell(row, columnName);
        }
        return sheet;
    }

    private Row appendRow(XSSFSheet sheet) {
        return sheet.createRow(sheet.getRow(0) == null ? 0 : sheet.getLastRowNum() + 1);
    }

    private Cell appendCell(Row row, String value) {
        final Cell cell = row.createCell(row.getLastCellNum() == -1 ? 0 : row.getLastCellNum());
        cell.setCellType(CellType.STRING);
        cell.setCellValue(value);
        return cell;
    }
}
