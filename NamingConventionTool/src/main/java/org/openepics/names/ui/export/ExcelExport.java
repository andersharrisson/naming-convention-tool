/*-
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.names.ui.export;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevisionPair;
import org.openepics.names.nameViews.DeviceRecordView;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.ui.common.TreeNodeManager;
import org.openepics.names.util.As;
import org.primefaces.model.TreeNode;

import javax.annotation.Nullable;
import javax.ejb.Stateless;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.poi.ss.usermodel.CellType;

/**
 * A bean for exporting sections, device types and devices to Excel.
 */
@Stateless
public class ExcelExport {

    /**
     * Exports the entities from the database, producing a stream which can be streamed to the user over HTTP.
     *
     * @param deviceStructure the treeNode root of the device structure
     * @param areaStructure the treeNode root of the area structure
     * @param records the list with filtered records
     * @return an Excel input stream containing the exported data
     */
    public InputStream exportFile(TreeNode areaStructure, TreeNode deviceStructure, List<DeviceRecordView> records) {

        final XSSFWorkbook workbook = exportWorkbook(areaStructure, deviceStructure, records);
        final InputStream inputStream;
        try {
            final File temporaryFile = File.createTempFile("temp", "xlsx");
            FileOutputStream outputStream = new FileOutputStream(temporaryFile);
            workbook.write(outputStream);
            outputStream.close();
            inputStream = new FileInputStream(temporaryFile);
            temporaryFile.delete();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return inputStream;
    }

    private XSSFWorkbook exportWorkbook(TreeNode sectionsTree, TreeNode typesTree, List<DeviceRecordView> devices) {
        final XSSFWorkbook workbook = new XSSFWorkbook();

        final XSSFSheet superSectionSheet = createSheetWithHeader(workbook, "Super Section", "Super Section::ID", "Super Section::FullName", "Super Section::Mnemonic", "Super Section::Description", "Super Section::Date Approved");
        fillNamePartSheet(superSectionSheet, 1, sectionsTree);

        final XSSFSheet sectionSheet = createSheetWithHeader(workbook, "Section", "Super Section", "Section::ID", "Section::FullName", "Section::Mnemonic", "Section::Description", "Section::Date Approved");
        fillNamePartSheet(sectionSheet, 2, sectionsTree);

        final XSSFSheet subSectionSheet = createSheetWithHeader(workbook, "Subsection", "Super Section", "Section", "Subsection::ID", "Subsection::FullName", "Subsection::Mnemonic", "Subsection::Description", "Subsection::Date Approved");
        fillNamePartSheet(subSectionSheet, 3, sectionsTree);

        final XSSFSheet disciplineSheet = createSheetWithHeader(workbook, "Discipline", "Discipline::ID", "Discipline::FullName", "Discipline::Mnemonic", "Discipline::Description", "Discipline::Date Approved");
        fillNamePartSheet(disciplineSheet, 1, typesTree);

        final XSSFSheet categorySheet = createSheetWithHeader(workbook, "Device Group", "Discipline", "Device Group::ID", "Device Group::FullName", "Device Group::Mnemonic", "Device Group::Description", "Device Group::Date Approved");
        fillNamePartSheet(categorySheet, 2, typesTree);

        final XSSFSheet deviceTypeSheet = createSheetWithHeader(workbook, "Device Type", "Discipline", "Device Group", "Device Type::ID", "Device Type::FullName", "Device Type::Mnemonic", "Device Type::Description", "Device Type::Date Approved");
        fillNamePartSheet(deviceTypeSheet, 3, typesTree);

        final XSSFSheet namedDeviceSheet = createSheetWithHeader(workbook, "Device Names", "ID", "Super Section", "Section", "Subsection", "Discipline", "Device Type", "Instance Index", "Device Name", "Description", "Date Modified");
        fillDeviceSheet(namedDeviceSheet, devices);

        return workbook;
    }

    private void fillNamePartSheet(XSSFSheet sheet, int maxLevel, TreeNode node) {
        fillNamePartSheet(sheet, maxLevel, 1, node, Lists.<String>newArrayList());
    }

    private void fillNamePartSheet(XSSFSheet sheet, int maxLevel, int currentLevel, TreeNode node, List<String> rowData) {
        for (TreeNode child : node.getChildren()) {
            final @Nullable
            NameView childView = TreeNodeManager.nameView(child);
            NameRevisionPair revisionPair = childView.getRevisionPair();
            if (revisionPair != null && revisionPair.getNameStage().isActive()) {
                NameElement element = revisionPair.getBaseRevision().getNameElement();
                if (currentLevel < maxLevel) {
                    //                    final List<String> ancestorData = ImmutableList.<String>builder().addAll(rowData).add(childView.getName(), childView.getMnemonic()!=null? childView.getMnemonic():"").build();
                    final List<String> ancestorData = ImmutableList.<String>builder().addAll(rowData).add(element.toString()).build();
                    fillNamePartSheet(sheet, maxLevel, currentLevel + 1, child, ancestorData);
                    //                } else if (!childView.isDeleted()){
                } else {
                    final Row row = appendRow(sheet);
                    for (String sectionInfo : rowData) {
                        appendCell(row, sectionInfo);
                    }
                    //                    appendCell(row, childView.getNamePart().getUuid().toString());
                    //                    appendCell(row, childView.getName());
                    //                    appendCell(row, childView.getMnemonic()!=null? childView.getMnemonic():"");
                    //                    appendCell(row, new SimpleDateFormat("yyyy-MM-dd").format(As.notNull(childView.getCurrentRevision()).getProcessDate()));

                    appendCell(row, childView.getRevisionPair().getBaseRevision().getNameArtifact().getUuid().toString());
                    appendCell(row, element.getFullName());
                    appendCell(row, As.emptyIfNull(element.getMnemonic()));
                    appendCell(row, As.emptyIfNull(element.getDescription()));
                    appendCell(row, new SimpleDateFormat("yyyy-MM-dd").format(As.notNull(revisionPair.getApprovedRevision()).getProcess().getDate()));
                }
            } else {
                return;
            }
        }
    }

    private void fillDeviceSheet(XSSFSheet sheet, List<DeviceRecordView> records) {
        for (DeviceRecordView record : records) {
            if (!record.getDevice().getRevisionPair().getNameStage().isArchived()) {
                final Row row = appendRow(sheet);
                appendCell(row, record.getDevice().getRevisionPair().getBaseRevision().getNameArtifact().getUuid().toString());
                appendCell(row, record.getSuperSection().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                appendCell(row, record.getSection().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                appendCell(row, record.getSubsection().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                appendCell(row, record.getDiscipline().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                appendCell(row, record.getDeviceType().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                appendCell(row, record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getMnemonic());
                appendCell(row, record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getFullName());
                appendCell(row, record.getDevice().getRevisionPair().getBaseRevision().getNameElement().getDescription());
                appendCell(row, new SimpleDateFormat("yyyy-MM-dd").format(record.getDevice().getRevisionPair().getBaseRevision().getUserAction().getDate()));
            }
        }
    }

    private XSSFSheet createSheetWithHeader(XSSFWorkbook workbook, String sheetName, String... columnNames) {
        final XSSFSheet sheet = workbook.createSheet(sheetName);
        final Row row = appendRow(sheet);
        for (String columnName : columnNames) {
            appendCell(row, columnName);
        }
        return sheet;
    }

    private Row appendRow(XSSFSheet sheet) {
        return sheet.createRow(sheet.getRow(0) == null ? 0 : sheet.getLastRowNum() + 1);
    }

    private Cell appendCell(Row row, String value) {
        final Cell cell = row.createCell(row.getLastCellNum() == -1 ? 0 : row.getLastCellNum());
        cell.setCellType(CellType.STRING);
        cell.setCellValue(value);
        return cell;
    }
}
