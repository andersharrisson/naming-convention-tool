/*
 * Copyright (c) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package org.openepics.names.ui.parts;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameType;
import org.openepics.names.nameviews.NameView;
import org.openepics.names.nameviews.NameViewProvider;
import org.openepics.names.nameviews.TransactionBean;
import org.openepics.names.nameviews.TransactionResult;
import org.openepics.names.operation.Add;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.util.As;
import org.openepics.names.util.ExcelUtility;
import org.openepics.names.util.UiUtility;
import org.openepics.names.util.ValidationException;

import com.google.common.collect.Sets;

/**
 * A bean for importing name parts from Excel.
 *
 * @author Lars Johansson
 */
@Stateless
public class ExcelImportNamePart {

    private static final String EMPTY = "";
    private static final String IMPORT_FAILED_ROW = "Import failed on row {0}.";

    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private TransactionBean transactionBean;

    private NameView systemRoot;
    private Map<String, List<String>> uuidMnemonicMap;

    private void init() {
        uuidMnemonicMap = new HashMap<>();
        systemRoot = nameViewProvider.nameStructure(NameType.SYSTEM_STRUCTURE);
    }

    /**
     * Parses the input stream read from an Excel file, creating name parts in the database.
     * If the name part already exists, it's silently ignored.
     *
     * @param input the input stream
     */
    public void parseNamePartImportFile(InputStream input) {
        final Set<NameOperation> operations = Sets.newHashSet();
        int rownum = -1;
        try (final XSSFWorkbook workbook = new XSSFWorkbook(input)) {
            init();

            final XSSFSheet sheet = workbook.getSheetAt(0);
            for (Row row : sheet) {
                if (row.getRowNum() > 0) {
                	rownum = row.getRowNum();
                    ImportRow importRow = new ImportRow(rownum);
                    int i = 0;

                    importRow.setKey(row.getCell(i++), row.getCell(i++));
                    importRow.setFullName(row.getCell(i++));
                    importRow.setMnemonic(row.getCell(i++));
                    importRow.setDescription(row.getCell(i++));
                    importRow.setCommitMessage(row.getCell(i++));
                    NameOperation operation = importRow.operation();
                    if (operation != null) {
                        operations.add(operation);
                    }
                }
            }

            TransactionResult result =  transactionBean.submit(operations);
            UiUtility.showMessage(result);
        } catch (ValidationException e) {
            UiUtility.showErrorMessage(
            		EMPTY, MessageFormat.format(IMPORT_FAILED_ROW, rownum + 1), e.getMessage(), e);
        } catch (IOException e) {
            UiUtility.showErrorMessage(
            		EMPTY, MessageFormat.format(IMPORT_FAILED_ROW, rownum + 1), e.getMessage(), e);
        } catch (IllegalStateException e) {
            UiUtility.showErrorMessage(
            		EMPTY, MessageFormat.format(IMPORT_FAILED_ROW, rownum + 1), e.getMessage(), e);
        }
    }

    /**
     * Returns an Excel input stream containing an empty template, which can be streamed to the user over HTTP.
     *
     * @return an Excel input stream containing an empty template
     */
    public InputStream templateForImport() {
        final XSSFWorkbook workbook = exportWorkbook();
        return ExcelUtility.getInputStreamForWorkbook(workbook);
    }

    private XSSFWorkbook exportWorkbook() {
        final XSSFWorkbook workbook = new XSSFWorkbook();
        final XSSFSheet sheet = workbook.createSheet("ImportTemplateNamePart.csv");

        // sheet may be protected to prevent modification

        final ArrayList<String> titles = new ArrayList<>();
        titles.add("Parent Uuid");
        titles.add("Parent Mnemonic Path");
        titles.add("Full Name");
        titles.add("Mnemonic");
        titles.add("Description");
        titles.add("Commit Message");
        XSSFFont boldFont = workbook.createFont();
        boldFont.setBold(true);
        XSSFDataFormat defaultFormat = workbook.createDataFormat();

        XSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFont(boldFont);
        headerStyle.setDataFormat(defaultFormat.getFormat("@"));
        XSSFCellStyle defaultStyle = workbook.createCellStyle();
        defaultStyle.setDataFormat(defaultFormat.getFormat("@"));
        defaultStyle.setLocked(false);

        Row head = sheet.createRow(0);
        for (int i = 0; i < titles.size(); i++) {
            sheet.getColumnHelper().setColDefaultStyle(i, defaultStyle);

            Cell cell = head.createCell(i);
            cell.setCellValue(titles.get(i));
            cell.setCellStyle(headerStyle);
        }

        for (int i = 0; i < titles.size(); i++) {
            sheet.autoSizeColumn(i);
        }
        return workbook;
    }

    private class ImportRow {

    	// This class handles content for one row to import. In general, it does not handle which row
    	// information is about. There may be exceptions to that for validation messages although
    	// such usage may also be handled outside this class.

        private static final String MORE_THAN_ONE_NAME_PART_FOR_MNEMONIC_PATH =
                "More than one name part for Mnemonic Path.";
        private static final String NEITHER_UUID_NOR_MNEMONIC_PATH =
                "Neither UUID nor Mnemonic Path.";
        private static final String NO_NAME_PART_FOR_MNEMONIC_PATH =
                "No name part for Mnemonic Path.";
        private static final String UUID_AND_MNEMONIC_PATH_DO_NOT_CORRESPOND_TO_A_NAME_PART_IN_SYSTEM_STRUCTURE =
                "UUID and Mnemonic Path do not correspond to a name part in System Structure.";
        private static final String UUID_DOES_NOT_MATCH_MNEMONIC_PATH =
                "UUID does not match Mnemonic Path.";

        private static final String MNEMONIC_ALREADY_AVAILABLE_FOR_GIVEN_PARENT =
                "Mnemonic already available for given parent.";
        private static final String NO_COMMIT_MESSAGE =
                "No Commit Message.";
        private static final String NO_DESCRIPTION =
                "No Description.";
        private static final String NO_FULL_NAME =
                "No Full Name.";
        private static final String NO_MNEMONIC =
                "No Mnemonic.";


        private final int row;
        private NameView parentNameView;

        private String parentUuid;
        private String parentMnemonicPath;
        private String fullName;
        private String mnemonic;
        private String description;
        private String commitMessage;

        public ImportRow(int row) {
            this.row = row + 1;

            this.parentUuid = null;
            this.parentMnemonicPath = null;
            this.fullName = null;
            this.mnemonic = null;
            this.description = null;
            this.commitMessage = null;
        }

        /**
         * Check statement ant throws exception if it is not true
         *
         * @param statement statement to check
         * @param detail error message
         * @throws ValidationException
         */
        private void validate(boolean statement, String detail) throws ValidationException {
            if (!statement) {
                throw new ValidationException(detail);
            }
        }

        private void setKey(Cell cellParentUuid, Cell cellParentMnemonicPath) throws ValidationException {
            // parent uuid, parent mnemonic path - either, both, none
            // mnemonic path to correspond to one name part (uuid)
            //
            // (1.  get uuid for mnemonic path)
            //  2. uuid or get uuid for mnemonic path
            //  3. uuid to correspond to name part in structure

            this.parentUuid = As.nullIfEmpty(ExcelUtility.asString(cellParentUuid));
            this.parentMnemonicPath = As.nullIfEmpty(ExcelUtility.asString(cellParentMnemonicPath));

            // options
            //     parentUuid but not parentMnemonicPath
            //     not parentUuid but parentMnemonicPath
            //     parentUuid and parentMnemonicPath
            //     neither parentUuid nor parentMnemonicPath
            //         could validate to false with message NEITHER_UUID_NOR_MNEMONIC_PATH
            //         but could be for last row in Excel
            //         not handle this, operation method will return null for row

            if (!StringUtils.isEmpty(parentUuid) && StringUtils.isEmpty(parentMnemonicPath)) {
                // parentUuid but not parentMnemonicPath

                parentNameView = nameViewProvider.getNameViews().get(UUID.fromString(parentUuid));
                validate(parentNameView != null && parentNameView.isInStructure(NameType.SYSTEM_STRUCTURE),
                        UUID_AND_MNEMONIC_PATH_DO_NOT_CORRESPOND_TO_A_NAME_PART_IN_SYSTEM_STRUCTURE);
            } else if (StringUtils.isEmpty(parentUuid) && !StringUtils.isEmpty(parentMnemonicPath)) {
                // not parentUuid but parentMnemonicPath

                List<NameView> parts = nameViewProvider.getNameViews().getInSystemStructure(parentMnemonicPath);
                if (parts.isEmpty()) {
                    validate(false, NO_NAME_PART_FOR_MNEMONIC_PATH);
                } else if (parts.size() > 1) {
                    validate(false, MORE_THAN_ONE_NAME_PART_FOR_MNEMONIC_PATH);
                }

                String uuid = parts.get(0).getRevisionPair().getBaseRevision().getNameArtifact().getUuid().toString();
                parentNameView = nameViewProvider.getNameViews().get(UUID.fromString(uuid));
                validate(parentNameView != null && parentNameView.isInStructure(NameType.SYSTEM_STRUCTURE),
                        UUID_AND_MNEMONIC_PATH_DO_NOT_CORRESPOND_TO_A_NAME_PART_IN_SYSTEM_STRUCTURE);

                parentUuid = uuid;
            } else if (!StringUtils.isEmpty(parentUuid) && !StringUtils.isEmpty(parentMnemonicPath)) {
                // parentUuid and parentMnemonicPath

                List<NameView> parts = nameViewProvider.getNameViews().getInSystemStructure(parentMnemonicPath);
                if (parts.size() != 1) {
                    validate(false, MORE_THAN_ONE_NAME_PART_FOR_MNEMONIC_PATH);
                }
                String uuid = parts.get(0).getRevisionPair().getBaseRevision().getNameArtifact().getUuid().toString();
                if (!StringUtils.equals(parentUuid, uuid)) {
                    validate(false, UUID_DOES_NOT_MATCH_MNEMONIC_PATH);
                }

                parentNameView = nameViewProvider.getNameViews().get(UUID.fromString(parentUuid));
                validate(parentNameView != null && parentNameView.isInStructure(NameType.SYSTEM_STRUCTURE),
                        UUID_AND_MNEMONIC_PATH_DO_NOT_CORRESPOND_TO_A_NAME_PART_IN_SYSTEM_STRUCTURE);
            }
        }
        private void setFullName(Cell cell) {
            this.fullName = As.nullIfEmpty(ExcelUtility.asString(cell));
        }
        private void setMnemonic(Cell cell) {
            this.mnemonic = As.nullIfEmpty(ExcelUtility.asString(cell));
        }
        private void setDescription(Cell cell) {
            this.description = As.nullIfEmpty(ExcelUtility.asString(cell));
        }
        private void setCommitMessage(Cell cell) {
            this.commitMessage = As.nullIfEmpty(ExcelUtility.asString(cell));
        }

        // see also ExcelImport#ImportRow#operation
        private NameOperation operation() throws ValidationException {
            boolean emptyRow = fullName == null
                                && mnemonic == null
                                && description == null
                                && commitMessage == null;
            if (emptyRow) {
                return null;
            } else {
                // get name view for parent
                //     parentNameView
                validate(parentNameView != null && parentNameView.isInStructure(NameType.SYSTEM_STRUCTURE),
                        UUID_AND_MNEMONIC_PATH_DO_NOT_CORRESPOND_TO_A_NAME_PART_IN_SYSTEM_STRUCTURE);

                // check full name, mnemonic, description, commit message
                validate(!StringUtils.isEmpty(fullName), NO_FULL_NAME);
                validate(!StringUtils.isEmpty(mnemonic), NO_MNEMONIC);
                validate(!StringUtils.isEmpty(description), NO_DESCRIPTION);
                validate(!StringUtils.isEmpty(commitMessage), NO_COMMIT_MESSAGE);

                // for given parent, mnemonic only once
                //     additional rules apply
                validate(!containsUuidMnemonic(parentUuid, mnemonic), MNEMONIC_ALREADY_AVAILABLE_FOR_GIVEN_PARENT);
                addUuidMnemonic(parentUuid, mnemonic);

                // prepare name element for name part
                NameElement nameElement = new NameElement(fullName, mnemonic, description);
                // pass on commit message
                //     operation

                // not handle revision that is deleted
                //     parentNameView
                if (parentNameView.getRevisionPair().getApprovedRevision().isDeleted()) {
                    return null;
                }

                NameOperation operation = null;

                Set<NameView> selectedParentSystemStructure = Sets.newHashSet(parentNameView);
                Add add = new Add(selectedParentSystemStructure, systemRoot);
                add.updateOtherParentView(null);
                add.setNameElement(nameElement);
                add.setMessage(commitMessage);
                operation = add;

                return operation;
            }
        }
    }

    private void addUuidMnemonic(String uuid, String mnemonic) {
        List<String> mnemonics = uuidMnemonicMap.get(uuid);
        if (mnemonics != null) {
            mnemonics.add(mnemonic);
        } else {
            mnemonics = new ArrayList<>();
            mnemonics.add(mnemonic);
            uuidMnemonicMap.put(uuid, mnemonics);
        }
    }

    private boolean containsUuidMnemonic(String uuid, String mnemonic) {
        if (!uuidMnemonicMap.containsKey(uuid)) {
            return false;
        } else {
            List<String> mnemonics = uuidMnemonicMap.get(uuid);
            return mnemonics.contains(mnemonic);
        }
    }

}
