package org.openepics.names.ui.devices;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.openepics.names.business.NameType;
import org.openepics.names.business.RowData;
import org.openepics.names.nameViews.DeviceRecordView;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.nameViews.NameViewProvider;
import org.openepics.names.operation.AddDevice;
import org.openepics.names.operation.DeleteDevices;
import org.openepics.names.operation.ModifyDevice;
import org.openepics.names.operation.NameOperation;
import org.openepics.names.operation.SingleNameOperation;
import org.openepics.names.ui.common.TreeNodeManager;
import org.openepics.names.ui.export.ExcelExport;
import org.openepics.names.util.As;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.ByteStreams;
import org.openepics.names.business.NameFilter;
import org.openepics.names.business.NameRevision;
import org.openepics.names.nameViews.TransactionBean;
import org.openepics.names.services.SessionService;
import org.openepics.names.ui.common.LoginController;
import org.openepics.names.util.ValidationException;

@ManagedBean
@ViewScoped
public class DeviceTableController implements Serializable {

    private static final long serialVersionUID = -262176781057441889L;
    @Inject
    private ExcelImport excelImport;
    @Inject
    private TreeNodeManager treeNodeManager;
    @Inject
    private NameViewProvider nameViewProvider;
    @Inject
    private ExcelExport excelExport;
    @Inject
    private TransactionBean transactionBean;
    @Inject
    private LoginController loginController;
    @Inject 
    private SessionService sessionService;

    private byte[] importData;
    private String importFileName;
    private List<DeviceRecordView> records;
    private List<DeviceRecordView> filteredRecords;
    private List<DeviceRecordView> selectedRecords;
    private int rowNumber;
    private NameOperation nameOperation;
    private List<RowData> historyRevisionsAsRowData;
    private Wizard wizard;
    private FilterWizard filterWizard;
    private boolean importIsModify;
    private int rows;

    @PostConstruct
    public void init() {
        update();
        final @Nullable
        String deviceName = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("deviceName");
        rows = 20;
        rowNumber = rowNumber(deviceName);
        importIsModify = false;
    }

    /**
     * update all data
     */
    public void update() {
        filterWizard = null;
        wizard = null;
        nameOperation = null;
        records = treeNodeManager.deviceRecordViews();
    }

    /**
     * @param deviceName the device name
     * @return the row number of the specified deviceName.
     */
    private int rowNumber(String deviceName) {
        if (deviceName != null) {
            NameRevision revision = nameViewProvider.getNameRevisions().get(deviceName);
            if(revision!=null){
                int n = 0;
                for (DeviceRecordView record : records) {
                    n++;
                    if (record.getDevice().getRevisionPair().getBaseRevision().equals(revision)) {
                        setSelectedRecords(Lists.newArrayList(record));
                        return n;
                    }
                } 
            } 
        }
        return 0;
    }

    /**
     *
     * @param actionString the string specifying the selected operation
     * @return the nameOpreation containing all data for the opration
     */
    public NameOperation nameOperation(String actionString) {
        NameView rootView = getRoot();
        Set<NameView> selectedNameViews = selectedDevices();

        switch (actionString) {
            case "add":
                return new AddDevice(Sets.newHashSet(rootView), rootView);
            case "delete":
                return new DeleteDevices(selectedNameViews, rootView);
            case "modify":
                return new ModifyDevice(selectedNameViews, rootView);
            default:
                return null;
        }
    }

    /**
     *
     * @return Link to the device in the controls configuration database (CCDB)
     */
    public String getCcdbUrl() {
        String deviceName = getSelectedRecord() != null ? getSelectedRecord().getDevice().getRevisionPair().getBaseRevision().getNameElement().getFullName() : null;
        return deviceName != null ? System.getProperty("names.ccdbURL").concat("?name=").concat(deviceName) : "";
    }

    /**
     *
     * @return the filtered records
     */
    public List<DeviceRecordView> getRecords() {
        return records;
    }

    /**
     * @return the single selected record, null otherwise
     */
    public DeviceRecordView getSelectedRecord() {
        return getSelectedRecords() != null && getSelectedRecords().size() == 1 ? getSelectedRecords().get(0) : null;
    }

    /**
     *
     * @return a list of selected device records
     */
    public @Nullable
    List<DeviceRecordView> getSelectedRecords() {
        return selectedRecords;
    }

    /**
     *
     * @return the selected devices
     */
    private Set<NameView> selectedDevices() {
        Set<NameView> devices = Sets.newHashSet();
        if (getSelectedRecords() != null) {
            for (DeviceRecordView record : getSelectedRecords()) {
                devices.add(record.getDevice());
            }
        }
        return devices;
    }

    /**
     * sets the selected records.
     *
     * @param selectedRecords the list of selected records.
     */
    public void setSelectedRecords(@Nullable List<DeviceRecordView> selectedRecords) {
        this.selectedRecords = selectedRecords;
    }

    /**
     *
     * @return true if it is possible to direct to the device in ccdb
     */
    public boolean canConfigure() {
        return getSelectedRecord() != null && !getSelectedRecord().getDevice().getRevisionPair().getNameStage().isArchived();
    }

    /**
     *
     * @param operationString the string specifying the operation
     * @return true if the specified operation is allowed for the selection.
     */
    public boolean can(String operationString) {
        try {
            nameOperation(operationString).validateOnSelect();
            return true;
        } catch (ValidationException e) {
            return operationString.equals("add");
        }
    }

     /**
     * @param actionString indicating the required operation
     * @return true if the user is allowed to operate
     */
    public boolean rendered(String actionString) {
        try {
            nameOperation(actionString).validateUser(sessionService.isEditor(), sessionService.isSuperUser());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    /**
     *
     * @return true if the global filtering is possible.
     */
    public boolean canFilter() {
        return true;
        //TODO remove this... 
    }

    /**
     *
     * @return true if the history for the device can be shown.
     */
    public boolean canShowHistory() {
        return getSelectedRecord() != null;
    }

    /**
     * Generates the history data for the selected device.
     */
    public void prepareHistoryPopup() {
        historyRevisionsAsRowData = nameViewProvider.getHistoryRevisionsAsRowData(getSelectedRecord().getDevice());
    }

    /**
     *
     * @return the history revisions as row data to be displayed on history popup
     */
    public List<RowData> getHistoryRevisionsAsRowData() {
        return historyRevisionsAsRowData;
    }

    /**
     * Submits the nameOperation and updates the view.
     *
     */
    public synchronized void onSubmit() {
        FacesMessage message;
        message = transactionBean.submit(nameOperation);
        update();
        loginController.showMessage(message);
    }

    /**
     *
     * @return true if the operation can be submitted
     */
    public boolean canSubmit() {
        if (nameOperation.isMessageRequired() && nameOperation.getMessage() == null) {
            loginController.showMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", "Please enter a message"));
            return false;
        } else {
            return true;
        }
    }

    /**
     * Imports devices from excel
     */
    public void onImport() {
        try (InputStream inputStream = new ByteArrayInputStream(importData)) {
            FacesMessage importMessage = excelImport.parseDeviceImportFile(inputStream, importIsModify);
            update();
            loginController.showMessage(importMessage);
        } catch (IOException e) {
            throw new RuntimeException();
        } finally {
            importIsModify = false;
        }
    }

    /**
     * initializes for import action
     *
     * @param action specify if the action is modify or add
     */
    public void prepareImportPopup(String action) {
        importData = null;
        importFileName = null;
        importIsModify = action.equals("modify");
    }

    public boolean isImportModify() {
        return importIsModify;
    }

    /**
     * Handles the file upload event on import
     *
     * @param event the file upload event
     */
    public void handleFileUpload(FileUploadEvent event) {
        try (InputStream inputStream = event.getFile().getInputstream()) {
            this.importData = ByteStreams.toByteArray(inputStream);
            this.importFileName = FilenameUtils.getName(event.getFile().getFileName());
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    /**
     *
     * @return the imortFileName
     */
    public String getImportFileName() {
        return importFileName;
    }

    /**
     *
     * @return the Name template excel file
     */
    public StreamedContent getDownloadableNamesTemplate() {
        return new DefaultStreamedContent(excelImport.templateForImport(records, importIsModify), "xlsx", "NamingImportTemplate.xlsx");
    }

    /**
     *
     * @return the data on export
     */
    public StreamedContent getAllDataExport() {
        TreeNode areaStructure = As.notNull(treeNodeManager.getStructureTree(null, NameType.AREA_STRUCTURE));
        TreeNode deviceStructure = As.notNull(treeNodeManager.getStructureTree(null, NameType.DEVICE_STRUCTURE));
        return new DefaultStreamedContent(excelExport.exportFile(areaStructure, deviceStructure, records), "xlsx", "NamingConventionExport.xlsx");
    }

    /**
     *
     * @return the filtered records in table.
     */
    public List<DeviceRecordView> getFilteredRecords() {
        return filteredRecords;
    }

    /**
     * sets the filtered records in table
     *
     * @param filteredRecords the filtered records to set
     */
    public void setFilteredRecords(List<DeviceRecordView> filteredRecords) {
        this.filteredRecords = filteredRecords;
    }

    /**
     *
     * @return the n
     */
    public int getRowNumber() {
        return getRows() * (rowNumber / getRows());
    }

    /**
     *
     * @return the number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets the number of rows per page
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * @return the nameOperation
     */
    public NameOperation getNameOperation() {
        return nameOperation;
    }

    public NameView getRoot() {
        return nameViewProvider.nameStructure(NameType.AREA_STRUCTURE);
    }

    /**
     * @return the view filter used in tables and trees.
     */
    public NameFilter getViewFilter() {
        return treeNodeManager.getNameFilter();
    }

    /**
     * @return the wizard
     */
    public Wizard getWizard() {
        return wizard;
    }

    /**
     * prepare the operation dialog
     *
     * @param actionString string specifying the operation to be performed.
     */
    public void prepareOperationDialog(String actionString) {
        this.nameOperation = nameOperation(actionString);
        if (nameOperation instanceof SingleNameOperation) {
            this.wizard = new Wizard((SingleNameOperation) nameOperation);
        }
    }

    /**
     * sets the filter wizard
     */
    public void prepareFilter() {
        this.filterWizard = new FilterWizard();
    }

    /**
     *
     * @return the filtered wizard
     */
    public FilterWizard getFilterWizard() {
        return filterWizard;
    }

    /**
     * Class for the device data wizard to add and modify a single device name.
     *
     * @author karinrathsman
     *
     */
    public class Wizard {

        private TreeNode selectedRootParent;
        private TreeNode selectedOtherParent;
        private TreeNode rootTree;
        private TreeNode otherTree;
        private final List<String> tabs = Lists.newArrayList("rootParentTab", "otherParentTab", "nameElementTab", "finishTab");
        private SingleNameOperation singleNameOperation;

        /**
         * Constructor
         *
         * @param nameOperation contains the data need to add or modify a device name.
         */
        public Wizard(SingleNameOperation nameOperation) {
            this.singleNameOperation = nameOperation;
            Preconditions.checkState(singleNameOperation != null, "Single Name Operation is null");
            this.rootTree = tree(singleNameOperation != null ? singleNameOperation.rootType() : NameType.AREA_STRUCTURE);
            this.otherTree = tree(singleNameOperation != null ? singleNameOperation.otherType() : NameType.AREA_STRUCTURE);
            this.selectedRootParent = selectedParent(rootTree);
            this.selectedOtherParent = selectedParent(otherTree);
        }

        /**
         * @param tree is the root of the area or device structure tree.
         * @return the selected parent node in the specified tree as given by the nameOperation.
         */
        private TreeNode selectedParent(TreeNode tree) {
            NameType nameType = TreeNodeManager.nameView(tree).getNameType();
            TreeNode selectedParent = singleNameOperation != null ? treeNodeManager.nodeOf(tree, this.singleNameOperation.getParentView(nameType)) : null;
            if (selectedParent != null) {
                selectedParent.setSelected(true);
                treeNodeManager.expandParents(selectedParent);
            }
            return selectedParent;
        }

        /**
         * @param nameType
         * @return tree root of the device registry parent tree
         */
        private TreeNode tree(NameType nameType) {
            return treeNodeManager.getDeviceRegistryParentTree(singleNameOperation, nameType);
        }

        /**
         * @return the nameOperation
         */
        public SingleNameOperation getSingleNameOperation() {
            return singleNameOperation;
        }

        /**
         *
         * @return title to be used in headers
         */
        public String getHeader() {
            return singleNameOperation != null ? nameViewProvider.getHeader(singleNameOperation) : null;
        }

        /**
         *
         * @return true if subsection is selected
         */
        public boolean isRootParentSelected() {
            return selectedRootParent != null;
        }

        /**
         *
         * @return true if device type is selected
         */
        public boolean isOtherParentSelected() {
            return selectedOtherParent != null;
        }

        /**
         *
         * @param tab the tab number
         * @return true if the tab is rendered
         */
        public boolean isTabRendered(int tab) {
            return tab >= 0 && tab < tabs.size();
        }

        public String onFlowProcess(FlowEvent event) {
            final String oldStep = event.getOldStep();
            final String newStep = event.getNewStep();
            final int next = tabs.indexOf(newStep);
            final int prev = tabs.indexOf(oldStep);

            if (isTabRendered(prev) && next > prev) {
                if (prev == 0) {
                    if (selectedRootParent == null || selectedRootParent.equals(rootTree)) {
                        showMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", "Please select from list"));
                        return oldStep;
                    }
                } else if (prev == 1) {
                    if (selectedOtherParent == null) {
                        showMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", "Please select from list"));
                        return oldStep;
                    }

                } else if (prev == 2) {
                    try {
                        transactionBean.validateMnemonic(singleNameOperation);
                    } catch (ValidationException e) {
                        showMessage(e.getFacesMessage());
                        return oldStep;
                    }

                }

            }
            return isTabRendered(next) ? newStep : tabs.get(next + 1);

        }

        /**
         * @return the areaStructureTree
         */
        public TreeNode getRootTree() {
            return rootTree;
        }

        /**
         * @return the deviceStructure tree
         */
        public TreeNode getOtherTree() {
            return otherTree;
        }

        /**
         *
         * @param nameView the nameView
         * @return return the convention name of a nameView
         */
        public String conventionName(NameView nameView) {
            return transactionBean.conventionName(nameView);
        }

        /**
         * @return the menemonic (instance index)
         */
        public String getMnemonic() {
            return As.emptyIfNull(singleNameOperation.getNameElement().getMnemonic());
        }

        /**
         * Sets the instance index and fullName.
         *
         * @param mnemonic the mnemonic (instance index) to set
         */
        public void setMnemonic(String mnemonic) {
            singleNameOperation.getNameElement().setMnemonic(mnemonic);
            if (singleNameOperation.isInDeviceRegistry()) {
                singleNameOperation.getNameElement().setFullName(transactionBean.conventionName(singleNameOperation));
            }
        }

        /**
         * set the selectedParent
         *
         * @param event the node select event
         */
        public void onSelectRootParent(NodeSelectEvent event) {
            TreeNode node = event.getTreeNode();
            if (node != null) {
                TreeNodeManager.updateSelected(this.selectedRootParent, node);
                setSelectedRootParent(node);
            } else {
                TreeNodeManager.updateSelected(this.selectedRootParent, node);
                setSelectedRootParent(rootTree);
            }
        }

        /**
         * update when unselecting the parent in root structure
         *
         * @param event the unselect node event
         */
        public void onUnselectRootParent(NodeUnselectEvent event) {
            TreeNode node = event.getTreeNode();
            node.setSelected(false);
            setSelectedRootParent(rootTree);
        }

        /**
         * @return the formSelectedSection
         */
        public TreeNode getSelectedRootParent() {
            return selectedRootParent;
        }

        /**
         * set the selectedParent
         *
         * @param event the node select event
         */
        public void onSelectOtherParent(NodeSelectEvent event) {
            TreeNode node = event.getTreeNode();

            setSelectedOtherParent(node);
        }

        /**
         * update when unselecting the parent in other structure
         *
         * @param event the node unselect event
         */
        public void onUnselectOtherParent(NodeUnselectEvent event) {
            setSelectedOtherParent(null);
        }

        /**
         * @param selectedRootParent the formSelectedSubsection to set
         */
        public void setSelectedRootParent(TreeNode selectedRootParent) {
            this.selectedRootParent = selectedRootParent;
            NameView parentView = TreeNodeManager.nameView(selectedRootParent);
            if (singleNameOperation != null) {
                singleNameOperation.setParentView(parentView);
            }
        }

        /**
         * @return the formSelectedDeviceType
         */
        public TreeNode getSelectedOtherParent() {
            return selectedOtherParent;
        }

        /**
         * @param selectedOtherParent the formSelectedDeviceType to set
         */
        public void setSelectedOtherParent(TreeNode selectedOtherParent) {
            TreeNodeManager.updateSelected(this.selectedOtherParent, selectedOtherParent);
            this.selectedOtherParent = selectedOtherParent;
            NameView parentView = selectedOtherParent != null ? TreeNodeManager.nameView(selectedOtherParent) : null;
            if (singleNameOperation != null) {
                singleNameOperation.updateOtherParentView(parentView);
            }
        }

        /**
         * shows the faces message
         *
         * @param message the faces message
         */
        private void showMessage(FacesMessage message) {
            final FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, message);
        }

    }

    /**
     * Subclass to set the global filter
     *
     * @author karinrathsman
     *
     */
    public class FilterWizard {

        private final TreeNode areaStructure;
        private final TreeNode deviceStructure;

        /**
         * constructor sets the two trees to be filtered
         */
        public FilterWizard() {
            this.areaStructure = treeNodeManager.getFilterTree(NameType.AREA_STRUCTURE);
            this.deviceStructure = treeNodeManager.getFilterTree(NameType.DEVICE_STRUCTURE);
        }

        /**
         * selects all nodes in area structure
         */
        public void onAreaStructureSelectAll() {
            treeNodeManager.filterAll(areaStructure, true);
        }

        /**
         * unselects all nodes in area structuere
         */
        public void onAreaStructureUnselectAll() {
            treeNodeManager.filterAll(areaStructure, false);
        }

        /**
         * selects all nodes in device structure
         */
        public void onDeviceStructureSelectAll() {
            treeNodeManager.filterAll(deviceStructure, true);
        }

        /**
         * unselects all nodes in device structuere
         */
        public void onDeviceStructureUnselectAll() {
            treeNodeManager.filterAll(deviceStructure, false);
        }

        /**
         * @return the areaStructureTree
         */
        public TreeNode getAreaStructure() {
            return areaStructure;
        }

        /**
         * @return the deviceStructure tree
         */
        public TreeNode getDeviceStructure() {
            return deviceStructure;
        }

    }
}
