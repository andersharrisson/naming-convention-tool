/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.operation;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.NameType;
import org.openepics.names.nameviews.NameView;

/**
 * Class handling all data needed to add a device name.
 *
 * @author karinrathsman
 */
public class AddDevice extends Add {

    /**
     * Constructs a new operation.
     *
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public AddDevice(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public boolean acceptedAsParent(NameView parentView) {
        boolean isSystemStructure = NameType.SYSTEM_STRUCTURE.equals(parentView.getNameType());
        int level = parentView.getLevel();
        boolean isLevelOk = level == 1 || level == 2 || level == 3;
        String mnemonic = parentView.getRevisionPair().getLatestRevision() != null
                && parentView.getRevisionPair().getLatestRevision().getNameElement() != null
                ? parentView.getRevisionPair().getLatestRevision().getNameElement().getMnemonic()
                : null;

        return super.acceptedAsParent(parentView)
                && (parentView.isChildInDeviceRegistry()
                        || (isSystemStructure && isLevelOk && !StringUtils.isEmpty(mnemonic)));
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        NameStage requestStage = super.operatedStage(stage);
        return requestStage != null ? requestStage.nextProcessedStage(true) : null;
    }

    @Override
    public String getResult() {
        return "added";
    }

    @Override
    public String getTitle() {
        return "Add ";
    }
}
