package org.openepics.names.operation;

import java.util.Set;

import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.nameViews.NameView;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * Class handling all data needed to approve proposal to add, delete or modify names.
 *
 * @author karinrathsman
 */
public class Approve extends MultipleNameOperation {

    /**
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public Approve(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        return stage.nextProcessedStage(true);
    }

    @Override
    public boolean isMessageRequired() {
        return false;
    }

    @Override
    public String getResult() {
        return "approved";
    }

    @Override
    public String getTitle() {
        return "Approve proposals, modifications and/or deletions";
    }

    @Override
    public boolean affectsDevices(NameView nameView) {
        return nameView.getRevisionPair().isValidationNeeded();
    }

    @Override
    protected NameRevision nextApprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getLatestRevision();
    }

    @Override
    protected NameRevision nextUnapprovedRevision(NameView nameView) {
        return null;
    }

    @Override
    public void validateUser(boolean editor, boolean superUser) throws ValidationException {
        As.validateState(superUser, "User is not authorized");
    }
}
