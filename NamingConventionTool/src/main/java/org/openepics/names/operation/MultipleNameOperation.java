/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.operation;

import java.util.List;
import java.util.Set;

import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.RowData;
import org.openepics.names.nameviews.NameView;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * Abstract class handling data needed for multiple name operations.
 *
 * @author karinrathsman
 */
public abstract class MultipleNameOperation extends NameOperation {

    /**
     * Constructs a new operation (multiple names).
     *
     * @param selectedNameViews the nameView seletion to operate on
     * @param root the root of the name hierarchy.
     */
    public MultipleNameOperation(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
        update();
    }

    /**
     *
     * @param nameView nameView to be tested
     * @return true if the operation affects devices
     */
    protected abstract boolean affectsDevices(NameView nameView);

    @Override
    public Set<NameView> getAffectedDevices() {
        Set<NameView> devices = Sets.newHashSet();
        for (NameView nameView : getAffectedNameViews()) {
            if (affectsDevices(nameView)) {
                for (NameView child : nameView.getDevices()) {
                    if (!child.getRevisionPair().getNameStage().isArchived()) {
                        devices.add(child);
                    }
                }
            }
        }
        return devices;
    }

    /**
     *
     * @return list of devices that must be deleted before the operation can be made.
     */
    private Set<NameView> getDevicesToBeDeletedBeforeOperation() {
        // TODO fix this;
        Set<NameView> devicesToBeDeletedBeforeOperation = Sets.newHashSet();
        for (NameView nameView : getAffectedNameViews()) {
            if (nameView.isChildInDeviceRegistry() && affectsDevices(nameView)) {
                for (NameView child : nameView.getChildren()) {
                    if (!allowedAsParentChild(nameView, child)) {
                        devicesToBeDeletedBeforeOperation.add(child);
                    }
                }
            }
        }
        return devicesToBeDeletedBeforeOperation;
    }
    private boolean anyDeviceToBeDeletedBeforeOperation() {
        for (NameView nameView : getAffectedNameViews()) {
            if (nameView.isChildInDeviceRegistry() && affectsDevices(nameView)) {
                for (NameView child : nameView.getChildren()) {
                    if (!allowedAsParentChild(nameView, child)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    @Override
    protected List<NameView> affectedNameViews() {
        Set<NameView> included = Sets.newHashSet();
        for (NameView selectedNameView : getSelectedNameViews()) {
            if (!included.contains(selectedNameView) && affects(selectedNameView)) {
                included.addAll(includedWithChildren(selectedNameView));
            }
        }
        return Lists.newArrayList(included);
    }

    /**
     *
     * @param nameView to be operated on
     * @return set of nameView and children thereof that shall be operated on.
     */
    private Set<NameView> includedWithChildren(NameView includedNameView) {
        Set<NameView> included = Sets.newHashSet(includedNameView);
        NameStage nameStage = operatedStage(nameStage(includedNameView));
        for (NameView childView : includedNameView.getChildren()) {
            if (affects(childView)
                    && !nameStage.isAllowedAsParentOf(nameStage(childView))
                    && nameStage.isAllowedAsParentOf(operatedStage(nameStage(childView)))) {
                included.addAll(includedWithChildren(childView));
            }
        }
        return included;
    }

    @Override
    public void validateOnSelect() throws ValidationException {
        As.validateState(!getAffectedNameViews().isEmpty(), "No names affected by the operation");
        for (NameView nameView : getAffectedNameViews()) {
            checkIfDeletingAllowed(nameView);
            NameView parentView = nameView.getParent(rootType());
            As.validateState(allowedAsParentChild(parentView, nameView), "Name state is not allowed under the parent");
            for (NameView child : nameView.getChildren()) {
                // investigation ongoing concerning rules
                // ----------
                //     getDevicesToBeDeletedBeforeOperation().contains(child)
                //         replaced by
                //     anyDeviceToBeDeletedBeforeOperation()
                As.validateState(
                        allowedAsParentChild(nameView, child) || anyDeviceToBeDeletedBeforeOperation(),
                        "Name status is not allowed as parent to child states");
            }
        }
    }

    /**
     * It is not allowed to delete records with PENDING state.
     * Used on the UI to enable/disable DELETE item in ACTIONS menu!
     *
     * @param nameView the record that has to be checked whether it could be deleted, or not
     * @throws ValidationException if record is PENDING state method will throw ValidationException
     */
    private void checkIfDeletingAllowed(NameView nameView) throws ValidationException {
        if (this instanceof Delete) {
            As.validateState(!nameView.getRevisionPair().isPending(), "Pending entity can not be deleted!");
        }
    }


    @Override
    public void validate() throws ValidationException {
        validateOnSelect();
        validateUpToDate();
    }

    @Override
    public boolean affects(NameView nameView) {
        NameStage operatedStage = operatedStage(nameStage(nameView));
        return operatedStage != null && !(operatedStage.isPending() && nameView.isInDeviceRegistry());

    }

    /**
     *
     * @param nameView  the name view to be operated on.
     * @return          the next approved revision after operation if the nameView is affected and included
     *                  by the operation, null otherwise
     */
    protected abstract NameRevision nextApprovedRevision(NameView nameView);

    /**
     *
     * @param nameView  the name view to be operated on.
     * @return          the next unapproved revision after operation if the nameView is affected and included
     *                  by the operation, null otherwise
     */
    protected abstract NameRevision nextUnapprovedRevision(NameView nameView);

    /**
     * Return the operated rowData after operation. Null if the nameView is not affected by the operation.
     *
     * @param nameView the nameView to create the row data for
     * @return the operated rowData after operation. Null if the nameView is not affected by the operation
     */
    public RowData getOperatedRowData(NameView nameView) {
        if (getAffectedNameViews().contains(nameView)) {
            NameStage nextStage = operatedOrUnaffectedStage(nameView);
            NameRevision unapproved = nextUnapprovedRevision(nameView);
            NameRevision approved = nextApprovedRevision(nameView);
            NameElement unapprovedElement = unapproved != null ? unapproved.getNameElement() : null;
            NameElement approvedElement = approved != null ? approved.getNameElement() : null;

            return new RowData(
                    unapprovedElement, approvedElement, null,
                    nextStage.isPending(), nextStage.isDeleted(), nextStage.isApproved(),
                    nextStage.isArchived(), nextStage.isCancelled(), false);
        }
        return null;
    }
}
