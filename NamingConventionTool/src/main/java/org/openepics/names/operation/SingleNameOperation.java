/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.operation;

import java.util.Set;

import org.openepics.names.business.NameArtifact;
import org.openepics.names.business.NameElement;
import org.openepics.names.business.NameStage;
import org.openepics.names.business.NameType;
import org.openepics.names.nameviews.NameView;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Abstract class handling data needed for single name operations (add and modify)
 *
 * @author karinrathsman
 */
public abstract class SingleNameOperation extends NameOperation {

    private NameElement nameElement;
    private NameView parentView;
    private NameView otherParentView;
    private NameView nameView;

    /**
     * Constructs a new operation (single name).
     *
     * @param selectedNameViews the name views selected for the operation
     * @param root the root of the tree
     */
    public SingleNameOperation(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
        singleUpdate();
    }

    /**
     * Update after a new selection.
     */
    protected abstract void singleUpdate();

    /**
     * @return true if the operation applies on the system structure
     */
    public boolean isInSystemStructure() {
        return getParentView() != null
                && getParentView().isInStructure()
                && NameType.SYSTEM_STRUCTURE.equals(getParentView().getNameType())
                && getOtherParentView() == null;
    }

    /**
     *
     * @return true if the operation applies on the system or device structure
     */
    public boolean isInStructure() {
        return getParentView() != null
                && getParentView().isChildInStructure()
                && getOtherParentView() == null;
    }

    /**
     *
     * @return true if the operation applies on the device registry
     */
    public boolean isInDeviceRegistry() {
        return (getParentView() != null
                && (getParentView().isChildInStructure() || getParentView().isChildInDeviceRegistry())
                && getOtherParentView() != null
                && getOtherParentView().isChildInDeviceRegistry())
                ||
                (nameView != null
                && nameView.isInDeviceRegistry());
    }

    /**
     * @return the selected nameView, if one and only one is selected. null otherwise
     */
    protected NameView singleSelectedNameView() {
        if (getSelectedNameViews().size() == 1) {
            NameView nameView = (Lists.newArrayList(getSelectedNameViews())).get(0);
            Preconditions.checkArgument(nameView == null || !nameView.getNameType().equals(otherType()));
            return nameView;
        } else {
            return null;
        }
    }

    @Override
    public boolean isMessageRequired() {
        return false;
    }

    @Override
    public Set<NameView> getAffectedDevices() {
        Set<NameView> devices = Sets.newHashSet();
        if (affectsDevices()) {
            for (NameView child : getNameView().getDevices()) {
                if (!child.getRevisionPair().getNameStage().isArchived()) {
                    devices.add(child);
                }
            }
        }
        return devices;
    }

    /**
     *
     * @return true if validation is needed, i.e. if the mnemonic or any of the parents are to be changed.
     */
    public boolean affectsDevices() {
        if (getNameView() == null || getNameView().isRoot()) {
            return false;
        } else {
            NameArtifact systemParentArtifact = parentArtifact(NameType.SYSTEM_STRUCTURE);
            NameArtifact deviceParentArtifact = parentArtifact(NameType.DEVICE_STRUCTURE);
            return getNameView().getRevisionPair().getBaseRevision()
                    .isValidationNeededForProposedChanges(
                            false, getNameElement(), systemParentArtifact, deviceParentArtifact);
        }
    }

    /**
     *
     * @param type the parent type
     * @return the parent artifact
     */
    private NameArtifact parentArtifact(NameType type) {
        NameView parent = getParentView(type);
        return parent == null || parent.isRoot() ? null : parent.getRevisionPair().getBaseRevision().getNameArtifact();
    }

    /**
     * Return the parent name view of given name type.
     *
     * @param parentType the parent type
     * @return the parent name view of given name type
     */
    public NameView getParentView(NameType parentType) {
        if (rootType().equals(parentType)) {
            return getParentView();
        } else if (otherType().equals(parentType)) {
            return getOtherParentView();
        } else {
            throw new IllegalStateException("parent NameType " + parentType.toString() + " cannot be parent type");
        }
    }

    /**
     *
     * @return the parent view.
     */
    public NameView getParentView() {
        return parentView;
    }

    /**
     * sets the proposed parentView, if null the root is set as parent
     *
     * @param parentView the parentView to update
     */
    public void setParentView(NameView parentView) {
        this.parentView = parentView != null ? parentView : getRoot();
        update();
    }

    /**
     * return the proposed name Element
     *
     * @return the nameElement
     */
    public NameElement getNameElement() {
        return nameElement;
    }

    /**
     * sets the name Element
     *
     * @param nameElement the name element to set
     */
    public void setNameElement(NameElement nameElement) {
        this.nameElement = nameElement;
    }

    /**
     * @return the otherParentView
     */
    public NameView getOtherParentView() {
        return otherParentView;
    }

    /**
     * Update other parent view of operation according to given name view.
     *
     * @param otherParentView the otherParentView to set
     */
    public void updateOtherParentView(NameView otherParentView) {
        this.otherParentView = otherParentView;
        update();
    }

    /**
     * @return the nameView
     */
    public NameView getNameView() {
        return nameView;
    }

    /**
     * Update name view of operation according to given name view.
     *
     * @param nameView the nameView to set
     */
    public void updateNameView(NameView nameView) {
        this.nameView = nameView;
        if (nameView != null) {
            setNameElement(new NameElement(nameView.getRevisionPair().getBaseRevision().getNameElement()));
        } else {
            setNameElement(new NameElement(null, null, null));
        }
        update();
    }

    /**
     * Return if the specified parent view is accepted as a parent to nameView.
     *
     * @param parentView the parentView to be tested
     * @return true if the specified parent view is accepted as a parent to nameView
     */
    public boolean acceptedAsParent(NameView parentView) {
        // nok if
        //     not operation allowed on child
        if (!operationIsAllowedOnChild(parentView, getNameView())) {
            return false;
        }

        // nok if
        //     not (no name view or level difference 1)
        //     name view and not level difference 1 or 2 or 3
        if (!(getNameView() == null
                || (parentView.getLevel() == getNameView().getLevel() - 1
                    || parentView.getLevel() == getNameView().getLevel() - 2
                    || parentView.getLevel() == getNameView().getLevel() - 3))) {
            return false;
        }

        // nok if
        //     not (parent child in structure or parent child in device registry and parent is active)
        if (!(parentView.isChildInStructure()
                || parentView.isChildInDeviceRegistry()
                        && parentView.getRevisionPair().getNameStage().isActive())) {
            return false;
        }
        return true;
    }

    /**
     * Return if the specified parentView is accepted as parent in the root branch.
     *
     * @param parentView the parentView to be tested
     * @return true if the specified parentView is accepted as parent in the root branch
     */
    public boolean acceptedAsRootParent(NameView parentView) {
        return parentView != null && parentView.getNameType().equals(rootType()) && acceptedAsParent(parentView);
    }

    /**
     * Return if the specified parentView is accepted as parent in the other branch.
     *
     * @param otherParentView the parentView to be tested
     * @return true if the specified parentView is accepted as parent in the other branch
     */
    public boolean acceptedAsOtherParent(NameView otherParentView) {
        // if parent child in structure or parent child in device registry
        //     other parent
        //     other parent child in device registry
        //     other parent name type
        //     other parent accepted as parent
        // else

        if (getParentView().isChildInStructure() || getParentView().isChildInDeviceRegistry()) {
            return otherParentView != null
                        && otherParentView.isChildInDeviceRegistry()
                        && otherParentView.getNameType().equals(otherType())
                        && acceptedAsParent(otherParentView);
        } else {
            return false;
        }
    }

    /**
     *
     * @param parentView the parent view
     * @param childView the child view
     * @return true if the operation is allowed on the child
     */
    protected boolean operationIsAllowedOnChild(NameView parentView, NameView childView) {
        NameStage parentStage = nameStage(parentView);
        NameStage childStage = operatedStage(nameStage(childView));
        return parentStage != null && childStage != null && parentStage.isAllowedAsParentOf(childStage);
    }

    /**
     *
     * @param parentView the parent view
     * @param childView the child view
     * @return true if the operation is allowed on the parent
     */
    protected boolean operationIsAllowedOnParent(NameView parentView, NameView childView) {
        NameStage parentStage = operatedStage(nameStage(parentView));
        NameStage childStage = nameStage(childView);
        return parentStage != null && childStage != null && parentStage.isAllowedAsParentOf(childStage);
    }
}
