package org.openepics.names.operation;

import java.util.Set;
import org.openepics.names.business.NameRevision;
import org.openepics.names.business.NameStage;
import org.openepics.names.nameViews.NameView;

/**
 * Class handling all data needed to cancel proposals to add, delete or modify names.
 *
 * @author karinrathsman
 */
public class Cancel extends MultipleNameOperation {

    /**
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public Cancel(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        return stage.nextProcessedStage(false);
    }

    @Override
    public boolean isMessageRequired() {
        return false;
    }

    @Override
    public String getResult() {
        return "cancelled";
    }

    @Override
    public String getTitle() {
        return "Reject proposals, modifications and/or deletions";
    }

    @Override
    protected boolean affectsDevices(NameView nameView) {
        return false;
    }

    @Override
    protected NameRevision nextApprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getApprovedRevision();
    }

    @Override
    protected NameRevision nextUnapprovedRevision(NameView nameView) {
        return nameView.getRevisionPair().getUnapprovedRevision();
    }
}
