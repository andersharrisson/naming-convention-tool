/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.operation;

import java.util.List;
import java.util.Set;

import org.openepics.names.business.NameStage;
import org.openepics.names.nameviews.NameView;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import org.openepics.names.util.As;
import org.openepics.names.util.ValidationException;

/**
 * Class handling data for modification of a name part.
 *
 * @author karinrathsman
 */
public class Modify extends SingleNameOperation {

    /**
     * Constructs a new operation.
     *
     * @param selectedNameViews the nameViews selected for the operation
     * @param root the root of the tree
     */
    public Modify(Set<NameView> selectedNameViews, NameView root) {
        super(selectedNameViews, root);
    }

    @Override
    protected void singleUpdate() {
        NameView nameView = singleSelectedNameView();
        NameView parentView = nameView != null ? nameView.getParent(rootType()) : getRoot();
        NameView otherParentView = nameView != null ? nameView.getParent(otherType()) : null;
        updateNameView(nameView);
        setParentView(parentView);
        updateOtherParentView(otherParentView);
    }

    @Override
    public NameStage operatedStage(NameStage stage) {
        return stage.nextRequestStage(false);
    }

    @Override
    public String getResult() {
        return isInStructure() ? "proposed to be modified " : "modified";
    }

    @Override
    protected List<NameView> affectedNameViews() {
        List<NameView> affectedNameViews = Lists.newArrayList();
        if (getNameView() != null) {
            affectedNameViews.add(getNameView());
        }
        return affectedNameViews;
    }

    @Override
    public void validateOnSelect() throws ValidationException {
        As.validateState(getNameView() != null, "Name view is null");
        for (NameView child : getNameView().getChildren()) {
            As.validateState(
                    operationIsAllowedOnParent(getNameView(), child),
                    "Modification is not allowed with respect to children");
        }
        As.validateState(affects(getNameView()), "Cannot modify name");
        if (getOtherParentView() != null) {
            As.validateState(
                    acceptedAsRootParent(getParentView()) && acceptedAsOtherParent(getOtherParentView()),
                    "Parents are not accepted");
        } else {
            As.validateState(
                    acceptedAsRootParent(getParentView()),
                    "Parent is not accepted");
        }
    }

    @Override
    public void validate() throws ValidationException {
        // validation (also) performed as part of naming convention validation
        validateOnSelect();
        validateUpToDate();

        As.validateState(
                getNameElement().getFullName() != null,
                "[" + getNameElement() + "]: "
                        + "Full name is null");
        As.validateState(
                isModify(),
                "[" + getNameElement() + "]: "
                        + "No modification, the proposal equals the latest pending or approved revision");
        As.validateState(
                isInDeviceRegistry() || getParentView().equals(getNameView().getParent(rootType())),
                "[" + getNameElement() + "]: "
                        + "Cannot move name in structure (has not yet been implemented");
    }

    @Override
    public boolean affects(NameView nameView) {
        return operatedStage(nameView.getRevisionPair().getNameStage()) != null;
    }

    @Override
    public String getTitle() {
        return isInStructure() ? "Propose to modify " : "Modify";
    }

    /**
     *
     * @return true if there are modifications, null otherwise.
     */
    public boolean isModify() {
        boolean equalsCurrent =
                getNameElement().equals(getNameView().getRevisionPair().getBaseRevision().getNameElement());
        boolean equalsPending =
                getNameView().getRevisionPair().isPending() && getNameElement().equals(
                        getNameView().getRevisionPair().getLatestRevision().getNameElement());
        boolean sameParent = getNameView().getParent(rootType()).equals(getParentView());
        boolean sameOtherParent = Objects.equal(getNameView().getParent(otherType()), getOtherParentView());
        boolean same = (equalsCurrent || equalsPending) && sameParent && sameOtherParent;
        return !same;
    }
}
